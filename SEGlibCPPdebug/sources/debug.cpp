﻿#ifndef SEG_API_DLL_EXPORT

#include <org/segames/library/memory_tracker.h>

#include <org/segames/library/io/json/json_util.h>
#include <org/segames/library/thread/thread.h>
#include <org/segames/library/thread/condition.h>
#include <org/segames/library/thread/shared.h>
#include <org/segames/library/math/dimension.h>
#include <org/segames/library/math/vecmath.h>
#include <org/segames/library/util/string_util.h>
#include <org/segames/library/glfw/glfw_window.h>
#include <org/segames/library/gl/text/gl_font.h> 
#include <org/segames/library/gl/texture/gl_physical_texture_wrapper.h>
#include <org/segames/library/gl/texture/gl_texture_atlaser.h>
#include <org/segames/library/gl/fbo/gl_framebuffer_wrapper.h> 
#include <org/segames/library/gl/gl_binder.h>
#include <org/segames/library/gl/nk/nk_instance.h>
#include <org/segames/library/gl/nk/nk_font.h>
#include <org/segames/library/gl/nk/compat/glfw/nk_glfw_mouse_button_listener.h>
#include <org/segames/library/gl/nk/compat/glfw/nk_glfw_mouse_position_listener.h>
#include <org/segames/library/gl/nk/compat/glfw/nk_glfw_instance_config.h> 
#include <org/segames/library/core.h>
#include <org/segames/library/clonable.h>

#include <org/segames/library/io/image/image.h>

#include <memory>
#include <iostream>
#include <iomanip>

using namespace org::segames::library;

class Layout :
    public NkLayout
{
public:
    
    virtual bool initialize(nk_context* ctx)
    {
        return true;
    }

    virtual void layout(nk_context* ctx)
    {
        if (nk_begin(ctx, "Test window", nk_rect(100, 100, 200, 400), NK_WINDOW_SCALABLE | NK_WINDOW_MOVABLE | NK_WINDOW_TITLE | NK_WINDOW_CLOSABLE))
        {
            nk_layout_row_dynamic(ctx, 30, 1);
            if (nk_button_label(ctx, "Button"))
                std::cout << "BUTTON!" << std::endl;
        }
        nk_end(ctx);
    }

};

ArrayList<GLTextureRectangle> sections;
GLTexture atlasTest()
{
    File folder("C:\\Users\\MasterIII\\Desktop\\temp_content\\minidict");
    ArrayList<GLPhysicalTextureWrapper> baseTextures;
    ArrayList<GLTextureRectangle> sectionsIn;

    for (auto& file : folder.listFiles())
        if (!file.isDirectory() && (StringUtil::endsWith(file.getName(), ".png") || StringUtil::endsWith(file.getName(), ".jpg") || StringUtil::endsWith(file.getName(), ".jpeg")))
        {
            std::cout << file.getName() << std::endl;
            GLPhysicalTextureWrapper tex = GLPhysicalTextureWrapper(file.getPath());
            tex.importTexture();
            tex.upload();
            tex.bind();
            tex.setParameters(GL_REPEAT, GL_LINEAR, GL_NEAREST);
            tex.release();
            baseTextures.add(std::move(tex));
        }


    for (auto& tex : baseTextures)
        sectionsIn.add(GLTextureRectangle(tex));

    Dimension2i atlasRes(256, 256);
    ArrayList<GLTextureAtlaser::AtlasTile> tiling = GLTextureAtlaser::tileAtlas(5, atlasRes, sectionsIn);
    
    GLTexture tex = GLTextureAtlaser::renderAtlas(4, atlasRes, tiling, GLTextureAtlaser::RescaleFilter::LANCZOS);
    GLTextureAtlaser::computeAtlasSections(true, tex, tiling, sections);
    return tex;
}

void v3()
{
    GLFWWindow window;
    window.setSize(512, 512);
    window.setVisible(true);
    window.makeContextCurrent();
    GLFWWindow::setSwapInterval(0);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    GLFramebufferWrapper fbo(true, false, Dimension2i(window.getWidth(), window.getHeight()));
    fbo.build();

    GLPhysicalTextureWrapper tex = GLPhysicalTextureWrapper("C:/Users/MasterIII/Desktop/plan_flat.dds");
    tex.importTexture();
    tex.upload();

    GLFont font = GLFont(24, 4, "../../MeiryoUI-03.ttf", nullptr, GLFont::ASCII_CHARACTERS);
    font.upload();

    NkGLFWInstanceConfig nkCfg = NkGLFWInstanceConfig(window.getWindowID());
    NkFont nkFont = NkFont(font);
    NkInstance nkInstance = NkInstance(NkInstance::DEFAULT_SHADER, nkCfg);
    Layout layout;

    GLTexture atlas = atlasTest();
    GLTextureRectangle& sec = sections.get(1);

    window.setListener(std::unique_ptr<NkGLFWMousePositionListener>(new NkGLFWMousePositionListener(nkInstance)));
    window.setListener(std::unique_ptr<NkGLFWMouseButtonListener>(new NkGLFWMouseButtonListener(nkInstance)));

    nkFont.setAsCurrent(nkInstance.getContext());
    nkInstance.getLayoutList().add(&layout);

    size_t triangles = 1, updateStep = 2;
    ArrayList<GLfloat> tempBuffer;
    GLFloatBuffer buffer;
    buffer.setPointerBinding(GLDataType::VERTEX, GLPointerInf(GL_FLOAT, 3, 9 * sizeof(GLfloat), 0));
    buffer.setPointerBinding(GLDataType::TEX_COORD, GLPointerInf(GL_FLOAT, 2, 9 * sizeof(GLfloat), 3 * sizeof(GLfloat)));
    buffer.setPointerBinding(GLDataType::COLOR, GLPointerInf(GL_FLOAT, 4, 9 * sizeof(GLfloat), 5 * sizeof(GLfloat)));

    int fps = 0, fpsDone = 0;
    Timer diffTimer, fpsClock(1);
    glClearColor(0.0f, 0.5f, 0.5f, 1.0f);
    while (!window.isCloseRequested())
    {
        if (window.isPressed(GLFW_KEY_KP_ADD))
            triangles++;
        else if (window.isPressed(GLFW_KEY_KP_SUBTRACT))
            if(triangles > 1)
                triangles--;

        glViewport(0, 0, window.getWidth(), window.getHeight());
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, window.getWidth(), window.getHeight(), 0, -1, 1);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        
        // Upload
        for (size_t i = 0; i < triangles; i++)
        {
            int x = i % 100, y = i / 100;
            for (size_t j = 0; j < 3; j++)
            {
                buffer.add((x + (j > 0)) * 20); buffer.add((y + (j > 1)) * 20); buffer.add(0);
                buffer.add((x + (j > 0)) * 1); buffer.add((y + (j > 1)) * 1);
                buffer.add(1); buffer.add(1); buffer.add(1); buffer.add(1);
            }

        }
        buffer.upload(GL_DYNAMIC_DRAW);
        buffer.clear();

        // Rendering
        tex.bind();
        buffer.bind().setPointerInf();
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glEnableClientState(GL_COLOR_ARRAY);

        glDrawArrays(GL_TRIANGLES, 0, triangles * 3);

        glDisableClientState(GL_VERTEX_ARRAY);
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);
        glDisableClientState(GL_COLOR_ARRAY);
        buffer.release();
        tex.release();

        double dt = diffTimer.deltaTime();
        font.render(5, 5, std::to_string(triangles) + ": " + std::to_string(dt) + " (" + std::to_string(fpsDone) + ")\nStep: " + std::to_string(updateStep), Color::YELLOW);

        //nkInstance.update();

        window.swapBuffers();
        window.pollEvents();
        
        fps++;
        if (fpsClock.time())
        {
            fpsDone = fps;
            fps = 0;
        }

    }

}

int main()
{
    v3();
    return 0;
}

#endif