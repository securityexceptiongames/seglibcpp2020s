#include <org/segames/library/stack_trace.h>
#include <Windows.h>
#include <DbgHelp.h>

namespace org::segames::library
{

    bool StackTrace::s_symInitialized = false;

	void StackTrace::getStackTrace(const size_t count, const size_t offset, std::vector<Frame>& output)
	{
        const size_t realCount = (count > StackTrace::MAX_STACK_FRAME_COUNT ? StackTrace::MAX_STACK_FRAME_COUNT : count) + 1;
        HANDLE process = GetCurrentProcess();

        SymSetOptions(SYMOPT_LOAD_LINES);
        if (s_symInitialized || SymInitialize(process, NULL, TRUE))
        {
            s_symInitialized = true;

            void* stack[StackTrace::MAX_STACK_FRAME_COUNT + 1];
            WORD numberOfFrames = CaptureStackBackTrace(0, realCount, stack, NULL);
            SYMBOL_INFO* symbol = (SYMBOL_INFO*)malloc(sizeof(SYMBOL_INFO) + (StackTrace::MAX_FUNCTION_NAME_LENGTH - 1) * sizeof(TCHAR));
            if (symbol != NULL)
            {
                symbol->MaxNameLen = static_cast<ULONG>(StackTrace::MAX_FUNCTION_NAME_LENGTH);
                symbol->SizeOfStruct = sizeof(SYMBOL_INFO);
                DWORD displacement;
                IMAGEHLP_LINE64* line = (IMAGEHLP_LINE64*)malloc(sizeof(IMAGEHLP_LINE64));
                if (line != NULL)
                {
                    line->SizeOfStruct = sizeof(IMAGEHLP_LINE64);
                    for (size_t i = offset + 1; i < static_cast<size_t>(numberOfFrames); i++)
                    {
                        DWORD64 address = (DWORD64)(stack[i]);
                        if (SymFromAddr(process, address, NULL, symbol))
                        {
                            Frame frame;
                            if (SymGetLineFromAddr64(process, address, &displacement, line))
                            {
                                frame.line = line->LineNumber;
                                frame.address = symbol->Address;
                                frame.file = std::string(line->FileName);
                                frame.function = std::string(symbol->Name);
                            }
                            else
                            {
                                frame.line = 0;
                                frame.address = symbol->Address;
                                frame.file = "???";
                                frame.function = std::string(symbol->Name);
                            }
                            output.push_back(std::move(frame));
                        }

                    }

                }

                free(line);
            }

            free(symbol);
        }

	}

    void StackTrace::getStackTraceCAlloc(const size_t count, const size_t offset, size_t* outCount, CFrame** output)
    {
        const size_t realCount = (count > StackTrace::MAX_STACK_FRAME_COUNT ? StackTrace::MAX_STACK_FRAME_COUNT : count) + 1;
        HANDLE process = GetCurrentProcess();

        *outCount = 0;
        *output = NULL;

        SymSetOptions(SYMOPT_LOAD_LINES);
        if (s_symInitialized || SymInitialize(process, NULL, TRUE))
        {
            s_symInitialized = true;

            void* stack[StackTrace::MAX_STACK_FRAME_COUNT + 1];
            WORD numberOfFrames = CaptureStackBackTrace(0, realCount, stack, NULL);
            SYMBOL_INFO* symbol = (SYMBOL_INFO*)malloc(sizeof(SYMBOL_INFO) + (StackTrace::MAX_FUNCTION_NAME_LENGTH - 1) * sizeof(TCHAR));
            if (symbol != NULL)
            {
                symbol->MaxNameLen = static_cast<ULONG>(StackTrace::MAX_FUNCTION_NAME_LENGTH);
                symbol->SizeOfStruct = sizeof(SYMBOL_INFO);
                DWORD displacement;
                IMAGEHLP_LINE64* line = (IMAGEHLP_LINE64*)malloc(sizeof(IMAGEHLP_LINE64));
                if (line != NULL && offset < numberOfFrames)
                {
                    line->SizeOfStruct = sizeof(IMAGEHLP_LINE64);
                    *output = static_cast<CFrame*>(malloc((static_cast<size_t>(numberOfFrames) - offset) * sizeof(CFrame)));
                    if (*output != NULL)
                    {
                        for (size_t i = offset + 1; i < static_cast<size_t>(numberOfFrames); i++)
                        {
                            DWORD64 address = (DWORD64)(stack[i]);
                            if (SymFromAddr(process, address, NULL, symbol))
                            {
                                CFrame& frame = (*output)[(*outCount)++];
                                if (SymGetLineFromAddr64(process, address, &displacement, line))
                                {
                                    frame.line = line->LineNumber;
                                    frame.address = symbol->Address;

                                    size_t strLen = strlen(line->FileName);
                                    frame.file = static_cast<char*>(malloc(strLen + 1));
                                    if (frame.file != NULL)
                                    {
                                        frame.file = strcpy(frame.file, line->FileName);
                                        frame.file[strLen] = '\0';
                                    }

                                    strLen = strlen(symbol->Name);
                                    frame.function = static_cast<char*>(malloc(strLen + 1));
                                    if (frame.function != NULL)
                                    {
                                        frame.function = strcpy(frame.function, symbol->Name);
                                        frame.function[strLen] = '\0';
                                    }

                                }
                                else
                                {
                                    frame.line = 0;
                                    frame.address = symbol->Address;
                                    frame.file = NULL;

                                    size_t strLen = strlen(symbol->Name);
                                    frame.function = static_cast<char*>(malloc(strLen + 1));
                                    if (frame.function != NULL)
                                    {
                                        frame.function = strcpy(frame.function, symbol->Name);
                                        frame.function[strLen] = '\0';
                                    }

                                }

                            }

                        }

                    }

                }

                free(line);
            }

            free(symbol);
        }

    }

}