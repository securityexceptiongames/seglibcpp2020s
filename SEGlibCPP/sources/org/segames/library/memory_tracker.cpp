#include <org/segames/library/debug_util.h>
#include <org/segames/library/memory_tracker.h>

#ifdef SEG_API_MEMORY_TRACKING
namespace org::segames::library
{

	bool MemoryTracker::s_enabled = false;

	std::mutex MemoryTracker::s_mutex;

}
#endif