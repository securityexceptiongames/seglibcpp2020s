#include <org/segames/library/debug_util.h>
#include <org/segames/library/audio/pcm_parser_wav.h>
#include <org/segames/library/math/math.h>
#include <org/segames/library/io/io_util.h>
#include <org/segames/library/core.h>

namespace org::segames::library
{

	PCMParserWAV::PCMParserWAV(std::unique_ptr<std::istream>&& input) noexcept(false) :
		m_dataRemaining(0),
		m_dataStart(0),
		m_format(),
		m_dataFormat(WAVDataFormat::PCM),
		m_input(std::move(input))
	{
		unsigned char channels;
		unsigned char bitDepth;
		ALsizei sampleRate;
		int blockSize;
		WAVBlockCode blockCode;
		do
		{
			blockCode = IOUtil::nextLE<WAVBlockCode>(*m_input);
			switch (blockCode)
			{
			case WAVBlockCode::FORMAT:
				blockSize = IOUtil::nextLE<int>(*m_input);
				m_dataFormat = IOUtil::nextLE<WAVDataFormat>(*m_input);

				channels = static_cast<unsigned char>(IOUtil::nextLE<short>(*m_input));
				sampleRate = static_cast<ALsizei>(IOUtil::nextLE<int>(*m_input));

				m_input->ignore(4 + 2); // Bytes per sec & Format block align

				bitDepth = static_cast<unsigned char>(IOUtil::nextLE<short>(*m_input));
				m_format = PCMFormat(channels, bitDepth, sampleRate);

				if (blockSize == 18)
				{
					int extraSize = IOUtil::nextLE<short>(*m_input);
					m_input->ignore(extraSize);
				}
				break;
			case WAVBlockCode::RIFF:
				m_input->ignore(8);
				break;
			default:
				blockSize = IOUtil::nextLE<int>(*m_input);
				m_input->ignore(blockSize);
				break;
			}

		} while (blockCode != WAVBlockCode::FORMAT && m_input->good());
		m_dataStart = m_input->tellg();
	}

	size_t PCMParserWAV::parse(size_t requestedSamples, PCMBuffer& output)
	{
		size_t successSamples = 0;
		while (successSamples < requestedSamples)
		{
			if (m_dataRemaining > 0)
			{
				try
				{
					switch (m_dataFormat)
					{
					case WAVDataFormat::IEEEFloatingPoint:
						m_dataRemaining -= extractSample__<float>(m_format.channels(), *m_input, output);
						successSamples++;
						break;
					default:
						if (m_format.bitDepth() == 8)
							m_dataRemaining -= extractSample__<unsigned char>(m_format.channels(), *m_input, output);
						else if (m_format.bitDepth() == 16)
							m_dataRemaining -= extractSample__<signed short>(m_format.channels(), *m_input, output);
						else
							successSamples--;
						successSamples++;
						break;
					}

				}
				catch (const std::exception& e)
				{
					Core::err() << e.what() << std::endl;
					m_dataRemaining = 0;
				}

			}
			else if ((m_dataRemaining = seekNextData__(*m_input)) == 0)
				break;
					
		}

		return successSamples;
	}

	bool PCMParserWAV::rewind()
	{
		try
		{
			m_input->seekg(m_dataStart);
			m_dataRemaining = 0;
			return true;
		}
		catch (const std::exception&)
		{
			return false;
		}

	}

	int PCMParserWAV::seekNextData__(std::istream& input)
	{
		int blockSize = 0;
		WAVBlockCode blockCode = WAVBlockCode::NONE;
		while (blockCode != WAVBlockCode::DATA && input.good())
		{
			blockCode = IOUtil::nextLE<WAVBlockCode>(input);
			switch (blockCode)
			{
			case WAVBlockCode::DATA:
				blockSize = IOUtil::nextLE<int>(input);
				break;
			case WAVBlockCode::RIFF:
				input.ignore(8);
				break;
			default:
				blockSize = IOUtil::nextLE<int>(input);
				input.ignore(blockSize);
				blockSize = 0;
				break;
			}

		}

		return blockSize;
	}

}