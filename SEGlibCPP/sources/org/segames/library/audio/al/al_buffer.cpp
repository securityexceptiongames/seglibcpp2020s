#include <org/segames/library/debug_util.h>
#include <org/segames/library/audio/al/al_buffer.h>
#include <org/segames/library/audio/al/al_exception.h>
#include <org/segames/library/audio/al/al_instance.h>
#include <org/segames/library/core.h>

namespace org::segames::library
{

	void ALBuffer::makeIntoImageOf(const ALBuffer& obj)
	{
		this->destroy();
		m_id = obj.m_id;
		m_uploadLength = obj.m_uploadLength;
		m_nextImage = obj.m_nextImage;
		const_cast<ALBuffer&>(obj).m_nextImage = this;
	}

	void ALBuffer::upload(const PCMFormat& format, ALsizei len, const ALvoid* data) noexcept(false)
	{
		this->create();
		alBufferData(m_id, format.alFormat(), data, len, format.sampleRate());
		m_uploadLength = len;
		m_format = format;

		ALBuffer* img = this;
		while ((img = img->m_nextImage) != this)
			img->m_uploadLength = m_uploadLength;
	}

	void ALBuffer::create() noexcept
	{
		try
		{
			ALInstance::getInstance();
		}
		catch (const std::exception& e)
		{
			Core::err() << e.what() << std::endl;
		}

		if (!m_id)
		{
			alGenBuffers(1, &m_id);

			ALBuffer* img = this;
			while ((img = img->m_nextImage) != this)
				img->m_id = m_id;
		}

	}

	void ALBuffer::destroy() noexcept
	{
		if (m_nextImage == this)
		{
			if (m_id)
				alDeleteBuffers(1, &m_id);
		}
		else
		{
			ALBuffer* img = this;
			while (img->m_nextImage != this)
				img = img->m_nextImage;
			img->m_nextImage = m_nextImage;
		}

	}

}