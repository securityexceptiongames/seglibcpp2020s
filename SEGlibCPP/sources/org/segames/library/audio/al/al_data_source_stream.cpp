#include <org/segames/library/debug_util.h>
#include <org/segames/library/audio/al/al_data_source_stream.h>

#include <iostream>

namespace org::segames::library
{

	const ALBuffer& ALDataSourceStream::nextBuffer()
	{
		static ALBuffer emptyBuffer;
		if (m_parserEnded)
			return emptyBuffer;
		else
		{
			size_t parseRequestSize = static_cast<size_t>(m_parser->getFormat().sampleRate() * getBufferLength());
			size_t parseSize = m_parser->parse(parseRequestSize, *m_conversion);

			if (parseSize > 0)
			{
				ALBuffer& buffer = m_buffers[m_current];
				buffer.upload(getFormat(), m_conversion->nativeSize(), m_conversion->nativePointer());
				m_conversion->discard(m_conversion->nativeSize());
				m_current = (m_current + 1) % BUFFER_COUNT;
				return buffer;
			}
			else
			{
				m_parserEnded = true;
				return emptyBuffer;
			}

		}

	}

	double ALDataSourceStream::s_bufferLength = 0.5;

}