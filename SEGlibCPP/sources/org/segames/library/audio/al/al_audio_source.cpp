#include <org/segames/library/debug_util.h>
#include <org/segames/library/audio/al/al_audio_source.h>
#include <org/segames/library/audio/al/al_instance.h>
#include <org/segames/library/thread/synchronized.h>
#include <org/segames/library/core.h>

namespace org::segames::library
{

	void ALAudioSource::play()
	{
		bool setup = false;
		{
			auto lock = Synchronize::lock(m_id);
			setup = (*m_id == AL_NONE);
		}

		create();
		if (setup)
			setupSourceInitial__();

		{
			auto lock = Synchronize::lock(m_id);
			alSourcef(*m_id, AL_GAIN, m_volume);
			alSourcePlay(*m_id);
		}

		m_playing = true;
		fireStateChange__(setup ? ALAudioSourceStateListener::Change::STARTED : ALAudioSourceStateListener::Change::RESUMED);
	}

	void ALAudioSource::pause()
	{
		{
			auto lock = Synchronize::lock(m_id);
			if (*m_id)
				alSourcePause(*m_id);
		}
		m_playing = false;
		fireStateChange__(ALAudioSourceStateListener::Change::PAUSED);
	}
			
	bool ALAudioSource::rewind()
	{
		bool rewound = true;
		{
			auto lock = Synchronize::lock(m_id);
			if (m_stream)
				rewound = m_data->rewind();
			if (rewound)
			{
				alSourceRewind(*m_id);
				m_finished = false;
			}

		}
		fireStateChange__(rewound ? ALAudioSourceStateListener::Change::REWOUND : ALAudioSourceStateListener::Change::REWIND_FAILURE);
		return rewound;
	}

	void ALAudioSource::tick()
	{
		if (m_playing)
		{
			ALAudioSourceStateListener::Change change = ALAudioSourceStateListener::Change::NONE;
			if (m_stream)
			{
				ALint state;
				ALint updatedCount = updateBuffers();

				auto lock = Synchronize::lock(m_id);
				alGetSourcei(*m_id, AL_SOURCE_STATE, &state);
				if (state != AL_PLAYING)
				{
					if (updatedCount > 0)
					{
						alSourcePlay(*m_id);
						change = ALAudioSourceStateListener::Change::RECOVERED;
					}
					else
					{
						m_playing = false;
						m_finished = true;
						change = ALAudioSourceStateListener::Change::FINISHED;
					}

				}

			}
			else
			{
				ALint state;
				auto lock = Synchronize::lock(m_id);
				alGetSourcei(*m_id, AL_SOURCE_STATE, &state);
				if (state != AL_PLAYING)
				{
					m_playing = false;
					m_finished = true;
					change = ALAudioSourceStateListener::Change::FINISHED;
				}

			}

			if (change != ALAudioSourceStateListener::Change::NONE)
				fireStateChange__(change);
		}

	}

	void ALAudioSource::setStateListener(ALAudioSourceStateListener* listener)
	{
		auto lock = Synchronize::lock(m_listener);
		*m_listener = listener;
	}

	void ALAudioSource::setVolume(float volume)
	{
		m_volume = volume;
		if (m_playing)
		{
			auto lock = Synchronize::lock(m_id);
			alSourcef(*m_id, AL_GAIN, m_volume);
		}

	}

	void ALAudioSource::create() noexcept
	{
		try
		{
			ALInstance::getInstance();
		}
		catch (const std::exception& e)
		{
			Core::err() << e.what() << std::endl;
		}

		auto lock = Synchronize::lock(m_id);
		if (!(*m_id))
			alGenSources(1, &(*m_id));
	}

	void ALAudioSource::destroy() noexcept
	{
		auto lock = Synchronize::lock(m_id);
		if (*m_id)
			alDeleteSources(1, &(*m_id));
	}

	ALint ALAudioSource::updateBuffers()
	{
		ALint count;
		ALint insertedCount;
		ALuint usedBuffers[QUEUE_COUNT];
		{
			auto lock = Synchronize::lock(m_id);
			alGetSourcei(*m_id, AL_BUFFERS_PROCESSED, &count);
			alSourceUnqueueBuffers(*m_id, count, usedBuffers);
		}

		for (insertedCount = 0; insertedCount < count; insertedCount++)
		{
			auto lock = Synchronize::lock(m_id);
			const ALBuffer& buffer = m_data->nextBuffer();
			if (buffer.getID())
			{
				ALuint bufferID = buffer.getID();
				alSourceQueueBuffers(*m_id, 1, &bufferID);
			}
			else
				break;
		}

		return insertedCount;
	}

	void ALAudioSource::setupSourceInitial__()
	{
		ALsizei bufferCount = 0;
		ALuint buffers[QUEUE_COUNT];
		for (size_t i = 0; i < QUEUE_COUNT; i++, bufferCount++)
		{
			auto lock = Synchronize::lock(m_id);
			const ALBuffer& buffer = m_data->nextBuffer();
			if (buffer.getID())
				buffers[i] = buffer.getID();
			else
				break;
		}

		if (bufferCount > 0)
		{
			auto lock = Synchronize::lock(m_id);
			if (bufferCount == 1)
				alSourcei(*m_id, AL_BUFFER, buffers[0]);
			else
			{
				alSourceQueueBuffers(*m_id, bufferCount, buffers);
				m_stream = true;
			}

		}

	}

	void ALAudioSource::fireStateChange__(ALAudioSourceStateListener::Change change)
	{
		auto lock = Synchronize::lock(m_listener);
		if (*m_listener)
			(*m_listener)->stateChanged(change, *this);
	}

}