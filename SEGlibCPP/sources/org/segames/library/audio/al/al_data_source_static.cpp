#include <org/segames/library/debug_util.h>
#include <org/segames/library/audio/al/al_data_source_static.h>
#include <org/segames/library/invalid_operation_exception.h>

namespace org::segames::library
{

	ALDataSourceStatic::ALDataSourceStatic(const PCMFormat& format, PCMParser& parser) noexcept(false) :
		m_used(false),
		m_buffer()
	{
		std::unique_ptr<PCMBuffer> conversion(std::move(PCMBuffer::getBufferFor(format)));
				
		size_t count = 0;
		for (; count < PARSE_BREAK_COUNT && parser.parse(PCMParser::ALL_SAMPLES, *conversion) > 0; count++);
		if (count == PARSE_BREAK_COUNT)
			throw InvalidOperationException("Faulty audio parser! Exceeded max parse attempt limit.");

		m_buffer.upload(format, conversion->nativeSize(), conversion->nativePointer());
	}

}