#include <org/segames/library/debug_util.h>
#include <org/segames/library/audio/pcm_parser_ogg.h>
#include <org/segames/library/io/io_exception.h>

#include <algorithm>
#include <iostream>

namespace org::segames::library
{

	size_t PCMParserOGG::s_readSize = 1024*1024;

	PCMParserOGG::PCMParserOGG(std::unique_ptr<std::istream>&& input) noexcept(false) :
		m_format(),
		m_bufferUsed(0),
		m_bufferLength(0),
		m_buffer(nullptr),
		m_dataStart(0),
		m_input(std::move(input)),
		m_vorbis(nullptr),
		m_outputUsed(0),
		m_outputLength(0),
		m_output(nullptr)
	{
		do
		{
			int consumed, errorCode;
			if (!aquireMoreData__())
				throw IOException("End of stream before vorbis data head could be parsed!");
			m_vorbis = stb_vorbis_open_pushdata(reinterpret_cast<unsigned char*>(m_buffer.get()), m_bufferLength, &consumed, &errorCode, NULL);
		} while (m_vorbis == nullptr);

		stb_vorbis_info info = stb_vorbis_get_info(m_vorbis);
		m_format = PCMFormat(static_cast<unsigned char>(info.channels >= 2 ? 2 : info.channels), 16, static_cast<ALsizei>(info.sample_rate));

		m_dataStart = m_input->tellg() - std::streampos(m_bufferLength - m_bufferUsed);
	}

	PCMParserOGG::~PCMParserOGG() noexcept
	{
		if (m_vorbis)
			stb_vorbis_close(m_vorbis);
	}

	size_t PCMParserOGG::parse(size_t requestedSamples, PCMBuffer& output)
	{
		size_t samples = 0;
		while (samples < requestedSamples)
		{
			if (m_output && m_outputUsed < m_outputLength)
			{
				for (; m_outputUsed < m_outputLength && samples < requestedSamples; m_outputUsed++, samples++)
					if (m_format.channels() == 2)
						output.put(m_output[0][m_outputUsed], m_output[1][m_outputUsed]);
					else
						output.put(m_output[0][m_outputUsed]);
			}
			else if (!aquireMoreSamples__())
				break;
		}

		return samples;
	}

	bool PCMParserOGG::rewind()
	{
		try
		{
			m_input->seekg(m_dataStart);
			stb_vorbis_flush_pushdata(m_vorbis);
			return true;
		}
		catch (const std::exception&)
		{
			return false;
		}

	}

	bool PCMParserOGG::aquireMoreData__()
	{
		if (m_input->good())
		{
			const size_t leftOverLength = m_bufferLength - m_bufferUsed;
			const size_t newBufferLength = leftOverLength + s_readSize;
					
			std::unique_ptr<char[]> newBuffer(new char[newBufferLength]);
			if(m_buffer && leftOverLength > 0)
				std::copy_n(m_buffer.get() + m_bufferUsed, leftOverLength, newBuffer.get());

			std::streamsize readLength = m_input->readsome(newBuffer.get() + leftOverLength, s_readSize);
			if (readLength > 0)
			{
				m_bufferLength = leftOverLength + readLength;
				m_bufferUsed = 0;
				m_buffer.swap(newBuffer);
				return true;
			}
			else 
				return false;
		}
		else
			return false;

	}

	bool PCMParserOGG::aquireMoreSamples__()
	{
		int outputLength;
		int bytesUsed = stb_vorbis_decode_frame_pushdata(
			m_vorbis,
			reinterpret_cast<unsigned char*>(m_buffer.get()) + m_bufferUsed,
			m_bufferLength - m_bufferUsed,
			NULL,
			&m_output,
			&outputLength
		);

		if (bytesUsed == 0)
		{
			if (!aquireMoreData__())
				return false;
			else
				return true;
		}
		else
		{
			m_bufferUsed += bytesUsed;
			m_outputUsed = 0;
			m_outputLength = outputLength;
			return true;
		}

	}

}