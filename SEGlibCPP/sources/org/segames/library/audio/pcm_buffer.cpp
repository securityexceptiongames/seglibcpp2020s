#include <org/segames/library/debug_util.h>
#include <org/segames/library/audio/pcm_buffer.h>
#include <org/segames/library/audio/pcm_buffer_16bit_mono.h>
#include <org/segames/library/audio/pcm_buffer_16bit_stereo.h>
#include <org/segames/library/audio/pcm_buffer_8bit_mono.h>
#include <org/segames/library/audio/pcm_buffer_8bit_stereo.h>
#include <org/segames/library/audio/invalid_format_exception.h>

namespace org::segames::library
{

	std::unique_ptr<PCMBuffer> PCMBuffer::getBufferFor(const PCMFormat& format) noexcept(false)
	{
		if (format.bitDepth() == 16)
		{
			if (format.channels() == 1)
				return std::unique_ptr<PCMBuffer>(new PCMBuffer16bitMono(format.sampleRate()));
			else if (format.channels() == 2)
				return std::unique_ptr<PCMBuffer>(new PCMBuffer16bitStereo(format.sampleRate()));
		}
		else if (format.bitDepth() == 8)
		{
			if (format.channels() == 1)
				return std::unique_ptr<PCMBuffer>(new PCMBuffer8bitMono(format.sampleRate()));
			else if (format.channels() == 2)
				return std::unique_ptr<PCMBuffer>(new PCMBuffer8bitStereo(format.sampleRate()));
		}
		throw InvalidFormatException(format);
	}

}