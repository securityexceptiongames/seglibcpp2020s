#include <org/segames/library/debug_util.h>
#include <org/segames/library/glfw/glfw_window.h>
#include <org/segames/library/glfw/glfw_exception.h>
#include <org/segames/library/io/file.h>
#include <org/segames/library/io/file_not_found_exception.h>
#include <org/segames/library/math/math.h>

#include <stb/stb_image.h>

namespace org::segames::library
{

	bool GLFWWindow::s_glProcLoaded = false;

	GLFWWindow::GLFWWindow() :
		GLFWWindow(setStandardCreationHints)
	{}

	GLFWWindow::GLFWWindow(GLFWwindowhintsfun hints) :
		m_iconifyListener(nullptr),
		m_keyListener(nullptr),
		m_mbListener(nullptr),
		m_mpListener(nullptr),
		m_sizeListener(nullptr),
		m_listeners()
	{
		if (!glfwInit())
			throw GLFWException("Could not initialize GLFW.");

		if (hints == nullptr)
			setStandardCreationHints();
		else
			hints();

		if ((m_window = glfwCreateWindow(800, 600, "", 0, 0)) == NULL)
		{
			glfwDefaultWindowHints();
			glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
			glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

			if ((m_window = glfwCreateWindow(800, 600, "", 0, 0)) == NULL)
				throw GLFWException("Could not create GLFW window.");
		}

		setListener(std::unique_ptr<GLFWIconificationListener>(new GLFWIconificationListener()));
		setListener(std::unique_ptr<GLFWKeyListener>(new GLFWKeyListener()));
		setListener(std::unique_ptr<GLFWMouseButtonListener>(new GLFWMouseButtonListener()));
		setListener(std::unique_ptr<GLFWMousePositionListener>(new GLFWMousePositionListener()));
		setListener(std::unique_ptr<GLFWWindowSizeListener>(new GLFWWindowSizeListener()));
	}

	GLFWWindow::GLFWWindow(GLFWWindow&& obj) noexcept :
		m_window(obj.m_window),
		m_iconifyListener(std::move(obj.m_iconifyListener)),
		m_keyListener(std::move(obj.m_keyListener)),
		m_mbListener(std::move(obj.m_mbListener)),
		m_mpListener(std::move(obj.m_mpListener)),
		m_sizeListener(std::move(obj.m_sizeListener)),
		m_listeners(std::move(obj.m_listeners))
	{
		obj.m_window = nullptr;
	}

	bool GLFWWindow::setIcon(const std::initializer_list<std::string_view>& iconImages)
	{
		int count = 0;
		bool retVal = true;
		std::unique_ptr<GLFWimage[]> images(new GLFWimage[iconImages.size()]);
		std::unique_ptr<std::unique_ptr<stbi_uc[]>[]> data(new std::unique_ptr<stbi_uc[]>[iconImages.size()]);

		for (auto itr = iconImages.begin(); itr != iconImages.end(); itr++)
		{
			try
			{
				File f(*itr);
				if (!f.exists())
					throw FileNotFoundException(f.getPath());

				int c;
				GLFWimage img;
				std::unique_ptr<stbi_uc[]> chars(stbi_load(f.getPath().c_str(), &img.width, &img.height, &c, 4));

				img.pixels = chars.get();
				images[count] = img;
				data[count].swap(chars);
				count++;
			}
			catch (const FileNotFoundException& e)
			{
				retVal = false;
			}

		}

		glfwSetWindowIcon(getWindowID(), count, images.get());
		return retVal;
	}

	std::unique_ptr<GLFWIconificationListener> GLFWWindow::setListener(std::unique_ptr<GLFWIconificationListener>&& list)
	{
		std::swap(m_iconifyListener, list);
		if (m_iconifyListener)
			m_iconifyListener->link(m_window);
		else
			GLFWIconificationListener::detach(m_window);
		return std::unique_ptr<GLFWIconificationListener>(std::move(list));
	}

	std::unique_ptr<GLFWKeyListener> GLFWWindow::setListener(std::unique_ptr<GLFWKeyListener>&& list)
	{
		std::swap(m_keyListener, list);
		if (m_keyListener)
			m_keyListener->link(m_window);
		else
			GLFWKeyListener::detach(m_window);
		return std::unique_ptr<GLFWKeyListener>(std::move(list));
	}

	std::unique_ptr<GLFWMouseButtonListener> GLFWWindow::setListener(std::unique_ptr<GLFWMouseButtonListener>&& list)
	{
		std::swap(m_mbListener, list);
		if (m_mbListener)
			m_mbListener->link(m_window);
		else
			GLFWMouseButtonListener::detach(m_window);
		return std::unique_ptr<GLFWMouseButtonListener>(std::move(list));
	}

	std::unique_ptr<GLFWMousePositionListener> GLFWWindow::setListener(std::unique_ptr<GLFWMousePositionListener>&& list)
	{
		std::swap(m_mpListener, list);
		if (m_mpListener)
			m_mpListener->link(m_window);
		else
			GLFWMousePositionListener::detach(m_window);
		return std::unique_ptr<GLFWMousePositionListener>(std::move(list));
	}

	std::unique_ptr<GLFWWindowSizeListener> GLFWWindow::setListener(std::unique_ptr<GLFWWindowSizeListener>&& list)
	{
		std::swap(m_sizeListener, list);
		if (m_sizeListener)
			m_sizeListener->link(m_window);
		else
			GLFWWindowSizeListener::detach(m_window);
		return std::unique_ptr<GLFWWindowSizeListener>(std::move(list));
	}

	GLFWWindow& GLFWWindow::operator=(GLFWWindow&& obj) noexcept
	{
		m_window = obj.m_window;
		m_iconifyListener = std::move(obj.m_iconifyListener);
		m_keyListener = std::move(obj.m_keyListener);
		m_mbListener = std::move(obj.m_mbListener);
		m_mpListener = std::move(obj.m_mpListener);
		m_sizeListener = std::move(obj.m_sizeListener);
		m_listeners = std::move(obj.m_listeners);
		obj.m_window = nullptr;
		return *this;
	}

	GLFWmonitor* GLFWWindow::getCurrentMonitorFor(GLFWwindow* window)
	{
		int numMonitors;
		GLFWmonitor** monitors = glfwGetMonitors(&numMonitors);

		int x, y, w, h;
		glfwGetWindowPos(window, &x, &y);
		glfwGetWindowSize(window, &w, &h);

		int bestSize = 0;
		GLFWmonitor* bestMonitor = glfwGetPrimaryMonitor();

		int mx, my, mw, mh;
		for (int i = 0; i < numMonitors; i++)
		{
			glfwGetMonitorPos(monitors[i], &mx, &my);
			glfwGetMonitorPhysicalSize(monitors[i], &mw, &mh);

			int size = Math::max(0, Math::min(x + w, mx + mw) - Math::max(x, mx))
				* Math::max(0, Math::min(y + h, my + mh) - Math::max(y, my));

			if (size > bestSize)
			{
				bestSize = size;
				bestMonitor = monitors[i];
			}

		}

		return bestMonitor;
	}

	void GLFWWindow::setStandardCreationHints()
	{
		glfwDefaultWindowHints();
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
#ifdef __APPLE__
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
#endif
	}

}