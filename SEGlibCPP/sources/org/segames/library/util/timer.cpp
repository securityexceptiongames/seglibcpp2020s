#include <org/segames/library/util/timer.h>
#include <org/segames/library/invalid_value_exception.h>

#include <thread>

using namespace std::chrono;

namespace org::segames::library
{

	bool Timer::time()
	{
		if (m_updateRate < 0)
			return false;
		else if (m_updateRate == 0)
			return true;
		else
		{
			const size_t time = nanoTime();
			if ((time - m_lastTime) > 1000000000.0 * m_updateTime)
			{
				m_lastTime = time;
				return true;
			}
			else
				return false;
		}

	}

	double Timer::deltaTime()
	{
		double out = 1.0;
		if (m_lastTime == -1ull)
		{
			if (m_updateTime != 0ull)
				out = m_updateTime;
		}
		else
			out = (nanoTime() - m_lastTime) / 1000000000.0;
		m_lastTime = nanoTime();
		return out;
	}

	void Timer::sync()
	{
		if (m_lastTime == -1ull)
		{
			m_lastTime = nanoTime();
			return;
		}

		if (m_updateTime == 0.0)
			return;

#ifdef SEG_API_TIMER_SYNC_CHECK
		if (m_updateTime < 0.0)
			throw InvalidValueException("Cannot sync time for update rates less than zero.");
#endif

		size_t sleep = (size_t)(m_updateTime * 1000000000.0) - (nanoTime() - m_lastTime);
		if (sleep > 0)
			std::this_thread::sleep_for(nanoseconds(sleep));
		m_lastTime = nanoTime();
	}

	size_t Timer::nanoTime()
	{
		return (size_t)duration_cast<nanoseconds>(high_resolution_clock::now().time_since_epoch()).count();
	}

	size_t Timer::microTime()
	{
		return (size_t)duration_cast<microseconds>(high_resolution_clock::now().time_since_epoch()).count();
	}

	size_t Timer::milliTime()
	{
		return (size_t)duration_cast<milliseconds>(high_resolution_clock::now().time_since_epoch()).count();
	}

}