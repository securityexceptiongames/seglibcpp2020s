#include <org/segames/library/util/color.h>
#include <org/segames/library/math/math.h>

#include <typeinfo>

namespace org::segames::library
{
			
	Color Color::BLACK			= Color(0, 0, 0, static_cast<unsigned char>(255));
	Color Color::DARK_GRAY		= Color(64, 64, 64, static_cast<unsigned char>(255));
	Color Color::GRAY			= Color(128, 128, 128, static_cast<unsigned char>(255));
	Color Color::LIGHT_GRAY		= Color(191, 191, 191, static_cast<unsigned char>(255));
	Color Color::WHITE			= Color(255, 255, 255, static_cast<unsigned char>(255));
	Color Color::RED			= Color(255, 0, 0, static_cast<unsigned char>(255));
	Color Color::GREEN			= Color(0, 255, 0, static_cast<unsigned char>(255));
	Color Color::BLUE			= Color(0, 0, 255, static_cast<unsigned char>(255));
	Color Color::YELLOW			= Color(255, 255, 0, static_cast<unsigned char>(255));
	Color Color::CYAN			= Color(0, 255, 255, static_cast<unsigned char>(255));
	Color Color::MAGENTA		= Color(255, 0, 255, static_cast<unsigned char>(255));
	Color Color::MAROON			= Color(128, 0, 0, static_cast<unsigned char>(255));
	Color Color::OLIVE			= Color(128, 128, 0, static_cast<unsigned char>(255));
	Color Color::DARK_GREEN		= Color(0, 128, 0, static_cast<unsigned char>(255));
	Color Color::PURPLE			= Color(128, 0, 128, static_cast<unsigned char>(255));
	Color Color::TEAL			= Color(0, 128, 128, static_cast<unsigned char>(255));
	Color Color::NAVY_BLUE		= Color(0, 0, 128, static_cast<unsigned char>(255));
	Color Color::ORANGE			= Color(255, 165, 0, static_cast<unsigned char>(255));
	Color Color::BROWN			= Color(150, 75, 0, static_cast<unsigned char>(255));
	Color Color::PINK			= Color(255, 192, 203, static_cast<unsigned char>(255));

	Color Color::RGBtoCMYK(const Color& color)
	{
		const float K = 1 - Math::max(color.getRed(), color.getGreen(), color.getBlue());
		return Color(
			(1 - color.getRed() - K) / (1 - K),
			(1 - color.getGreen() - K) / (1 - K),
			(1 - color.getBlue() - K) / (1 - K),
			K
		);
	}

	Color Color::CMYKtoRGB(const Color& color)
	{
		return Color(
			(1 - color.getRed()) * (1 - color.getAlpha()),
			(1 - color.getGreen()) * (1 - color.getAlpha()),
			(1 - color.getBlue()) * (1 - color.getAlpha()),
			1
		);
	}

}