#include <org/segames/library/util/utf8_iterator.h>

namespace org::segames::library
{

	UTF8Iterator& UTF8Iterator::operator++() noexcept
	{
		char firstC = *m_itr;
		std::string::difference_type offset = 1;

		if (firstC & BitMask::FIRST)
		{
			if (firstC & BitMask::THIRD)
			{
				if (firstC & BitMask::FOURTH)
					offset = 4;
				else
					offset = 3;
			}
			else
				offset = 2;
		}

		m_itr += offset;
		m_moved = true;
		return *this;
	}

	UTF8Iterator& UTF8Iterator::operator--() noexcept
	{
		--m_itr;
		if (*m_itr & BitMask::FIRST)
		{
			--m_itr;
			if ((*m_itr & BitMask::SECOND) == 0)
			{
				--m_itr;
				if ((*m_itr & BitMask::SECOND) == 0)
					--m_itr;
			}

		}
		m_moved = true;
		return *this;
	}

	unsigned int UTF8Iterator::operator*() const
	{
		if (m_moved)
		{
			unsigned int codePoint = 0;
			char firstC = *m_itr;

			if (firstC & BitMask::FIRST)
			{
				if (firstC & BitMask::THIRD)
				{
					if (firstC & BitMask::FOURTH)
					{
						codePoint = (firstC & 0x07) << 18;

						char secondC = *(m_itr + 1);
						codePoint += (secondC & 0x3f) << 12;

						char thirdC = *(m_itr + 2);
						codePoint += (thirdC & 0x3f) << 6;

						char fourthC = *(m_itr + 3);
						codePoint += (fourthC & 0x3f);
					}
					else
					{
						codePoint = (firstC & 0x0f) << 12;

						char secondC = *(m_itr + 1);
						codePoint += (secondC & 0x3f) << 6;

						char thirdC = *(m_itr + 2);
						codePoint += (thirdC & 0x3f);
					}

				}
				else
				{
					codePoint = (firstC & 0x1f) << 6;
					char secondC = *(m_itr + 1);
					codePoint += (secondC & 0x3f);
				}

			}
			else
			{
				codePoint = (unsigned int)firstC;
			}

			m_currentCodepoint = codePoint;
			m_moved = false;
		}

		return m_currentCodepoint;
	}

	void UTF8Iterator::codepointToString(const unsigned int codepoint, std::string& str)
	{
		if (codepoint <= 0x7F) 
		{
			str += static_cast<unsigned char>(codepoint);
		}
		else if (codepoint <= 0x7FF)
		{
			str += static_cast<unsigned char>((codepoint >> 6) + 192);
			str += static_cast<unsigned char>((codepoint & 63) + 128);
		}
		else if (0xd800 <= codepoint && codepoint <= 0xdfff) {} //invalid block of utf8
		else if (codepoint <= 0xFFFF)
		{
			str += static_cast<unsigned char>((codepoint >> 12) + 224);
			str += static_cast<unsigned char>(((codepoint >> 6) & 63) + 128);
			str += static_cast<unsigned char>((codepoint & 63) + 128);
		}
		else if (codepoint <= 0x10FFFF)
		{
			str += static_cast<unsigned char>((codepoint >> 18) + 240);
			str += static_cast<unsigned char>(((codepoint >> 12) & 63) + 128);
			str += static_cast<unsigned char>(((codepoint >> 6) & 63) + 128);
			str += static_cast<unsigned char>((codepoint & 63) + 128);
		}

	}

}