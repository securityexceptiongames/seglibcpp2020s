#include <org/segames/library/debug_util.h>
#include <org/segames/library/exception.h>

#include <sstream>
#include <iostream>

namespace org::segames::library
{

	std::string Exception::s_projectPath = "";

	std::string Exception::createCompleteMessage(const std::string_view& name, const std::string_view& message, const std::vector<StackTrace::Frame>& stackFrames)
	{
		std::string out = std::string(name) + ": ";
		if (message.size() > 0)
			out += message;

		if (stackFrames.size() > 0)
		{
			for (size_t i = 0; i < stackFrames.size(); i++)
			{
				const StackTrace::Frame& frame = stackFrames[i];

				std::string file = frame.file;
				auto pos = file.find(s_projectPath);
				if (pos != std::string::npos)
					file.erase(pos, s_projectPath.size());

				std::stringstream strStream;
				strStream << "\n    at (";
				strStream << std::hex << frame.address << std::dec << ") " << file << ":" << frame.line << " - " << frame.function << "()";
				out += strStream.str();
			}

		}

		return out;
	}

}