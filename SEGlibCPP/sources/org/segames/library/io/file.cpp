#include <org/segames/library/debug_util.h>
#include <org/segames/library/io/file.h>
#include <org/segames/library/io/file_not_found_exception.h>
#include <org/segames/library/util/string_util.h>

#ifdef _WIN32
#include <Windows.h>
#include <direct.h>
#endif

namespace org::segames::library
{

	const File File::NULL_FILE;
	const File File::PROJECT_DIR_FILE("");
	const File File::DISC_ROOT_DIR_FILE("/");

	std::string File::getParent() const
	{
		if (m_attributes.get())
		{
			std::string out;
			const ArrayList<std::string_view>& pathDiss = m_attributes->getPathDissection();
			for (size_t i = 0; i < pathDiss.size() - 1; i++)
				(out += pathDiss.get(i)) += SEG_API_SYSTEM_DIR_SEPARATOR;
			return out;
		}
		return "";
	}

	std::ifstream File::openInputBin() const
	{
		if (!exists())
			throw FileNotFoundException(getPath());
					
		std::ifstream stream(getPath(), std::fstream::binary);
		if (!stream.good())
			throw IOException("Could not open input stream to file \"" + getPath() + "\"");
		return stream;
	}

	std::ifstream File::openInputChar() const
	{
		if (!exists())
			throw FileNotFoundException(getPath());

		std::ifstream stream(getPath());
		if (!stream.good())
			throw IOException("Could not open input stream to file \"" + getPath() + "\"");
		return stream;
	}

	std::ofstream File::openOutputBin() const
	{
		if (!exists())
			throw FileNotFoundException(getPath());

		std::ofstream stream(getPath(), std::fstream::binary);
		if (!stream.good())
			throw IOException("Could not open output stream to file \"" + getPath() + "\"");
		return stream;
	}

	std::ofstream File::openOutputChar() const
	{
		if (!exists())
			throw FileNotFoundException(getPath());

		std::ofstream stream(getPath());
		if (!stream.good())
			throw IOException("Could not open output stream to file \"" + getPath() + "\"");
		return stream;
	}

	bool File::createNewFileWindows__()
	{
#ifdef _WIN32
		bool retVal = false;
		if (m_attributes.get())
		{
			HANDLE hand = CreateFileA(getPath().c_str(), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
			if (hand != INVALID_HANDLE_VALUE)
				retVal = true;
			CloseHandle(hand);
		}
		return retVal;
#else
		return false;
#endif
	}

	bool File::mkdirsWindows__()
	{
#ifdef _WIN32
		bool retVal = false;
		if (m_attributes.get())
		{
			std::string tempPath = std::string(m_attributes->getPathDissection().get(0));
			for (size_t i = 1; i < m_attributes->getPathDissection().size(); i++)
			{
				if (m_attributes->getPathDissection().get(i).size() > 0)
				{
					(tempPath += SEG_API_SYSTEM_DIR_SEPARATOR) += m_attributes->getPathDissection().get(i);

					DWORD attrib = GetFileAttributesA(tempPath.c_str());
					if (attrib == INVALID_FILE_ATTRIBUTES)
					{
						int retValInt = _mkdir(tempPath.c_str());
						if (retValInt == 0)
							retVal = true;
					}

				}

			}

		}
		return retVal;
#else
		return false;
#endif
	}

	void File::listWindows__(ArrayList<std::string>& names) const
	{
#ifdef _WIN32
		HANDLE dir;
		WIN32_FIND_DATAA findData;
		if ((dir = FindFirstFileA((getPath() + SEG_API_SYSTEM_DIR_SEPARATOR + "*").c_str(), &findData)) != INVALID_HANDLE_VALUE)
		{
			while (FindNextFileA(dir, &findData))
			{
				std::string name = std::string(findData.cFileName);
				if (name != "..")
					names.add(findData.cFileName);
			}
			FindClose(dir);
		}
#endif
	}

	void File::listFilesWindows__(ArrayList<File>& files) const
	{
#ifdef _WIN32
		HANDLE dir;
		WIN32_FIND_DATAA findData;
		if ((dir = FindFirstFileA((getPath() + SEG_API_SYSTEM_DIR_SEPARATOR + "*").c_str(), &findData)) != INVALID_HANDLE_VALUE)
		{
			while (FindNextFileA(dir, &findData))
			{
				std::string name = std::string(findData.cFileName);
				if (name != "..")
					files.add(File(getPath() + SEG_API_SYSTEM_DIR_SEPARATOR + findData.cFileName));
			}
			FindClose(dir);
		}
#endif
	}

}