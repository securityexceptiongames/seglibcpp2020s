#include <org/segames/library/debug_util.h>
#include <org/segames/library/io/network/tcp/tcp_server.h>
#include <org/segames/library/io/network/socket_exception.h>
#include <org/segames/library/thread/synchronized.h>
#include <org/segames/library/core.h>

namespace org::segames::library
{

	TCPServer::TCPServer(unsigned short port, TCPServerConfigurator* configurator, TCPConnectionListener* connectionListener, std::unique_ptr<TCPTickClock>&& tickClock) noexcept(false) :
		m_shutdown(false),
		m_destructionAllowed(0),
		m_configurator(configurator),
		m_connectionListener(connectionListener),
		m_tickClock(std::move(tickClock)),
		m_socket(Socket(port, Socket::SocketType::STREAM)),
		m_thread(this),
		m_destructionCondition(),
		m_communicators(),
		m_disposeCommunicators()
	{
		m_thread.start();
	}

	TCPServer::~TCPServer() noexcept
	{
		close();
				
		do {
			m_destructionCondition.waitIf([](const bool& flag, const Volatile<std::unordered_map<int, std::unique_ptr<TCPCommunicator>>>& com) { 
				return !flag || com->size() > 0;
			}, m_destructionAllowed, m_communicators);
		} while (!m_destructionAllowed || m_communicators->size() > 0);
	}
			
	void TCPServer::close() noexcept
	{
		if (!m_shutdown)
		{
			m_shutdown = true;
			{
				auto lock = Synchronize::lock(m_communicators);
				for (auto itr = m_communicators->begin(); itr != m_communicators->end(); itr++)
					itr->second->close();
			}

			{
				auto lock = Synchronize::lock(m_socket);
				m_socket->close();
			}

		}
				
	}

	void TCPServer::onConnect(TCPCommunicatorBase& communicator)
	{
		if (m_connectionListener)
			m_connectionListener->onConnect(communicator);
	}

	void TCPServer::onDisconnect(TCPCommunicatorBase& communicator)
	{
		{
			std::scoped_lock<std::mutex, std::mutex> lock(m_communicators.mutex_b(), m_disposeCommunicators.mutex_b());
			auto res = m_communicators->find(communicator.getID());
			if (res != m_communicators->end())
			{
				m_disposeCommunicators->insert(std::move(res->second));
				m_communicators->erase(res);
			}

		}

		if (m_connectionListener)
			m_connectionListener->onDisconnect(communicator);

		if (m_communicators->size() && m_shutdown)
			m_destructionCondition.notifyAll();
	}

	void TCPServer::run()
	{
		while (!m_shutdown)
		{
			{
				auto lock = Synchronize::lock(m_disposeCommunicators);
				m_disposeCommunicators->clear();
			}

			try
			{
				Socket socket = m_socket->acceptConnection();
				std::unique_ptr<TCPCommunicator> communicator(
					new TCPCommunicator(std::move(socket), m_configurator, this, m_tickClock ? m_tickClock->newInstance() : nullptr)
				);
				{
					auto lock = Synchronize::lock(m_communicators);
					m_communicators->insert_or_assign(communicator->getID(), std::move(communicator));
				}

			}
			catch (const SocketException& e)
			{
				if(e.code() != WSAEINTR)
					Core::err() << e.what() << std::endl;
			}
			catch (const std::exception& e2)
			{
				Core::err() << e2.what() << std::endl;
			}

		}

		m_destructionAllowed = true;
		m_destructionCondition.notifyAll();
	}

}