#include <org/segames/library/debug_util.h>
#include <org/segames/library/io/network/tcp/tcp_client.h>
#include <org/segames/library/thread/synchronized.h>

namespace org::segames::library
{

	TCPClient::TCPClient(const InetAddress& address, TCPClientConfigurator* configurator, TCPConnectionListener* connectionListener, std::unique_ptr<TCPTickClock>&& tickClock) noexcept(false) :
		TCPCommunicatorBase(Socket(address), connectionListener, std::move(tickClock)),
		m_configurator(configurator),
		m_actionHandlers(configurator ? std::move(configurator->getActionHandlers(*this)) : std::unordered_map<int, TCPActionHandler*>()),
		m_messageHandlers(configurator ? std::move(configurator->getMessageHandlers(*this)) : std::unordered_map<short, TCPMessageHandlerClient*>())
	{
		m_thread.start();
	}

	void TCPClient::processAction(std::ostream& output, std::unique_ptr<TCPAction>&& action) noexcept(false)
	{
		auto lock = Synchronize::lock(m_actionHandlers);
		auto res = m_actionHandlers->find(action->getActionID());
		if (res != m_actionHandlers->end())
			res->second->processAction(output, std::move(action));
	}

	void TCPClient::processMessage(std::istream& input, short messageID) noexcept(false)
	{
		auto lock = Synchronize::lock(m_messageHandlers);
		auto res = m_messageHandlers->find(messageID);
		if (res != m_messageHandlers->end())
			res->second->processMessage(*this, input);
	}

}