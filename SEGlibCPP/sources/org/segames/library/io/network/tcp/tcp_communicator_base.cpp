#include <org/segames/library/debug_util.h>
#include <org/segames/library/io/network/tcp/tcp_communicator_base.h>
#include <org/segames/library/io/network/inet_util.h>
#include <org/segames/library/io/io_exception.h>
#include <org/segames/library/thread/synchronized.h>
#include <org/segames/library/core.h>

namespace org::segames::library
{

	std::atomic<size_t> TCPCommunicatorBase::s_counterID = 0;
	Volatile<std::unordered_set<size_t>> TCPCommunicatorBase::s_currentlyUsedIDs;

	TCPCommunicatorBase::~TCPCommunicatorBase()
	{
		if (m_socket->isConnected())
		{
			auto lock = Synchronize::lock(m_socket);
			m_socket->close();
		}
				
		do { 
			m_destructionCondition.waitIf([](const bool& flag) { return !flag; }, m_destructionAllowed);
		} while (!m_destructionAllowed);

		if (m_id != 0)
		{
			auto lock = Synchronize::lock(s_currentlyUsedIDs);
			s_currentlyUsedIDs->erase(m_id);
		}

	}

	void TCPCommunicatorBase::addAction(std::unique_ptr<TCPAction>&& action)
	{
		auto lock = Synchronize::lock(m_actions);
		m_actions->enqueue(std::move(action));
	}

	void TCPCommunicatorBase::run()
	{
		{
			auto lock = Synchronize::lock(m_connectionListener);
			if (*m_connectionListener)
				(*m_connectionListener)->onConnect(*this);
		}

		while (m_socket->isConnected())
		{
			try
			{
				if (m_disconnect)
					break;
				else
					doCommunication__();
			}
			catch (const IOException& e)
			{
				Core::err() << e.what() << std::endl;
				break;
			}
			catch (const std::exception& e2)
			{
				Core::err() << e2.what() << std::endl;
			}

		}

		{
			auto lock = Synchronize::lock(m_connectionListener);
			if (*m_connectionListener)
				(*m_connectionListener)->onDisconnect(*this);
		}

		if (m_socket->isConnected())
		{
			auto lock = Synchronize::lock(m_socket);
			m_socket->close();
		}

		m_destructionAllowed = true;
		m_destructionCondition.notifyAll();
	}

	void TCPCommunicatorBase::doCommunication__() noexcept(false)
	{
		m_idleLoops++;

		{
			auto lock = Synchronize::lock(m_socket);
			if (m_socket->getInputStream().hasAvaliableForRead())
			{
				short messageID = InetUtil::next<short>(m_socket->getInputStream());
				processMessage(m_socket->getInputStream(), messageID);
				m_idleLoops = 0;
			}

		}

		std::unique_ptr<TCPAction> action;
		{
			auto lock = Synchronize::lock(m_actions);
			if (!m_actions->empty())
				action = std::move(m_actions->dequeue());
		}

		if (action)
		{
			auto lock = Synchronize::lock(m_socket);
			processAction(m_socket->getOutputStream(), std::move(action));
			m_idleLoops = 0;
		}

		m_tickClock->sleepTick(m_idleLoops);
	}

	size_t TCPCommunicatorBase::generateID__() noexcept(false)
	{
		for (int i = 0; i < 100000; i++)
		{
			size_t id = ++s_counterID;
			if (id == 0)
				id = ++s_counterID;

			auto lock = Synchronize::lock(s_currentlyUsedIDs);
			if (s_currentlyUsedIDs->find(id) == s_currentlyUsedIDs->end())
			{
				s_currentlyUsedIDs->insert(id);
				return id;
			}

		}

		throw InvalidOperationException("Could not find an avaliable ID!");
	}

}