#include <org/segames/library/debug_util.h>
#include <org/segames/library/io/network/socket_streambuf.h>
#include <org/segames/library/io/network/socket.h>
#include <org/segames/library/io/network/socket_exception.h>

#include <fstream>

namespace org::segames::library
{

#ifdef _WIN32
	bool SocketStreambuf::hasAvaliableForRead() const noexcept(false)
	{
		if (gptr() != egptr())
			return true;
		else
		{
			char value;
			int error;
			u_long nonBlocking = 1;
			if (ioctlsocket(m_socket->getSocket(), FIONBIO, &nonBlocking) == SOCKET_ERROR)
			{
				error = WSAGetLastError();
				throw SocketException("Could not change socket to non-blocking! Error: " + std::to_string(error), error);
			}

			int readSize = recv(m_socket->getSocket(), &value, 1, MSG_PEEK);
			error = WSAGetLastError();
			if (readSize == SOCKET_ERROR && error != WSAEWOULDBLOCK)
				throw SocketException("Could not receive from socket! Error: " + std::to_string(error), error);
			else
			{
				nonBlocking = 0;
				if (ioctlsocket(m_socket->getSocket(), FIONBIO, &nonBlocking) == SOCKET_ERROR)
				{
					error = WSAGetLastError();
					throw SocketException("Could not change socket back to blocking! Error: " + std::to_string(error), error);
				}

				return readSize > 0;
			}

		}

	}

	std::streambuf::int_type SocketStreambuf::underflow() noexcept(false)
	{
		int readSize = recv(m_socket->getSocket(), m_buffer.nativePointer(), m_buffer.nativeSize(), 0);
		if (readSize == 0)
		{
			m_socket->setDisconnected(false);
			return traits_type::eof();
		}
		else if (readSize > 0)
		{
			setg(m_buffer.nativePointer(), m_buffer.nativePointer(), m_buffer.nativePointer() + readSize);
			return traits_type::to_int_type(*gptr());
		}
		else
		{
			int error = WSAGetLastError();
			m_socket->setDisconnected(true);
			throw SocketException("Could not receive from socket! Error: " + std::to_string(error), error);
		}

	}

	std::streambuf::int_type SocketStreambuf::overflow(std::streambuf::int_type value) noexcept(false)
	{
		int sendSize = pptr() - pbase();
		if (!traits_type::eq_int_type(value, traits_type::eof()))
			m_buffer[sendSize++] = traits_type::to_char_type(value);

		char* pointer = m_buffer.nativePointer();
		while (sendSize > 0)
		{
			int sent = send(m_socket->getSocket(), pointer, sendSize, 0);
			if (sent == SOCKET_ERROR)
			{
				int error = WSAGetLastError();
				m_socket->setDisconnected(true);
				throw SocketException("Could not send through socket! Error: " + std::to_string(error), error);
			}
			else
				sendSize -= sent;
		}

		setp(m_buffer.nativePointer(), m_buffer.nativePointer() + m_buffer.nativeSize() - 1);
		return traits_type::not_eof(value);
	}

	int SocketStreambuf::sync() noexcept(false)
	{
		return traits_type::eq_int_type(this->overflow(traits_type::eof()), traits_type::eof()) ? -1 : 0;
	}
#endif

}