#include <org/segames/library/debug_util.h>
#include <org/segames/library/io/network/socket.h>
#include <org/segames/library/io/network/socket_exception.h>

#ifdef _WIN32
#include <ws2tcpip.h>
#endif

namespace org::segames::library
{

	size_t Socket::s_inputBufferSize = 1024;
	size_t Socket::s_outputBufferSize = 128;

	Socket::Socket(const InetAddress& address, SocketFamily family) noexcept(false) :
		m_broken(false),
		m_connected(true),
		m_bound(true),
		m_type(SocketType::STREAM),
		m_family(family),
		m_address(address),
#ifdef _WIN32
		m_socket(initClientTCPSocketWin32__(address, family)),
		m_inputStream(new SocketStreambuf(s_inputBufferSize, *this)),
		m_outputStream(new SocketStreambuf(s_outputBufferSize, *this))
#endif
	{}

	Socket::Socket(unsigned short port, SocketType type, SocketFamily family) noexcept(false) :
		m_broken(false),
		m_connected(false),
		m_bound(true),
		m_type(type),
		m_family(family),
		m_address(),
#ifdef _WIN32
		m_socket(initReceiverSocketWin32__(port, type, family)),
		m_inputStream(new SocketStreambuf(s_inputBufferSize, *this)),
		m_outputStream(new SocketStreambuf(s_outputBufferSize, *this))
#endif
	{}

	Socket::Socket(AutoAssignedDatagram, SocketFamily family) noexcept(false) :
		m_broken(false),
		m_connected(false),
		m_bound(false),
		m_type(SocketType::DATAGRAM),
		m_family(family),
		m_address(),
#ifdef _WIN32
		m_socket(initUnboundUDPSocketWin32__(family)),
		m_inputStream(new SocketStreambuf(s_inputBufferSize, *this)),
		m_outputStream(new SocketStreambuf(s_outputBufferSize, *this))
#endif
	{}
			
	Socket::~Socket()
	{
#ifdef _WIN32
		destroyWin32__(m_type, m_socket);
#endif
	}

#ifdef _WIN32
	Socket::Socket(SOCKET socket, SocketType type, SocketFamily family, InetAddress&& address) noexcept :
		m_broken(false),
		m_connected(true),
		m_bound(true),
		m_type(type),
		m_family(family),
		m_address(std::move(address)),
		m_socket(socket),
		m_inputStream(new SocketStreambuf(s_inputBufferSize, *this)),
		m_outputStream(new SocketStreambuf(s_outputBufferSize, *this))
	{}
#endif
			
	bool Socket::hasBeenConnected() const noexcept(false)
	{
		if (m_type == SocketType::DATAGRAM)
			throw SocketException("Cannot check connections with UPD datagram socket!");
#ifdef _WIN32
		return m_socket != INVALID_SOCKET;
#endif
	}

	bool Socket::wasBroken() const noexcept(false)
	{
		if (m_type == SocketType::DATAGRAM)
			throw SocketException("Cannot check connections with UPD datagram socket!");
		return m_broken;
	}

	bool Socket::isConnected() const noexcept(false)
	{
		if (m_type == SocketType::DATAGRAM)
			throw SocketException("Cannot check connections with UPD datagram socket!");
		return m_connected;
	}

	SocketInputStream& Socket::getInputStream() noexcept(false)
	{
		if (m_type == SocketType::DATAGRAM)
			throw SocketException("Cannot use streams with UDP datagram socket!");
		return m_inputStream;
	}

	SocketOutputStream& Socket::getOutputStream() noexcept(false)
	{
		if (m_type == SocketType::DATAGRAM)
			throw SocketException("Cannot use streams with UDP datagram socket!");
		return m_outputStream;
	}

	UniqueDatagram Socket::receiveDatagram() noexcept(false)
	{
		if (m_type == SocketType::STREAM)
			throw SocketException("Cannot send UDP datagrams via TCP stream socket!");
#ifdef _WIN32
		return receiveDatagramWin32__(m_socket, static_cast<int>(m_family == SocketFamily::IPV4 ? DATAGRAM_MAX_DATA_SIZE_IPV4 : DATAGRAM_MAX_DATA_SIZE_IPV6));
#endif
	}

	void Socket::sendDatagram(const Datagram& datagram) noexcept(false)
	{
		if (m_type == SocketType::STREAM)
			throw SocketException("Cannot send UDP datagrams via TCP stream socket!");
		else if (datagram.nativeSize() > (m_family == SocketFamily::IPV4 ? DATAGRAM_MAX_DATA_SIZE_IPV4 : DATAGRAM_MAX_DATA_SIZE_IPV6))
			throw SocketException("Datagram size (" + std::to_string(datagram.nativeSize()) + ") is too large!");
#ifdef _WIN32
		sendDatagramWin32__(datagram, m_socket, m_family);
#endif
		m_bound = true;
	}

	Socket Socket::acceptConnection() noexcept(false)
	{
		if (m_type == SocketType::DATAGRAM)
			throw SocketException("Cannot accept TCP stream connections via UDP datagram socket!");
#ifdef _WIN32
		return acceptConnectionWin32__(m_socket, m_family);
#endif
	}
			
	void Socket::close() noexcept
	{
#ifdef _WIN32
		destroyWin32__(m_type, m_socket);
#endif
		setDisconnected(false);
	}

#ifdef _WIN32
	void Socket::ensureWSAIsInitializedWin32__() noexcept(false)
	{
		static int wsaError = -1;
		static WSAData wsaData;
		if (wsaError != 0)
			if ((wsaError = WSAStartup(MAKEWORD(2, 2), &wsaData)) != 0)
				throw SocketException("Could not init WSA (network system). Error: " + std::to_string(wsaError), wsaError);
	}

	int Socket::getTypeIDWin32__(SocketType type) noexcept
	{
		switch (type)
		{
		case SocketType::STREAM:
			return SOCK_STREAM;
		default:
			return SOCK_DGRAM;
		}

	}

	int Socket::getProtocolIDWin32__(SocketType type) noexcept
	{
		switch (type)
		{
		case SocketType::STREAM:
			return IPPROTO_TCP;
		default:
			return IPPROTO_UDP;
		}

	}

	int Socket::getFamilyIDWin32__(SocketFamily family) noexcept
	{
		switch (family)
		{
		case SocketFamily::IPV6:
			return AF_INET6;
		default:
			return AF_INET;
		}

	}

	SOCKET Socket::initClientTCPSocketWin32__(const InetAddress& address, SocketFamily family) noexcept(false)
	{
		int error = NO_ERROR;
		SOCKET clientSocket = INVALID_SOCKET;
		addrinfo connectionHints;
		addrinfo* connectionAvaliable;

		ZeroMemory(&connectionHints, sizeof(connectionHints));
		connectionHints.ai_family = getFamilyIDWin32__(family);
		connectionHints.ai_socktype = SOCK_STREAM;
		connectionHints.ai_protocol = IPPROTO_TCP;

		ensureWSAIsInitializedWin32__();
		if((error = getaddrinfo(address.address().c_str(), std::to_string(address.port()).c_str(), &connectionHints, &connectionAvaliable)) != NO_ERROR)
			throw SocketException("getaddrinfo() failed! Error: " + std::to_string(error), error);

		for (addrinfo* addr = connectionAvaliable; addr != nullptr; addr = addr->ai_next)
		{
			if ((clientSocket = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol)) == INVALID_SOCKET)
			{
				freeaddrinfo(connectionAvaliable);
				error = WSAGetLastError();
				throw SocketException("Could not create socket! Error: " + std::to_string(error), error);
			}

			if (connect(clientSocket, addr->ai_addr, addr->ai_addrlen) == SOCKET_ERROR)
			{
				closesocket(clientSocket);
				clientSocket = INVALID_SOCKET;
			}
			else
				break;
		}
		freeaddrinfo(connectionAvaliable);

		if (clientSocket == INVALID_SOCKET)
			throw SocketException("Could not connect to " + std::to_string(address));
		else
			return clientSocket;
	}

	SOCKET Socket::initReceiverSocketWin32__(unsigned short port, SocketType type, SocketFamily family) noexcept(false)
	{
		SOCKET recSocket = INVALID_SOCKET;
		sockaddr_in recSocketAddr;

		ZeroMemory(&recSocketAddr, sizeof(recSocketAddr));
		recSocketAddr.sin_family = getFamilyIDWin32__(family);
		recSocketAddr.sin_port = htons(port);
		recSocketAddr.sin_addr.s_addr = htonl(INADDR_ANY);

		ensureWSAIsInitializedWin32__();
		if ((recSocket = socket(recSocketAddr.sin_family, getTypeIDWin32__(type), getProtocolIDWin32__(type))) == INVALID_SOCKET)
		{
			int error = WSAGetLastError();
			throw SocketException("Could not create socket! Error: " + std::to_string(error), error);
		}

		if (bind(recSocket, (sockaddr*)&recSocketAddr, sizeof(recSocketAddr)) == SOCKET_ERROR)
		{
			closesocket(recSocket);
			throw SocketException("Could not bind socket to port: " + std::to_string(port));
		}

		return recSocket;
	}

	SOCKET Socket::initUnboundUDPSocketWin32__(SocketFamily family) noexcept(false)
	{
		SOCKET unboundSocket = INVALID_SOCKET;
				
		ensureWSAIsInitializedWin32__();
		if ((unboundSocket = socket(getFamilyIDWin32__(family), getTypeIDWin32__(SocketType::DATAGRAM), getProtocolIDWin32__(SocketType::DATAGRAM))) == INVALID_SOCKET)
		{
			int error = WSAGetLastError();
			throw SocketException("Could not create socket! Error: " + std::to_string(error), error);
		}

		return unboundSocket;
	}

	void Socket::destroyWin32__(SocketType type, SOCKET& socket) noexcept
	{
		if (socket != INVALID_SOCKET)
		{
			if (type == SocketType::STREAM)
				shutdown(socket, SD_BOTH);
			closesocket(socket);
			socket = INVALID_SOCKET;
		}

	}
			
	UniqueDatagram Socket::receiveDatagramWin32__(SOCKET& socket, int bufferLength) noexcept(false)
	{
		int addrSize = sizeof(sockaddr_in);
		sockaddr_in addr;
		std::unique_ptr<char[]> buffer(new char[bufferLength]);

		int receivedLength = recvfrom(socket, buffer.get(), bufferLength, 0, (sockaddr*)&addr, &addrSize);
		if (receivedLength == 0)
			throw SocketException("Connection closed.");
		else if (receivedLength < 0)
		{
			int error = WSAGetLastError();
			throw SocketException("Connection closed. Error: " + std::to_string(error), error);
		}
		else
			return UniqueDatagram(receivedLength, std::move(buffer), InetAddress(htons(addr.sin_port), inet_ntoa(addr.sin_addr)));
	}

	void Socket::sendDatagramWin32__(const Datagram& datagram, SOCKET& socket, SocketFamily family) noexcept(false)
	{
		sockaddr_in addr;
		addr.sin_family = getFamilyIDWin32__(family);
		addr.sin_port = htons(datagram.address().port());
		addr.sin_addr.s_addr = inet_addr(datagram.address().address().c_str());
				
		if (sendto(socket, datagram.nativePointer(), datagram.nativeSize(), 0, (sockaddr*)&addr, sizeof(addr)) == SOCKET_ERROR)
		{
			int error = WSAGetLastError();
			throw SocketException("Could not send datagram! Error: " + std::to_string(error), error);
		}

	}

	Socket Socket::acceptConnectionWin32__(SOCKET& socket, SocketFamily family) noexcept(false)
	{
		int error;
		int addrSize = sizeof(sockaddr_in);
		sockaddr_in addr;
		SOCKET inSock;

		if (listen(socket, 3) == SOCKET_ERROR)
		{
			error = WSAGetLastError();
			throw SocketException("Socket listen failed! Error: " + std::to_string(error), error);
		}

		inSock = accept(socket, (sockaddr*)&addr, &addrSize);
		if (inSock == INVALID_SOCKET)
		{
			error = WSAGetLastError();
			throw SocketException("Socket accept failed! Error: " + std::to_string(error), error);
		}

		return Socket(inSock, SocketType::STREAM, family, InetAddress(htons(addr.sin_port), inet_ntoa(addr.sin_addr)));
	}
#endif

}