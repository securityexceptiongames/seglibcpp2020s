#include <org/segames/library/debug_util.h>
#include <org/segames/library/io/file_attributes.h>

#ifdef _WIN32
#include <Windows.h>
#include <direct.h>
#endif

#include <fstream>

namespace org::segames::library
{

	void FileAttributes::loadAttribWindows__(const char* path)
	{
#ifdef _WIN32
		DWORD nameLen = GetFullPathNameA(path, 0, nullptr, nullptr);
		std::unique_ptr<char> strBuff(new char[nameLen]);
		GetFullPathNameA(path, nameLen, strBuff.get(), nullptr);
		m_path = std::string(strBuff.get());

		DWORD attrib = GetFileAttributesA(strBuff.get());
		if (attrib == INVALID_FILE_ATTRIBUTES)
		{
			m_exists = false;
			m_directory = false;
			m_length = 0;
		}
		else
		{
			m_exists = true;

			if (attrib & FILE_ATTRIBUTE_DIRECTORY)
			{
				m_directory = true;
				m_length = 0;
			}
			else
			{
				m_directory = false;

				std::ifstream in(strBuff.get(), std::ifstream::ate | std::ifstream::binary);
				signed long long len = in.tellg();
				in.close();

				if (len == -1)
					m_length = 0;
				else
					m_length = (size_t)len;
			}

		}

		m_dissectedPath = StringUtil::split(m_path, SEG_API_SYSTEM_DIR_SEPARATOR);
		if (m_dissectedPath.size() > 0 && m_dissectedPath.get(m_dissectedPath.size() - 1).size() == 0)
			m_dissectedPath.remove(m_dissectedPath.size() - 1);
#endif
	}

}