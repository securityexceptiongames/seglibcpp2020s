#include <org/segames/library/debug_util.h>
#include <org/segames/library/io/json/json_util.h>
#include <org/segames/library/io/json/json_array.h>
#include <org/segames/library/io/json/json_boolean.h>
#include <org/segames/library/io/json/json_number.h>
#include <org/segames/library/io/json/json_object.h>
#include <org/segames/library/io/json/json_string.h>

#include <cctype>

namespace org::segames::library
{

	std::unique_ptr<JSONElement> JSONUtil::read(std::istream& input)
	{
		int c;
		int line = 1, lineChar = 1;
		std::unique_ptr<JSONElement> element;
		while (characterSearchContinue__(c = input.peek()))
		{
			lineChar++;
			if (c == '\n' || c == '\r')
			{
				lineChar = 1;
				line++;
			}
			input.get();
		}

		switch (c)
		{
		case '[':
			input.get();
			element.reset(new JSONArray(input, line, lineChar));
			break; 
		case 'f':
		case 't':
		case 'F':
		case 'T':
			element.reset(new JSONBoolean(input, line, lineChar));
			break;
		case '{':
			input.get();
			element.reset(new JSONObject(input, line, lineChar));
			break;
		case '\"':
			input.get();
			element.reset(new JSONString(input, line, lineChar));
			break;
		default:
			if (!std::isspace(c) && JSONNumber::checkChar(false, c))
				element.reset(new JSONNumber(input, line, lineChar));
			else
				element.reset();
			break;
		}

		return element;
	}

	bool JSONUtil::characterSearchContinue__(const int c)
	{
		if (c == 0xEF || c == 0xBB || c == 0xBF) // Ignore BOM of UTF-8
			return true;
		else
			return std::isspace(c);
	}

}