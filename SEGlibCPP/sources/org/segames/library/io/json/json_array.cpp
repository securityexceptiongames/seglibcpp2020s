#include <org/segames/library/debug_util.h>
#include <org/segames/library/io/json/json_array.h>
#include <org/segames/library/io/json/json_boolean.h>
#include <org/segames/library/io/json/json_number.h>
#include <org/segames/library/io/json/json_object.h>
#include <org/segames/library/io/json/json_string.h>
#include <org/segames/library/io/io_exception.h>

#include <cctype>

namespace org::segames::library
{

	JSONArray::JSONArray(std::istream& input, int& line, int& lineChar)
	{
		bool expectingMore = false;
		int c, state = 0;
		std::unique_ptr<JSONElement> element;
		while ((c = input.peek()) != ']')
		{
			if (c == EOF)
				throw IOException("EOF reached while evaluating json array.");

			lineChar++;
			if (c == '\n' || c == '\r')
			{
				lineChar = 0;
				line++;
			}

			switch (state)
			{
			case 0: // Parse element
				switch (c)
				{
				case '[':
					input.get();
					element.reset(new JSONArray(input, line, lineChar));
					break;
				case 'f':
				case 't':
				case 'F':
				case 'T':
					element.reset(new JSONBoolean(input, line, lineChar));
					break;
				case '{':
					input.get();
					element.reset(new JSONObject(input, line, lineChar));
					break;
				case '\"':
					input.get();
					element.reset(new JSONString(input, line, lineChar));
					break;
				default:
					if (!std::isspace(c) && JSONNumber::checkChar(false, c))
						element.reset(new JSONNumber(input, line, lineChar));
					else
					{
						element.reset();
						input.get();
					}
					break;
				}

				if (element)
				{
					m_elements.add(std::move(element));
					state = 1;
					expectingMore = false;
				}
				break;
			case 1: // Check comma
				if (c == ',')
				{
					state = 0;
					expectingMore = true;
				}
				else if (!std::isspace(c))
					throw IOException("Unexpected token while evaluating json array at line " + std::to_string(line) + ":" + std::to_string(lineChar) + ", expected ',' or end of array.");
				input.get();
				break;
			default:
				break;
			}

		}

		if (expectingMore)
			throw IOException("Unexpected end of array while evaluating json array at line " + std::to_string(line) + ":" + std::to_string(lineChar) + ".");
		input.get(); // Remove ']'

		m_elements.compact();
	}

	JSONArray::JSONArray(const JSONArray& obj) :
		m_elements(obj.m_elements.size())
	{
		for (size_t i = 0; i < obj.m_elements.size(); i++)
			switch (obj.m_elements.get(i)->getType())
			{
			case JSONElementType::OBJECT:
				m_elements.add(std::unique_ptr<JSONObject>(new JSONObject(*dynamic_cast<JSONObject*>(obj.m_elements.get(i).get()))));
				break;
			case JSONElementType::ARRAY:
				m_elements.add(std::unique_ptr<JSONArray>(new JSONArray(*dynamic_cast<JSONArray*>(obj.m_elements.get(i).get()))));
				break;
			case JSONElementType::STRING:
				m_elements.add(std::unique_ptr<JSONString>(new JSONString(*dynamic_cast<JSONString*>(obj.m_elements.get(i).get()))));
				break;
			case JSONElementType::NUMBER:
				m_elements.add(std::unique_ptr<JSONNumber>(new JSONNumber(*dynamic_cast<JSONNumber*>(obj.m_elements.get(i).get()))));
				break;
			case JSONElementType::BOOLEAN:
				m_elements.add(std::unique_ptr<JSONBoolean>(new JSONBoolean(*dynamic_cast<JSONBoolean*>(obj.m_elements.get(i).get()))));
				break;
			default:
				break;
			}
	}

	template<>
	JSONArray* JSONArray::get(const size_t index) const
	{
		JSONElement* element = get(index);
		if (element)
			return (element->getType() == JSONElementType::ARRAY ? dynamic_cast<JSONArray*>(element) : nullptr);
		else
			return nullptr;
	}

	template<>
	JSONBoolean* JSONArray::get(const size_t index) const
	{
		JSONElement* element = get(index);
		if (element)
			return (element->getType() == JSONElementType::BOOLEAN ? dynamic_cast<JSONBoolean*>(element) : nullptr);
		else
			return nullptr;
	}

	template<>
	JSONNumber* JSONArray::get(const size_t index) const
	{
		JSONElement* element = get(index);
		if (element)
			return (element->getType() == JSONElementType::NUMBER ? dynamic_cast<JSONNumber*>(element) : nullptr);
		else
			return nullptr;
	}

	class JSONObject;
	template<>
	JSONObject* JSONArray::get(const size_t index) const
	{
		JSONElement* element = get(index);
		if (element)
			return (element->getType() == JSONElementType::OBJECT ? dynamic_cast<JSONObject*>(element) : nullptr);
		else
			return nullptr;
	}

	template<>
	JSONString* JSONArray::get(const size_t index) const
	{
		JSONElement* element = get(index);
		if (element)
			return (element->getType() == JSONElementType::STRING ? dynamic_cast<JSONString*>(element) : nullptr);
		else
			return nullptr;
	}

	void JSONArray::write(const int tabs, std::ostream& output) const
	{
		size_t written = 0;
		output << "[\n";
		for (size_t i = 0; i < m_elements.size(); i++)
		{
			if (m_elements.get(i))
			{
				for (int t = 0; t < tabs + 1; t++)
					output << '\t';

				m_elements.get(i)->write(tabs + 1, output);
						
				if (written < m_elements.size() - 1)
					output << ',';
				output << '\n';
			}
			written++;
		}
		for (int t = 0; t < tabs; t++)
			output << '\t';
		output << ']';
	}

	bool JSONArray::operator==(const JSONArray& obj) const
	{
		const JSONArray& ref = dynamic_cast<const JSONArray&>(obj);

		if (ref.m_elements.size() != m_elements.size())
			return false;

		for (size_t i = 0; i < m_elements.size(); i++)
		{
			JSONElement* element = get(i);
			JSONElement* refElement = ref.get(i);
			if ((element == nullptr) != (refElement == nullptr))
				return false;

			if (element != nullptr)
			{
				if (element->getType() != refElement->getType())
					return false;
				else
				{
					switch (element->getType())
					{
					case JSONElementType::ARRAY:
						if (dynamic_cast<const JSONArray&>(*element) != dynamic_cast<const JSONArray&>(*refElement))
							return false;
						break;
					case JSONElementType::BOOLEAN:
						if (dynamic_cast<const JSONBoolean&>(*element) != dynamic_cast<const JSONBoolean&>(*refElement))
							return false;
						break;
					case JSONElementType::NUMBER:
						if (dynamic_cast<const JSONNumber&>(*element) != dynamic_cast<const JSONNumber&>(*refElement))
							return false;
						break;
					case JSONElementType::OBJECT:
						if (dynamic_cast<const JSONObject&>(*element) != dynamic_cast<const JSONObject&>(*refElement))
							return false;
						break;
					case JSONElementType::STRING:
						if (dynamic_cast<const JSONString&>(*element) != dynamic_cast<const JSONString&>(*refElement))
							return false;
						break;
					default:
						break;
					}

				}

			}
		}

		return true;

	}

}