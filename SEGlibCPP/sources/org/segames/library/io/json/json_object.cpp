#include <org/segames/library/debug_util.h>
#include <org/segames/library/io/json/json_object.h>
#include <org/segames/library/io/json/json_array.h>
#include <org/segames/library/io/json/json_boolean.h>
#include <org/segames/library/io/json/json_number.h>
#include <org/segames/library/io/json/json_string.h>
#include <org/segames/library/io/io_exception.h>

#include <cctype>
#include <sstream>

namespace org::segames::library
{
			
	JSONObject::JSONObject(std::istream& input, int& line, int& lineChar)
	{
		bool expectingMore = false;
		int c, state = 0;
		std::string name;
		std::unique_ptr<JSONElement> element;
		while ((c = input.peek()) != '}')
		{
			if(c == EOF)
				throw IOException("EOF reached while evaluating json object.");

			lineChar++;
			if (c == '\n' || c == '\r')
			{
				lineChar = 0;
				line++;
			}

			switch (state)
			{
			case 0: // Parse element name
				input.get();
				if (c == '\"')
				{
					name = JSONString::readJSONString(input, line, lineChar);
					state = 1;
					expectingMore = true;
				}
				else if (!std::isspace(c))
					throw IOException("Unexpected token while evaluating json object at line " + std::to_string(line) + ":" + std::to_string(lineChar) + ", expected '\"'.");
				break;
			case 1: // Check colon
				if (c == ':')
					state = 2;
				else if(!std::isspace(c))
					throw IOException("Unexpected token while evaluating json object at line " + std::to_string(line) + ":" + std::to_string(lineChar) + ", expected ':'.");
				input.get();
				break;
			case 2: // Parse element
				switch (c)
				{
				case '[':
					input.get();
					element.reset(new JSONArray(input, line, lineChar));
					break;
				case 'f':
				case 't':
				case 'F':
				case 'T':
					element.reset(new JSONBoolean(input, line, lineChar));
					break;
				case '{':
					input.get();
					element.reset(new JSONObject(input, line, lineChar));
					break;
				case '\"':
					input.get();
					element.reset(new JSONString(input, line, lineChar));
					break;
				default:
					if (!std::isspace(c) && JSONNumber::checkChar(false, c))
						element.reset(new JSONNumber(input, line, lineChar));
					else
					{
						element.reset();
						input.get();
					}
					break;
				}

				if (element)
				{
					m_elements.insert_or_assign(name, std::move(element));
					state = 3;
					expectingMore = false;
				}
				break;
			case 3: // Check comma
				if (c == ',')
				{
					state = 0;
					expectingMore = true;
				}
				else if(!std::isspace(c))
					throw IOException("Unexpected token while evaluating json object at line " + std::to_string(line) + ":" + std::to_string(lineChar) + ", expected ',' or end of object.");
				input.get();
				break;
			default:
				break;
			}

		}

		if (expectingMore)
			throw IOException("Unexpected end of object while evaluating json object at line " + std::to_string(line) + ":" + std::to_string(lineChar) + ".");
		input.get(); // Remove '}'
	}

	JSONObject::JSONObject(const JSONObject& obj)
	{
		m_elements.reserve(obj.m_elements.size());
		for (auto itr = obj.m_elements.begin(); itr != obj.m_elements.end(); itr++)
			switch (itr->second->getType())
			{
			case JSONElementType::OBJECT:
				m_elements.insert_or_assign(itr->first, std::unique_ptr<JSONObject>(new JSONObject(*dynamic_cast<JSONObject*>(itr->second.get()))));
				break;
			case JSONElementType::ARRAY:
				m_elements.insert_or_assign(itr->first, std::unique_ptr<JSONArray>(new JSONArray(*dynamic_cast<JSONArray*>(itr->second.get()))));
				break;
			case JSONElementType::STRING:
				m_elements.insert_or_assign(itr->first, std::unique_ptr<JSONString>(new JSONString(*dynamic_cast<JSONString*>(itr->second.get()))));
				break;
			case JSONElementType::NUMBER:
				m_elements.insert_or_assign(itr->first, std::unique_ptr<JSONNumber>(new JSONNumber(*dynamic_cast<JSONNumber*>(itr->second.get()))));
				break;
			case JSONElementType::BOOLEAN:
				m_elements.insert_or_assign(itr->first, std::unique_ptr<JSONBoolean>(new JSONBoolean(*dynamic_cast<JSONBoolean*>(itr->second.get()))));
				break;
			default:
				break;
			}
	}
			
	class JSONArray;
	template<>
	JSONArray* JSONObject::get(const std::string& name) const
	{
		JSONElement* element = get(name);
		if (element)
			return (element->getType() == JSONElementType::ARRAY ? dynamic_cast<JSONArray*>(element) : nullptr);
		else
			return nullptr;
	}

	template<>
	JSONBoolean* JSONObject::get(const std::string& name) const
	{
		JSONElement* element = get(name);
		if (element)
			return (element->getType() == JSONElementType::BOOLEAN ? dynamic_cast<JSONBoolean*>(element) : nullptr);
		else
			return nullptr;
	}

	template<>
	JSONNumber* JSONObject::get(const std::string& name) const
	{
		JSONElement* element = get(name);
		if (element)
			return (element->getType() == JSONElementType::NUMBER ? dynamic_cast<JSONNumber*>(element) : nullptr);
		else
			return nullptr;
	}

	template<>
	JSONObject* JSONObject::get(const std::string& name) const
	{
		JSONElement* element = get(name);
		if (element)
			return (element->getType() == JSONElementType::OBJECT ? dynamic_cast<JSONObject*>(element) : nullptr);
		else
			return nullptr;
	}

	template<>
	JSONString* JSONObject::get(const std::string& name) const
	{
		JSONElement* element = get(name);
		if (element)
			return (element->getType() == JSONElementType::STRING ? dynamic_cast<JSONString*>(element) : nullptr);
		else
			return nullptr;
	}

	void JSONObject::write(const int tabs, std::ostream& output) const
	{
		int written = 0;
		output << "{\n";
		for (auto itr = m_elements.begin(); itr != m_elements.end(); itr++)
		{
			if (itr->second)
			{
				for (int t = 0; t < tabs + 1; t++)
					output << '\t';

				output << '\"' << itr->first << "\": ";
				itr->second->write(tabs + 1, output);

				if (written < static_cast<int>(m_elements.size()) - 1)
					output << ',';
				output << '\n';
			}
			written++;
		}
		for (int t = 0; t < tabs; t++)
			output << '\t';
		output << '}';
	}

	bool JSONObject::operator==(const JSONObject& obj) const
	{
		const JSONObject& ref = dynamic_cast<const JSONObject&>(obj);
					
		if (ref.m_elements.size() != m_elements.size())
			return false;

		for (auto itr = m_elements.begin(); itr != m_elements.end(); itr++)
		{
			JSONElement* refElement = ref.get(itr->first);
			if ((itr->second == nullptr) != (refElement == nullptr))
				return false;

			if (itr->second != nullptr)
			{
				if (itr->second->getType() != refElement->getType())
					return false;
				else
				{
					switch (itr->second->getType())
					{
					case JSONElementType::ARRAY:
						if (dynamic_cast<const JSONArray&>(*itr->second) != dynamic_cast<const JSONArray&>(*refElement))
							return false;
						break;
					case JSONElementType::BOOLEAN:
						if (dynamic_cast<const JSONBoolean&>(*itr->second) != dynamic_cast<const JSONBoolean&>(*refElement))
							return false;
						break;
					case JSONElementType::NUMBER:
						if (dynamic_cast<const JSONNumber&>(*itr->second) != dynamic_cast<const JSONNumber&>(*refElement))
							return false;
						break;
					case JSONElementType::OBJECT:
						if (dynamic_cast<const JSONObject&>(*itr->second) != dynamic_cast<const JSONObject&>(*refElement))
							return false;
						break;
					case JSONElementType::STRING:
						if (dynamic_cast<const JSONString&>(*itr->second) != dynamic_cast<const JSONString&>(*refElement))
							return false;
						break;
					default:
						break;
					}

				}

			}

		}

		return true;
	}

}