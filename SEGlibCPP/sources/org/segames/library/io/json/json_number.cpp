#include <org/segames/library/debug_util.h>
#include <org/segames/library/io/json/json_number.h>
#include <org/segames/library/io/io_exception.h>

#include <cctype>

namespace org::segames::library
{

	JSONNumber::JSONNumber(std::istream& input, int& line, int& lineChar)
	{
		int c;
		bool isFloat = false, ended = false, hex = false;
		std::string num = "";
		while ((c = input.peek()) != ',' && c != '}' && c != ']' && c != EOF)
		{
			bool space;
			lineChar++;
			if (!checkChar(hex, c))
				throw IOException("Unexpected token while evaluating json number at line " + std::to_string(line) + ":" + std::to_string(lineChar) + ".");
					
			space = std::isspace(c);
			if (space)
				ended = true;
			else if(ended)
				throw IOException("Unexpected token while evaluating json number at line " + std::to_string(line) + ":" + std::to_string(lineChar) + ".");

			switch (c)
			{
			case '\n':
			case '\r':
				lineChar = 0;
				line++;
				break;
			case 'e':
			case 'E':
			case '.':
				isFloat = true;
				break;
			case 'x':
			case 'X':
				hex = true;
				break;
			default:
				break;
			}

			if (!space)
				num += static_cast<unsigned char>(input.get());
			else
				input.get();
		}

		if (isFloat)
		{
			m_float = std::stod(num);
			m_int = static_cast<long long>(m_float);
		}
		else
		{
			if(hex)
				m_int = std::stoll(num, 0, 16);
			else
				m_int = std::stoll(num);
			m_float = static_cast<double>(m_int);
		}

	}

	bool JSONNumber::checkChar(const bool hex, const int c)
	{
		switch (c)
		{
		case '+':
		case '-':
		case 'x':
		case 'X':
		case 'e':
		case 'E':
		case '.':
			return true;
		case 'a':
		case 'A':
		case 'b':
		case 'B':
		case 'c':
		case 'C':
		case 'd':
		case 'D':
		case 'f':
		case 'F':
			return hex;
		default:
			if (c >= '0' && c <= '9')
				return true;
			else if (std::isspace(c))
				return true;
			return false;
		}

	}

}