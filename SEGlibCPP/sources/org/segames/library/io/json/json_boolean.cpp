#include <org/segames/library/debug_util.h>
#include <org/segames/library/io/json/json_boolean.h>
#include <org/segames/library/io/io_exception.h>

#include <cctype>

namespace org::segames::library
{

	JSONBoolean::JSONBoolean(std::istream& input, int& line, int& lineChar)
	{
		int c = 0;
		bool ended = false;
		std::string str = "";
		while ((c = input.peek()) != ',' && c != '}' && c != ']' && c != EOF)
		{
			bool space;
			lineChar++;
			if (!checkChar(c))
				throw IOException("Unexpected token while evaluating json boolean at line " + std::to_string(line) + ":" + std::to_string(lineChar) + ".");

			space = std::isspace(c);
			if (space)
				ended = true;
			else if (ended)
				throw IOException("Unexpected token while evaluating json boolean at line " + std::to_string(line) + ":" + std::to_string(lineChar) + ".");

			switch (c)
			{
			case '\n':
			case '\r':
				lineChar = 0;
				line++;
				break;
			default:
				break;
			}

			if (!space)
				str += static_cast<unsigned char>(std::tolower(input.get()));
			else
				input.get();
		}
				
		if (str == "true")
			m_flag = true;
		else
			m_flag = false;
	}

	bool JSONBoolean::checkChar(const int c)
	{
		switch (c)
		{
		case 't':
		case 'T':
		case 'r':
		case 'R':
		case 'u':
		case 'U':
		case 'e':
		case 'E':
		case 'f':
		case 'F':
		case 'a':
		case 'A':
		case 'l':
		case 'L':
		case 's':
		case 'S':
			return true;
		default:
			if (std::isspace(c))
				return true;
			return false;
		}

	}

}