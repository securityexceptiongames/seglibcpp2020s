#include <org/segames/library/debug_util.h>
#include <org/segames/library/io/image/image.h>
#include <org/segames/library/io/file_not_found_exception.h>
#include <org/segames/library/io/file.h>
#include <org/segames/library/math/math.h>

#include <stb/stb_image.h>

#include <string>

#include <iostream>

namespace org::segames::library
{

	Image::Image(const std::string_view& path) noexcept(false) :
		Image()
	{
		File file(path);
		if (!file.exists())
			throw FileNotFoundException(path);

		int x, y, c;
		stbi_uc* data = stbi_load(path.data(), &x, &y, &c, 0);
		if (!data)
			throw IOException("Could not read image from " + std::string(path));

		m_width = x;
		m_height = y;
		m_array.reset(new Color[size()]);

		if (c == 4)
			memcpy(nativePointer(), data, nativeSize());
		else
		{
			const size_t cUnsigned = static_cast<size_t>(c);
			unsigned char* nativePtr = reinterpret_cast<unsigned char*>(nativePointer());
			unsigned char* dataPtr = reinterpret_cast<unsigned char*>(data);
			for (size_t i = 0; i < size(); i++)
			{
				for (size_t j = 0; j < cUnsigned; j++)
					nativePtr[j] = dataPtr[j];
				for (size_t j = cUnsigned; j < 4; j++)
					nativePtr[j] = (j == 3)*255;
						
				nativePtr += 4;
				dataPtr += cUnsigned;
			}

		}
				
		stbi_image_free(data);
	}

	Image Image::subImage(signed long long x, signed long long y, size_t w, size_t h)
	{
		Image sub(w, h);

		const signed long long startX = Math::max(x, static_cast<signed long long>(0));
		const signed long long startY = Math::max(y, static_cast<signed long long>(0));
		const signed long long endX = Math::min(x + static_cast<signed long long>(w), static_cast<signed long long>(width()));
		const signed long long endY = Math::min(y + static_cast<signed long long>(h), static_cast<signed long long>(height()));

		for (signed long long i = startX; i < endX; i++)
			for (signed long long j = startY; j < endY; j++)
				sub[i - x][j - y] = (*this)[i][j];

		for (signed long long i = x; i < startX; i++)
		{
			for (signed long long j = y; j < startY; j++)
				sub[i - x][j - y] = 0x00000000;
			for (signed long long j = endY; j < y + static_cast<signed long long>(h); j++)
				sub[i - x][j - y] = 0x00000000;
		}

		for (signed long long i = endX; i < x + static_cast<signed long long>(h); i++)
		{
			for (signed long long j = y; j < startY; j++)
				sub[i - x][j - y] = 0x00000000;
			for (signed long long j = endY; j < y + static_cast<signed long long>(h); j++)
				sub[i - x][j - y] = 0x00000000;
		}

		return sub;
	}

	Image Image::subImageStrict(signed long long x, signed long long y, size_t w, size_t h)
	{
		if (x < 0)
			throw InvalidValueException(x, bool());
		else if (y < 0)
			throw InvalidValueException(y, bool());

		const size_t endX = static_cast<size_t>(x) + w;
		const size_t endY = static_cast<size_t>(y) + h;

		if (endX > width())
			throw InvalidValueException(w, bool());
		else if (endY > height())
			throw InvalidValueException(h, bool());

		Image sub(w, h);
		for (size_t i = x; i < endX; i++)
			for (size_t j = x; j < endY; j++)
				sub[i - x][j - y] = (*this)[i][j];
		return sub;
	}

	void Image::drawOverwrite__(signed long long x, signed long long y, const Image& img)
	{
		const signed long long startX = Math::max(x, static_cast<signed long long>(0));
		const signed long long startY = Math::max(y, static_cast<signed long long>(0));
		const signed long long endX = Math::min(x + static_cast<signed long long>(img.width()), static_cast<signed long long>(width()));
		const signed long long endY = Math::min(y + static_cast<signed long long>(img.height()), static_cast<signed long long>(height()));

		for (signed long long i = startX; i < endX; i++)
			for (signed long long j = startY; j < endY; j++)
				(*this)[i][j] = img[i - x][j - y];
	}

	void Image::drawSrcAlphaOneMinusSrcAlpha__(signed long long x, signed long long y, const Image& img)
	{
		const signed long long startX = Math::max(x, static_cast<signed long long>(0));
		const signed long long startY = Math::max(y, static_cast<signed long long>(0));
		const signed long long endX = Math::min(x + static_cast<signed long long>(img.width()), static_cast<signed long long>(width()));
		const signed long long endY = Math::min(y + static_cast<signed long long>(img.height()), static_cast<signed long long>(height()));

		for (signed long long i = startX; i < endX; i++)
			for (signed long long j = startY; j < endY; j++)
			{
				const int base = (*this)[i][j].asInteger();
				const int color = img[i - x][j - y].asInteger();
						
				const int a = (color >> 24) & 0xff;
				const int s = 0xff - a;

				const int rd = ((base & 0xff) * s + (color & 0xff) * a) / 0xff;
				const int gd = (((base >> 8) & 0xff) * s + ((color >> 8) & 0xff) * a) / 0xff;
				const int bd = (((base >> 16) & 0xff) * s + ((color >> 16) & 0xff) * a) / 0xff;
				const int ad = (((base >> 24) & 0xff) * s + a * a) / 0xff;

				(*this)[i][j] = Color((ad << 24) | (bd << 16) | (gd << 8) | rd);
			}

	}

}