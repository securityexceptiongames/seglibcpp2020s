#include <org/segames/library/debug_util.h>
#include <org/segames/library/gl/texture/gl_texture_atlaser.h>
#include <org/segames/library/gl/fbo/gl_framebuffer_wrapper.h>

#include <stb/stb_rect_pack.h>

namespace org::segames::library
{

	void GLTextureAtlaser::createShaders__(std::unordered_map<RescaleFilter, GLShader>& shaders)
	{
		shaders.insert_or_assign(RescaleFilter::NEAREST_NEIGHBOR, GLShader(
			"GLTextureAtlaser::NEAREST_NEIGHBOR",
			"#version 120\nvoid main() {\ngl_TexCoord[0] = gl_MultiTexCoord0;\ngl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;\n}",
			"",
			"#version 120\nuniform sampler2D tex;\nuniform vec2 texSize;\nvoid main() {\ngl_FragColor = texture2D(tex, gl_TexCoord[0].xy);\n}"
		)); 
		shaders.insert_or_assign(RescaleFilter::BILINEAR, GLShader(
			"GLTextureAtlaser::BILINEAR", 
			"#version 120\nuniform vec2 texSize;\nvarying vec2 sULCoord;\nvarying vec2 sLLCoord;\nvarying vec2 sURCoord;\nvarying vec2 sLRCoord;\nvoid main() {\nvec2 texelSize = vec2(1.0 / texSize.x, 1.0 / texSize.y);\nsULCoord = gl_MultiTexCoord0.xy - 0.498 * texelSize;\nsLLCoord = sULCoord + vec2(0.0, texelSize.y);\nsURCoord = sULCoord + vec2(texelSize.x, 0.0);\nsLRCoord = sULCoord + texelSize;\ngl_TexCoord[0] = gl_MultiTexCoord0;\ngl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;\n}", 
			"", 
			"#version 120\nuniform sampler2D tex;\nuniform vec2 texSize;\nvarying vec2 sULCoord;\nvarying vec2 sLLCoord;\nvarying vec2 sURCoord;\nvarying vec2 sLRCoord;\nvoid main() {\nvec4 sUL = texture2D(tex, sULCoord);\nvec4 sLL = texture2D(tex, sLLCoord);\nvec4 sUR = texture2D(tex, sURCoord);\nvec4 sLR = texture2D(tex, sLRCoord);\nfloat fHorz = fract(sULCoord.x * texSize.x + 1.0);\nfloat fVert = fract(sULCoord.y * texSize.y + 1.0);\ngl_FragColor = mix(mix(sUL, sLL, fVert), mix(sUR, sLR, fVert), fHorz);\n}"
		));
		shaders.insert_or_assign(RescaleFilter::LANCZOS, GLShader(
			"GLTextureAtlaser::LANCZOS",
			"#version 120\n// Credits to Brad Larson for base code\nuniform vec2 texSize;\nvarying vec2 centerTextureCoordinate;\nvarying vec2 oneStepLeftTextureCoordinate;\nvarying vec2 twoStepsLeftTextureCoordinate;\nvarying vec2 threeStepsLeftTextureCoordinate;\nvarying vec2 fourStepsLeftTextureCoordinate;\nvarying vec2 oneStepRightTextureCoordinate;\nvarying vec2 twoStepsRightTextureCoordinate;\nvarying vec2 threeStepsRightTextureCoordinate;\nvarying vec2 fourStepsRightTextureCoordinate;\nvoid main()\n{\nvec2 firstOffset = vec2(0.3333 / texSize.x, 0.3333 / texSize.y);\nvec2 secondOffset = vec2(2.0 * firstOffset.x, 2.0 * firstOffset.y);\nvec2 thirdOffset = vec2(3.0 * firstOffset.x, 3.0 * firstOffset.y);\nvec2 fourthOffset = vec2(4.0 * firstOffset.x, 4.0 * firstOffset.y);\ncenterTextureCoordinate = gl_MultiTexCoord0.xy;\noneStepLeftTextureCoordinate = gl_MultiTexCoord0.xy - firstOffset;\ntwoStepsLeftTextureCoordinate = gl_MultiTexCoord0.xy - secondOffset;\nthreeStepsLeftTextureCoordinate = gl_MultiTexCoord0.xy - thirdOffset;\nfourStepsLeftTextureCoordinate = gl_MultiTexCoord0.xy - fourthOffset;\noneStepRightTextureCoordinate = gl_MultiTexCoord0.xy + firstOffset;\ntwoStepsRightTextureCoordinate = gl_MultiTexCoord0.xy + secondOffset;\nthreeStepsRightTextureCoordinate = gl_MultiTexCoord0.xy + thirdOffset;\nfourStepsRightTextureCoordinate = gl_MultiTexCoord0.xy + fourthOffset;\ngl_TexCoord[0] = gl_MultiTexCoord0;\ngl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;\n}",
			"",
			"#version 120\nuniform sampler2D tex;\nuniform vec2 texSize;\nvarying vec2 centerTextureCoordinate;\nvarying vec2 oneStepLeftTextureCoordinate;\nvarying vec2 twoStepsLeftTextureCoordinate;\nvarying vec2 threeStepsLeftTextureCoordinate;\nvarying vec2 fourStepsLeftTextureCoordinate;\nvarying vec2 oneStepRightTextureCoordinate;\nvarying vec2 twoStepsRightTextureCoordinate;\nvarying vec2 threeStepsRightTextureCoordinate;\nvarying vec2 fourStepsRightTextureCoordinate;\nvoid main()\n{\nvec4 fragmentColor = texture2D(tex, centerTextureCoordinate) * 0.38026;\nfragmentColor += texture2D(tex, oneStepLeftTextureCoordinate) * 0.27667;\nfragmentColor += texture2D(tex, oneStepRightTextureCoordinate) * 0.27667;\nfragmentColor += texture2D(tex, twoStepsLeftTextureCoordinate) * 0.08074;\nfragmentColor += texture2D(tex, twoStepsRightTextureCoordinate) * 0.08074;\nfragmentColor += texture2D(tex, threeStepsLeftTextureCoordinate) * -0.02612;\nfragmentColor += texture2D(tex, threeStepsRightTextureCoordinate) * -0.02612;\nfragmentColor += texture2D(tex, fourStepsLeftTextureCoordinate) * -0.02143;\nfragmentColor += texture2D(tex, fourStepsRightTextureCoordinate) * -0.02143;\ngl_FragColor = fragmentColor;\n}"
		));
	}

	ArrayList<GLTextureAtlaser::AtlasTile> GLTextureAtlaser::tileAtlas__(const size_t border, const Dimension2i& size, const ArrayList<GLTextureRectangle>& sections)
	{
		ArrayList<GLTextureAtlaser::AtlasTile> tiling(sections.size());
		stbrp_context context;
		std::unique_ptr<stbrp_node[]> nodes(new stbrp_node[size.width()]);
		std::unique_ptr<stbrp_rect[]> rects(new stbrp_rect[sections.size()]);

		stbrp_init_target(&context, size.width(), size.height(), nodes.get(), size.width());

		for (size_t i = 0; i < sections.size(); i++)
		{
			stbrp_rect& rect = rects[i];
			const GLTextureRectangle& sec = sections.get(i);

			float left = Math::min(sec.uv(0)[0], sec.uv(1)[0], sec.uv(2)[0], sec.uv(3)[0]);
			float right = Math::max(sec.uv(0)[0], sec.uv(1)[0], sec.uv(2)[0], sec.uv(3)[0]);
			float top = Math::min(sec.uv(0)[1], sec.uv(1)[1], sec.uv(2)[1], sec.uv(3)[1]);
			float bottom = Math::max(sec.uv(0)[1], sec.uv(1)[1], sec.uv(2)[1], sec.uv(3)[1]);

			rect.id = i;
			rect.x = 0;
			rect.y = 0;
			rect.w = static_cast<stbrp_coord>(sec.texture().getSize().width() * (right - left) + border);
			rect.h = static_cast<stbrp_coord>(sec.texture().getSize().height() * (bottom - top) + border);
			rect.was_packed = false;
		}

		stbrp_pack_rects(&context, rects.get(), sections.size());

		for (size_t i = 0; i < sections.size(); i++)
		{
			stbrp_rect& rect = rects[i];
			if (rect.was_packed)
			{
				const GLTextureRectangle& sec = sections.get(rect.id);
				tiling.add({
					Rectanglei(rect.x, rect.y, rect.w - border, rect.h - border),
					sec
				});
			}

		}

		return tiling;
	}

	void GLTextureAtlaser::renderTile__(const GLint texSizeUniform, const AtlasTile& tile)
	{
		const Rectanglei& rect = tile.tile;
		const GLTextureRectangle& sec = tile.section;
		float data[16] = {
			sec.uv(0)[0], sec.uv(0)[1], 
			static_cast<float>(rect.position().x()), static_cast<float>(rect.position().y()),
			sec.uv(1)[0], sec.uv(1)[1], 
			static_cast<float>(rect.position().x() + rect.dimensions().width()), static_cast<float>(rect.position().y()),
			sec.uv(2)[0], sec.uv(2)[1], 
			static_cast<float>(rect.position().x() + rect.dimensions().width()), static_cast<float>(rect.position().y() + rect.dimensions().height()),
			sec.uv(3)[0], sec.uv(3)[1], 
			static_cast<float>(rect.position().x()), static_cast<float>(rect.position().y() + rect.dimensions().height()),
		};

		m_buffer.upload(16 * sizeof(float), data, GL_DYNAMIC_DRAW);

		glUniform2f(texSizeUniform, rect.dimensions().width(), rect.dimensions().height());

		sec.texture().bind();
		m_buffer.bind().setPointerInf();
		glDrawArrays(GL_QUADS, 0, 4);
		m_buffer.release();
		sec.texture().release();
	}

	GLTexture GLTextureAtlaser::renderAtlas__(const size_t levels, const Dimension2i& size, const ArrayList<AtlasTile>& inputTiles, RescaleFilter filter)
	{
		GLTexture atlas(Dimension3i(size.width(), size.height(), 0));
		GLFramebufferWrapper fbo(true, false, size);
		const GLShader& shader = (filter == RescaleFilter::OPENGL_LINEAR ? getShaderForFilter__(RescaleFilter::NEAREST_NEIGHBOR) : getShaderForFilter__(filter));
				
		atlas.upload(GL_UNSIGNED_BYTE, nullptr);

		for (const AtlasTile& tile : inputTiles)
			tile.section.texture().bind().setParameters(GL_CLAMP, filter == RescaleFilter::OPENGL_LINEAR ? GL_LINEAR : GL_NEAREST, GL_NEAREST).release();

		glClearColor(0, 0, 0, 0);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, size.width(), 0, size.height(), -1, 1);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		shader.bind();
		glUniform1i(glGetUniformLocation(shader.getID(), "tex"), 0);
		const GLint texSizeUniform = glGetUniformLocation(shader.getID(), "texSize");
		for (size_t i = 0; i < levels; i++)
		{
			fbo.build();
			fbo.bind();
			glViewport(0, 0, fbo.getSize().width(), fbo.getSize().height());
			glClear(GL_COLOR_BUFFER_BIT);

			for (const AtlasTile& tile : inputTiles)
				renderTile__(texSizeUniform, tile);

			atlas.bind();
			glCopyTexImage2D(atlas.getType(), i, atlas.getInternalFormat(), 0, 0, fbo.getSize().width(), fbo.getSize().height(), 0);
			atlas.release();

			fbo.setSize(Dimension2i(
				Math::max(fbo.getSize().width() / 2, 1),
				Math::max(fbo.getSize().height() / 2, 1)
			));
		}
		fbo.release();
		shader.release();

		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);

		atlas.bind();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, levels-1);
		if (levels > 1)
			atlas.setParameters(GL_REPEAT, GL_NEAREST_MIPMAP_NEAREST, GL_NEAREST);
		else
			atlas.setParameters(GL_REPEAT, GL_NEAREST, GL_NEAREST);
		atlas.release();

		return atlas;
	}

	void GLTextureAtlaser::computeAtlasSections__(const bool halfPixelPadding, const GLTexture& atlas, const ArrayList<AtlasTile>& inputTiles, ArrayList<GLTextureRectangle>& outputSections)
	{
		const float width = static_cast<float>(atlas.getSize().width());
		const float height = static_cast<float>(atlas.getSize().height());
		const float hppXOff = halfPixelPadding ? 0.5f / width : 0.0f;
		const float hppYOff = halfPixelPadding ? 0.5f / height : 0.0f;
		const float hppWOff = 2.0f * hppXOff;
		const float hppHOff = 2.0f * hppYOff;
		for (const AtlasTile& tile : inputTiles)
			outputSections.add(GLTextureRectangle(atlas,
				static_cast<float>(tile.tile.position().x()) / width + hppXOff,
				static_cast<float>(tile.tile.position().y()) / height + hppYOff,
				static_cast<float>(tile.tile.dimensions().width()) / width - hppWOff,
				static_cast<float>(tile.tile.dimensions().height()) / height - hppHOff
			));
	}

}