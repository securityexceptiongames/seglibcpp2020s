#include <org/segames/library/debug_util.h>
#include <org/segames/library/gl/texture/gl_disc_texture.h>
#include <org/segames/library/io/file_not_found_exception.h>
#include <org/segames/library/io/file.h>

#include <stb/stb_image.h>

namespace org::segames::library
{

	GLDiscTexture::~GLDiscTexture()
	{
		if (m_data)
			stbi_image_free(m_data);
	}

	void GLDiscTexture::upload()
	{
		GLTexture::upload(GL_UNSIGNED_BYTE, m_data);
		if (m_data)
		{
			stbi_image_free(m_data);
			m_data = nullptr;
		}

	}

	void GLDiscTexture::importTexture(const std::string_view& path, const int forceChannel)
	{
		File file(path);
		if (!file.exists())
			throw FileNotFoundException(path);

		int x, y, c;
		stbi_uc* data = stbi_load(path.data(), &x, &y, &c, forceChannel);
		if (data)
		{
			if(m_data)
				stbi_image_free(m_data);
			m_data = data;
			m_size = Dimension3i(x, y, 1);
						
			const int channels = forceChannel ? forceChannel : c;
			switch (channels)
			{
			case 1:
				m_internalFormat = GL_R8;
				break;
			case 2:
				m_internalFormat = GL_RG8;
				break;
			case 3:
				m_internalFormat = GL_RGB8;
				break;
			default:
				m_internalFormat = GL_RGBA8;
				break;
			}

			m_format = GLTexture::formatFromInternalFormat(m_internalFormat);
			m_type = GL_TEXTURE_2D;
		}

	}

}