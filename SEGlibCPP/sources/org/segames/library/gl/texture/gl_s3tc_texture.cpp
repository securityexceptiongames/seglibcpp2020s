#include <org/segames/library/debug_util.h>
#include <org/segames/library/gl/texture/gl_s3tc_texture.h>
#include <org/segames/library/io/file_not_found_exception.h>
#include <org/segames/library/io/file.h>
#include <org/segames/library/math/math.h>
#include <org/segames/library/system.h>

#include <fstream>

namespace org::segames::library
{

	void GLS3TCTexture::upload()
	{
		bool newlyGen = false;
		if (!m_id)
		{
			glGenTextures(1, &m_id);
			if (!glIsEnabled(m_type))
				glEnable(m_type);

			bind();
			newlyGen = true;
		}
		else
			bind();

		if (m_mipmapData.get() != nullptr)
		{
			const int blockSize = getFormat() == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT ? 8 : 16;
			for (int i = 0; i < m_mipmapLevels; i++)
			{
				const int size = ((m_mipmapSizes[i].width() + 3) / 4) * ((m_mipmapSizes[i].height() + 3) / 4) * blockSize;
				glCompressedTexImage2D(m_type, i, m_internalFormat, m_mipmapSizes[i].width(), m_mipmapSizes[i].height(), 0, size, m_mipmapData[i].get());
			}

			if (m_mipmapLevels == 1)
			{
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
				if (newlyGen)
					setParameters(GL_REPEAT, GL_NEAREST, GL_NEAREST);
			}
			else if (newlyGen)
				setParameters(GL_REPEAT, GL_NEAREST_MIPMAP_NEAREST, GL_NEAREST);

			m_mipmapData.reset();
		}

		release();
	}

	void GLS3TCTexture::importTexture(const std::string& path)
	{
		File file(path);
		if (!file.exists())
			throw FileNotFoundException(path);

		std::ifstream input(file.getPath(), std::fstream::binary);
		if (!input.good())
			throw IOException("Could not open ifstream to \"" + file.getPath() + "\"");

		importTexture(input);
		input.close();
	}

	void GLS3TCTexture::importTexture(std::istream& input)
	{
		char fileCode[4];
		input.read(fileCode, 4);
		if (std::string(fileCode, 4) != "DDS ")
			throw IOException("Given stream does not contain DDS data!");

		char header[124];
		input.read(header, 124);

		m_mipmapLevels = *bo_cast<int, LITTLE_ENDIAN | SYSTEM_ENDIAN>(1, header + 24);
		if (m_mipmapLevels == 0)
			m_mipmapLevels = 1;

		m_mipmapSizes = std::unique_ptr<Dimension2i[]>(new Dimension2i[m_mipmapLevels]);
		m_mipmapData = std::unique_ptr<std::unique_ptr<char[]>[]>(new std::unique_ptr<char[]>[m_mipmapLevels]);

		m_internalFormat = asGLFormat(*bo_cast<int, LITTLE_ENDIAN | SYSTEM_ENDIAN>(1, header + 80));
		m_format = GLTexture::formatFromInternalFormat(m_internalFormat);
		m_type = GL_TEXTURE_2D;

		const int blockSize = getFormat() == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT ? 8 : 16;
		int width = *bo_cast<int, LITTLE_ENDIAN | SYSTEM_ENDIAN>(1, header + 12);
		int height = *bo_cast<int, LITTLE_ENDIAN | SYSTEM_ENDIAN>(1, header + 8);

		m_size = Dimension3i(width, height, 1);

		for (int i = 0; i < m_mipmapLevels; i++)
		{
			const int size = ((width + 3) / 4) * ((height + 3) / 4) * blockSize;

			m_mipmapSizes[i] = Dimension2i(width, height);
			m_mipmapData[i] = std::unique_ptr<char[]>(new char[size]);
			input.read(m_mipmapData[i].get(), size);

			width = Math::max(width / 2, 1);
			height = Math::max(height / 2, 1);
		}

	}

}