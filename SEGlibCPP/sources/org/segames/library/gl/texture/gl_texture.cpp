#include <org/segames/library/debug_util.h>
#include <org/segames/library/gl/texture/gl_texture.h>
#include <org/segames/library/gl/gl_exception.h>
#include <org/segames/library/gl/gl_core.h>

namespace org::segames::library
{

	int GLTexture::s_glMaxTextureSize = 0;

	const GLTexture& GLTexture::setParameters(const GLenum wrapping, const GLenum minFilter, const GLenum magFilter) const
	{
		glTexParameteri(m_type, GL_TEXTURE_WRAP_S, wrapping);
		glTexParameteri(m_type, GL_TEXTURE_WRAP_T, wrapping);
		glTexParameteri(m_type, GL_TEXTURE_WRAP_R, wrapping);
		glTexParameteri(m_type, GL_TEXTURE_MIN_FILTER, minFilter);
		glTexParameteri(m_type, GL_TEXTURE_MAG_FILTER, magFilter);

#ifdef SEG_API_GL_ERROR_EXCEPTIONS
		GLenum error = glGetError();
		if (error != GL_NO_ERROR)
			throw GLException(error);
#endif
		return *this;
	}

	void GLTexture::upload(const GLenum storage, const void* data)
	{
		upload(0, m_size, storage, data);
	}

	void GLTexture::upload(const GLint level, const Dimension3i& size, const GLenum storage, const void* data)
	{
		if (!m_id)
		{
			glGenTextures(1, &m_id);
			if (!glIsEnabled(m_type))
				glEnable(m_type);

			bind();
			setParameters(GL_REPEAT, GL_NEAREST, GL_NEAREST);
		}
		else
			bind();

		switch (m_type)
		{
		case GL_TEXTURE_1D:
			glTexImage1D(m_type, level, m_internalFormat, size.width(), 0, m_format, storage, data);
			break;
		case GL_TEXTURE_2D:
		case GL_TEXTURE_1D_ARRAY:
			glTexImage2D(m_type, level, m_internalFormat, size.width(), size.height(), 0, m_format, storage, data);
			break;
		case GL_TEXTURE_3D:
		case GL_TEXTURE_2D_ARRAY:
			glTexImage3D(m_type, level, m_internalFormat, size.width(), size.height(), size.depth(), 0, m_format, storage, data);
			break;
		default:
			break;
		}

		release();

#ifdef SEG_API_GL_ERROR_EXCEPTIONS
		GLenum error = glGetError();
		if (error != GL_NO_ERROR)
			throw GLException(error);
#endif
	}

	GLenum GLTexture::formatFromInternalFormat(const GLenum internalFormat)
	{
		switch (internalFormat)
		{
		case GL_RGBA8:
		case GL_RGBA16:
			return GL_RGBA;
		case GL_RGB8:
		case GL_RGB16:
			return GL_RGB;
		case GL_RG8:
		case GL_RG16:
			return GL_RG;
		case GL_R8:
		case GL_R16:
			return GL_RED;
		case GL_DEPTH_COMPONENT:
		case GL_DEPTH_COMPONENT16:
		case GL_DEPTH_COMPONENT24:
		case GL_DEPTH_COMPONENT32:
			return GL_DEPTH_COMPONENT;
		default:
			return internalFormat;
		}

	}

	int GLTexture::glMaxTextureSize()
	{
		if (!s_glMaxTextureSize)
			glGetIntegerv(GL_MAX_TEXTURE_SIZE, &s_glMaxTextureSize);
		return s_glMaxTextureSize;
	}

}