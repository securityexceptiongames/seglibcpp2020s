#include <org/segames/library/debug_util.h>
#include <org/segames/library/gl/texture/gl_physical_texture_wrapper.h>
#include <org/segames/library/gl/texture/gl_disc_texture.h>
#include <org/segames/library/gl/texture/gl_s3tc_texture.h>
#include <org/segames/library/util/string_util.h>

namespace org::segames::library
{

	void GLPhysicalTextureWrapper::importTexture(const std::string_view& path)
	{
		setPath(path);

		if (m_texType == TextureType::S3TC_TEXTURE)
		{
			GLS3TCTexture* ptr;
			std::unique_ptr<GLPhysicalTexture> temp(ptr = new GLS3TCTexture(path));
						
			ptr->importTexture();
			m_texture.swap(temp);
		}
		else
		{
			GLDiscTexture* ptr;
			std::unique_ptr<GLPhysicalTexture> temp(ptr = new GLDiscTexture(path));

			ptr->importTexture();
			m_texture.swap(temp);
		}
					
		if (m_texture)
		{
			m_type = m_texture->getType();
			m_internalFormat = m_texture->getInternalFormat();
			m_format = m_texture->getFormat();
			m_size = m_texture->getSize();
		}

	}

	GLPhysicalTextureWrapper::TextureType GLPhysicalTextureWrapper::determineTypeFrom(const std::string_view& path)
	{
		if (path.length() == 0)
			return TextureType::DISC_TEXTURE;
		else if (StringUtil::endsWith(path, ".dds"))
			return TextureType::S3TC_TEXTURE;
		else
			return TextureType::DISC_TEXTURE;
	}

}