#include <org/segames/library/debug_util.h>
#include <org/segames/library/gl/nk/nk_instance.h>
#include <org/segames/library/null_pointer_exception.h>

namespace org::segames::library
{

	nk_draw_null_texture NkInstance::DRAW_NULL_TEXTURE = nk_draw_null_texture();
	nk_draw_vertex_layout_element NkInstance::DRAW_VERTEX_LAYOUT_ELEMENT[4] = {
		{ NK_VERTEX_POSITION, NK_FORMAT_FLOAT, 0 },
		{ NK_VERTEX_TEXCOORD, NK_FORMAT_FLOAT, 8 },
		{ NK_VERTEX_COLOR, NK_FORMAT_R8G8B8A8, 16 },
		{ NK_VERTEX_LAYOUT_END }
	};
	nk_convert_config NkInstance::DEFAULT_CONVERT_CONFIG = makeDefaultConvertConfig();
	GLShader NkInstance::DEFAULT_SHADER = GLShader();

	nk_size NkInstance::s_bufferSize = 1024 * 1024;
	int NkInstance::s_numInstaces = 0;
	std::unordered_map<int, NkInstance*> NkInstance::s_mappedInstances = std::unordered_map<int, NkInstance*>();

	NkInstance::NkInstance(const GLShader& shader, const NkInstanceConfig& config) :
		m_geometryData(new char[s_bufferSize]),
		m_indexData(new char[s_bufferSize]),
		m_context(new nk_context()),
		m_drawCommands(new nk_buffer()),
		m_shader(&shader),
		m_geometry(new GLBuffer()),
		m_indices(new GLBuffer()),
		m_config(&config),
		m_layouts()
	{
		nk_init_default(m_context.get(), nullptr);
		nk_buffer_init_default(m_drawCommands.get());
				
		m_context->clip.userdata.id = ++s_numInstaces;
		s_mappedInstances.insert_or_assign(m_context->clip.userdata.id, this);
		m_context->clip.paste = pasteCallback;
		m_context->clip.copy = copyCallback;

		ensureDrawNullTexture();
		if (m_shader == &DEFAULT_SHADER)
			ensureDefaultShader();
		m_shader->bind();
		glUniform1i(glGetUniformLocation(m_shader->getID(), "texture_img"), 0);
		m_shader->release();

		m_geometry->setPointerBinding(GLDataType::VERTEX, GLPointerInf(GL_FLOAT, 2, 20, 0));
		m_geometry->setPointerBinding(GLDataType::TEX_COORD, GLPointerInf(GL_FLOAT, 2, 20, 8));
		m_geometry->setPointerBinding(GLDataType::COLOR, GLPointerInf(GL_UNSIGNED_BYTE, 4, 20, 16));
		m_geometry->upload(s_bufferSize, nullptr, GL_STREAM_DRAW);

		m_indices->setType(GL_ELEMENT_ARRAY_BUFFER);
		m_indices->upload(s_bufferSize, nullptr, GL_STREAM_DRAW);
	}

	void NkInstance::update() const
	{
		nk_input_end(m_context.get());
		workAroundWindowCloseBug__();
		for (size_t i = 0; i < m_layouts.size(); i++)
			m_layouts.get(i)->updateInternal(m_context.get());

		glEnable(GL_BLEND);
		glBlendEquation(GL_FUNC_ADD);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDisable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_SCISSOR_TEST);
				
		nk_buffer gBuf, iBuf;
		nk_buffer_init_fixed(&gBuf, m_geometryData.get(), s_bufferSize);
		nk_buffer_init_fixed(&iBuf, m_indexData.get(), s_bufferSize);
		nk_convert(m_context.get(), m_drawCommands.get(), &gBuf, &iBuf, &DEFAULT_CONVERT_CONFIG);

		m_shader->bind();
		m_geometry->bind().setPointerInf();
		m_indices->bind();

		glBufferSubData(m_geometry->getType(), 0, gBuf.needed, m_geometryData.get());
		glBufferSubData(m_indices->getType(), 0, iBuf.needed, m_indexData.get());

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);

		const GLint fontRenderLocation = glGetUniformLocation(m_shader->getID(), "is_font_rendering");
		const int canvasHeight = m_config->getCanvasHeight();
		nk_draw_index* offset = NULL;
		const nk_draw_command* cmd;
		nk_draw_foreach(cmd, m_context.get(), m_drawCommands.get())
		{
			if (cmd->elem_count == 0)
				continue;

			if (fontRenderLocation != -1)
				glUniform1i(fontRenderLocation, NkFont::isFontTexture((GLuint)cmd->texture.id));

			glBindTexture(GL_TEXTURE_2D, (GLuint)cmd->texture.id);
			glScissor(
				(GLint)(cmd->clip_rect.x),
				(GLint)(canvasHeight - (cmd->clip_rect.y + cmd->clip_rect.h)),
				(GLint)(cmd->clip_rect.w),
				(GLint)(cmd->clip_rect.h)
			);

			glDrawElements(GL_TRIANGLES, (GLsizei)cmd->elem_count, GL_UNSIGNED_SHORT, offset);
			offset += cmd->elem_count;
		}
		glBindTexture(GL_TEXTURE_2D, GL_NONE);

		glDisableClientState(GL_COLOR_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);

		m_indices->release();
		m_geometry->release();
		m_shader->release();

		glDisable(GL_SCISSOR_TEST);

		nk_clear(m_context.get());
		nk_input_begin(m_context.get());
	}
			
	void NkInstance::workAroundWindowCloseBug__() const
	{
		if (m_context)
		{
			nk_begin(m_context.get(), "##BUGFIX##", nk_rect(-100, -100, 1, 1), 0);
			nk_end(m_context.get());
		}

	}

	nk_convert_config NkInstance::makeDefaultConvertConfig()
	{
		nk_convert_config config;
		config.vertex_layout = DRAW_VERTEX_LAYOUT_ELEMENT;
		config.vertex_size = 20;
		config.vertex_alignment = 4;
		config.null = DRAW_NULL_TEXTURE;
		config.circle_segment_count = 22;
		config.curve_segment_count = 22;
		config.arc_segment_count = 22;
		config.global_alpha = 1.0f;
		config.shape_AA = NK_ANTI_ALIASING_ON;
		config.line_AA = NK_ANTI_ALIASING_ON;
		return config;
	}

	void NkInstance::ensureDrawNullTexture()
	{
		if (DRAW_NULL_TEXTURE.texture.id == GL_NONE)
		{
			GLuint nullTexId;
			glGenTextures(1, &nullTexId);
			DRAW_NULL_TEXTURE.texture.id = nullTexId;
			DRAW_NULL_TEXTURE.uv.x = 0.5f;
			DRAW_NULL_TEXTURE.uv.y = 0.5f;

			glBindTexture(GL_TEXTURE_2D, DRAW_NULL_TEXTURE.texture.id);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			unsigned int data = 0xFFFFFFFF;
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 1, 1, 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8_REV, &data);
			glBindTexture(GL_TEXTURE_2D, GL_NONE);

			DEFAULT_CONVERT_CONFIG.null = DRAW_NULL_TEXTURE;
		}

	}

	void NkInstance::ensureDefaultShader()
	{
		if (DEFAULT_SHADER.getID() == 0)
		{
			DEFAULT_SHADER.setVertexData("#version 120\nvoid main() {\ngl_FrontColor = gl_Color;\ngl_TexCoord[0].xy = gl_MultiTexCoord0.xy;\ngl_Position = gl_ModelViewProjectionMatrix * vec4(gl_Vertex.xy, 0, 1);\n}\n");
			DEFAULT_SHADER.setFragmentData("#version 120\nuniform sampler2D texture_img;\nuniform int is_font_rendering;\nvoid main() {\nif(is_font_rendering == 1) {\ngl_FragColor = gl_Color;\ngl_FragColor.a *= texture2D(texture_img, gl_TexCoord[0].xy).r;\n} else\ngl_FragColor = gl_Color * texture2D(texture_img, gl_TexCoord[0].xy);\n}\n");
			DEFAULT_SHADER.upload("NkInstance::DEFAULT_SHADER");
		}

	}

}