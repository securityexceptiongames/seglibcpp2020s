#include <org/segames/library/debug_util.h>
#include <org/segames/library/gl/nk/nk_font.h>

namespace org::segames::library
{

	int NkFont::s_numFonts = 0;
	std::unordered_map<int, NkFont*> NkFont::s_mappedFonts = std::unordered_map<int, NkFont*>();
	std::unordered_map<GLuint, int> NkFont::s_fontTextureIds = std::unordered_map<GLuint, int>();

	NkFont::NkFont(const GLFont& font) :
		m_info(),
		m_font(&font)
	{
		if (m_font)
		{
			m_info.texture.id = m_font->getAtlas()->getID();
			m_info.width = queryWidthCallback;
			m_info.height = m_font->getHeight();
			m_info.query = queryGlyphCallback;

			m_info.userdata.id = ++s_numFonts;
			s_mappedFonts.insert_or_assign(m_info.userdata.id, this);
					
			if (m_info.texture.id)
			{
				if (s_fontTextureIds.find(m_info.texture.id) == s_fontTextureIds.end())
					s_fontTextureIds.insert_or_assign(m_info.texture.id, 1);
				else
					s_fontTextureIds.find(m_info.texture.id)->second++;
			}

		}

	}

	NkFont::NkFont(const NkFont& obj) :
		m_info(obj.m_info),
		m_font(obj.m_font)
	{
		if (m_info.userdata.id != 0)
		{
			m_info.userdata.id = ++s_numFonts;
			s_mappedFonts.insert_or_assign(m_info.userdata.id, this);
		}

		if (m_info.texture.id)
			s_fontTextureIds.find(m_info.texture.id)->second++;
	}

	NkFont::~NkFont()
	{
		if (m_info.userdata.id != 0)
			s_mappedFonts.erase(m_info.userdata.id);

		if (m_info.texture.id)
		{
			if (s_fontTextureIds.find(m_info.texture.id)->second == 1)
				s_fontTextureIds.erase(m_info.texture.id);
			else
				s_fontTextureIds.find(m_info.texture.id)->second--;
		}

	}

	float NkFont::queryWidth(nk_handle handle, float height, const char* str, int len)
	{
		if (m_font)
		{
			float text_width = 0;
			nk_rune unicode;

			int glyph_len = nk_utf_decode(str, &unicode, len);
			int text_len = glyph_len;

			if (glyph_len == 0)
				return 0;

			while (text_len <= len && glyph_len != 0)
			{
				if (unicode == NK_UTF_INVALID)
					break;
				else
				{
					const GLFontGlyph* glyph = m_font->getGlyph(unicode);
					if (glyph != nullptr)
						text_width += glyph->getAdvance();
					glyph_len = nk_utf_decode(str, &unicode, len - text_len);
					text_len += glyph_len;
				}

			}

			return text_width;
		}
		return 0;
	}

	void NkFont::queryGlyph(nk_handle handle, float height, nk_user_font_glyph* glyphOut, nk_rune codepoint, nk_rune nextCodepoint)
	{
		if (m_font)
		{
			const GLFontGlyph* glyph = m_font->getGlyph(codepoint);
			if (glyph != nullptr)
			{
				glyphOut->xadvance = glyph->getAdvance();
				glyphOut->width = glyph->getWidth();
				glyphOut->height = glyph->getHeight();
				glyphOut->offset.x = glyph->getXOffset();
				glyphOut->offset.y = glyph->getYOffset();
				glyphOut->uv[0].x = glyph->getQuad().s0; glyphOut->uv[0].y = glyph->getQuad().t0;
				glyphOut->uv[1].x = glyph->getQuad().s1; glyphOut->uv[1].y = glyph->getQuad().t1;
			}

		}

	}

}