#include <org/segames/library/debug_util.h>
#include <org/segames/library/gl/nk/compat/glfw/nk_glfw_key_listener.h>

namespace org::segames::library
{
			
	void NkGLFWKeyListener::invoke(int key, int scancode, int action, int mods)
	{
		switch (key)
		{
		case GLFW_KEY_LEFT_SHIFT:
		case GLFW_KEY_RIGHT_SHIFT:
			nk_input_key(m_instance->getContext(), NK_KEY_SHIFT, action != GLFW_RELEASE);
			break;
		case GLFW_KEY_LEFT_CONTROL:
		case GLFW_KEY_RIGHT_CONTROL:
			nk_input_key(m_instance->getContext(), NK_KEY_CTRL, action != GLFW_RELEASE);
			break;
		case GLFW_KEY_DELETE:
			nk_input_key(m_instance->getContext(), NK_KEY_DEL, action != GLFW_RELEASE);
			break;
		case GLFW_KEY_ENTER:
		case GLFW_KEY_KP_ENTER:
			nk_input_key(m_instance->getContext(), NK_KEY_ENTER, action != GLFW_RELEASE);
			break;
		case GLFW_KEY_TAB:
			nk_input_key(m_instance->getContext(), NK_KEY_TAB, action != GLFW_RELEASE);
			break;
		case GLFW_KEY_BACKSPACE:
			nk_input_key(m_instance->getContext(), NK_KEY_BACKSPACE, action != GLFW_RELEASE);
			break;
		case GLFW_KEY_C:
			nk_input_key(m_instance->getContext(), NK_KEY_COPY, action == GLFW_PRESS && (mods & GLFW_MOD_CONTROL) == GLFW_MOD_CONTROL);
		case GLFW_KEY_X:
			nk_input_key(m_instance->getContext(), NK_KEY_CUT, action == GLFW_PRESS && (mods & GLFW_MOD_CONTROL) == GLFW_MOD_CONTROL);
			break;
		case GLFW_KEY_V:
			nk_input_key(m_instance->getContext(), NK_KEY_PASTE, action == GLFW_PRESS && (mods & GLFW_MOD_CONTROL) == GLFW_MOD_CONTROL);
			break;
		case GLFW_KEY_UP:
			nk_input_key(m_instance->getContext(), NK_KEY_UP, action != GLFW_RELEASE);
			break;
		case GLFW_KEY_DOWN:
			nk_input_key(m_instance->getContext(), NK_KEY_DOWN, action != GLFW_RELEASE);
			break;
		case GLFW_KEY_LEFT:
			nk_input_key(m_instance->getContext(), NK_KEY_LEFT, action != GLFW_RELEASE);
			break;
		case GLFW_KEY_RIGHT:
			nk_input_key(m_instance->getContext(), NK_KEY_RIGHT, action != GLFW_RELEASE);
			break;
		case GLFW_KEY_Z:
			nk_input_key(m_instance->getContext(), NK_KEY_TEXT_UNDO, action == GLFW_PRESS && (mods & GLFW_MOD_CONTROL) == GLFW_MOD_CONTROL);
			break;
		case GLFW_KEY_Y:
			nk_input_key(m_instance->getContext(), NK_KEY_TEXT_REDO, action == GLFW_PRESS && (mods & GLFW_MOD_CONTROL) == GLFW_MOD_CONTROL);
			break;
		case GLFW_KEY_A:
			nk_input_key(m_instance->getContext(), NK_KEY_TEXT_SELECT_ALL, action == GLFW_PRESS && (mods & GLFW_MOD_CONTROL) == GLFW_MOD_CONTROL);
			break;
		default:
			break;
		}

		GLFWKeyListener::invoke(key, scancode, action, mods);
	}

}