#include <org/segames/library/debug_util.h>
#include <org/segames/library/gl/fbo/gl_framebuffer_wrapper.h>
#include <org/segames/library/gl/fbo/gl_framebuffer_30.h>
#include <org/segames/library/gl/fbo/gl_framebuffer_ext.h>

namespace org::segames::library
{

	std::unique_ptr<GLAbstractFramebuffer> GLFramebufferWrapper::newSupportedFBO(std::unique_ptr<GLTexture>&& color, std::unique_ptr<GLTexture>&& depth, const Dimension2i& size)
	{
		if (GLFramebuffer30::isSupported())
			return std::unique_ptr<GLAbstractFramebuffer>(new GLFramebuffer30(std::move(color), std::move(depth), size));
		else if (GLFramebufferEXT::isSupported())
			return std::unique_ptr<GLAbstractFramebuffer>(new GLFramebufferEXT(std::move(color), std::move(depth), size));
		return nullptr;
	}

	bool GLFramebufferWrapper::isSupported()
	{
		return
			GLFramebuffer30::isSupported() ||
			GLFramebufferEXT::isSupported();
	}

}