#include <org/segames/library/debug_util.h>
#include <org/segames/library/gl/fbo/gl_abstract_framebuffer.h>
#include <org/segames/library/gl/gl_exception.h>

namespace org::segames::library
{

	void GLAbstractFramebuffer::setSize(const Dimension2i& size)
	{
		m_size = size;
		Dimension3i texSize = Dimension3i(m_size.width(), m_size.height(), m_color->getSize().depth());
		if (m_color)
			m_color->setSize(texSize);
		for (size_t i = 0; i < m_additionalColors.size(); i++)
			if (m_additionalColors.get(i))
				m_additionalColors.get(i)->setSize(texSize);
		if(m_depth)
			m_depth->setSize(texSize);
		m_resetTexture = true;
	}

	void GLAbstractFramebuffer::buildTextures(bool& changed)
	{
#ifdef SEG_API_GL_ERROR_EXCEPTIONS
		GLenum error;
#endif
		// Depth comp
		if (m_depth)
		{
			bool newTex = !m_depth->getID();
			if (newTex || m_resetTexture)
			{
				m_depth->upload(GL_FLOAT, nullptr);
				if (newTex)
					m_depth->bind().setParameters(GL_CLAMP, GL_NEAREST, GL_NEAREST).release();
				changed = true;
			}

#ifdef SEG_API_GL_ERROR_EXCEPTIONS
			error = glGetError();
			if (error != GL_NO_ERROR)
				throw GLException(error);
#endif
		}

		// Additional color comp
		for (size_t i = 0; i < m_additionalColors.size(); i++)
		{
			bool newTex = !m_additionalColors.get(i)->getID();
			if (newTex || m_resetTexture)
			{
				m_additionalColors.get(i)->upload(GL_UNSIGNED_BYTE, nullptr);
				changed = true;
			}

#ifdef SEG_API_GL_ERROR_EXCEPTIONS
			error = glGetError();
			if (error != GL_NO_ERROR)
				throw GLException(error);
#endif
		}

		// Color comp
		if (m_color)
			if (!m_color->getID() || m_resetTexture)
			{
				m_color->upload(GL_UNSIGNED_BYTE, nullptr);
				changed = true;
			}

#ifdef SEG_API_GL_ERROR_EXCEPTIONS
		error = glGetError();
		if (error != GL_NO_ERROR)
			throw GLException(error);
#endif
	}

}