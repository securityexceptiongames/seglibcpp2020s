#include <org/segames/library/debug_util.h>
#include <org/segames/library/gl/fbo/gl_framebuffer_30.h>
#include <org/segames/library/gl/gl_exception.h>
#include <org/segames/library/gl/gl_core.h>
#include <org/segames/library/math/math.h>

namespace org::segames::library
{

	void GLFramebuffer30::buildFBO()
	{
		GLint numColors;
		glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &numColors);
		numColors -= 1;

		if (!m_id)
			glGenFramebuffers(1, &m_id);

		glBindFramebuffer(GL_FRAMEBUFFER, m_id);

		// Main color comp
		if (m_color)
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_color->getType(), m_color->getID(), 0);
		else
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, GL_NONE, 0);

		// Additional color comp
		for (size_t i = 0; i < Math::min(m_additionalColors.size(), static_cast<size_t>(numColors)); i++)
		{
			std::unique_ptr<GLTexture>& tex = m_additionalColors.get(i);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1 + i, tex->getType(), tex->getID(), 0);
		}
		for (size_t i = m_additionalColors.size(); i < static_cast<size_t>(numColors); i++)
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1 + i, GL_TEXTURE_2D, GL_NONE, 0);

		// Depth comp
		if (m_depth)
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, m_depth->getType(), m_depth->getID(), 0);
		else
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, GL_NONE, 0);

		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			throw GLException("Could not complete framebuffer.");

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	bool GLFramebuffer30::isSupported()
	{
		return GLCore::glVersion() >= 3.0f;
	}

}