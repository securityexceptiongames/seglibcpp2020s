#include <org/segames/library/debug_util.h>
#include <org/segames/library/gl/fbo/gl_framebuffer_ext.h>
#include <org/segames/library/gl/gl_exception.h>
#include <org/segames/library/gl/gl_core.h>
#include <org/segames/library/math/math.h>

namespace org::segames::library
{

	void GLFramebufferEXT::buildFBO()
	{
		GLint numColors;
		glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS_EXT, &numColors);
		numColors -= 1;

		if (!m_id)
			glGenFramebuffersEXT(1, &m_id);

		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, m_id);

		// Main color comp
		if (m_color)
			glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, m_color->getType(), m_color->getID(), 0);
		else
			glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, GL_NONE, 0);

		// Additional color comp
		for (size_t i = 0; i < Math::min(m_additionalColors.size(), static_cast<size_t>(numColors)); i++)
		{
			std::unique_ptr<GLTexture>& tex = m_additionalColors.get(i);
			glFramebufferTexture2D(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT1_EXT + i, tex->getType(), tex->getID(), 0);
		}
		for (size_t i = m_additionalColors.size(); i < static_cast<size_t>(numColors); i++)
			glFramebufferTexture2D(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT1_EXT + i, GL_TEXTURE_2D, GL_NONE, 0);

		// Depth comp
		if (m_depth)
			glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, m_depth->getType(), m_depth->getID(), 0);
		else
			glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, GL_NONE, 0);

		if (glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT) != GL_FRAMEBUFFER_COMPLETE_EXT)
			throw GLException("Could not complete framebuffer.");

		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	}

	bool GLFramebufferEXT::isSupported()
	{
		return GLCore::hasExtension("GL_EXT_framebuffer_object");
	}

}