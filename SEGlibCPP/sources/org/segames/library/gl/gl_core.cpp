#include <org/segames/library/debug_util.h>
#include <org/segames/library/gl/gl_core.h>
#include <org/segames/library/util/array_list.h>
#include <org/segames/library/util/string_util.h>

namespace org::segames::library
{

	float GLCore::s_version = 0.0f;
	std::unique_ptr<std::unordered_set<std::string>> GLCore::s_extensions(nullptr);

	const std::unordered_set<std::string>& GLCore::glExtensions()
	{
		if (!s_extensions.get())
		{
			std::unique_ptr<std::unordered_set<std::string>> temp(new std::unordered_set<std::string>());
			s_extensions.swap(temp);

			const GLubyte* str = glGetString(GL_EXTENSIONS);
			if (str)
			{
				std::string full(reinterpret_cast<const char*>(str));
				ArrayList<std::string_view> list;
				StringUtil::split(full, " ", list);

				s_extensions->reserve(list.size());
				for (size_t i = 0; i < list.size(); i++)
					s_extensions->insert(std::string(list.get(i)));
			}

		}
		return *s_extensions;
	}

}