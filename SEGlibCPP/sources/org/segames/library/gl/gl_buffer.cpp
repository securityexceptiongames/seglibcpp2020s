#include <org/segames/library/debug_util.h>
#include <org/segames/library/gl/gl_buffer.h>
#include <org/segames/library/gl/gl_exception.h>

namespace org::segames::library
{

	const GLBuffer& GLBuffer::setPointerInf() const
	{
#ifdef SEG_API_GL_MISC_EXCEPTIONS
		bool ptrInf = false;
#endif

		for (size_t i = 0; i < to_underlying(GLDataType::ATTRIBUTE); i++)
			if (m_infBindings.get(i))
			{
				m_infBindings.get(i)->setPointer();
#ifdef SEG_API_GL_MISC_EXCEPTIONS
				ptrInf = true;
#endif
			}

		for (size_t i = to_underlying(GLDataType::ATTRIBUTE); i < m_infBindings.size(); i++)
			if (m_infBindings.get(i))
			{
				m_infBindings.get(i)->setPointer(i - to_underlying(GLDataType::ATTRIBUTE));
#ifdef SEG_API_GL_MISC_EXCEPTIONS
				ptrInf = true;
#endif
			}

#ifdef SEG_API_GL_MISC_EXCEPTIONS
		if (!ptrInf)
			throw GLException("No pointer information set for buffer.");
#endif
		return *this;
	}

	GLBuffer& GLBuffer::setPointerBinding(const GLDataType type, const GLPointerInf& inf)
	{
#ifdef SEG_API_GL_MISC_EXCEPTIONS
		if (type == GLDataType::ATTRIBUTE)
			throw GLException("Cannot set attribute pointer information without attribute id.");
#endif
		if (m_infBindings.get(to_underlying(type)))
			(*m_infBindings.get(to_underlying(type))) = GLPointerBinding(type, inf);
		else
			m_infBindings.set(to_underlying(type), std::unique_ptr<GLPointerBinding>(new GLPointerBinding(type, inf)));
		return *this;
	}

	GLBuffer& GLBuffer::setPointerBinding(const GLDataType type, const GLPointerInf& inf, const GLuint location)
	{
		if (type != GLDataType::ATTRIBUTE)
			setPointerBinding(type, inf);
		else
		{
			size_t index = location + to_underlying(GLDataType::ATTRIBUTE);
			while (index >= m_infBindings.size())
				m_infBindings.add(nullptr);
						
			if (m_infBindings.get(index))
				*m_infBindings.get(index) = GLPointerBinding(type, inf);
			else
				m_infBindings.set(index, std::unique_ptr<GLPointerBinding>(new GLPointerBinding(type, inf))).reset();
		}
		return *this;
	}

	GLBuffer& GLBuffer::removePointerBinding(const GLDataType type)
	{
#ifdef SEG_API_GL_MISC_EXCEPTIONS
		if (type == GLDataType::ATTRIBUTE)
			throw GLException("Cannot remove attribute pointer information without attribute id.");
#endif
		m_infBindings.get(to_underlying(type)).reset();
		return *this;
	}

	GLBuffer& GLBuffer::removePointerBinding(const GLDataType type, const GLuint location)
	{
		if (type != GLDataType::ATTRIBUTE)
			removePointerBinding(type);
		else
		{
			size_t index = location + to_underlying(GLDataType::ATTRIBUTE);
			if (index < m_infBindings.size())
				m_infBindings.get(index).reset();
		}
		return *this;
	}

	void GLBuffer::makeIntoImageOf(const GLBuffer& obj)
	{
		this->destroy();
		m_id = obj.m_id;
		m_uploadLength = obj.m_uploadLength;
		m_nextImage = obj.m_nextImage;
		const_cast<GLBuffer&>(obj).m_nextImage = this;
	}

	void GLBuffer::upload(const GLsizeiptr len, const void* data, const GLenum mode)
	{
		this->create();
		glBindBuffer(m_bufferType, m_id);
		if (len <= m_uploadLength)
			glBufferSubData(m_bufferType, 0, len, data);
		else
			glBufferData(m_bufferType, len, data, mode);
		glBindBuffer(m_bufferType, GL_NONE);
		m_uploadLength = len;
				
		GLBuffer* img = this;
		while ((img = img->m_nextImage) != this)
			img->m_uploadLength = m_uploadLength;
	}

	void GLBuffer::uploadSubData(const GLintptr offset, const GLsizeiptr len, const void* data) noexcept(false)
	{
#ifdef SEG_API_GL_MISC_EXCEPTIONS
		if (offset < 0)
			throw GLException("Cannot upload data to a sub-section that begins before the buffer data.");
		else if(offset + len > m_uploadLength)
			throw GLException("Cannot upload data to a sub-section that ends after the buffer data.");
#endif
		glBufferSubData(m_bufferType, offset, len, data);
	}

	void GLBuffer::create() noexcept
	{
		if (!m_id)
		{
			glGenBuffers(1, &m_id);

			GLBuffer* img = this;
			while ((img = img->m_nextImage) != this)
				img->m_id = m_id;
		}

	}

	void GLBuffer::destroy() noexcept
	{
		if (m_nextImage == this)
		{
			if (m_id)
				glDeleteBuffers(1, &m_id);
		}
		else
		{
			GLBuffer* img = this;
			while (img->m_nextImage != this)
				img = img->m_nextImage;
			img->m_nextImage = m_nextImage;
		}

	}

}