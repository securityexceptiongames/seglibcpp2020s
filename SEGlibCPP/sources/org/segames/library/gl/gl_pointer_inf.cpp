#include <org/segames/library/debug_util.h>
#include <org/segames/library/gl/gl_pointer_inf.h>

namespace org::segames::library
{

	GLPointerInf GLPointerInf::ONE_FLOAT_POINTER	= GLPointerInf(GL_FLOAT, 1, 0, 0);
	GLPointerInf GLPointerInf::TWO_FLOAT_POINTER	= GLPointerInf(GL_FLOAT, 2, 0, 0);
	GLPointerInf GLPointerInf::THREE_FLOAT_POINTER	= GLPointerInf(GL_FLOAT, 3, 0, 0);
	GLPointerInf GLPointerInf::FOUR_FLOAT_POINTER	= GLPointerInf(GL_FLOAT, 4, 0, 0);

	GLPointerInf GLPointerInf::ONE_INT_POINTER		= GLPointerInf(GL_INT, 1, 0, 0);
	GLPointerInf GLPointerInf::TWO_INT_POINTER		= GLPointerInf(GL_INT, 2, 0, 0);
	GLPointerInf GLPointerInf::THREE_INT_POINTER	= GLPointerInf(GL_INT, 3, 0, 0);
	GLPointerInf GLPointerInf::FOUR_INT_POINTER		= GLPointerInf(GL_INT, 4, 0, 0);

}