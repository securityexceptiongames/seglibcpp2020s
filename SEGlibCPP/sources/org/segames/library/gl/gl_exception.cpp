#include <org/segames/library/debug_util.h>
#include <org/segames/library/gl/gl_exception.h>
#include <org/segames/library/core.h>

namespace org::segames::library
{

	std::string_view GLException::stringFromCode__(const GLenum error)
	{
		switch (error)
		{
		case GL_INVALID_ENUM:
			return "GL_INVALID_ENUM";
		case GL_INVALID_VALUE:
			return "GL_INVALID_VALUE";
		case GL_INVALID_OPERATION:
			return "GL_INVALID_OPERATION";
		case GL_STACK_OVERFLOW:
			return "GL_STACK_OVERFLOW";
		case GL_STACK_UNDERFLOW:
			return "GL_STACK_UNDERFLOW";
		case GL_OUT_OF_MEMORY:
			return "GL_OUT_OF_MEMORY";
		case GL_INVALID_FRAMEBUFFER_OPERATION:
			return "GL_INVALID_FRAMEBUFFER_OPERATION";
		case GL_CONTEXT_LOST:
			return "GL_CONTEXT_LOST";
		case GL_TABLE_TOO_LARGE:
			return "GL_TABLE_TOO_LARGE";
		default:
			return "???";
		}

	}

	void GLAPIENTRY GLException::messageCallback__(GLenum source,
		GLenum type,
		GLuint id,
		GLenum severity,
		GLsizei length,
		const GLchar* message,
		const void* userParam)
	{
		Core::err() << "GL callback: " <<
			(type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "") <<
			" type=" <<
			type <<
			" severity=" <<
			severity <<
			" message=\"" <<
			message <<
			"\"" <<
			std::endl;
	}

}