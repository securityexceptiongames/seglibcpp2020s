#include <org/segames/library/debug_util.h>
#include <org/segames/library/core.h>

#ifdef ERROR
#undef ERROR
#endif

namespace org::segames::library
{

	bool Core::s_printTime = true;
	std::istream* Core::s_in = &std::cin;
	std::ostream* Core::s_err = &std::cerr;
	std::ostream* Core::s_wrn = &std::cout;
	std::ostream* Core::s_out = &std::cout;
	const char* Core::s_labels[4] = {
		" [Info]   ",
		"<Warning> ",
		"<<ERROR>> ",
		" *Note*   "
	};

	const DateTime& Core::libraryCompileDate()
	{
		static DateTime date(__DATE__, __TIME__);
		return date;
	}

	void Core::println(const PrintLabel label, const std::string_view& message)
	{
		switch (label)
		{
		case PrintLabel::WARNING:
			if (s_wrn)
				printlnInternal__(label, message, wrn());
			else
				printlnInternal__(label, message, out());
			break;
		case PrintLabel::ERROR:
			if (s_err)
				printlnInternal__(label, message, err());
			else
				printlnInternal__(label, message, out());
			break;
		default:
			printlnInternal__(label, message, out());
			break;
		}

	}

	void Core::printlnInternal__(const PrintLabel label, const std::string_view& message, std::ostream& stream)
	{
		if (label != PrintLabel::NONE)
		{
			if (s_printTime)
				stream << s_labels[(size_t)label] << "[" << DateTime::today().timeToString() << "] " << message << std::endl;
			else
				stream << s_labels[(size_t)label] << message << std::endl;
		}
		else
		{
			if (s_printTime)
				stream << "[" << DateTime::today().timeToString() << "] " << message << std::endl;
			else
				stream << message << std::endl;
		}

	}

}