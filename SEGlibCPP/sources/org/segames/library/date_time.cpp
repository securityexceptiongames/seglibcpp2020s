#include <org/segames/library/debug_util.h>
#include <org/segames/library/date_time.h>
#include <org/segames/library/invalid_value_exception.h>

#include <chrono>
#include <ctime>
#include <charconv>

namespace org::segames::library
{

	DateTime::DateTime(const std::string_view& date, const std::string& time) noexcept(false) :
		DateTime(1970, Month::JANUARY, 1)
	{
		if (date.length() != 11)
			throw InvalidValueException("Invalid date format!");
		else if(time.length() != 8)
			throw InvalidValueException("Invalid time format!");

		if ((date[0] == 'J' || date[0] == 'j') && date[1] == 'a' && date[2] == 'n')
			m_month = Month::JANUARY;
		else if ((date[0] == 'F' || date[0] == 'f') && date[1] == 'e' && date[2] == 'b')
			m_month = Month::FEBRUARY;
		else if ((date[0] == 'M' || date[0] == 'm') && date[1] == 'a' && date[2] == 'r')
			m_month = Month::MARCH;
		else if ((date[0] == 'A' || date[0] == 'a') && date[1] == 'p' && date[2] == 'r')
			m_month = Month::APRIL;
		else if ((date[0] == 'M' || date[0] == 'm') && date[1] == 'a' && date[2] == 'y')
			m_month = Month::MAY;
		else if ((date[0] == 'J' || date[0] == 'j') && date[1] == 'u' && date[2] == 'n')
			m_month = Month::JUNE;
		else if ((date[0] == 'J' || date[0] == 'j') && date[1] == 'u' && date[2] == 'l')
			m_month = Month::JULY;
		else if ((date[0] == 'A' || date[0] == 'a') && date[1] == 'u' && date[2] == 'g')
			m_month = Month::AUGUST;
		else if ((date[0] == 'S' || date[0] == 's') && date[1] == 'e' && date[2] == 'p')
			m_month = Month::SEPTEMBER;
		else if ((date[0] == 'O' || date[0] == 'o') && date[1] == 'c' && date[2] == 't')
			m_month = Month::OCTOBER;
		else if ((date[0] == 'N' || date[0] == 'n') && date[1] == 'o' && date[2] == 'v')
			m_month = Month::NOVEMBER;
		else if ((date[0] == 'D' || date[0] == 'd') && date[1] == 'e' && date[2] == 'c')
			m_month = Month::DECEMBER;
		else
			throw InvalidValueException("Invalid date month format!");

		if (std::from_chars(&date[4], &date[6], m_day).ec == std::errc::invalid_argument)
			throw InvalidValueException("Invalid date day format!");

		if (std::from_chars(&date[7], &date[11], m_year).ec == std::errc::invalid_argument)
			throw InvalidValueException("Invalid date year format!");

		if (std::from_chars(&time[0], &time[2], m_hour).ec == std::errc::invalid_argument)
			throw InvalidValueException("Invalid time hour format!");

		if (std::from_chars(&time[3], &time[5], m_minute).ec == std::errc::invalid_argument)
			throw InvalidValueException("Invalid time minute format!");

		if (std::from_chars(&time[6], &time[8], m_second).ec == std::errc::invalid_argument)
			throw InvalidValueException("Invalid time second format!");
	}

	bool DateTime::isValidDate() const
	{
		if (m_month < Month::JANUARY || m_month > Month::DECEMBER)
			return false;
		else if (m_day > getDaysOfMonth(m_year, m_month))
			return false;
		else if (m_hour > 23 || m_minute > 59 || m_second > 59)
			return false;
		return true;
	}

	unsigned int DateTime::getDaysOfMonth(unsigned int year, Month month)
	{
		if (month == Month::FEBRUARY)
		{
			if (year % 4 != 0)
				return 28;
			else if (year % 100 != 0)
				return 29;
			else if (year % 400 != 0)
				return 28;
			else
				return 29;
		}
		else
			return 30 + (year % 2);
	}

	DateTime DateTime::today()
	{
		std::time_t timePoint = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
		std::tm now = *localtime(&timePoint);

		return DateTime(
			static_cast<unsigned int>(now.tm_year + 1900),
			static_cast<Month>(now.tm_mon + 1),
			static_cast<unsigned int>(now.tm_mday),
			static_cast<unsigned int>(now.tm_hour),
			static_cast<unsigned int>(now.tm_min),
			static_cast<unsigned int>(now.tm_sec)
		);

	}

}