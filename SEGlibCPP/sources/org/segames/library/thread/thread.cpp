#include <org/segames/library/debug_util.h>
#include <org/segames/library/thread/thread.h>
#include <org/segames/library/thread/synchronized.h>
#include <org/segames/library/null_pointer_exception.h>

#include <iostream>

namespace org::segames::library
{

	int Thread::s_maxIdleThreads = -1;
	int Thread::s_allocatedThreads = 0;
			
	Thread::~Thread()
	{
		if (m_thread)
		{
			m_thread->setCallback(nullptr);
			{
				Synchronized<std::unordered_map<std::thread::id, Thread*>> lock(s_threadMapping__());
				s_threadMapping__()->erase(m_thread->getThread()->get_id());
			}

		}

	}

	void Thread::start()
	{
		if (!m_runnable)
			throw NullPointerException("Cannot start a thread with a NULL runnable.");

		if(isRunning())
			throw ThreadException("Cannot start a thread that is already running.");

		m_thread = fetchThreadCarrier();

		{
			Synchronized<std::unordered_map<std::thread::id, Thread*>> lock(s_threadMapping__());
			s_threadMapping__()->insert_or_assign(m_thread->getThread()->get_id(), this);
		}

		m_thread->setCallback(this);
		m_thread->start(m_runnable);
	}

	ThreadCarrier* Thread::fetchThreadCarrier()
	{
		{
			Synchronized<ArrayStack<std::unique_ptr<ThreadCarrier>>> lock(s_avaliableThreads__());
			if (s_avaliableThreads__()->size() > 0)
				return s_avaliableThreads__()->pop().release();
		}

		s_allocatedThreads++;
		return new ThreadCarrier(Thread::staticCallbackFun);
	}

	void Thread::returnThreadCarrier(ThreadCarrier* thread)
	{
		{
			Synchronized<ArrayStack<std::unique_ptr<ThreadCarrier>>> lock(s_avaliableThreads__());
			if (s_maxIdleThreads == -1 || s_avaliableThreads__()->size() < static_cast<size_t>(s_maxIdleThreads))
			{
				s_avaliableThreads__()->push(std::unique_ptr<ThreadCarrier>(thread));
				return;
			}

		}

		s_allocatedThreads--;
		delete thread;
	}

	void Thread::staticCallbackFun(ThreadCarrier* carrier)
	{
		Synchronized<std::unordered_map<std::thread::id, Thread*>> lock(s_threadMapping__());
		auto itr = s_threadMapping__()->find(carrier->getThread()->get_id());
		if (itr != s_threadMapping__()->end())
			itr->second->callbackFun(carrier);
		s_threadMapping__()->erase(carrier->getThread()->get_id());
	}

	Volatile<std::unordered_map<std::thread::id, Thread*>>& Thread::s_threadMapping__()
	{
		static Volatile<std::unordered_map<std::thread::id, Thread*>> mapping;
		return mapping;
	}

	Volatile<ArrayStack<std::unique_ptr<ThreadCarrier>>>& Thread::s_avaliableThreads__()
	{
		static Volatile<ArrayStack<std::unique_ptr<ThreadCarrier>>> avaliable;
		return avaliable;
	}

	void Thread::cleanupThreads()
	{
		auto lock = Synchronize::lock(s_avaliableThreads__());
		for (auto& itr : *s_avaliableThreads__())
			itr.reset();
		s_avaliableThreads__()->clear();
	}

	Thread* Thread::currentThread()
	{
		Synchronized<std::unordered_map<std::thread::id, Thread*>> lock(s_threadMapping__());
		auto itr = s_threadMapping__()->find(std::this_thread::get_id());
		if (itr != s_threadMapping__()->end())
			return itr->second;
		else
			return nullptr;
	}

	void Thread::sleep(size_t millis)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(millis));
	}

}