#include <org/segames/library/debug_util.h>
#include <org/segames/library/thread/thread_carrier.h>
#include <org/segames/library/exception.h>

#include <iostream>

namespace org::segames::library
{

	ThreadCarrier::ThreadCarrier() :
		m_stop(false),
		m_callback(nullptr),
		m_callbackFun(nullptr),
		m_runnable(nullptr)
	{}

	ThreadCarrier::ThreadCarrier(ThreadCarrierCallbackFun callbackFun) :
		m_stop(false),
		m_callback(nullptr),
		m_callbackFun(callbackFun),
		m_runnable(nullptr),
		m_thread(threadStart, this)
	{}

	ThreadCarrier::ThreadCarrier(const ThreadCarrier& obj) :
		m_stop(false),
		m_callback(obj.m_callback),
		m_callbackFun(obj.m_callbackFun),
		m_runnable(obj.m_runnable),
		m_thread(threadStart, this)
	{}

	ThreadCarrier::ThreadCarrier(ThreadCarrier&& obj) noexcept :
		m_stop(obj.m_stop),
		m_callback(obj.m_callback),
		m_callbackFun(obj.m_callbackFun),
		m_runnable(obj.m_runnable),
		m_thread(std::move(obj.m_thread))
	{
		obj.m_stop = false;
		obj.m_callback = nullptr;
		obj.m_callbackFun = nullptr;
		obj.m_runnable = nullptr;
	}

	ThreadCarrier::~ThreadCarrier()
	{
		if (!m_stop)
			this->requestStop();
		if (m_thread.joinable())
		{
			try
			{
				m_thread.join();
			}
			catch (const std::system_error& e)
			{
				std::cerr << "std::system_error on thread join (" << e.code() << "): " << e.what() << std::endl;
			}

		}

	}

	void ThreadCarrier::threadLife()
	{
		while (!m_stop)
		{
			if (m_runnable)
			{
				try
				{
					m_runnable->run();
				}
				catch (const std::exception& e)
				{
					std::cerr << e.what() << std::endl;
				}

				m_runnable = nullptr;
				if (m_callback)
					m_callback->callbackFun(this);
				else
					m_callbackFun(this);
			}
			else
				m_condition.wait();
		}

	}

}