# Sources
set(SEGlibCPP_src
	"sources/org/segames/library/audio/al/al_audio_source.cpp"
	"sources/org/segames/library/audio/al/al_buffer.cpp"
	"sources/org/segames/library/audio/al/al_data_source_static.cpp"
	"sources/org/segames/library/audio/al/al_data_source_stream.cpp"
	"sources/org/segames/library/audio/al/al_exception.cpp"
	"sources/org/segames/library/audio/pcm_buffer.cpp"
	"sources/org/segames/library/audio/pcm_parser_ogg.cpp"
	"sources/org/segames/library/audio/pcm_parser_wav.cpp"
	"sources/org/segames/library/core.cpp"
	"sources/org/segames/library/date_time.cpp"
	"sources/org/segames/library/exception.cpp"
	"sources/org/segames/library/gl/fbo/gl_abstract_framebuffer.cpp"
	"sources/org/segames/library/gl/fbo/gl_framebuffer_30.cpp"
	"sources/org/segames/library/gl/fbo/gl_framebuffer_ext.cpp"
	"sources/org/segames/library/gl/fbo/gl_framebuffer_wrapper.cpp"
	"sources/org/segames/library/gl/gl_buffer.cpp"
	"sources/org/segames/library/gl/gl_core.cpp"
	"sources/org/segames/library/gl/gl_exception.cpp"
	"sources/org/segames/library/gl/gl_pointer_inf.cpp"
	"sources/org/segames/library/gl/gl_shader.cpp"
	"sources/org/segames/library/gl/nk/compat/glfw/nk_glfw_key_listener.cpp"
	"sources/org/segames/library/gl/nk/nk_font.cpp"
	"sources/org/segames/library/gl/nk/nk_instance.cpp"
	"sources/org/segames/library/gl/text/gl_font.cpp"
	"sources/org/segames/library/gl/text/gl_font_glyph.cpp"
	"sources/org/segames/library/gl/texture/gl_disc_texture.cpp"
	"sources/org/segames/library/gl/texture/gl_physical_texture_wrapper.cpp"
	"sources/org/segames/library/gl/texture/gl_s3tc_texture.cpp"
	"sources/org/segames/library/gl/texture/gl_texture.cpp"
	"sources/org/segames/library/gl/texture/gl_texture_atlaser.cpp"
	"sources/org/segames/library/glfw/glfw_window.cpp"
	"sources/org/segames/library/hashable.cpp"
	"sources/org/segames/library/io/file.cpp"
	"sources/org/segames/library/io/file_attributes.cpp"
	"sources/org/segames/library/io/image/image.cpp"
	"sources/org/segames/library/io/json/json_array.cpp"
	"sources/org/segames/library/io/json/json_boolean.cpp"
	"sources/org/segames/library/io/json/json_number.cpp"
	"sources/org/segames/library/io/json/json_object.cpp"
	"sources/org/segames/library/io/json/json_string.cpp"
	"sources/org/segames/library/io/json/json_util.cpp"
	"sources/org/segames/library/io/network/socket.cpp"
	"sources/org/segames/library/io/network/socket_streambuf.cpp"
	"sources/org/segames/library/io/network/tcp/tcp_client.cpp"
	"sources/org/segames/library/io/network/tcp/tcp_communicator.cpp"
	"sources/org/segames/library/io/network/tcp/tcp_communicator_base.cpp"
	"sources/org/segames/library/io/network/tcp/tcp_server.cpp"
	"sources/org/segames/library/math/math.cpp"
	"sources/org/segames/library/memory_tracker.cpp"
	"sources/org/segames/library/stack_trace.cpp"
	"sources/org/segames/library/thread/thread.cpp"
	"sources/org/segames/library/thread/thread_carrier.cpp"
	"sources/org/segames/library/util/color.cpp"
	"sources/org/segames/library/util/fractal_noise.cpp"
	"sources/org/segames/library/util/simplex_noise.cpp"
	"sources/org/segames/library/util/timer.cpp"
	"sources/org/segames/library/util/utf8_iterator.cpp"
	"sources/single_header_definitions.c"
	"sources/glad.c"
)

# Create library
if (SEG_API_SHARED)
	add_definitions(-DSEG_API_DLL)
	add_definitions(-DSEG_API_DLL_EXPORT)
	add_definitions(-DGLAD_GLAPI_EXPORT)
	add_definitions(-DGLAD_GLAPI_EXPORT_BUILD)
	add_library(SEGlibCPP SHARED ${SEGlibCPP_src})
else ()
	add_library(SEGlibCPP STATIC ${SEGlibCPP_src})
endif ()
target_include_directories(SEGlibCPP PUBLIC "headers")
set_target_properties(SEGlibCPP PROPERTIES LINKER_LANGUAGE CXX)