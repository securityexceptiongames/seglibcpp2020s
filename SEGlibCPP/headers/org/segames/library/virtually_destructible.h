#pragma once

namespace org::segames::library
{
			
	/*
		Interface for virtually destructible types.

		* @author	Philip Rosberg
		* @since	2020-06-18
		* @edited	2020-06-18
	*/
	class VirtuallyDestructible
	{
	public:

		/*
			Virtual destructor.
		*/
		inline virtual ~VirtuallyDestructible() {}

	};

}