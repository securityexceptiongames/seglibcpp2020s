#pragma once
#include <org/segames/library/glfw/glfw_iconification_listener.h>
#include <org/segames/library/glfw/glfw_key_listener.h>
#include <org/segames/library/glfw/glfw_listener.h>
#include <org/segames/library/glfw/glfw_mouse_button_listener.h>
#include <org/segames/library/glfw/glfw_mouse_position_listener.h>
#include <org/segames/library/glfw/glfw_window_size_listener.h>

#include <string>
#include <string_view>
#include <memory>

namespace org::segames::library
{

	/*
		A signature for a function called when creating a window for setting window creation hints.
		No parameters.
	*/
	typedef void(*GLFWwindowhintsfun)();

	/*
		A GLFW based window with a creation similar to that of a Java JFrame.

		* @author	Philip Rosberg
		* @since	2019-05-09
		* @edited	2020-06-18
	*/
	class SEG_API GLFWWindow
	{
	private:

		/*
			True if the glad OpenGL loader has been initialized.
		*/
		static bool s_glProcLoaded;

	protected:

		/*
			The window id.
		*/
		GLFWwindow* m_window;

		/*
			The window iconification listener.
		*/
		std::unique_ptr<GLFWIconificationListener> m_iconifyListener;

		/*
			The key listener.
		*/
		std::unique_ptr<GLFWKeyListener> m_keyListener;

		/*
			The mouse button listener.
		*/
		std::unique_ptr<GLFWMouseButtonListener> m_mbListener;

		/*
			The mouse position listener.
		*/
		std::unique_ptr<GLFWMousePositionListener> m_mpListener;

		/*
			The window size listener.
		*/
		std::unique_ptr<GLFWWindowSizeListener> m_sizeListener;

		/*
			Other window listeners.
		*/
		std::unordered_map<std::string, std::unique_ptr<VirtuallyDestructible>> m_listeners;

	public:

		/*
			Creates a new GLFW window with the standard hints.
		*/
		GLFWWindow();

		/*
			Creates a new GLFW window with the given creation hints.
			* @param[in] 
		*/
		explicit GLFWWindow(GLFWwindowhintsfun hints);

		/*
			Move constructor.
		*/
		GLFWWindow(GLFWWindow&&) noexcept;

		/*
			Destructor.
		*/
		inline virtual ~GLFWWindow()
		{
			if (m_window != NULL) glfwDestroyWindow(m_window);
		}

	public:

		/*
			Returns true if the window is requested to close by the user.
		*/
		inline virtual bool isCloseRequested() const
		{
			return glfwWindowShouldClose(m_window);
		}

		/*
			Returns true if the button or key of the given id is pressed.
			* @param[in] button The GLFW button/key id to check
		*/
		inline virtual bool isPressed(int button) const
		{
			if (button <= GLFW_MOUSE_BUTTON_LAST)
			{
				if (m_mbListener)
					return m_mbListener->isPressed(button);
				else
					return false;
			}
			else if (m_keyListener)
				return m_keyListener->isPressed(button);
			else
				return false;
		}

		/*
			Returns the mouse x position.
		*/
		inline virtual int getMX() const
		{
			if (m_mpListener)
				return m_mpListener->getMX();
			else
				return 0;
		}

		/*
			Returns the mouse y position.
		*/
		inline virtual int getMY() const
		{
			if (m_mpListener)
				return m_mpListener->getMY();
			else
				return 0;
		}

		/*
			Returns the current width of the window.
		*/
		inline virtual int getWidth() const
		{
			if (m_sizeListener)
				return m_sizeListener->getWidth();
			else
				return 0;
		}

		/*
			Returns the current height of the window.
		*/
		inline virtual int getHeight() const
		{
			if (m_sizeListener)
				return m_sizeListener->getHeight();
			else
				return 0;
		}

		/*
			Returns the id of the GLFW window.
		*/
		inline virtual GLFWwindow* getWindowID() const
		{
			return m_window;
		}

		/*
			Sets if the window should be visible.
			* @param[in] flag True if the window should be set to visible, otherwise false
		*/
		inline virtual void setVisible(const bool flag)
		{
			if (flag)
				glfwShowWindow(m_window);
			else
				glfwHideWindow(m_window);
		}

		/*
			Sets the size of the window.
			* @param[in] width The width to set
			* @param[in] height The height to set
		*/
		inline virtual void setSize(const int width, const int height)
		{
			glfwSetWindowSize(m_window, width, height);
		}

		/*
			Sets if the window should be in fullscreen mode, ie. use an entire monitor.
			* @param[in] flag True if the window should be set to fullscreen, otherwise false
		*/
		inline virtual void setFullscreen(const bool flag)
		{
			int w, h;
			glfwGetWindowSize(m_window, &w, &h);
			if (flag)
				glfwSetWindowMonitor(m_window, getCurrentMonitorFor(m_window), 0, 0, w, h, GLFW_DONT_CARE);
			else
				glfwSetWindowMonitor(m_window, 0, 0, 0, w, h, GLFW_DONT_CARE);
		}

		/*
			Sets the position of the window relative to the current monitor.
			* @param[in] x The x position to set
			* @param[in] y The y position to set
		*/
		inline virtual void setPosition(const int x, const int y)
		{
			glfwSetWindowPos(m_window, x, y);
		}

		/*
			Sets the position of the window to the center of the current monitor.
		*/
		inline virtual void setPositionCenter()
		{
			int w, h;
			const GLFWvidmode* vidMode = glfwGetVideoMode(getCurrentMonitorFor(m_window));
			glfwGetWindowSize(m_window, &w, &h);
			glfwSetWindowPos(m_window, (vidMode->width - w) / 2, (vidMode->height - h) / 2);
		}

		/*
			Sets the title of the window.
			* @param[in] title The title to set
		*/
		virtual void setTitle(const std::string_view& title)
		{
			glfwSetWindowTitle(m_window, title.data());
		}

		/*
			Sets the window icon. Returns true on success.
			* @param[in] iconImages Path to the icon images, good sizes include 16x16, 24x24, 32x32 and 48x48
		*/
		virtual bool setIcon(const std::initializer_list<std::string_view>& iconImages);

		/*
			Sets the given window listener and returns the last listener of this type.
			All rights, such as deallocation, for the listener is transferred to this GLFWWindow instance.
			* @param[in] list The pointer of the listener
		*/
		template<class sub_class>
		std::unique_ptr<GLFWListener<sub_class>> setListener(std::unique_ptr<GLFWListener<sub_class>>&& list)
		{
			const std::string& key = typeid(sub_class).name();
			std::unique_ptr<GLFWListener<sub_class>> retVal;

			auto& pair = m_listeners.find(key);
			if (pair != m_listeners.end())
				retVal.reset(dynamic_cast<GLFWListener<sub_class>*>(pair->second.release()));

			if (list)
			{
				list->link(m_window);
				m_listeners.insert_or_assign(key, std::move(list));
			}
			else
			{
				sub_class::detach(m_window);
				m_listeners.erase(key);
			}

			return retVal;
		}

		/*
			Sets the given window iconification listener and returns the last window iconification listener.
			All rights, such as deallocation, for the listener is transferred to this GLFWWindow instance.
			* @param[in] list The pointer of the listener
		*/
		std::unique_ptr<GLFWIconificationListener> setListener(std::unique_ptr<GLFWIconificationListener>&& list);

		/*
			Sets the given window key listener and returns the last window key listener.
			All rights, such as deallocation, for the listener is transferred to this GLFWWindow instance.
			* @param[in] list The pointer of the listener
		*/
		std::unique_ptr<GLFWKeyListener> setListener(std::unique_ptr<GLFWKeyListener>&& list);
					
		/*
			Sets the given mouse button listener and returns the last mouse button listener.
			All rights, such as deallocation, for the listener is transferred to this GLFWWindow instance.
			* @param[in] list The pointer of the listener
		*/
		std::unique_ptr<GLFWMouseButtonListener> setListener(std::unique_ptr<GLFWMouseButtonListener>&& list);

		/*
			Sets the given mouse position listener and returns the last mouse position listener.
			All rights, such as deallocation, for the listener is transferred to this GLFWWindow instance.
			* @param[in] list The pointer of the listener
		*/
		std::unique_ptr<GLFWMousePositionListener> setListener(std::unique_ptr<GLFWMousePositionListener>&& list);

		/*
			Sets the given window size listener and returns the last window size listener.
			All rights, such as deallocation, for the listener is transferred to this GLFWWindow instance.
			* @param[in] list The pointer of the listener
		*/
		std::unique_ptr<GLFWWindowSizeListener> setListener(std::unique_ptr<GLFWWindowSizeListener>&& list);

		/*
			Makes this window's context the current one for OpenGL/OpenGLES rendering.
		*/
		inline virtual void makeContextCurrent()
		{
			glfwMakeContextCurrent(m_window);
			if (!s_glProcLoaded)
				s_glProcLoaded = gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
		}

		/*
			Swaps the window buffers.
		*/
		inline virtual void swapBuffers()
		{
			glfwSwapBuffers(m_window);
		}

	public:

		/*
			Move-assignment operator.
		*/
		GLFWWindow& operator=(GLFWWindow&&) noexcept;

	public:

		/*
			Returns the current monitor of the given window.
			* @param[in] window The window to find the monitor of
		*/
		static GLFWmonitor* getCurrentMonitorFor(GLFWwindow* window);

		/*
			Sets the standard window creation hints for GLFW.
		*/
		static void setStandardCreationHints();

		/*
			Polls the GLFW events.
		*/
		inline static void pollEvents()
		{
			glfwPollEvents();
		}

		/*
			Sets the GLFW buffer swap interval, ie. 1 to enable vsync, 0 to turn it off.
			* @param[in] interval The swap interval, 1 to enable vsync, 0 to turn it off.
		*/
		inline static void setSwapInterval(const int interval)
		{
			glfwSwapInterval(interval);
		}

	};

}