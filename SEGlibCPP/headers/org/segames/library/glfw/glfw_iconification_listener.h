#pragma once
#include <org/segames/library/glfw/glfw_listener.h>
#include <org/segames/library/dllmain.h>

namespace org::segames::library
{

	/*
		A listener for detecting when a window is iconified (minimized).

		* @author	Philip Rosberg
		* @since	2019-05-11
		* @edited	2020-06-18
	*/
	class SEG_API GLFWIconificationListener :
		public GLFWListener<GLFWIconificationListener>
	{
	protected:

		/*
			True if the window is iconified.
		*/
		bool m_iconified;

	public:

		/*
			Returns true if the window is iconified.
		*/
		inline virtual bool isIconified() const
		{
			return m_iconified;
		}

		/*
			The method that is called when the listener is activated.
			* @param[in] flag True if the window is iconified
		*/
		inline virtual void invoke(bool flag)
		{
			m_iconified = flag;
		}

	protected:

		/*
			Links this listener to GLFW.
			* @param[in] window The GLFW window id
		*/
		inline virtual void linkGLFW(GLFWwindow* window) override
		{
			m_iconified = false;
			glfwSetWindowIconifyCallback(window, callback);
		}

	public:

		/*
			Unlinks the callback for the given window.
			* @param[in] window The GLFW window id
		*/
		inline static void detach(GLFWwindow* window)
		{
			glfwSetWindowIconifyCallback(window, NULL);
		}

	private:

		/*
			Callback for GLFW.
		*/
		inline static void callback(GLFWwindow* window, int flag)
		{
			auto pair = m_listeners.find(window);
			if (pair != m_listeners.end())
				pair->second->invoke(flag != 0);
		}

	};

}