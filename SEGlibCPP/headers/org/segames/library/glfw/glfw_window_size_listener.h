#pragma once
#include <org/segames/library/glfw/glfw_listener.h>
#include <org/segames/library/dllmain.h>

namespace org::segames::library
{

	/*
		A window size listener for GLFW.

		* @author	Philip Rosberg
		* @since	2019-05-08
		* @edited	2020-06-18
	*/
	class SEG_API GLFWWindowSizeListener :
		public GLFWListener<GLFWWindowSizeListener>
	{
	protected:

		/*
			Window width.
		*/
		int m_w;

		/*
			Window height.
		*/
		int m_h;

	public:

		/*
			Returns the window width.
		*/
		inline virtual int getWidth() const
		{
			return m_w;
		}

		/*
			Returns the window height.
		*/
		inline virtual int getHeight() const
		{
			return m_h;
		}

		/*
			The method that is called when the listener is activated.
			* @param[in] w The window width
			* @param[in] h The window height
		*/
		inline virtual void invoke(int w, int h)
		{
			m_w = w;
			m_h = h;
		}

	protected:

		/*
			Links this listener to GLFW.
			* @param[in] window The GLFW window id
		*/
		inline virtual void linkGLFW(GLFWwindow* window) override
		{
			glfwGetWindowSize(window, &m_w, &m_h);
			glfwSetWindowSizeCallback(window, callback);
		}

	public:

		/*
			Unlinks the callback for the given window.
			* @param[in] window The GLFW window id
		*/
		inline static void detach(GLFWwindow* window)
		{
			glfwSetWindowSizeCallback(window, NULL);
		}

	private:

		/*
			Callback for GLFW.
		*/
		inline static void callback(GLFWwindow* window, int w, int h)
		{
			auto pair = m_listeners.find(window);
			if (pair != m_listeners.end())
				pair->second->invoke(w, h);
		}

	};

}