#pragma once
#include <org/segames/library/glfw/glfw_listener.h>
#include <org/segames/library/dllmain.h>

#include <unordered_set>

namespace org::segames::library
{

	/*
		A listener for detecting key actions.

		* @author	Philip Rosberg
		* @since	2019-05-11
		* @edited	2020-06-18
	*/
	class SEG_API GLFWKeyListener :
		public GLFWListener<GLFWKeyListener>
	{
	protected:

		/*
			The active keys.
		*/
		std::unordered_set<int> m_keys;

	public:

		/*
			Returns true if the key of the given id is pressed.
			* @param[in] key The GLFW id of the key to check
		*/
		inline virtual bool isPressed(int key) const
		{
			return m_keys.find(key) != m_keys.end();
		}

		/*
			The method that is called when the listener is activated.
			* @param[in] key The keyboard key that was pressed or released
			* @param[in] scancode The system-specific scancode of the key
			* @param[in] action The key action. One of: GLFW_PRESS, GLFW_RELEASE, GLFW_REPEAT
			* @param[in] mods Bitfield describing which modifiers keys were held down
		*/
		inline virtual void invoke(int key, int scancode, int action, int mods)
		{
			if (action == GLFW_PRESS)
				m_keys.insert(key);
			else if (action == GLFW_RELEASE)
				m_keys.erase(key);
		}

	protected:

		/*
			Links this listener to GLFW.
			* @param[in] window The GLFW window id
		*/
		inline virtual void linkGLFW(GLFWwindow* window) override
		{
			m_keys.clear();
			glfwSetKeyCallback(window, callback);
		}

	public:

		/*
			Unlinks the callback for the given window.
			* @param[in] window The GLFW window id
		*/
		inline static void detach(GLFWwindow* window)
		{
			glfwSetKeyCallback(window, NULL);
		}

	private:

		/*
			Callback for GLFW.
		*/
		inline static void callback(GLFWwindow* window, int key, int scancode, int action, int mods)
		{
			auto pair = m_listeners.find(window);
			if (pair != m_listeners.end())
				pair->second->invoke(key, scancode, action, mods);
		}

	};

}