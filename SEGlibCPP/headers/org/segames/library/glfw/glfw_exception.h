#pragma once
#include <org/segames/library/exception.h>

namespace org::segames::library
{

	/*
		Exception for GLFW errors.

		* @author	Philip Rosberg
		* @since	2020-06-18
		* @edited	2020-06-18
	*/
	class GLFWException :
		public Exception
	{
	public:

		/*
			Creates a GLFW exception without message.
		*/
		inline GLFWException() :
			Exception("org::segames::library::NullPointerException", "", StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1))
		{}

		/*
			Creates a GLFW exception with the given message.
			* @param[in] message The additional message to display
		*/
		inline explicit GLFWException(const std::string_view& message) :
			Exception("org::segames::library::NullPointerException", message, StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1))
		{}

	};

}