#pragma once
#include <org/segames/library/glfw/glfw_listener.h>
#include <org/segames/library/dllmain.h>

namespace org::segames::library
{

	/*
		A listener for detecting the cursor position relative to a window.

		* @author	Philip Rosberg
		* @since	2019-05-11
		* @edited	2020-06-18
	*/
	class SEG_API GLFWMousePositionListener :
		public GLFWListener<GLFWMousePositionListener>
	{
	protected:

		/*
			Mouse x position.
		*/
		int m_mx;

		/*
			Mouse y position.
		*/
		int m_my;

	public:

		/*
			Returns the x position of the cursor relative to the window.
		*/
		inline virtual int getMX() const
		{
			return m_mx;
		}

		/*
			Returns the y position of the cursor relative to the window.
		*/
		inline virtual int getMY() const
		{
			return m_my;
		}

		/*
			The method that is called when the listener is activated.
			* @param[in] mx The mouse x position
			* @param[in] my The mouse y position
		*/
		inline virtual void invoke(double mx, double my)
		{
			m_mx = (int)mx;
			m_my = (int)my;
		}

	protected:

		/*
			Links this listener to GLFW.
			* @param[in] window The GLFW window id
		*/
		inline virtual void linkGLFW(GLFWwindow* window) override
		{
			double mx, my;
			glfwGetCursorPos(window, &mx, &my);
			invoke(mx, my);

			glfwSetCursorPosCallback(window, callback);
		}

	public:

		/*
			Unlinks the callback for the given window.
			* @param[in] window The GLFW window id
		*/
		inline static void detach(GLFWwindow* window)
		{
			glfwSetCursorPosCallback(window, NULL);
		}

	private:

		/*
			Callback for GLFW.
		*/
		inline static void callback(GLFWwindow* window, double mx, double my)
		{
			auto pair = m_listeners.find(window);
			if (pair != m_listeners.end())
				pair->second->invoke(mx, my);
		}

	};

}