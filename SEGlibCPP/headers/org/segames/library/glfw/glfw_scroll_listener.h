#pragma once
#include <org/segames/library/glfw/glfw_listener.h>
#include <org/segames/library/dllmain.h>

namespace org::segames::library
{

	/*
		A listener for detecting scroll actions.

		* @author	Philip Rosberg
		* @since	2019-08-04
		* @edited	2020-06-18
	*/
	class SEG_API GLFWScrollListener :
		public GLFWListener<GLFWScrollListener>
	{
	protected:

		/*
			The scroll x value.
		*/
		double m_scrollX;

		/*
			The scroll y value.
		*/
		double m_scrollY;

	public:

		/*
			Returns the scroll x value.
		*/
		inline virtual double getScrollX() const
		{
			return m_scrollX;
		}

		/*
			Returns the scroll y (regular) value.
		*/
		inline virtual double getScrollY() const
		{
			return m_scrollY;
		}

		/*
			The method that is called when the listener is activated.
			* @param[in] sx The scroll x value
			* @param[in] sy The scroll y value
		*/
		inline virtual void invoke(double sx, double sy)
		{
			m_scrollX = sx;
			m_scrollY = sy;
		}

	protected:

		/*
			Links this listener to GLFW.
			* @param[in] window The GLFW window id
		*/
		inline virtual void linkGLFW(GLFWwindow* window) override
		{
			m_scrollX = 0;
			m_scrollY = 0;
			glfwSetScrollCallback(window, callback);
		}

	public:

		/*
			Unlinks the callback for the given window.
			* @param[in] window The GLFW window id
		*/
		inline static void detach(GLFWwindow* window)
		{
			glfwSetScrollCallback(window, NULL);
		}

	private:

		/*
			Callback for GLFW.
		*/
		inline static void callback(GLFWwindow* window, double sx, double sy)
		{
			auto itr = m_listeners.find(window);
			if (itr != m_listeners.end())
				itr->second->invoke(sx, sy);
		}

	};

}