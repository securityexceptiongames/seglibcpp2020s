#pragma once
#include <org/segames/library/virtually_destructible.h>

#ifndef GLFW_INCLUDED
#define GLFW_DLL
#include <glad/glad.h>
#include <glfw/glfw3.h>
#define GLFW_INCLUDED
#endif

#include <unordered_map>

namespace org::segames::library
{

	/*
		Abstract super class for GLFW event listeners.

		* @author	Philip Rosberg
		* @since	2019-05-08
		* @edited	2020-06-18
	*/
	template<class sub_class>
	class GLFWListener :
		public VirtuallyDestructible
	{
	protected:

		/*
			A map with references to the various listeners.
		*/
		static std::unordered_map<GLFWwindow*, sub_class*> m_listeners;

	protected:

		/*
			True if this listener is the currently bound one.
		*/
		bool m_bound;

		/*
			The ID of the window which this listener is linked to.
		*/
		GLFWwindow* m_window;

	public:

		/*
			Creates a non-linked GLFW listener.
		*/
		inline GLFWListener() :
			m_bound(false),
			m_window(nullptr)
		{}

		/*
			Ensure no copying of raw listeners.
		*/
		GLFWListener(const GLFWListener&) = delete;

		/*
			Move constructor.
		*/
		inline GLFWListener(GLFWListener&& obj) noexcept :
			m_bound(obj.m_bound),
			m_window(obj.m_window)
		{
			obj.m_bound = false;
			obj.m_window = nullptr;
		}

		/*
			Destroys the listener and removes the map references to it.
		*/
		inline virtual ~GLFWListener()
		{
			if (m_bound)
				m_listeners.erase(m_window);
		}

	public:

		/*
			Adds this listener to the listeners map and calls linkGLFW().
			* @param[in] window The GLFW window id
		*/
		inline virtual void link(GLFWwindow* window)
		{
			m_window = window;

			auto itr = m_listeners.find(window);
			if (itr != m_listeners.end())
				itr->second->m_bound = false;
			m_listeners.insert_or_assign(window, dynamic_cast<sub_class*>(this));
			m_bound = true;

			linkGLFW(window);
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline GLFWListener& operator=(const GLFWListener&) = delete;

		/*
			Move-assignment operator.
		*/
		inline GLFWListener& operator=(GLFWListener&&) noexcept = default;

	protected:

		/*
			Links this listener to GLFW
			* @param[in] window The GLFW window id
		*/
		virtual void linkGLFW(GLFWwindow* window) = 0;

	};

	template<class sub_class>
	std::unordered_map<GLFWwindow*, sub_class*> GLFWListener<sub_class>::m_listeners;

}