#pragma once
#include <org/segames/library/glfw/glfw_listener.h>
#include <org/segames/library/util/array_list.h>
#include <org/segames/library/util/utf8_iterator.h>

namespace org::segames::library
{

	/*
		A text input listener for GLFW.

		* @author	Philip Rosberg
		* @since	2019-08-05
		* @edited	2020-06-15
	*/
	class SEG_API GLFWTextListener :
		public GLFWListener<GLFWTextListener>
	{
	protected:

		/*
			True if the listener is active and listening for input.
		*/
		bool m_active;

		/*
			The character buffer.
		*/
		ArrayList<unsigned int> m_buffer;

	public:

		/*
			Creates a GLFW text listener.
		*/
		inline GLFWTextListener() :
			GLFWListener<GLFWTextListener>(),
			m_active(false),
			m_buffer()
		{}

	public:

		/*
			Returns true if the listener is active and listening for input.
		*/
		inline virtual bool isActive() const
		{
			return m_active;
		}

		/*
			Returns the character buffer.
		*/
		inline ArrayList<unsigned int>& getBuffer()
		{
			return m_buffer;
		}

		/*
			Returns the character buffer.
		*/
		inline const ArrayList<unsigned int>& getBuffer() const
		{
			return m_buffer;
		}

		/*
			Returns the stored content of the listener as a string.
		*/
		inline virtual std::string getString() const
		{
			std::string out = "";
			for (size_t i = 0; i < m_buffer.size(); i++)
				out += UTF8Iterator::codepointToString(m_buffer.get(i));
			return out;
		}

		/*
			Sets if the listener should be active and listening for input or not.
			* @param[in] flag True if the listener should be active and listening for input.
		*/
		inline virtual void setActive(const bool flag)
		{
			m_active = flag;
		}

		/*
			The method that is called when the listener is activated.
			* @param[in] codepoint The UTF8 codepoint
		*/
		inline virtual void invoke(unsigned int codepoint)
		{
			if (m_active)
				m_buffer.add(codepoint);
		}

	protected:

		/*
			Links this listener to GLFW.
			* @param[in] window The GLFW window id
		*/
		inline virtual void linkGLFW(GLFWwindow* window) override
		{
			glfwSetCharCallback(window, callback);
		}

	public:

		/*
			Unlinks the callback for the given window.
			* @param[in] window The GLFW window id
		*/
		inline static void detach(GLFWwindow* window)
		{
			glfwSetCharCallback(window, NULL);
		}

	private:

		/*
			Callback for GLFW.
		*/
		inline static void callback(GLFWwindow* window, unsigned int codepoint)
		{
			auto pair = m_listeners.find(window);
			if (pair != m_listeners.end())
				pair->second->invoke(codepoint);
		}

	};

}