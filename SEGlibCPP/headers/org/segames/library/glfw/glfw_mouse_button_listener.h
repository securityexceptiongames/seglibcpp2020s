#pragma once
#include <org/segames/library/glfw/glfw_listener.h>
#include <org/segames/library/dllmain.h>
#include <org/segames/library/index_out_of_bounds_exception.h>
#include <org/segames/library/library.h>

namespace org::segames::library
{

	/*
		A listener for detecting when the cursor buttons are pressed.

		* @author	Philip Rosberg
		* @since	2019-05-11
		* @edited	2020-06-18
	*/
	class SEG_API GLFWMouseButtonListener :
		public GLFWListener<GLFWMouseButtonListener>
	{
	protected:

		/*
			The mouse buttons toggles.
		*/
		bool m_buttons[8];

	public:

		/*
			Returns true ifthe given mouse button is pressed.
			* @param[in] button The button id in range [0, 7]
		*/
		inline virtual bool isPressed(int button) const
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (button < 0 || button > GLFW_MOUSE_BUTTON_LAST)
				throw IndexOutOfBoundsException(button);
#endif
			return m_buttons[button];
		}

		/*
			The method that is called when the listener is activated.
			* @param[in] button The mouse button that was pressed or released
			* @param[in] action One of GLFW_PRESS of GLFW_RELEASE
			* @param[in] mods Bitfield describing which modifiers keys were held down
		*/
		inline virtual void invoke(int button, int action, int mods)
		{
			if (button >= 0 && button <= GLFW_MOUSE_BUTTON_LAST)
			{
				if (action == GLFW_RELEASE)
					m_buttons[button] = false;
				else if (mods == 0)
					m_buttons[button] = true;
			}

		}

	protected:

		/*
			Links this listener to GLFW.
			* @param[in] window The GLFW window id
		*/
		inline virtual void linkGLFW(GLFWwindow* window) override
		{
			for (int i = 0; i <= GLFW_MOUSE_BUTTON_LAST; i++)
				m_buttons[i] = false;
			glfwSetMouseButtonCallback(window, callback);
		}

	public:

		/*
			Unlinks the callback for the given window.
			* @param[in] window The GLFW window id
		*/
		inline static void detach(GLFWwindow* window)
		{
			glfwSetMouseButtonCallback(window, NULL);
		}

	private:

		/*
			Callback for GLFW.
		*/
		inline static void callback(GLFWwindow* window, int button, int action, int mods)
		{
			auto pair = m_listeners.find(window);
			if (pair != m_listeners.end())
				pair->second->invoke(button, action, mods);
		}

	};

}