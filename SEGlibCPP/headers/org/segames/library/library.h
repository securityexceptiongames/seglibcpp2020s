#pragma once
#include <type_traits>

// Compiler macros:
//	SEG_API_DLL						Enables the dll features of the API, dll-import as standard
//	SEG_API_DLL_EXPORT				Sets the dll to export
//	SEG_API_DEBUG					Enables all debug macros
//
//	SEG_API_MEMORY_TRACKING			Enables the (dynamic) memory tracker
//
//  SEG_API_GL_NO_TYPEDEFS			Disables the typedefs in the OpenGL part of the library
//	SEG_API_GL_ERROR_EXCEPTIONS		Enables OpenGL error checking to throw OpenGLException
//	SEG_API_GL_MISC_EXCEPTIONS		Enables various other debug tests to throw OpenGLException
//
//	SEG_API_NETWORK_BYTE_ORDER_BE	Switches the SEG network byte order to big-endian.	
//
//  SEG_API_MATH_NO_TYPEDEFS		Disables the typedefs in the math library
//
//	SEG_API_VECMATH_NO_TYPEDEFS		Disables the typedefs in vecmath.h
//  SEG_API_VECMATH_ZERO_OFFSET		Long double offset for floating point precision near zero
//
//	SEG_API_TIMER_SYNC_CHECK		Enables the timer synchronization time check
//
//	SEG_API_BOUNDS_CHECKING			Enables bounds checking in arrays, lists etc.

#ifdef SEG_API_DEBUG //Enables all debug macros

#ifndef SEG_API_BOUNDS_CHECKING
#define SEG_API_BOUNDS_CHECKING
#endif

#ifndef SEG_API_GL_ERROR_EXCEPTIONS
#define SEG_API_GL_ERROR_EXCEPTIONS
#endif

#ifndef SEG_API_GL_MISC_EXCEPTIONS
#define SEG_API_GL_MISC_EXCEPTIONS
#endif

#endif // !SEG_API_DEBUG

#ifndef NULL
#define NULL 0						//Null
#endif

#ifndef TRUE
#define TRUE 1						//True
#endif

#ifndef FALSE
#define FALSE 0						//False
#endif

/*
	Standard library functionallity
*/
namespace org::segames::library
{

	/*
		Casts an enum type to its underlying type.
		* @param[in] e The enum type value
	*/
	template <typename E>
	constexpr auto to_underlying(const E& e) noexcept
	{
		return static_cast<std::underlying_type_t<E>>(e);
	}

}