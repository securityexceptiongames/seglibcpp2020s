#pragma once
#include <org/segames/library/math/math.h>
#include <org/segames/library/dllmain.h>
#include <org/segames/library/string_visualizable.h>

#include <memory>
#include <cstdint>

namespace org::segames::library
{

	/*
		Implementation of simplex noise based on C++ code by Sebastien Rombauts 
		(sebastien.rombauts@gmail.com) and on the original Java code from 2012 by 
		Stefan Gustavson (stegu@itn.liu.se) and Peter Eastman (peastman@drizzle.stanford.edu).

		* @author	Philip Rosberg
		* @since	2019-05-26
		* @edited	2020-06-15
	*/
	class SEG_API SimplexNoise final :
		public StringVisualizable
	{
	private:

		/*
			The seed of the noise.
		*/
		unsigned int m_seed;

		/*
			The local permutation table.
		*/
		std::unique_ptr<uint8_t[]> m_perm;

	public:

		/*
			Creates a new simplex noise instance with a random seed.
		*/
		inline SimplexNoise() :
			SimplexNoise((unsigned int)(Math::randomf() * 0xffffffff))
		{}

		/*
			Creates a new simplex noise instance with a given seed.
			* @param[in] seed The seed to use
		*/
		inline explicit SimplexNoise(const unsigned int seed) :
			SimplexNoise(seed, 400)
		{}

		/*
			Creates a new simplex noise instance with a given seed and the given amount of random swaps of the permutation table.
			* @param[in] seed The seed to use
			* @param[in] numSwaps The amount of swaps to perform in the permutation table
		*/
		explicit SimplexNoise(const unsigned int seed, const int numSwaps);

		/*
			Copy constructor.
		*/
		inline SimplexNoise(const SimplexNoise& obj) :
			m_seed(obj.m_seed),
			m_perm(new uint8_t[256])
		{
			std::copy(obj.m_perm.get(), obj.m_perm.get() + 256, m_perm.get());
		}

		/*
			Move constructor.
		*/
		inline SimplexNoise(SimplexNoise&& obj) noexcept :
			m_seed(obj.m_seed),
			m_perm(std::move(obj.m_perm))
		{
			obj.m_seed = 0;
		}

	public:

		/*
			Returns the seed of the noise.
		*/
		inline unsigned int getSeed() const
		{
			return m_seed;
		}

		/*
			Generates a simplex noise value (1D).
			* @param[in] x The x coordinate
			* @author Sebastien Rombauts
		*/
		double noise(const double x) const;

		/*
			Generates a simplex noise value (2D).
			* @param[in] x The x coordinate
			* @param[in] y The y coordinate
			* @author Sebastien Rombauts
		*/
		double noise(const double x, const double y) const;

		/*
			Generates a simplex noise value (3D).
			* @param[in] x The x coordinate
			* @param[in] y The y coordinate
			* @param[in] z The z coordinate
			* @author Sebastien Rombauts
		*/
		double noise(const double x, const double y, const double z) const;

		/*
			Returns a string representation of the type.
		*/
		inline std::string toString() const override
		{
			return "SimplexNoise(seed=" + std::to_string(m_seed) + ")";
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline SimplexNoise& operator=(const SimplexNoise& obj)
		{
			m_seed = obj.m_seed;
			m_perm.reset(new uint8_t[256]);
			std::copy(obj.m_perm.get(), obj.m_perm.get() + 256, m_perm.get());
			return *this;
		}

		/*
			Move-assignment operator.
		*/
		inline SimplexNoise& operator=(SimplexNoise&& obj) noexcept
		{
			m_seed = obj.m_seed;
			m_perm = std::move(obj.m_perm);
			obj.m_seed = 0;
			return *this;
		}

	private:

		/*
			Helper function to hash an integer using the permutation table.
			* @param[in] i The integer value to hash
			* @author Sebastien Rombauts
		*/
		inline uint8_t hash__(const uint32_t i) const
		{
			return m_perm[static_cast<uint8_t>(i)];
		}

	private:

		/*
			Computes the largest integer value not greater than the double one.
			This method is faster than using (int32_t)std::floor(d).
			* @param[in] d The double to floor
			* @author Sebastien Rombauts
		*/
		inline static uint32_t fastFloor__(const double d)
		{
			int32_t i = static_cast<int32_t>(d);
			return (d < i) ? (i - 1) : (i);
		}

		/*
			Helper function to compute a gradients-dot-residual vector (1D).
			* @param[in] hash The hash value
			* @param[in] x The x distance to the corner
			* @author Sebastien Rombauts
		*/
		inline static double grad__(uint32_t hash, const double x)
		{
			const int32_t h = hash & 0x0F;
			double grad = 1.0 + (h & 7);
			if ((h & 8) != 0) grad = -grad;
			return (grad * x);
		}

		/*
			Helper function to compute a gradients-dot-residual vector (2D).
			* @param[in] hash The hash value
			* @param[in] x The x distance to the corner
			* @param[in] y The y distance to the corner
			* @author Sebastien Rombauts
		*/
		inline static double grad__(uint32_t hash, const double x, const double y)
		{
			const int32_t h = hash & 0x3F;
			const double u = h < 4 ? x : y;
			const double v = h < 4 ? y : x;
			return ((h & 1) ? -u : u) + ((h & 2) ? -2.0f * v : 2.0f * v);
		}

		/*
			Helper function to compute a gradients-dot-residual vector (3D).
			* @param[in] hash The hash value
			* @param[in] x The x distance to the corner
			* @param[in] y The y distance to the corner
			* @param[in] z The z distance to the corner
			* @author Sebastien Rombauts
		*/
		inline static double grad__(uint32_t hash, const double x, const double y, const double z)
		{
			const int32_t h = hash & 15;
			double u = h < 8 ? x : y;
			double v = h < 4 ? y : h == 12 || h == 14 ? x : z;
			return ((h & 1) ? -u : u) + ((h & 2) ? -v : v);
		}

	};

}