#pragma once

namespace org::segames::library
{

	/*
		Interface for array-backed types.

		* @author	Philip Rosberg
		* @since	2020-06-11
		* @edited	2020-06-11
	*/
	template<typename T, typename pos_t>
	class ArrayBacked
	{
	public:

		/*
			Returns the size of the array.
		*/
		virtual pos_t size() const = 0;

		/*
			Returns the array pointer.
		*/
		virtual T* pointer() = 0;

		/*
			Returns the (constant) array pointer.
		*/
		virtual const T* pointer() const = 0;

	};

}