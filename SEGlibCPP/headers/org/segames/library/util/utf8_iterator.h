#pragma once
#include <org/segames/library/dllmain.h>

#include <string>
#include <string_view>

namespace org::segames::library
{
			
	/*
		A class for iterating through an UTF-8 encoded byte array.
					
		* @author	Philip Rosberg
		* @since	2019-06-20
		* @edited	2019-11-07
	*/
	class SEG_API UTF8Iterator final
	{
	public:

		using value_type = unsigned int;
		using difference_type = std::string::difference_type;
		using reference = const unsigned int&;
		using pointer = const unsigned int*;

	private:
					
		/*
			The bit masks.
		*/
		enum BitMask
		{
			FIRST = 128,
			SECOND = 64,
			THIRD = 32,
			FOURTH = 16
		};

		/*
			True if the iterator has moved.
		*/
		mutable bool m_moved;

		/*
			The current UTF-8 codepoint.
		*/
		mutable unsigned int m_currentCodepoint;
					
		/*
			The character (byte) pointer.
		*/
		std::string_view::const_iterator m_itr;

	public:
				
		/*
			UTF-8 iterator constructor.
			* @param[in] src The source string
		*/
		inline explicit UTF8Iterator(const std::string_view& src) :
			UTF8Iterator(src.begin())
		{}

		/*
			UTF-8 iterator constructor.
			* @param[in] itr The string iterator
		*/
		inline explicit UTF8Iterator(const std::string_view::const_iterator& itr) :
			m_moved(true),
			m_currentCodepoint(0),
			m_itr(itr)
		{}

		/*
			Copy constructor.
		*/
		inline UTF8Iterator(const UTF8Iterator&) = default;

		/*
			Move constructor.
		*/
		inline UTF8Iterator(UTF8Iterator&&) noexcept = default;

	public:

		/*
			Copy-assignment operator.
		*/
		inline UTF8Iterator& operator=(const UTF8Iterator&) = default;

		/*
			Move-assignment operator.
		*/
		inline UTF8Iterator& operator=(UTF8Iterator&&) noexcept = default;

		/*
			Moves the iterator one positive step in UTF-8 characters.
		*/
		UTF8Iterator& operator++() noexcept;

		/*
			Moves the iterator one positive step in UTF-8 characters.
		*/
		inline UTF8Iterator operator++(int) noexcept
		{
			UTF8Iterator temp = *this;
			++(*this);
			return temp;
		}

		/*
			Moves the iterator one negative step in UTF-8 characters.
		*/
		UTF8Iterator& operator--() noexcept;

		/*
			Moves the iterator one negative step in UTF-8 characters.
		*/
		inline UTF8Iterator operator--(int) noexcept
		{
			UTF8Iterator temp = *this;
			--(*this);
			return temp;
		}

		/*
			Dereference operator, returns the UTF-8 character at the given position.
		*/
		unsigned int operator*() const;

		/*
			The equal to operator.
			* @param[in] itr The UTF-8 iterator to compare
		*/
		inline bool operator==(const UTF8Iterator& itr) const
		{
			return m_itr == itr.m_itr;
		}

		/*
			The equal to operator.
			* @param[in] itr The iterator to compare
		*/
		inline bool operator==(const std::string_view::iterator& itr) const
		{
			return m_itr == itr;
		}

		/*
			The not equal to operator.
			* @param[in] itr The UTF-8 iterator to compare
		*/
		inline bool operator!=(const UTF8Iterator& itr) const
		{
			return m_itr != itr.m_itr;
		}

		/*
			The not equal to operator.
			* @param[in] itr The iterator to compare
		*/
		inline bool operator!=(const std::string_view::iterator& itr) const
		{
			return m_itr != itr;
		}

	public:

		/*
			Converts the given codepoint to string.
			* @param[in] codepoint The UTF-8 codepoint
		*/
		inline static std::string codepointToString(const unsigned int codepoint)
		{
			std::string str;
			str.reserve(4);
			codepointToString(codepoint, str);
			return str;
		}

		/*
			Converts the given codepoint to string.
			* @param[in] codepoint The UTF-8 codepoint
			* @param[out] str The string to write to
		*/
		static void codepointToString(const unsigned int codepoint, std::string& str);

	};

}