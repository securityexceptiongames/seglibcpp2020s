#pragma once
#include <org/segames/library/util/array_iterator.h>
#include <org/segames/library/util/list.h>
#include <org/segames/library/util/native_resource.h>
#include <org/segames/library/index_out_of_bounds_exception.h>
#include <org/segames/library/invalid_value_exception.h>
#include <org/segames/library/library.h>

#include <memory>

namespace org::segames::library
{
			
	/*
		A list in the form of a dynamically growing array.
		THREADING INFO: This implementation is not synchronized.

		* @author	Philip Rosberg
		* @since	2020-06-10
		* @edited	2020-06-10
	*/
	template<typename T, typename pos_t = size_t>
	class ArrayList :
		public List<T, pos_t, ArrayIterator<T, pos_t>, ConstArrayIterator<T, pos_t>>,
		public ArrayBacked<T, pos_t>,
		public NativeResource
	{
	public:

		using iterator = typename List<T, pos_t, ArrayIterator<T, pos_t>, ConstArrayIterator<T, pos_t>>::iterator;
		using const_iterator = typename List<T, pos_t, ArrayIterator<T, pos_t>, ConstArrayIterator<T, pos_t>>::const_iterator;
				
	protected:

		/*
			The lower index of the relevant part of the array.
		*/
		pos_t m_lower;

		/*
			The upper index of the relevant part of the array.
		*/
		pos_t m_upper;

		/*
			The capacity of the backing array.
		*/
		pos_t m_capacity;

		/*
			The backing array.
		*/
		std::unique_ptr<T[]> m_array;

	public:

		/*
			Creates an empty array list.
		*/
		inline ArrayList() :
			ArrayList(0)
		{}

		/*
			Creates a list with the given initial capacity.
			@param[in] initialCapacity The initial capacity of the backing array
		*/
		inline explicit ArrayList(const pos_t& initialCapacity) :
			m_lower(0),
			m_upper(0),
			m_capacity(initialCapacity),
			m_array(initialCapacity == 0 ? nullptr : new T[initialCapacity])
		{}

		/*
			Copy-constructor.
		*/
		inline ArrayList(const ArrayList<T, pos_t>& list) :
			ArrayList(list.size())
		{
			std::copy(list.m_array.get() + list.m_lower, list.m_array.get() + list.m_upper, m_array.get());
		}

		/*
			Move-constructor.
		*/
		inline ArrayList(ArrayList<T, pos_t>&& list) noexcept :
			m_lower(list.m_lower),
			m_upper(list.m_upper),
			m_capacity(list.m_capacity),
			m_array(list.m_array.release())
		{
			list.m_lower = 0;
			list.m_upper = 0;
			list.m_capacity = 0;
		}

	public:

		/*
			Returns the number of elements in the list.
		*/
		inline virtual pos_t size() const override
		{
			return m_upper - m_lower;
		}

		/*
			Returns the array pointer.
		*/
		inline virtual T* pointer() override
		{
			return m_array.get() + m_lower;
		}

		/*
			Returns the (constant) array pointer.
		*/
		virtual const T* pointer() const override
		{
			return m_array.get() + m_lower;
		}

		/*
			Returns the standard begining iterator.
		*/
		inline virtual iterator begin() override
		{
			return iterator(0, this);
		}

		/*
			Returns the const begining iterator.
		*/
		inline virtual const_iterator begin() const override
		{
			return const_iterator(0, this);
		}

		/*
			Returns the standard end iterator.
		*/
		inline virtual iterator end() override
		{
			return iterator(size(), this);
		}

		/*
			Returns the const end iterator.
		*/
		inline virtual const_iterator end() const override
		{
			return const_iterator(size(), this);
		}
				
		/*
			Returns the number of avaliable native chars.
		*/
		inline virtual size_t nativeSize() const override
		{
			return size() * sizeof(T);
		}

		/*
			Returns the pointer to the begining of native chars.
		*/
		inline virtual char* nativePointer() override
		{
			return reinterpret_cast<char*>(pointer());
		}

		/*
			Returns the pointer to the begining of native chars.
		*/
		inline virtual const char* nativePointer() const override
		{
			return reinterpret_cast<const char*>(pointer());
		}

		/*
			Adds the given value to the list.
			COMPLEXITY: Constant O(1) on average, linear O(n) on resize.
			* @param[in] value The value to add
		*/
		inline virtual pos_t add(const T& value) override
		{
			return add__(value, std::is_copy_assignable<T>());
		}

		/*
			Adds the given value to the list.
			COMPLEXITY: Constant O(1) on average, linear O(n) on resize.
			* @param[in] value The value to add
		*/
		inline virtual pos_t add(T&& value) override
		{
			return add__(std::move(value), std::is_move_assignable<T>());
		}

		/*
			Returns true if the list is empty.
		*/
		inline virtual bool empty() const override
		{
			return size() == 0;
		}

		/*
			Returns a reference to the value at the given position.
			COMPLEXITY: Constant O(1).
			* @param[in] index The position in the list
		*/
		inline virtual T& get(const pos_t& index) override
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (index < 0 || index > size())
				throw IndexOutOfBoundsException(index);
#endif
			return m_array[index + m_lower];
		}

		/*
			Returns a reference to the value at the given position.
			COMPLEXITY: Constant O(1).
			* @param[in] index The position in the list
		*/
		inline virtual const T& get(const pos_t& index) const override
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (index < 0 || index > size())
				throw IndexOutOfBoundsException(index);
#endif
			return m_array[index + m_lower];
		}

		/*
			Inserts the given value into the list at the given position.
			COMPLEXITY: Linear O(n) on average, constant O(1) for last element and sometimes first after a removal.
			* @param[in] index The position in the list
			* @param[in] value The value to insert
		*/
		inline virtual pos_t insert(const pos_t& index, const T& value) override
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (index < 0 || index > size() + 1)
				throw IndexOutOfBoundsException(index);
#endif
			return insert__(index, value, std::is_copy_assignable<T>());
		}

		/*
			Inserts the given value into the list at the given position.
			COMPLEXITY: Linear O(n) on average, constant O(1) for last element and sometimes first after a removal.
			* @param[in] index The position in the list
			* @param[in] value The value to insert
		*/
		inline virtual pos_t insert(const pos_t& index, T&& value) override
		{

#ifdef SEG_API_BOUNDS_CHECKING
			if (index < 0 || index > size() + 1)
				throw IndexOutOfBoundsException(index);
#endif
			return insert__(index, std::move(value), std::is_move_assignable<T>());
		}

		/*
			Inserts the given value in the list at the given position.
			Returns the value that was previously located there.
			COMPLEXITY: Constant O(1).
			* @param[in] index The position in the list
			* @param[in] value The value to put in the list
		*/
		inline virtual T set(const pos_t& index, const T& value) override
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (index < 0 || index > size())
				throw IndexOutOfBoundsException(index);
#endif
			return set__(index, value, std::is_copy_assignable<T>());
		}

		/*
			Inserts the given value in the list at the given position.
			Returns the value that was previously located there.
			COMPLEXITY: Constant O(1).
			* @param[in] index The position in the list
			* @param[in] value The value to put in the list
		*/
		inline virtual T set(const pos_t& index, T&& value) override
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (index < 0 || index > size())
				throw IndexOutOfBoundsException(index);
#endif
			return set__(index, std::move(value), std::is_move_assignable<T>());
		}

		/*
			Removes the element at the given position.
			Returns the value that was located there.
			COMPLEXITY: Linear O(n) on average, constant O(1) for first and last elements.
			* @param[in] index The position in the list
		*/
		inline virtual T remove(const pos_t& index) override
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (index < 0 || index > size())
				throw IndexOutOfBoundsException(index);
#endif
			return remove__(index);
		}

		/*
			Removes all elements in the list.
			COMPLEXITY: Linear O(n) on average, constant O(1) for non-destructible types.
		*/
		inline virtual void clear() override
		{
			clear__(std::is_destructible<T>());
		}

		/*
			Resizes the backing buffer to only use as much memory as the number of elements actually takes.
			COMPLEXITY: Linear O(n).
		*/
		inline virtual void compact()
		{
			resize__(size());
		}

		/*
			Ensures that the list has atleast the given size amount of slots in the backing array.
			If size is smaller than the current capacity nothing is done. If the given size is equal
			to the current capacity the call to this method compacts the existing data towards the end.
			COMPLEXITY: Linear O(n).
			* @param[in] capacity The capacity to ensure
		*/
		inline virtual void reserve(const pos_t& capacity)
		{
			if (capacity >= m_capacity)
				resize__(capacity);
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline ArrayList<T, pos_t>& operator=(const ArrayList<T, pos_t>& list)
		{
			m_lower = list.m_lower;
			m_upper = list.m_upper;
			m_capacity = list.m_capacity;
			m_array.reset(new T[list.m_capacity]);
			std::copy(list.m_array.get() + list.m_lower, list.m_array.get() + list.m_upper, m_array.get());
			return *this;
		}

		/*
			Move-assignment operator.
		*/
		inline ArrayList<T, pos_t>& operator=(ArrayList<T, pos_t>&& list) noexcept
		{
			m_lower = list.m_lower;
			m_upper = list.m_upper;
			m_capacity = list.m_capacity;
			list.m_lower = 0;
			list.m_upper = 0;
			list.m_capacity = 0;
			m_array = std::move(list.m_array);
			return *this;
		}

	private:

		inline static const T& passThrough__(const T& value)
		{
			return value;
		}

		inline T&& relocate__(T& v, std::true_type)
		{
			return std::move(v);
		}

		inline const T& relocate__(T& v, std::false_type)
		{
			return static_cast<const T&>(v);
		}

		inline decltype(auto) relocate__(T& v)
		{
			return relocate__(v, std::is_move_assignable<T>());
		}

		void resize__(const pos_t newCapacity)
		{
			const pos_t currentSize = size();
			const pos_t moveSize = (newCapacity > currentSize ? currentSize : newCapacity);

			if (newCapacity != m_capacity)
			{
				std::unique_ptr<T[]> newArray(new T[newCapacity]);

				for (pos_t i = 0; i < moveSize; i++)
					newArray[i] = relocate__(m_array[i + m_lower]);
				std::swap(newArray, m_array);
			}
			else
				for (pos_t i = 0; i < moveSize; i++)
					m_array[i] = relocate__(m_array[i + m_lower]);

			m_lower = 0;
			m_upper = moveSize;
			m_capacity = newCapacity;
		}

		template<typename insFun_t>
		inline pos_t addOperation__(T& value, insFun_t insFun__)
		{
			if (m_upper == m_capacity)
			{
				if (size() > m_capacity * 3 / 4)
					resize__(m_capacity * 2);
				else if (m_capacity == 0)
					resize__(1);
				else
					resize__(m_capacity);
			}
			m_array[m_upper++] = insFun__(value);
			return m_upper - 1 - m_lower;
		}

		inline pos_t add__(const T& value, std::true_type)
		{
			return addOperation__(const_cast<T&>(value), passThrough__);
		}

		inline pos_t add__(const T&, std::false_type)
		{
			throw InvalidValueException("Cannot add trough copy assign a non-copy assignable type!");
		}

		inline pos_t add__(T&& value, std::true_type)
		{
			return addOperation__(value, std::move<T&>);
		}

		inline pos_t add__(T&& value, std::false_type)
		{
			return add__(static_cast<const T&>(value), std::is_copy_assignable<T>());
		}

		template<typename insFun_t>
		pos_t insertOperation__(const pos_t index, T& value, insFun_t insFun__)
		{
			if ((index + m_lower) == m_upper)
				return addOperation__(value, insFun__);
			else if (index == 0)
				if (m_lower > 0)
				{
					--m_lower;
					m_array[m_lower] = insFun__(value);
					return 0;
				}

			if (m_upper == m_capacity)
			{
				if (size() > m_capacity * 3 / 4)
					resize__(m_capacity * 2);
				else if (m_capacity == 0)
					resize__(1);
				else
					resize__(m_capacity);
			}

			const pos_t posInArray = index + m_lower;
			for (pos_t i = m_upper; i > posInArray; i--)
				m_array[i] = relocate__(m_array[i - 1]);
			++m_upper;

			m_array[posInArray] = insFun__(value);
			return index;
		}

		inline pos_t insert__(const pos_t index, const T& value, std::true_type)
		{
			return insertOperation__(index, const_cast<T&>(value), passThrough__);
		}

		inline pos_t insert__(const pos_t, const T&, std::false_type)
		{
			throw InvalidValueException("Cannot insert trough copy assign a non-copy assignable type!");
		}

		inline pos_t insert__(const pos_t index, T&& value, std::true_type)
		{
			return insertOperation__(index, value, std::move<T&>);
		}

		inline pos_t insert__(const pos_t index, T&& value, std::false_type)
		{
			return insert__(index, static_cast<const T&>(value), std::is_copy_assignable<T>());
		}

		inline T set__(const pos_t index, const T& value, std::true_type)
		{
			T retVal = relocate__(m_array[m_lower + index]);
			m_array[m_lower + index] = value;
			return retVal;
		}

		inline T set__(const pos_t, const T&, std::false_type)
		{
			throw InvalidValueException("Cannot set trough copy assign a non-copy assignable type!");
		}

		inline T set__(const pos_t index, T&& value, std::true_type)
		{
			T retVal = relocate__(m_array[m_lower + index]);
			m_array[m_lower + index] = std::move(value);
			return retVal;
		}

		inline T set__(const pos_t index, T&& value, std::false_type)
		{
			return set__(index, static_cast<const T&>(value), std::is_copy_assignable<T>());
		}

		T remove__(const pos_t index)
		{
			const pos_t posInArray = index + m_lower;
			T retVal = relocate__(m_array[posInArray]);

			if (posInArray == m_upper - 1)
				--m_upper;
			else if (index == 0)
				++m_lower;
			else if (index > (m_upper - m_lower) / 2)
			{
				for (pos_t i = posInArray; i < m_upper; i++)
					m_array[i] = relocate__(m_array[i + 1]);
				--m_upper;
			}
			else
			{
				for (pos_t i = posInArray; i > m_lower; i--)
					m_array[i] = relocate__(m_array[i - 1]);
				++m_lower;
			}

			return retVal;
		}

		inline void clear__(std::true_type)
		{
			for (pos_t i = m_lower; i < m_upper; i++)
				m_array[i] = std::move(T());
			clear__(std::false_type());
		}

		inline void clear__(std::false_type)
		{
			m_upper = m_lower;
		}

	};

}