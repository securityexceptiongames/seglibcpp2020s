#pragma once
#include <org/segames/library/util/array_backed.h>
#include <org/segames/library/index_out_of_bounds_exception.h>

namespace org::segames::library
{

	/*
		Constant array iterator.

		* @author	Philip Rosberg
		* @since	2019-03-16
		* @edited	2019-03-16
	*/
	template<typename T, typename pos_t>
	class ConstArrayIterator
	{
	public:

		using value_type = const T;
		using difference_type = pos_t;
		using reference = const T&;
		using pointer = const T*;

	protected:

		/*
			Current index in the array being observed.
		*/
		pos_t m_index;

		/*
			Reference pointer to the array being iterated.
		*/
		const ArrayBacked<T, pos_t>* m_array;

	public:

		/*
			Creates an empty iterator.
		*/
		inline ConstArrayIterator() :
			m_index(0),
			m_array(nullptr)
		{}

		/*
			Creates an iterator at the given position.
			* @param[in] index The index in the array being observed
			* @param[in] arr The array-backed type being iterated
		*/
		inline explicit ConstArrayIterator(const pos_t& index, const ArrayBacked<T, pos_t>* arr) :
			m_index(index),
			m_array(arr)
		{}

		/*
			Copy-constructor.
		*/
		inline ConstArrayIterator(const ConstArrayIterator<T, pos_t>& itr) = default;
				
		/*
			Move-constructor.
		*/
		inline ConstArrayIterator(ConstArrayIterator<T, pos_t>&& itr) noexcept = default;

	public:

		/*
			Add-assignement operator.
		*/
		inline ConstArrayIterator<T, pos_t>& operator+=(const pos_t& n) const
		{
			m_index += n;
			return *this;
		}

		/*
			Subtraction-assignement operator.
		*/
		inline ConstArrayIterator<T, pos_t>& operator-=(const pos_t& n) const
		{
			m_index -= n;
			return *this;
		}

		/*
			Increment operator.
		*/
		inline ConstArrayIterator<T, pos_t>& operator++()
		{
			++m_index;
			return *this;
		}

		/*
			Increment operator.
		*/
		inline ConstArrayIterator<T, pos_t> operator++(int)
		{
			ConstArrayIterator<T, pos_t> out = *this;
			++m_index;
			return out;
		}

		/*
			Decrement operator.
		*/
		inline ConstArrayIterator<T, pos_t>& operator--()
		{
			--m_index;
			return *this;
		}

		/*
			Decrement operator.
		*/
		inline ConstArrayIterator<T, pos_t> operator--(int)
		{
			ConstArrayIterator<T, pos_t> out = *this;
			--m_index;
			return out;
		}

		/*
			Dereference operator.
		*/
		inline const T& operator*() const
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (m_index < 0 || m_index > m_array->size())
				throw IndexOutOfBoundsException(m_index);
#endif
			return m_array->pointer()[m_index];
		}

		/*
			Pointer access operator.
		*/
		inline const T* operator->() const
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (m_index < 0 || m_index > m_array->size())
				throw IndexOutOfBoundsException(m_index);
#endif
			return &m_array->pointer()[m_index];
		}

		/*
			Random access operator.
		*/
		inline const T& operator[](const pos_t& n) const
		{
			const pos_t index = m_index + n;
#ifdef SEG_API_BOUNDS_CHECKING
			if (index < 0 || index > m_array->size())
				throw IndexOutOfBoundsException(index);
#endif
			return m_array->pointer()[index];
		}

		/*
			Copy-assignment operator.
		*/
		inline ConstArrayIterator<T, pos_t>& operator=(const ConstArrayIterator<T, pos_t>&) = default;

		/*
			Move-assignment operator.
		*/
		inline ConstArrayIterator<T, pos_t>& operator=(ConstArrayIterator<T, pos_t>&&) = default;

	public:

		/*
			Equals operator.
		*/
		inline bool operator==(const ConstArrayIterator<T, pos_t>& itr) const
		{
			return (m_index == itr.m_index) && (m_array == itr.m_array);
		}

		/*
			Not equals operator.
		*/
		inline bool operator!=(const ConstArrayIterator<T, pos_t>& itr) const
		{
			return !this->operator==(itr);
		}

		/*
			Gequals operator.
		*/
		inline bool operator>=(const ConstArrayIterator<T, pos_t>& itr) const
		{
			return (m_index >= itr.m_index) && (m_array == itr.m_array);
		}

		/*
			Lequals operator.
		*/
		inline bool operator<=(const ConstArrayIterator<T, pos_t>& itr) const
		{
			return (m_index <= itr.m_index) && (m_array == itr.m_array);
		}

		/*
			Greater-than operator.
		*/
		inline bool operator>(const ConstArrayIterator<T, pos_t>& itr) const
		{
			return (m_index > itr.m_index) && (m_array == itr.m_array);
		}

		/*
			Less-than operator.
		*/
		inline bool operator<(const ConstArrayIterator<T, pos_t>& itr) const
		{
			return (m_index < itr.m_index) && (m_array == itr.m_array);
		}

	};

	template<typename T, typename pos_t>
	class ArrayIterator :
		public ConstArrayIterator<T, pos_t>
	{
	private:

		using base__ = ConstArrayIterator<T, pos_t>;

	public:

		using value_type = T;
		using difference_type = pos_t;
		using reference = T&;
		using pointer = T*;

	public:

		/*
			Creates an empty iterator.
		*/
		inline ArrayIterator() :
			ConstArrayIterator<T, pos_t>()
		{}

		/*
			Creates an iterator at the given position.
			* @param[in] index The index in the array being observed
			* @param[in] arr The array-backed type being iterated
		*/
		inline explicit ArrayIterator(const pos_t& index, ArrayBacked<T, pos_t>* arr) :
			ConstArrayIterator<T, pos_t>(index, arr)
		{}

		/*
			Copy-constructor.
		*/
		inline ArrayIterator(const ArrayIterator<T, pos_t>& itr) = default;

		/*
			Move-constructor.
		*/
		inline ArrayIterator(ArrayIterator<T, pos_t> && itr) noexcept = default;

	public:

		/*
			Add-assignement operator.
		*/
		inline ArrayIterator<T, pos_t>& operator+=(const pos_t& n) const
		{
			base__::operator+=(n);
			return *this;
		}

		/*
			Subtraction-assignement operator.
		*/
		inline ArrayIterator<T, pos_t>& operator-=(const pos_t& n) const
		{
			base__::operator-=(n);
			return *this;
		}

		/*
			Increment operator.
		*/
		inline ArrayIterator<T, pos_t>& operator++()
		{
			base__::operator++();
			return *this;
		}

		/*
			Increment operator.
		*/
		inline ArrayIterator<T, pos_t> operator++(int)
		{
			ArrayIterator<T, pos_t> out = *this;
			base__::operator++();
			return out;
		}

		/*
			Decrement operator.
		*/
		inline ArrayIterator<T, pos_t>& operator--()
		{
			base__::operator--();
			return *this;
		}

		/*
			Decrement operator.
		*/
		inline ArrayIterator<T, pos_t> operator--(int)
		{
			ArrayIterator<T, pos_t> out = *this;
			base__::operator--();
			return out;
		}

		/*
			Dereference operator.
		*/
		inline T& operator*() const
		{
			return const_cast<reference>(base__::operator*());
		}

		/*
			Pointer access operator.
		*/
		inline const T* operator->() const
		{
			return const_cast<pointer>(base__::operator->());
		}

		/*
			Random access operator.
		*/
		inline T& operator[](const pos_t& n) const
		{
			return const_cast<reference>(base__::operator[](n));
		}

		/*
			Copy-assignment operator.
		*/
		inline ArrayIterator<T, pos_t>& operator=(const ArrayIterator<T, pos_t>&) = default;

		/*
			Move-assignment operator.
		*/
		inline ArrayIterator<T, pos_t>& operator=(ArrayIterator<T, pos_t>&&) = default;

	};

	template<typename T, typename pos_t>
	inline ConstArrayIterator<T, pos_t> operator+(const pos_t& n, const ConstArrayIterator<T, pos_t>& itr)
	{
		return ConstArrayIterator<T, pos_t>(itr.m_index + n, itr.m_array);
	}
			
	template<typename T, typename pos_t>
	inline ConstArrayIterator<T, pos_t> operator+(const ConstArrayIterator<T, pos_t>& itr, const pos_t& n)
	{
		return n + itr;
	}

	template<typename T, typename pos_t>
	inline ConstArrayIterator<T, pos_t> operator-(const pos_t& n, const ConstArrayIterator<T, pos_t>& itr)
	{
		return ConstArrayIterator<T, pos_t>(itr.m_index - n, itr.m_array);
	}

	template<typename T, typename pos_t>
	inline ConstArrayIterator<T, pos_t> operator-(const ConstArrayIterator<T, pos_t>& itr, const pos_t& n)
	{
		return n - itr;
	}

	template<typename T, typename pos_t>
	inline ArrayIterator<T, pos_t> operator+(const pos_t& n, const ArrayIterator<T, pos_t>& itr)
	{
		return ArrayIterator<T, pos_t>(itr.m_index + n, itr.m_array);
	}

	template<typename T, typename pos_t>
	inline ArrayIterator<T, pos_t> operator+(const ArrayIterator<T, pos_t>& itr, const pos_t& n)
	{
		return n + itr;
	}

	template<typename T, typename pos_t>
	inline ArrayIterator<T, pos_t> operator-(const pos_t& n, const ArrayIterator<T, pos_t>& itr)
	{
		return ArrayIterator<T, pos_t>(itr.m_index - n, itr.m_array);
	}

	template<typename T, typename pos_t>
	inline ArrayIterator<T, pos_t> operator-(const ArrayIterator<T, pos_t>& itr, const pos_t& n)
	{
		return n - itr;
	}

}