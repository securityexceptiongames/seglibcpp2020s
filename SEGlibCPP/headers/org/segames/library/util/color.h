#pragma once
#include <org/segames/library/dllmain.h>

namespace org::segames::library
{

	/*
		A class storing color information for a 32bit, 4 channel color.
		Channels are (rows):
			RGBA:		CMYK:			HSB:
			- Red		- Cyan			- Hue
			- Green		- Magenta		- Saturation
			- Blue		- Yellow		- Brightness
			- Alpha		- Black key		- Alpha

		* @author	Philip Rosberg
		* @since	2019-06-22
		* @edited	2019-11-07
	*/
	class SEG_API Color final
	{
	public:

		//RGB opaque colors

		static Color BLACK;
		static Color DARK_GRAY;
		static Color GRAY;
		static Color LIGHT_GRAY;
		static Color WHITE;
		static Color RED;
		static Color GREEN;
		static Color BLUE;
		static Color YELLOW;
		static Color CYAN;
		static Color MAGENTA;
		static Color MAROON;
		static Color OLIVE;
		static Color DARK_GREEN;
		static Color PURPLE;
		static Color TEAL;
		static Color NAVY_BLUE;
		static Color ORANGE;
		static Color BROWN;
		static Color PINK;

	protected:

		/*
			The red component.
		*/
		unsigned char m_red;

		/*
			The green component.
		*/
		unsigned char m_green;

		/*
			The blue component.
		*/
		unsigned char m_blue;

		/*
			The alpha component.
		*/
		unsigned char m_alpha;

	public:

		/*
			Creates a color without setting any information.
		*/
		inline constexpr Color() :
			Color(0.0f, 0.0f, 0.0f, 0.0f)
		{}

		/*
			Creates an opaque color with the given values.
			* @param[in] red The red component
			* @param[in] green The green component
			* @param[in] blue The blue component
		*/
		inline explicit constexpr Color(unsigned char red, unsigned char green, unsigned char blue) :
			Color(red, green, blue, 255)
		{}

		/*
			Creates a color with the given values.
			* @param[in] red The red component
			* @param[in] green The green component
			* @param[in] blue The blue component
			* @param[in] alpha The alpha component
		*/
		inline explicit constexpr Color(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha) :
			m_red(red),
			m_green(green),
			m_blue(blue),
			m_alpha(alpha)
		{}

		/*
			Creates an opaque color with the given values.
			* @param[in] red The red component
			* @param[in] green The green component
			* @param[in] blue The blue component
		*/
		inline explicit constexpr Color(float red, float green, float blue) :
			Color(red, green, blue, 1.0f)
		{}

		/*
			Creates a color with the given values.
			* @param[in] red The red component
			* @param[in] green The green component
			* @param[in] blue The blue component
			* @param[in] alpha The alpha component
		*/
		inline explicit constexpr Color(float red, float green, float blue, float alpha) :
			Color(
				static_cast<unsigned char>(red * 255),
				static_cast<unsigned char>(green * 255),
				static_cast<unsigned char>(blue * 255),
				static_cast<unsigned char>(alpha * 255)
			)
		{}
				
		/*
			Creates a color with the given data value.
			* @param[in] data The data value of the color, eg. hexcode 0x00FF00FF
		*/
		inline constexpr Color(int data) :
			Color(
				static_cast<unsigned char>((data & 0x000000FF)),
				static_cast<unsigned char>((data & 0x0000FF00) >> 8),
				static_cast<unsigned char>((data & 0x00FF0000) >> 16),
				static_cast<unsigned char>((data & 0xFF000000) >> 24)
			)
		{}

		/*
			Default copy constructor.
		*/
		inline constexpr Color(const Color&) = default;

		/*
			Default move constructor.
		*/
		inline constexpr Color(Color&&) noexcept = default;

	public:

		/*
			Returns the red value.
		*/
		inline constexpr unsigned char red() const
		{
			return m_red;
		}

		/*
			Returns the green value.
		*/
		inline constexpr unsigned char green() const
		{
			return m_green;
		}

		/*
			Returns the blue value.
		*/
		inline constexpr unsigned char blue() const
		{
			return m_blue;
		}

		/*
			Returns the alpha value.
		*/
		inline constexpr unsigned char alpha() const
		{
			return m_alpha;
		}

		/*
			Returns the red value.
		*/
		inline constexpr float getRed() const
		{
			return m_red * 0.0039215686274509803921568627451f;
		}

		/*
			Returns the green value.
		*/
		inline constexpr float getGreen() const
		{
			return m_green * 0.0039215686274509803921568627451f;
		}

		/*
			Returns the blue value.
		*/
		inline constexpr float getBlue() const
		{
			return m_blue * 0.0039215686274509803921568627451f;
		}

		/*
			Returns the alpha value.
		*/
		inline constexpr float getAlpha() const
		{
			return m_alpha * 0.0039215686274509803921568627451f;
		}

		/*
			Returns the color as an integer value.
		*/
		inline constexpr int asInteger() const
		{
			return
				(static_cast<int>(m_alpha) << 24) |
				(static_cast<int>(m_blue) << 16) |
				(static_cast<int>(m_green) << 8) |
				static_cast<int>(m_red);
		}

		/*
			Sets the red value.
			* @param[in] value The value
		*/
		inline Color& setRed(const unsigned char value)
		{
			m_red = value;
			return *this;
		}

		/*
			Sets the green value.
			* @param[in] value The value
		*/
		inline Color& setGreen(const unsigned char value)
		{
			m_green = value;
			return *this;
		}

		/*
			Sets the blue value.
			* @param[in] value The value
		*/
		inline Color& setBlue(const unsigned char value)
		{
			m_blue = value;
			return *this;
		}

		/*
			Sets the alpha value.
			* @param[in] value The value
		*/
		inline Color& setAlpha(const unsigned char value)
		{
			m_alpha = value;
			return *this;
		}

		/*
			Sets the red value.
			* @param[in] value The value
		*/
		inline Color& setRed(const float value)
		{
			m_red = value;
			return *this;
		}

		/*
			Sets the green value.
			* @param[in] value The value
		*/
		inline Color& setGreen(const float value)
		{
			m_green = value;
			return *this;
		}

		/*
			Sets the blue value.
			* @param[in] value The value
		*/
		inline Color& setBlue(const float value)
		{
			m_blue = value;
			return *this;
		}

		/*
			Sets the alpha value.
			* @param[in] value The value
		*/
		inline Color& setAlpha(const float value)
		{
			m_alpha = value;
			return *this;
		}

	public:

		/*
			Set operator override.
		*/
		inline Color& operator=(const Color& obj)
		{
			m_red = obj.red();
			m_green = obj.green();
			m_blue = obj.blue();
			m_alpha = obj.alpha();
			return *this;
		}

	public:

		/*
			Converts the given RGB color to CMYK
			* @param[in] color The color to convert
		*/
		static Color RGBtoCMYK(const Color& color);

		/*
			Converts the given CMYK color to RGB
			* @param[in] color The color to convert
		*/
		static Color CMYKtoRGB(const Color& color);

	};

}