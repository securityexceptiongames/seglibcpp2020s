#pragma once
#include <org/segames/library/util/array_list.h>
#include <org/segames/library/util/stack.h>
#include <org/segames/library/util/iterable.h>
#include <org/segames/library/invalid_operation_exception.h>

namespace org::segames::library
{
			
	/*
		Implementation of an array backed stack.
		THREADING INFO: This implementation is not synchronized.

		* @author	Philip Rosberg
		* @since	2020-06-11
		* @edited	2020-06-11
	*/
	template<typename T, typename pos_t = size_t>
	class ArrayStack :
		public Stack<T>,
		public ArrayBacked<T, pos_t>,
		public Iterable<ArrayIterator<T, pos_t>, ConstArrayIterator<T, pos_t>>
	{
	public:

		using iterator = typename ArrayList<T, pos_t>::iterator;
		using const_iterator = typename ArrayList<T, pos_t>::const_iterator;

	protected:

		/*
			Backing array list.
		*/
		ArrayList<T, pos_t> m_list;

	public:

		/*
			Creates an empty stack.
		*/
		inline ArrayStack() :
			ArrayStack(0)
		{}
				
		/*
			Creates a stack with the given intiial capacity.
			* @param[in] initialCapacity The initial capacity of the backing array
		*/
		inline explicit ArrayStack(const pos_t& initialCapacity) :
			m_list(initialCapacity)
		{}

		/*
			Copy-constructor.
		*/
		inline ArrayStack(const ArrayStack<T, pos_t>&) = default;

		/*
			Move-constructor.
		*/
		inline ArrayStack(ArrayStack<T, pos_t>&&) noexcept = default;

	public:

		/*
			Returns the number of elements in the list.
		*/
		inline virtual pos_t size() const override
		{
			return m_list.size();
		}

		/*
			Returns the array pointer.
		*/
		inline virtual T* pointer() override
		{
			return m_list.pointer();
		}

		/*
			Returns the (constant) array pointer.
		*/
		virtual const T* pointer() const override
		{
			return m_list.pointer();
		}

		/*
			Returns the standard begining iterator.
		*/
		inline virtual iterator begin() override
		{
			return m_list.begin();
		}

		/*
			Returns the const begining iterator.
		*/
		inline virtual const_iterator begin() const override
		{
			return m_list.begin();
		}

		/*
			Returns the standard end iterator.
		*/
		inline virtual iterator end() override
		{
			return m_list.end();
		}

		/*
			Returns the const end iterator.
		*/
		inline virtual const_iterator end() const override
		{
			return m_list.end();
		}

		/*
			Returns true if the stack is empty.
		*/
		inline virtual bool empty() const override
		{
			return m_list.empty();
		}

		/*
			Returns the top value in the stack.
			COMPLEXITY: Constant O(1).
		*/
		inline virtual T& peek() override
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (empty())
				throw InvalidOperationException("Cannot peak empty stack!");
#endif
			return m_list.get(m_list.size() - 1);
		}

		/*
			Returns the top value in the stack.
			COMPLEXITY: Constant O(1).
		*/
		inline virtual const T& peek() const override
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (empty())
				throw InvalidOperationException("Cannot peak empty stack!");
#endif
			return m_list.get(m_list.size() - 1);
		}

		/*
			Removes and returns the top value in the stack.
			COMPLEXITY: Constant O(1).
		*/
		inline virtual T pop() override
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (empty())
				throw InvalidOperationException("Cannot pop empty stack!");
#endif
			return m_list.remove(m_list.size() - 1);
		}

		/*
			Adds the given value to the top of the stack.
			COMPLEXITY: Constant O(1) on average, linear O(n) on resize.
			* @param[in] value The value to add
		*/
		inline virtual void push(const T& value) override
		{
			m_list.add(value);
		}

		/*
			Adds the given value to the top of the stack.
			COMPLEXITY: Constant O(1) on average, linear O(n) on resize.
			* @param[in] value The value to add
		*/
		inline virtual void push(T&& value) override
		{
			m_list.add(std::move(value));
		}

		/*
			Removes all content from the stack.
			COMPLEXITY: Linear O(n) on average, constant O(1) for non-destructible types.
		*/
		inline virtual void clear() override
		{
			m_list.clear();
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline ArrayStack<T, pos_t>& operator=(const ArrayStack<T, pos_t>&) = default;

		/*
			Move-assignment operator.
		*/
		inline ArrayStack<T, pos_t>& operator=(ArrayStack<T, pos_t>&&) = default;

	};

}