#pragma once
#include <org/segames/library/util/timer.h>

#include <random>

namespace org::segames::library
{

	/*
		A random number generator wrapper, template should either be "float", "double" or "long double".

		* @author	Philip Rosberg
		* @since	2018-09-21
		* @edited	2020-06-11
	*/
	template<typename T> 
	class Random
	{
	private:

		/*
			The random number generator
		*/
		std::default_random_engine m_generator;

		/*
			The distribution control for the generator
		*/
		std::uniform_real_distribution<T> m_distribution;

	public:

		/*
			Creates a random number generator using the time as the seed.
		*/
		inline Random() :
			Random(static_cast<unsigned int>(Timer::milliTime()))
		{}

		/*
			Creates a random number generator using the given seed.
			* @param[in] seed The seed to use
		*/
		inline explicit Random(const unsigned int seed) :
			m_distribution(0, 1)
		{
			m_generator.seed(seed);
		}

		/*
			Copy-constructor.
		*/
		inline Random(const Random<T>&) = default;

		/*
			Move-constructor.
		*/
		inline Random(Random<T>&&) noexcept = default;

	public:

		/*
			Returns the next random value in the range [0, 1).
		*/
		inline virtual T random()
		{
			return m_distribution(m_generator);
		}

		/*
			Returns a random int in the given range [min, max].
			* @param[in] min The minimum value
			* @param[in] max The maximum value
		*/
		inline virtual int randomInt(int min, int max)
		{
			return (int)(random() * (max - min + 1) + min);
		}

	};

}