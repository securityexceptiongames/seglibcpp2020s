#pragma once
#include <org/segames/library/dllmain.h>

#include <cstddef>

namespace org::segames::library
{

	/*
		A simple timer class for time measurements and primitive frame synchronization.
		Note! Only one of time(), deltaTime() or sync() should be used with the
		same Timer instance.

		* @author	Philip Rosberg
		* @since	2018-06-08
		* @edited	2020-06-11
	*/
	class SEG_API Timer final
	{
	protected:

		/*
			The amount of updates per second.
		*/
		double m_updateRate;

		/*
			The time in seconds between each update.
		*/
		double m_updateTime;

		/*
			The last measured time, ie. the stored time used to measure delta time.
		*/
		size_t m_lastTime;

	public:

		/*
			Creates a timer without time limit.
		*/
		inline Timer() :
			m_updateRate(0),
			m_updateTime(0),
			m_lastTime(-1)
		{}

		/*
			Creates a timer using the given update rate.
			* @param[in] updateRate The rate at which the timer will consider it time to update, negative numbers will make time() allways return false
		*/
		inline explicit Timer(const double updateRate) :
			m_updateRate(updateRate),
			m_updateTime(updateRate != 0 ? 1.0 / updateRate : 0),
			m_lastTime(-1)
		{}

		/*
			Default copy constructor.
		*/
		inline Timer(const Timer&) = default;

		/*
			Default move constructor.
		*/
		inline Timer(Timer&&) noexcept = default;

	public:

		/*
			Returns the timer update rate, in updates per second.
		*/
		inline double getUpdateRate() const noexcept
		{
			return m_updateRate;
		}

		/*
			Returns the time per update
		*/
		inline double getUpdateTime() const noexcept
		{
			return m_updateTime;
		}

		/*
			Sets the update rate of the timer.
			* @param[in] updateRate The rate at which the timer will consider it time to update, negative numbers will make time() allways return false
		*/
		inline void setUpdateRate(const double updateRate) noexcept
		{
			m_updateRate = updateRate;
			m_updateTime = (updateRate != 0 ? 1.0 / updateRate : 0);
		}

		/*
			Returns true if the time of one update has passed.
		*/
		bool time();

		/*
			Returns the time difference in seconds between this call and the last.
		*/
		double deltaTime();

		/*
			Sleeps this thread to synchronize it with the update rate.
		*/
		void sync();

	public:

		/*
			Returns the time since epoch in nanoseconds, immitating System.nanoTime()
			from Java.
		*/
		static size_t nanoTime();

		/*
			Returns the time since epoch in microseconds.
		*/
		static size_t microTime();

		/*
			Returns the time since epoch in milliseconds.
		*/
		static size_t milliTime();

	};

}