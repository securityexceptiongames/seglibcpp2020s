#pragma once

namespace org::segames::library
{

	/*
		Super interface for (forward) iterable type.

		* @author	Philip Rosberg
		* @since	2020-06-11
		* @edited	2020-06-11
	*/
	template<typename itr_t, typename citr_t>
	class Iterable
	{
	public:

		/*
			Returns the standard begining iterator.
		*/
		virtual itr_t begin() = 0;

		/*
			Returns the const begining iterator.
		*/
		virtual citr_t begin() const = 0;

		/*
			Returns the standard end iterator.
		*/
		virtual itr_t end() = 0;

		/*
			Returns the const end iterator.
		*/
		virtual citr_t end() const = 0;
				
	};

}