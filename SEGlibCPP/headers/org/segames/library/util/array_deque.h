#pragma once
#include <org/segames/library/util/array_list.h>
#include <org/segames/library/util/queue.h>
#include <org/segames/library/util/iterable.h>
#include <org/segames/library/util/native_resource.h>
#include <org/segames/library/invalid_operation_exception.h>

namespace org::segames::library
{

	/*
		Implementation of an array backed double-ended queue.
		THREADING INFO: This implementation is not synchronized.

		* @author	Philip Rosberg
		* @since	2020-06-11
		* @edited	2020-06-11
	*/
	template<typename T, typename pos_t = size_t>
	class ArrayDeque :
		public Queue<T>,
		public ArrayBacked<T, pos_t>,
		public Iterable<ArrayIterator<T, pos_t>, ConstArrayIterator<T, pos_t>>,
		public NativeResource
	{
	public:

		using iterator = typename ArrayList<T, pos_t>::iterator;
		using const_iterator = typename ArrayList<T, pos_t>::const_iterator;

	protected:

		/*
			Backing array list.
		*/
		ArrayList<T, pos_t> m_list;

	public:

		/*
			Creates an empty array deque.
		*/
		inline ArrayDeque() :
			m_list()
		{}

		/*
			Creates a queue with the given intiial capacity.
			* @param[in] initialCapacity The initial capacity of the backing array
		*/
		inline explicit ArrayDeque(const pos_t& initialCapacity) :
			m_list(initialCapacity)
		{}

		/*
			Copy-constructor.
		*/
		inline ArrayDeque(const ArrayDeque<T, pos_t>&) = default;

		/*
			Move-constructor.
		*/
		inline ArrayDeque(ArrayDeque<T, pos_t>&&) noexcept = default;

	public:

		/*
			Returns the number of elements in the list.
		*/
		inline virtual pos_t size() const override
		{
			return m_list.size();
		}

		/*
			Returns the array pointer.
		*/
		inline virtual T* pointer() override
		{
			return m_list.pointer();
		}

		/*
			Returns the (constant) array pointer.
		*/
		virtual const T* pointer() const override
		{
			return m_list.pointer();
		}

		/*
			Returns the standard begining iterator.
		*/
		inline virtual iterator begin() override
		{
			return m_list.begin();
		}

		/*
			Returns the const begining iterator.
		*/
		inline virtual const_iterator begin() const override
		{
			return m_list.begin();
		}

		/*
			Returns the standard end iterator.
		*/
		inline virtual iterator end() override
		{
			return m_list.end();
		}

		/*
			Returns the const end iterator.
		*/
		inline virtual const_iterator end() const override
		{
			return m_list.end();
		}

		/*
			Returns the number of avaliable native chars.
		*/
		inline virtual size_t nativeSize() const override
		{
			return m_list.nativeSize();
		}

		/*
			Returns the pointer to the begining of native chars.
		*/
		inline virtual char* nativePointer() override
		{
			return m_list.nativePointer();
		}

		/*
			Returns the pointer to the begining of native chars.
		*/
		inline virtual const char* nativePointer() const override
		{
			return m_list.nativePointer();
		}

		/*
			Returns true if the queue is empty.
		*/
		inline virtual bool empty() const override
		{
			return size() == 0;
		}

		/*
			Returns the first value in the queue.
		*/
		inline virtual T& peek() override
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (empty())
				throw InvalidOperationException("Cannot peak empty queue!");
#endif
			return m_list.get(0);
		}

		/*
			Returns the first value in the queue.
		*/
		inline virtual const T& peek() const override
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (empty())
				throw InvalidOperationException("Cannot peak empty queue!");
#endif
			return m_list.get(0);
		}

		/*
			Removes the first element in the queue and returns the value.
		*/
		inline virtual T dequeue() override
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (empty())
				throw InvalidOperationException("Cannot dequeue empty queue!");
#endif
			return m_list.remove(0);
		}

		/*
			Adds the given value to the back of the queue.
			* @param[in] value The value to add
		*/
		inline virtual void enqueue(const T& value) override
		{
			m_list.add(value);
		}

		/*
			Adds the given value to the back of the queue.
			* @param[in] value The value to add
		*/
		inline virtual void enqueue(T&& value) override
		{
			m_list.add(std::move(value));
		}

		/*
			Removes all content from the queue.
		*/
		inline virtual void clear() override
		{
			m_list.clear();
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline ArrayDeque<T, pos_t>& operator=(const ArrayDeque<T, pos_t>&) = default;

		/*
			Move-assignment operator.
		*/
		inline ArrayDeque<T, pos_t>& operator=(ArrayDeque<T, pos_t>&&) = default;

	};

}