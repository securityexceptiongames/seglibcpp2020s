#pragma once
#include <cstddef>

namespace org::segames::library
{
			
	/*
		An interface for a native (char) resource.

		* @author	Philip Rosberg
		* @since	2020-06-11
		* @edited	2020-06-11
	*/
	class NativeResource
	{
	public:

		/*
			Returns the number of avaliable native chars.
		*/
		virtual size_t nativeSize() const = 0;
				
		/*
			Returns the pointer to the begining of native chars.
		*/
		virtual char* nativePointer() = 0;

		/*
			Returns the pointer to the begining of native chars.
		*/
		virtual const char* nativePointer() const = 0;

	};

}