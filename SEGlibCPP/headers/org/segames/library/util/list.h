#pragma once
#include <org/segames/library/util/iterable.h>

namespace org::segames::library
{

	/*
		Interface for a generic list type.

		* @author	Philip Rosberg
		* @since	2020-06-10
		* @edited	2020-06-10
	*/
	template<typename T, typename pos_t, typename itr_t, typename citr_t>
	class List :
		public Iterable<itr_t, citr_t>
	{
	public:

		using iterator = itr_t;
		using const_iterator = citr_t;

	public:

		/*
			Returns the standard begining iterator.
		*/
		virtual iterator begin() override = 0;

		/*
			Returns the const begining iterator.
		*/
		virtual const_iterator begin() const override = 0;

		/*
			Returns the standard end iterator.
		*/
		virtual iterator end() override = 0;

		/*
			Returns the const end iterator.
		*/
		virtual const_iterator end() const override = 0;
				
		/*
			Adds the given value to the list.
			* @param[in] value The value to add
		*/
		virtual pos_t add(const T& value) = 0;

		/*
			Adds the given value to the list.
			* @param[in] value The value to add
		*/
		virtual pos_t add(T&& value) = 0;

		/*
			Returns true if the list is empty.
		*/
		virtual bool empty() const = 0;

		/*
			Returns a reference to the value at the given position.
			* @param[in] index The position in the list
		*/
		virtual T& get(const pos_t& index) = 0;
				
		/*
			Returns a reference to the value at the given position.
			* @param[in] index The position in the list
		*/
		virtual const T& get(const pos_t& index) const = 0;

		/*
			Inserts the given value into the list at the given position.
			* @param[in] index The position in the list
			* @param[in] value The value to insert
		*/
		virtual pos_t insert(const pos_t& index, const T& value) = 0;

		/*
			Inserts the given value into the list at the given position.
			* @param[in] index The position in the list
			* @param[in] value The value to insert
		*/
		virtual pos_t insert(const pos_t& index, T&& value) = 0;

		/*
			Inserts the given value in the list at the given position.
			Returns the value that was previously located there.
			* @param[in] index The position in the list
			* @param[in] value The value to put in the list
		*/
		virtual T set(const pos_t& index, const T& value) = 0;

		/*
			Inserts the given value in the list at the given position.
			Returns the value that was previously located there.
			* @param[in] index The position in the list
			* @param[in] value The value to put in the list
		*/
		virtual T set(const pos_t& index, T&& value) = 0;
				
		/*
			Removes the element at the given position.
			Returns the value that was located there.
			* @param[in] index The position in the list
		*/
		virtual T remove(const pos_t& index) = 0;

		/*
			Removes all elements in the list.
		*/
		virtual void clear() = 0;

	};

}