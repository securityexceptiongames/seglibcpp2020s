#pragma once
#include <org/segames/library/util/array_iterator.h>
#include <org/segames/library/util/iterable.h>
#include <org/segames/library/util/native_resource.h>
#include <org/segames/library/index_out_of_bounds_exception.h>
#include <org/segames/library/invalid_value_exception.h>
#include <org/segames/library/library.h>

#include <memory>

namespace org::segames::library
{

	/*
		A dynamically allocated static sized array.
		THREADING INFO: This implementation is not synchronized.

		* @author	Philip Rosberg
		* @since	2020-06-11
		* @edited	2020-06-11
	*/
	template<typename T, typename pos_t = size_t>
	class Array :
		public ArrayBacked<T, pos_t>,
		public Iterable<ArrayIterator<T, pos_t>, ConstArrayIterator<T, pos_t>>,
		public NativeResource
	{
	public:

		using iterator = ArrayIterator<T, pos_t>;
		using const_iterator = ConstArrayIterator<T, pos_t>;

	protected:

		/*
			The number of elements in the array.
		*/
		pos_t m_capacity;

		/*
			The backing array.
		*/
		std::unique_ptr<T[]> m_array;

	public:

		/*
			Creates an empty array.
		*/
		inline Array() :
			Array(0)
		{}

		/*
			Creates an array of the given capacity.
			* @param[in] capacity The capacity of the array
		*/
		inline explicit Array(const pos_t& capacity) :
			m_capacity(capacity),
			m_array(capacity == 0 ? nullptr : new T[capacity])
		{}

		/*
			Copy-constructor.
		*/
		inline Array(const Array<T, pos_t>& arr) :
			m_capacity(arr.m_capacity),
			m_array(arr.m_capacity == 0 ? nullptr : new T[arr.m_capacity])
		{
			std::copy(arr.m_array.get(), arr.m_array.get() + m_capacity, m_array.get());
		}

		/*
			Move-constructor.
		*/
		inline Array(Array<T, pos_t>&& arr) noexcept :
			m_capacity(arr.m_capacity),
			m_array(arr.m_array.release())
		{
			arr.m_capacity = 0;
		}

	public:

		/*
			Returns the number of elements in the array.
		*/
		inline virtual pos_t size() const override
		{
			return m_capacity;
		}

		/*
			Returns the array pointer.
		*/
		inline virtual T* pointer() override
		{
			return m_array.get();
		}

		/*
			Returns the (constant) array pointer.
		*/
		virtual const T* pointer() const override
		{
			return m_array.get();
		}

		/*
			Returns the standard begining iterator.
		*/
		inline virtual iterator begin() override
		{
			return iterator(0, this);
		}

		/*
			Returns the const begining iterator.
		*/
		inline virtual const_iterator begin() const override
		{
			return const_iterator(0, this);
		}

		/*
			Returns the standard end iterator.
		*/
		inline virtual iterator end() override
		{
			return iterator(size(), this);
		}

		/*
			Returns the const end iterator.
		*/
		inline virtual const_iterator end() const override
		{
			return const_iterator(size(), this);
		}

		/*
			Returns the number of avaliable native chars.
		*/
		inline virtual size_t nativeSize() const override
		{
			return size() * sizeof(T);
		}

		/*
			Returns the pointer to the begining of native chars.
		*/
		inline virtual char* nativePointer() override
		{
			return reinterpret_cast<char*>(m_array.get());
		}

		/*
			Returns the pointer to the begining of native chars.
		*/
		inline virtual const char* nativePointer() const override
		{
			return reinterpret_cast<const char*>(m_array.get());
		}

	public:

		/*
			Returns a reference to the element at the given index.
			* @param[in] index The index of the element
		*/
		inline T& operator[](const pos_t& index)
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (index < 0 || index > size())
				throw IndexOutOfBoundsException(index);
#endif
			return m_array[index];
		}

		/*
			Returns a reference to the element at the given index.
			* @param[in] index The index of the element
		*/
		inline const T& operator[](const pos_t& index) const
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (index < 0 || index > size())
				throw IndexOutOfBoundsException(index);
#endif
			return m_array[index];
		}

		/*
			Copy-assignment operator.
		*/
		inline Array<T, pos_t>& operator=(const Array<T, pos_t>& arr)
		{
			m_capacity = arr.m_capacity;
			m_array.reset(new T[arr.m_capacity]);
			std::copy(arr.m_array.get(), arr.m_array.get() + m_capacity, m_array.get());
			return *this;
		}

		/*
			Move-assignment operator.
		*/
		inline Array<T, pos_t>& operator=(Array<T, pos_t>&& arr)
		{
			m_capacity = arr.m_capacity;
			m_array = std::move(arr.m_array);
			arr.m_capacity = 0;
			return *this;
		}

	};

}