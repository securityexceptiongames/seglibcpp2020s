#pragma once
#include <org/segames/library/util/array_iterator.h>
#include <org/segames/library/util/iterable.h>
#include <org/segames/library/util/native_resource.h>
#include <org/segames/library/index_out_of_bounds_exception.h>
#include <org/segames/library/invalid_value_exception.h>
#include <org/segames/library/library.h>

#include <memory>

namespace org::segames::library
{

	/*
		A dynamically allocated static sized two-dimensional array.
		THREADING INFO: This implementation is not synchronized.

		* @author	Philip Rosberg
		* @since	2021-03-12
		* @edited	2021-03-12
	*/
	template<typename T, typename pos_t = size_t>
	class Array2D :
		public ArrayBacked<T, pos_t>,
		public NativeResource
	{
	private:
				
		/*
			Auxilliary class to ensure proper bounds checking for double [][] operations.

			* @author	Philip Rosberg
			* @since	2021-03-12
			* @edited	2021-03-12
		*/
		class CheckedColumn final
		{
		private:

			/*
				The column index.
			*/
			pos_t m_x;
					
			/*
				Reference to the 2D array.
			*/
			Array2D& m_arr;

		public:

			/*
				Creates a checked column.
				* @param[in] x The column index
				* @param[in] arr Reference to the host array
			*/
			inline CheckedColumn(const pos_t& x, Array2D& arr) :
				m_x(x),
				m_arr(arr)
			{}

		public:

			/*
				Indexing operator for row lookup.
				* @param[in] y The row index
			*/
			inline T& operator[](const pos_t& y)
			{
#ifdef SEG_API_BOUNDS_CHECKING
				if (y < 0 || y >= m_arr.height())
					throw IndexOutOfBoundsException(y);
#endif
				return m_arr.m_array[y * m_arr.width() + m_x];
			}

			/*
				Indexing operator for row lookup.
				* @param[in] y The row index
			*/
			inline const T& operator[](const pos_t& y) const
			{
#ifdef SEG_API_BOUNDS_CHECKING
				if (y < 0 || y >= m_arr.height())
					throw IndexOutOfBoundsException(y);
#endif
				return m_arr.m_array[y * m_arr.width() + m_x];
			}

		};

	protected:

		/*
			The amount of elements in the x-direction.
		*/
		pos_t m_width;

		/*
			The amount of elements in the y-direction.
		*/
		pos_t m_height;

		/*
			The backing array.
		*/
		std::unique_ptr<T[]> m_array;

	public:

		/*
			Creates an empty 2D array.
		*/
		inline Array2D() :
			Array2D(0, 0)
		{}

		/*
			Creates a 2D array of the given width (x elements) and height (y elements).
			* @param[in] width The width (x element count)
			* @param[in] height The height (y element count)
		*/
		inline explicit Array2D(const pos_t& width, const pos_t& height) :
			m_width(width),
			m_height(height),
			m_array(width == 0 && height == 0 ? nullptr : new T[width*height])
		{}

		/*
			Copy-constructor.
		*/
		inline Array2D(const Array2D<T, pos_t>& arr) :
			m_width(arr.m_width),
			m_height(arr.m_height),
			m_array(arr.size() == 0 ? nullptr : new T[arr.size()])
		{
			std::copy(arr.m_array.get(), arr.m_array.get() + arr.size(), m_array.get());
		}

		/*
			Move-constructor.
		*/
		inline Array2D(Array2D<T, pos_t>&& arr) noexcept :
			m_width(arr.m_width),
			m_height(arr.m_height),
			m_array(std::move(arr.m_array))
		{
			arr.m_width = 0;
			arr.m_height = 0;
		}

	public:

		/*
			Returns the total number of elements in the array.
		*/
		inline virtual pos_t size() const override
		{
			return m_width * m_height;
		}

		/*
			Returns the array pointer.
		*/
		inline virtual T* pointer() override
		{
			return m_array.get();
		}

		/*
			Returns the (constant) array pointer.
		*/
		virtual const T* pointer() const override
		{
			return m_array.get();
		}

		/*
			Returns the number of avaliable native chars.
		*/
		inline virtual size_t nativeSize() const override
		{
			return size() * sizeof(T);
		}

		/*
			Returns the pointer to the begining of native chars.
		*/
		inline virtual char* nativePointer() override
		{
			return reinterpret_cast<char*>(m_array.get());
		}

		/*
			Returns the pointer to the begining of native chars.
		*/
		inline virtual const char* nativePointer() const override
		{
			return reinterpret_cast<const char*>(m_array.get());
		}

		/*
			Returns the number of elements in the x-direction.
		*/
		inline virtual pos_t width() const
		{
			return m_width;
		}

		/*
			Returns the number of elements in the y-direction.
		*/
		inline virtual pos_t height() const
		{
			return m_height;
		}

	public:

		/*
			Returns a bounds-checked indexer.
			* @param[in] x The x position in the array
		*/
		inline CheckedColumn operator[](const pos_t& x)
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (x < 0 || x >= width())
				throw IndexOutOfBoundsException(x);
#endif
			return CheckedColumn(x, *this);
		}

		/*
			Returns a bounds-checked indexer.
			* @param[in] x The x position in the array
		*/
		inline const CheckedColumn operator[](const pos_t& x) const
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (x < 0 || x >= width())
				throw IndexOutOfBoundsException(x);
#endif
			return CheckedColumn(x, const_cast<Array2D&>(*this));
		}

		/*
			Copy-assignment operator.
		*/
		inline Array2D<T, pos_t>& operator=(const Array2D<T, pos_t>& arr)
		{
			m_width = arr.m_width;
			m_height = arr.m_height;
			m_array.reset(new T[arr.size()]);
			std::copy(arr.m_array.get(), arr.m_array.get() + arr.size(), m_array.get());
			return *this;
		}

		/*
			Move-assignment operator.
		*/
		inline Array2D<T, pos_t>& operator=(Array2D<T, pos_t>&& arr)
		{
			m_width = arr.m_width;
			m_height = arr.m_height;
			m_array = std::move(arr.m_array);
			arr.m_width = 0;
			arr.m_height = 0;
			return *this;
		}

	};

}