#pragma once
#include <org/segames/library/util/array_list.h>

#include <string>
#include <string_view>
#include <iostream>

namespace org::segames::library
{

	/*
		Class/Namespace for string utility related functions.

		* @author	Philip Rosberg
		* @since	2019-05-11
		* @since	2020-06-15
	*/
	class SEG_API StringUtil final
	{
	public:

		/*
			Returns true if the given source string starts with the given expression.
			* @param[in] src The source string
			* @param[in] expr The expression to check for
		*/
		inline static bool startsWith(const std::string_view& src, const std::string_view& expr)
		{
			return src.compare(0, expr.size(), expr) == 0;
		}

		/*
			Returns true if the given source string ends with the given expression.
			* @param[in] src The source string
			* @param[in] expr The expression to check for
		*/
		inline static bool endsWith(const std::string_view& src, const std::string_view& expr)
		{
			return src.compare(src.size() - expr.size(), expr.size(), expr) == 0;
		}

		/*
			Concatenates the strings from the given collection of strings.
			* @param[in] first The begining iterator or pointer to a list/array of strings
			* @param[in] last The ending iterator or pointer
		*/
		template<typename ItrBeg, typename ItrEnd>
		inline static std::string concat(const ItrBeg& first, const ItrEnd& last)
		{
			size_t size = 0;
			for (ItrBeg itr = first; itr != last; itr++)
				size += (*itr).size();

			std::string out;
			out.reserve(size);
			for (ItrBeg itr = first; itr != last; itr++)
				out.append(*itr);
			return out;
		}

		/*
			Concatenates the strings from the given collection of strings and separates them by the given separator.
			* @param[in] separator The separator sequence, ie. the string inserted between the strings that are being concatenated
			* @param[in] first The begining iterator or pointer to a list/array of strings
			* @param[in] last The ending iterator or pointer
		*/
		template<typename ItrBeg, typename ItrEnd>
		static std::string concat(const std::string_view& separator, const ItrBeg& first, const ItrEnd& last)
		{
			size_t size = 0;
			for (ItrBeg itr = first; itr != last; itr++)
				size += (*itr).size() + separator.size();
			if (size > 0)
				size -= separator.size();

			std::string out;
			out.reserve(size);
			for (ItrBeg itr = first; itr != last; itr++)
			{
				const std::string& s = *itr;
				out.append(s);

				size -= s.size();
				if (size > 0)
				{
					out.append(separator);
					size -= separator.size();
				}

			}
			return out;
		}

		/*
			Concatenates the strings from the given collection of strings.
			* @param[in] collection The collection of strings to concatenate
		*/
		template<typename T, typename Itr>
		inline static std::string concat(const Iterable<T, Itr>& collection)
		{
			return concat(collection.begin(), collection.end());
		}

		/*
			Concatenates the strings from the given collection of strings and separates them by the given separator.
			* @param[in] separator The separator sequence, ie. the string inserted between the strings that are being concatenated
			* @param[in] collection The collection of strings to concatenate
		*/
		template<typename T, typename Itr>
		inline static std::string concat(const std::string_view& separator, const Iterable<T, Itr>& collection)
		{
			return concat(separator, collection.begin(), collection.end());
		}

		/*
			Splits the given string by the given separator sequence and stores the result in the given list.
			* @param[in] src The string to split
			* @param[in] separator The string to split the first one by
			* @param[out] output The list to write the resulting pieces to
		*/
		template<typename pos_t, typename itr_t, typename citr_t>
		inline static void splitCopy(const std::string_view& src, const std::string_view& separator, List<std::string, pos_t, itr_t, citr_t>& output)
		{
			size_t start = 0;
			size_t end = src.find(separator);
			while (end != std::string::npos)
			{
				output.add(std::string(src.substr(start, end - start)));
				start = end + separator.length();
				end = src.find(separator, start);
			}
			output.add(std::string(src.substr(start, src.length() - start)));
		}

		/*
			Splits the given string by the given separator sequence and returns an array list with the resulting pieces.
			* @param[in] src The string to split
			* @param[in] separator The string to split the first one by
		*/
		inline static ArrayList<std::string> splitCopy(const std::string_view& src, const std::string_view& separator)
		{
			ArrayList<std::string> out;
			splitCopy(src, separator, out);
			return out;
		}

		/*
			Splits the given string into string_view instances by the given separator sequence and stores the result in the given list.
			NOTE! The string_view instances are depedent on the existance of the source string!
			* @param[in] src The string to split
			* @param[in] separator The string to split the first one by
			* @param[out] output The list to write the resulting pieces to
		*/
		template<typename pos_t, typename itr_t, typename citr_t>
		inline static void split(const std::string_view& src, const std::string_view& separator, List<std::string_view, pos_t, itr_t, citr_t>& output)
		{
			size_t start = 0;
			size_t end = src.find(separator);
			while (end != std::string::npos)
			{
				output.add(std::string_view(src.data() + start, end - start));
				start = end + separator.length();
				end = src.find(separator, start);
			}
			output.add(std::string_view(src.data() + start, src.length() - start));
		}

		/*
			Splits the given string into string_view instances by the given separator sequence and returns an array list with the resulting pieces.
			* @param[in] src The string to split
			* @param[in] separator The string to split the first one by
		*/
		inline static ArrayList<std::string_view> split(const std::string_view& src, const std::string_view& separator)
		{
			ArrayList<std::string_view> out;
			split(src, separator, out);
			return out;
		}

	private:

		StringUtil() = delete;
		StringUtil(const StringUtil&) = delete;

	};

}