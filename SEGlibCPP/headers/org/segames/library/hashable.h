#pragma once
#include <org/segames/library/dllmain.h>

#include <string>
#include <string_view>

namespace org::segames::library
{

	/*
		Interface for all things hashable.

		* @author	Philip Rosberg
		* @since	2019-03-17
		* @edited	2020-06-11
	*/
	class SEG_API Hashable
	{
	public:

		/*
			Returns a hash code representation of the object.
		*/
		virtual size_t hashCode() const = 0;

	public:

		/*
			Calculates a hash code representation of the given hashable object.
			* @param[in] obj The hashable object to calculate the hash from
		*/
		inline static size_t hashCode(const Hashable& obj)
		{
			return obj.hashCode();
		}
				
		/*
			Calculates a hash code representation of the given string.
			* @param[in] str The null terminated c-string to calculate a hash from
		*/
		inline static size_t hashCode(const char* str)
		{
			return hashCode(std::string_view(str));
		}

		/*
			Calculates a hash code representation of the given string.
			* @param[in] str The string to calculate a hash from
		*/
		inline static size_t hashCode(const std::string& str)
		{
			return hashCode(std::string_view(str));
		}

		/*
			Calculates a hash code representation of the given string.
			* @param[in] str The string to calculate a hash from
		*/
		static size_t hashCode(const std::string_view& str);

		/*
			Calculates a hash code representation of the given data.
			Based on the FNV-1a hash algorithm.
			* @param[in] ptr The memory
			* @param[in] len The length of the memory
		*/
		static size_t hashCode(const void* ptr, const size_t len);

		/*
			Calculates a hash code representation of the given variable.
			This is just a pass-through function to allow most standard types
			such as int, short etc. to just use their regular value as a hash.
			* @param[in] var The variable to "calculate a hash from"
		*/
		template<typename T>
		inline static size_t hashCode(const T& var)
		{
			return (size_t)var;
		}

	public:

		/*
			Default hasher for hashables.

			* @author	Philip Rosberg
			* @since	2021-06-09
			* @edited	2021-06-09
		*/
		class HashableHasher final
		{
		public:

			/*
				Operator overload to allow for usage in stl-containers.
			*/
			inline size_t operator()(const Hashable& obj) const
			{
				return obj.hashCode();
			}

		};

	};

}