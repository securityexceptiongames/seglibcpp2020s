#pragma once
#include <org/segames/library/stack_trace.h>

#include <exception>
#include <typeinfo>
#include <string>
#include <string_view>

namespace org::segames::library
{

	/*
		Root class for exception type hierarchy.

		* @author	Philip Rosberg
		* @since	2020-06-10
		* @edited	2020-06-10
	*/
	class SEG_API Exception :
		public std::exception
	{
	private:

		/*
			The root path of the project to remove from file directory printout.
		*/
		static std::string s_projectPath;

	public:

		/*
			Sets the root path of the project to remove from file directory printout.
			* @param[in] path The path of the project
		*/
		inline static void setProjectPath(const std::string_view& path)
		{
			s_projectPath = path;
		}

	private:

		/*
			The complete exception string.
		*/
		std::string m_completeString;

	protected:

		/*
			The exception message.
		*/
		std::string m_message;

		/*
			The stack frames leading up to the exception.
		*/
		std::vector<StackTrace::Frame> m_stackFrames;

	protected:

		/*
			Creates an exception using the given message and stack frames.
			* @param[in] name The properly formatted name of the exception
			* @param[in] message The message of the exception
			* @param[in] stackFrames The stack frames leading up to the exception
		*/
		inline Exception(const std::string_view& name, const std::string_view& message, std::vector<StackTrace::Frame>&& stackFrames) :
			m_completeString(std::move(createCompleteMessage(name, message, stackFrames))),
			m_message(message),
			m_stackFrames(std::move(stackFrames))
		{}

		/*
			Copy-constructor.
		*/
		Exception(const Exception&) = delete;

		/*
			Move-constructor.
		*/
		inline Exception(Exception&& e) noexcept :
			m_completeString(std::move(e.m_completeString)),
			m_message(std::move(e.m_message)),
			m_stackFrames(std::move(e.m_stackFrames))
		{}

	public:

		/*
			Returns the stack frames leading up to the exception.
		*/
		inline const std::vector<StackTrace::Frame>& getStackFrames() const
		{
			return m_stackFrames;
		}

		/*
			Returns the entire message of the exception.
		*/
		inline virtual const char* what() const noexcept override
		{
			return m_completeString.c_str();
		}

		/*
			Creates a complete exception message using the given message and stack frames.
			* @param[in] name The name of the exception type
			* @param[in] message The message of the exception
			* @param[in] stackFrames The stack frames leading up to the exception
		*/
		static std::string createCompleteMessage(const std::string_view& name, const std::string_view& message, const std::vector<StackTrace::Frame>& stackFrames);

	};

}