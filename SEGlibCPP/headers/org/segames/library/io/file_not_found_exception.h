#pragma once
#include <org/segames/library/io/io_exception.h>

namespace org::segames::library
{

	/*
		Exception for when a file is not found when expected to be found.

		* @author	Philip Rosberg
		* @since	2020-06-16
		* @edited	2020-06-16
	*/
	class SEG_API FileNotFoundException :
		public IOException
	{
	public:

		/*
			Creates an i/o exception with the given message.
			* @param[in] path The path of the file that could not be found
		*/
		inline explicit FileNotFoundException(const std::string_view& path) :
			IOException("org::segames::library::FileNotFoundException", (std::string("File \"") += path) += "\" does not exist!", StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1))
		{}

	};

}