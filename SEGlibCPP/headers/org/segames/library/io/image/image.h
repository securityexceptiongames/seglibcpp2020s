#pragma once
#include <org/segames/library/util/array_2d.h>
#include <org/segames/library/util/color.h>

#include <string_view>

namespace org::segames::library
{

	/*
		A four channel per pixel image class.

		* @author	Philip Rosberg
		* @since	2021-03-13
		* @edited	2021-03-13
	*/
	class SEG_API Image :
		public Array2D<Color>
	{
	public:
				
		/*
			Blending modes for drawing of one image into another.
		*/
		enum class Blending : char
		{
			OVERWRITE = 0,
			SRC_ALPHA_ONE_MINUS_SRC_ALPHA = 1
		};

	public:

		/*
			Creates an empty image.
		*/
		inline Image() :
			Array2D<Color>()
		{}

		/*
			Creates an image with the given size.
			* @param[in] width The width of the image
			* @param[in] height The height of the image
		*/
		inline explicit Image(const size_t& width, const size_t& height) :
			Array2D<Color>(width, height)
		{}

		/*
			Imports the image from the given file.
			* @param[in] path The path of the image file
		*/
		explicit Image(const std::string_view& path) noexcept(false);

		/*
			Copy-constructor.
		*/
		inline Image(const Image& obj) :
			Array2D<Color>(obj)
		{}

		/*
			Move-constructor.
		*/
		inline Image(Image&& obj) noexcept :
			Array2D<Color>(std::move(obj))
		{}

	public:

		/*
			Clears the image by setting every pixel to the given color.
			* @param[in] clearColor The color to clear the image to
		*/
		inline virtual void clear(const Color& clearColor)
		{
			for (size_t i = 0; i < size(); i++)
				m_array[i] = clearColor;
		}

		/*
			Draws the given image into this one using the default (overwrite) blend mode.
			* @param[in] x The x position
			* @param[in] y The y position
			* @param[in] img The image to draw
		*/
		inline virtual void draw(signed long long x, signed long long y, const Image& img)
		{
			draw(x, y, img, Blending::OVERWRITE);
		}

		/*
			Draws the given image into this one.
			* @param[in] x The x position
			* @param[in] y The y position
			* @param[in] img The image to draw
			* @param[in] blending The blending mode to use
		*/
		inline virtual void draw(signed long long x, signed long long y, const Image& img, Blending blending)
		{
			switch (blending)
			{
			case Image::Blending::SRC_ALPHA_ONE_MINUS_SRC_ALPHA:
				drawSrcAlphaOneMinusSrcAlpha__(x, y, img);
				break;
			default:
				drawOverwrite__(x, y, img);
				break;
			}

		}

		/*
			Extracts a part from this image and returns it as a new image. 
			Padds any pixels outside the image with with transparent black.
			* @param[in] x The x position relative to this image origin
			* @param[in] y The y position relative to this image origin
			* @param[in] w The width of the final sub-image
			* @param[in] h The height of the final sub-image
		*/
		virtual Image subImage(signed long long x, signed long long y, size_t w, size_t h);

		/*
			Extracts a part from this image and returns it as a new image. 
			Throws an exception if the sub-image is not strictly a subset of this image.
			* @param[in] x The x position relative to this image origin
			* @param[in] y The y position relative to this image origin
			* @param[in] w The width of the final sub-image
			* @param[in] h The height of the final sub-image
		*/
		virtual Image subImageStrict(signed long long x, signed long long y, size_t w, size_t h);

	protected:

		/*
			Draws the given image into this one using overwrite blend mode.
			* @param[in] x The x position
			* @param[in] y The y position
			* @param[in] img The image to draw
		*/
		void drawOverwrite__(signed long long x, signed long long y, const Image& img);
				
		/*
			Draws the given image into this one using source alpha one minus source alpha blend mode.
			* @param[in] x The x position
			* @param[in] y The y position
			* @param[in] img The image to draw
		*/
		void drawSrcAlphaOneMinusSrcAlpha__(signed long long x, signed long long y, const Image& img);

	};

}