#pragma once
#include <org/segames/library/dllmain.h>
#include <org/segames/library/system.h>

#include <istream>
#include <ostream>

namespace org::segames::library
{

	/*
		Class/Namespace for input/output utilities.

		* @author	Philip Rosberg
		* @since	2020-10-17
		* @since	2020-10-17
	*/
	class SEG_API IOUtil final
	{
	public:

		/*
			Reads the given type from the input stream, not altering it.
			* @param[in] input The input stream to read from
		*/
		template<typename T>
		inline static T next(std::istream& input) noexcept(false)
		{
			union
			{
				char data[sizeof(T)];
				T value;
			} conversion;
			input.read(conversion.data, sizeof(T));
			return conversion.value;
		}

		/*
			Reads the given type from the input stream, changing its byte-order if necessary.
			* @param[in] input The input stream to read from
		*/
		template<typename T, char mask>
		inline static T next(std::istream& input) noexcept(false)
		{
			char data[sizeof(T)];
			input.read(data, sizeof(T));
			return *bo_cast<T, mask>(1, data);
		}

		/*
			Reads the given type from the input stream, changing its byte-order if necessary.
			* @param[in] input The input stream to read from
			* @param[in] mask The byte-order conversion mask
		*/
		template<typename T>
		inline static T next(std::istream& input, char mask) noexcept(false)
		{
			char data[sizeof(T)];
			input.read(data, sizeof(T));
			return *System::bo_cast<T>(1, data, mask);
		}

		/*
			Reads the given type from the input stream, changing its byte-order if necessary.
			NOTE! This method changes an input BIG_ENDIAN value to a SYSTEM_ENDIAN value.
			* @param[in] input The input stream to read from
		*/
		template<typename T>
		inline static T nextBE(std::istream& input) noexcept(false)
		{
			return next<T, BIG_ENDIAN | SYSTEM_ENDIAN>(input);
		}

		/*
			Reads the given type from the input stream, changing its byte-order if necessary.
			NOTE! This method changes an input LITTLE_ENDIAN value to a SYSTEM_ENDIAN value.
			* @param[in] input The input stream to read from
		*/
		template<typename T>
		inline static T nextLE(std::istream& input) noexcept(false)
		{
			return next<T, LITTLE_ENDIAN | SYSTEM_ENDIAN>(input);
		}

		/*
			Attemts to parse the given type from the given memory location, not altering it.
			NOTE! This method does not change the memory pointed to by ptr.
			WARNING! Make sure there is atleast sizeof(T) bytes avaliable at ptr.
			* @param[in] ptr The memory location pointer
		*/
		template<typename T>
		inline static T parse(void* ptr) noexcept(false)
		{
			return *reinterpret_cast<T*>(ptr);
		}

		/*
			Attemts to parse the given type from the given memory location changing its byte-order if necessary.
			WARNING! Make sure there is atleast sizeof(T) bytes avaliable at ptr.
			WARNING! This function changes the memory pointed to by ptr!
			* @param[in/out] ptr The memory location pointer
		*/
		template<typename T, char mask>
		inline static T parse(void* ptr) noexcept(false)
		{
			return *bo_cast<T, mask>(1, ptr);
		}

		/*
			Attemts to parse the given type from the given memory location changing its byte-order if necessary.
			WARNING! Make sure there is atleast sizeof(T) bytes avaliable at ptr.
			WARNING! This function changes the memory pointed to by ptr!
			* @param[in/out] ptr The memory location pointer
			* @param[in] mask The byte-order conversion mask
		*/
		template<typename T>
		inline static T parse(void* ptr, char mask) noexcept(false)
		{
			return *System::bo_cast<T>(1, ptr, mask);
		}

		/*
			Attemts to parse the given type from the given memory location changing its byte-order if necessary.
			NOTE! This method changes an input BIG_ENDIAN value to a SYSTEM_ENDIAN value.
			WARNING! Make sure there is atleast sizeof(T) bytes avaliable at ptr.
			WARNING! This function changes the memory pointed to by ptr!
			* @param[in/out] ptr The memory location pointer
		*/
		template<typename T>
		inline static T parseBE(void* ptr) noexcept(false)
		{
			return parse<T, BIG_ENDIAN | SYSTEM_ENDIAN>(ptr);
		}

		/*
			Attemts to parse the given type from the given memory location changing its byte-order if necessary.
			NOTE! This method changes an input LITTLE_ENDIAN value to a SYSTEM_ENDIAN value.
			WARNING! Make sure there is atleast sizeof(T) bytes avaliable at ptr.
			WARNING! This function changes the memory pointed to by ptr!
			* @param[in/out] ptr The memory location pointer
		*/
		template<typename T>
		inline static T parseLE(void* ptr) noexcept(false)
		{
			return parse<T, LITTLE_ENDIAN | SYSTEM_ENDIAN>(ptr);
		}

		/*
			Attemts to parse the given type from the given memory location changing its byte-order if necessary.
			NOTE! This method does not change the memory pointed to by ptr.
			WARNING! Make sure there is atleast sizeof(T) bytes avaliable at ptr.
			* @param[in/out] ptr The memory location pointer
		*/
		template<typename T, char mask>
		inline static T parseCopy(void* ptr) noexcept(false)
		{
			T temp = parse<T>(ptr);
			return *bo_cast<T, mask>(1, &temp);
		}

		/*
			Attemts to parse the given type from the given memory location changing its byte-order if necessary.
			NOTE! This method does not change the memory pointed to by ptr.
			WARNING! Make sure there is atleast sizeof(T) bytes avaliable at ptr.
			* @param[in/out] ptr The memory location pointer
			* @param[in] mask The byte-order conversion mask
		*/
		template<typename T>
		inline static T parseCopy(void* ptr, char mask) noexcept(false)
		{
			T temp = parse<T>(ptr);
			return *System::bo_cast<T>(1, temp, mask);
		}

		/*
			Attemts to parse the given type from the given memory location changing its byte-order if necessary.
			NOTE! This method changes an input BIG_ENDIAN value to a SYSTEM_ENDIAN value.
			NOTE! This method does not change the memory pointed to by ptr.
			WARNING! Make sure there is atleast sizeof(T) bytes avaliable at ptr.
			* @param[in/out] ptr The memory location pointer
		*/
		template<typename T>
		inline static T parseBECopy(void* ptr) noexcept(false)
		{
			return parseCopy<T, BIG_ENDIAN | SYSTEM_ENDIAN>(ptr);
		}

		/*
			Attemts to parse the given type from the given memory location changing its byte-order if necessary.
			NOTE! This method changes an input LITTLE_ENDIAN value to a SYSTEM_ENDIAN value.
			NOTE! This method does not change the memory pointed to by ptr.
			WARNING! Make sure there is atleast sizeof(T) bytes avaliable at ptr.
			* @param[in/out] ptr The memory location pointer
		*/
		template<typename T>
		inline static T parseLECopy(void* ptr) noexcept(false)
		{
			return parseCopy<T, LITTLE_ENDIAN | SYSTEM_ENDIAN>(ptr);
		}

		/*
			Writes the bytes of the given value in the given output stream.
			* @param[in] output The output stream to write to
			* @param[in] value The value whose bytes to write to the output stream
		*/
		template<typename T>
		inline static std::ostream& put(std::ostream& output, const T& value) noexcept(false)
		{
			return put<T, 0>(output, value);
		}

		/*
			Writes the bytes of the given value in the given output stream, changing its byte-order if necessary.
			* @param[in] output The output stream to write to
			* @param[in] value The value whose bytes to write to the output stream
		*/
		template<typename T, char mask>
		inline static std::ostream& put(std::ostream& output, const T& value) noexcept(false)
		{
			union
			{
				char data[sizeof(T)];
				T value;
			} conversion;
			conversion.value = value;
			output.write(reinterpret_cast<char*>(bo_cast<T, mask>(1, conversion.data)), sizeof(T));
			return output;
		}

		/*
			Writes the bytes of the given value in the given output stream, changing its byte-order if necessary.
			* @param[in] output The output stream to write to
			* @param[in] value The value whose bytes to write to the output stream
			* @param[in] mask The byte-order conversion mask
		*/
		template<typename T>
		inline static std::ostream& put(std::ostream& output, const T& value, char mask) noexcept(false)
		{
			union
			{
				char data[sizeof(T)];
				T value;
			} conversion;
			conversion.value = value;;
			output.write(reinterpret_cast<char*>(bo_cast<T>(1, conversion.data, mask)), sizeof(T));
			return output;
		}

		/*
			Writes the bytes of the given value in the given output stream, changing its byte-order if necessary.
			NOTE! This method changes a SYSTEM_ENDIAN value to an output BIG_ENDIAN value.
			* @param[in] output The output stream to write to
			* @param[in] value The value whose bytes to write to the output stream
		*/
		template<typename T>
		inline static std::ostream& putBE(std::ostream& output, const T& value) noexcept(false)
		{
			return put<T, BIG_ENDIAN | SYSTEM_ENDIAN>(output, value);
		}

		/*
			Writes the bytes of the given value in the given output stream, changing its byte-order if necessary.
			NOTE! This method changes a SYSTEM_ENDIAN value to an output LITTLE_ENDIAN value.
			* @param[in] output The output stream to write to
			* @param[in] value The value whose bytes to write to the output stream
		*/
		template<typename T>
		inline static std::ostream& putLE(std::ostream& output, const T& value) noexcept(false)
		{
			return put<T, LITTLE_ENDIAN | SYSTEM_ENDIAN>(output, value);
		}

		/*
			Writes the bytes of the given value to the given memory location, without changing its byte-order.
			Returns a pointer pointing to the end of the written memory (after the last inserted byte).
			WARNING! Make sure there is atleast sizeof(T) bytes avaliable at ptr.
			* @param[in] ptr The memory location to write the value to
			* @param[in] value The value whose bytes to write to the output stream
		*/
		template<typename T>
		inline static void* write(void* ptr, const T& value) noexcept(false)
		{
			*reinterpret_cast<T*>(ptr) = value;
			return reinterpret_cast<T*>(ptr) + 1;
		}

		/*
			Writes the bytes of the given value to the given memory location, changing its byte-order if necessary.
			Returns a pointer pointing to the end of the written memory (after the last inserted byte).
			WARNING! Make sure there is atleast sizeof(T) bytes avaliable at ptr.
			* @param[in] ptr The memory location to write the value to
			* @param[in] value The value whose bytes to write to the output stream
		*/
		template<typename T, char mask>
		inline static void* write(void* ptr, const T& value) noexcept(false)
		{
			*reinterpret_cast<T*>(ptr) = value;
			bo_cast<T, mask>(1, ptr);
			return reinterpret_cast<T*>(ptr) + 1;
		}

		/*
			Writes the bytes of the given value to the given memory location, changing its byte-order if necessary.
			Returns a pointer pointing to the end of the written memory (after the last inserted byte).
			WARNING! Make sure there is atleast sizeof(T) bytes avaliable at ptr.
			* @param[in] ptr The memory location to write the value to
			* @param[in] value The value whose bytes to write to the output stream
		*/
		template<typename T>
		inline static void* write(void* ptr, const T& value, char mask) noexcept(false)
		{
			*reinterpret_cast<T*>(ptr) = value;
			System::bo_cast<T>(1, ptr, mask);
			return reinterpret_cast<T*>(ptr) + 1;
		}

		/*
			Writes the bytes of the given value to the given memory location, changing its byte-order if necessary.
			Returns a pointer pointing to the end of the written memory (after the last inserted byte).
			NOTE! This method changes an input BIG_ENDIAN value to a SYSTEM_ENDIAN value.
			WARNING! Make sure there is atleast sizeof(T) bytes avaliable at ptr.
			* @param[in] ptr The memory location to write the value to
			* @param[in] value The value whose bytes to write to the output stream
		*/
		template<typename T>
		inline static void* writeBE(void* ptr, const T& value) noexcept(false)
		{
			return write<T, BIG_ENDIAN | SYSTEM_ENDIAN>(ptr, value);
		}

		/*
			Writes the bytes of the given value to the given memory location, changing its byte-order if necessary.
			Returns a pointer pointing to the end of the written memory (after the last inserted byte).
			NOTE! This method changes an input BIG_ENDIAN value to a SYSTEM_ENDIAN value.
			WARNING! Make sure there is atleast sizeof(T) bytes avaliable at ptr.
			* @param[in] ptr The memory location to write the value to
			* @param[in] value The value whose bytes to write to the output stream
		*/
		template<typename T>
		inline static void* writeLE(void* ptr, const T& value) noexcept(false)
		{
			return write<T, LITTLE_ENDIAN | SYSTEM_ENDIAN>(ptr, value);
		}

	private:

		IOUtil() = delete;
		IOUtil(const IOUtil&) = delete;

	};

}