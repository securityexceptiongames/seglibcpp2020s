#pragma once
#include <org/segames/library/util/array_list.h>
#include <org/segames/library/util/string_util.h>
#include <org/segames/library/string_visualizable.h>

#include <string>
#include <string_view>

#ifndef SEG_API_SYSTEM_DIR_SEPARATOR
#ifdef _WIN32
#define SEG_API_SYSTEM_DIR_SEPARATOR_CHAR '\\'
#define SEG_API_SYSTEM_DIR_SEPARATOR "\\"
#else
#define SEG_API_SYSTEM_DIR_SEPARATOR_CHAR '/'
#define SEG_API_SYSTEM_DIR_SEPARATOR "/"
#endif
#endif

namespace org::segames::library
{

	/*
		A class storing the attributes of a file, initialized on creation, copied on copy.

		* @author	Philip Rosberg
		* @since	2018-11-05
		* @edited	2020-06-17
	*/
	class SEG_API FileAttributes :
		public StringVisualizable
	{
	protected:

		/*
			True if the file exists.
		*/
		bool m_exists;

		/*
			True if the file is a directory.
		*/
		bool m_directory;

		/*
			The length/size of the file.
		*/
		size_t m_length;

		/*
			The file path.
		*/
		std::string m_path;

		/*
			The file path dissected.
		*/
		ArrayList<std::string_view> m_dissectedPath;

	public:

		/*
			Creates an empty file attributes object in the local path of the executable.
		*/
		inline FileAttributes() :
			m_exists(false),
			m_directory(false),
			m_length(0),
			m_path(""),
			m_dissectedPath(0)
		{}

		/*
			Loads the file attributes for the given path.
			* @param[in] path The path of the file
		*/
		inline explicit FileAttributes(const char* path) :
			FileAttributes()
		{
#ifdef _WIN32
			if (*path == '\0')
				loadAttribWindows__(".\\");
			else
				loadAttribWindows__(path);
#endif
		}

		/*
			Loads the file attributes for the given path.
			* @param[in] path The path of the file
		*/
		inline explicit FileAttributes(const std::string_view& path) :
			FileAttributes(path.data())
		{}

		/*
			Copy constructor.
		*/
		inline FileAttributes(const FileAttributes&) = default;

		/*
			Move constructor.
		*/
		inline FileAttributes(FileAttributes&&) = default;

	public:

		/*
			Returns true if the file exists.
		*/
		inline virtual bool exists() const
		{
			return m_exists;
		}

		/*
			Returns true if the given file is a directory.
		*/
		inline virtual bool isDirectory() const
		{
			return m_directory;
		}

		/*
			Returns the length of the file in num. bytes.
		*/
		inline virtual size_t length() const
		{
			return m_length;
		}

		/*
			Returns the file path (absolute).
		*/
		inline virtual const std::string& getPath() const
		{
			return m_path;
		}

		/*
			Retruns the path dissected into all sub-parts.
		*/
		inline virtual const ArrayList<std::string_view>& getPathDissection() const
		{
			return m_dissectedPath;
		}

		/*
			Sets if the file exists or not.
			WARNING! THIS METHOD ONLY CHANGES THE ATTRIBUTE OBJECT AND NOT THE FILE ON THE SYSTEM!
			* @param[in] flag True if the file exists
		*/
		inline virtual void setExisting(const bool flag)
		{
			m_exists = flag;
		}

		/*
			Sets if the file is a directory or not.
			WARNING! THIS METHOD ONLY CHANGES THE ATTRIBUTE OBJECT AND NOT THE FILE ON THE SYSTEM!
			* @param[in] float True if the file is a directory
		*/
		inline virtual void setDirectory(const bool flag)
		{
			m_directory = flag;
		}

		/*
			Sets the file representation length.
			WARNING! THIS METHOD ONLY CHANGES THE ATTRIBUTE OBJECT AND NOT THE FILE ON THE SYSTEM!
			* @param[in] length The length of the file
		*/
		inline virtual void setLength(const size_t length)
		{
			m_length = length;
		}

		/*
			Sets the path of the file representation.
			WARNING! THIS METHOD ONLY CHANGES THE ATTRIBUTE OBJECT AND NOT THE FILE ON THE SYSTEM!
			* @param[in] path The path
		*/
		inline virtual void setPath(const std::string& path)
		{
			m_path = path;
			m_dissectedPath = StringUtil::split(m_path, SEG_API_SYSTEM_DIR_SEPARATOR);
		}

		/*
			Updates the file attributes.
		*/
		inline virtual void update()
		{
#ifdef _WIN32
			loadAttribWindows__(m_path.c_str());
#endif
		}

		/*
			Returns a string representation of the file attributes.
		*/
		inline virtual std::string toString() const override
		{
			std::string out = "FileAttributes(";
			if (exists())
			{
				out += "E";
				out += (isDirectory() ? "D" : "F");
				out += "): \"";
			}
			else
				out += "--): \"";
			out += getPath();
			out += "\"";
			return out;
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline FileAttributes& operator=(const FileAttributes&) = default;

		/*
			Move-assignment operator.
		*/
		inline FileAttributes& operator=(FileAttributes&&) = default;

	private:

		/*
			Loads the attributes using the windows library.
			* @param[in] path The path of the file
		*/
		virtual void loadAttribWindows__(const char* path);

	};

}