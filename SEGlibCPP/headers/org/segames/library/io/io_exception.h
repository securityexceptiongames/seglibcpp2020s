#pragma once
#include <org/segames/library/exception.h>

namespace org::segames::library
{

	/*
		Super-class for various exceptions during i/o operations.

		* @author	Philip Rosberg
		* @since	2020-06-16
		* @edited	2020-06-16
	*/
	class SEG_API IOException :
		public Exception
	{
	public:

		/*
			Creates an i/o exception without message.
		*/
		inline IOException() :
			Exception("org::segames::library::IOException", "", StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1))
		{}

		/*
			Creates an i/o exception with the given message.
			* @param[in] message The additional message to display
		*/
		inline explicit IOException(const std::string_view& message) :
			Exception("org::segames::library::IOException", message, StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1))
		{}

	protected:

		/*
			Creates an i/o exception with the given message and stack frames.
			* @param[in] name The properly formatted name of the exception
			* @param[in] message The message of the exception
			* @param[in] stackFrames The stack frames leading up to the exception
		*/
		inline explicit IOException(const std::string_view& name, const std::string_view& message, std::vector<StackTrace::Frame>&& stackFrames) :
			Exception(name, message, std::move(stackFrames))
		{}

	};

}