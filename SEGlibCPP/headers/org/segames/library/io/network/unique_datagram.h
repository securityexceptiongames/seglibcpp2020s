#pragma once
#include <org/segames/library/io/network/datagram.h>

#include <memory>

namespace org::segames::library
{
			
	/*
		A unique memory datagram.

		* @author	Philip Rosberg
		* @since	2020-12-19
		* @edited	2020-12-19
	*/
	class UniqueDatagram :
		public Datagram
	{
	protected:

		/*
			The size of the data.
		*/
		size_t m_size;

		/*
			The data.
		*/
		std::unique_ptr<char[]> m_data;

		/*
			The datagram address.
		*/
		InetAddress m_address;

	public:

		/*
			Creates an empty unique datagram.
		*/
		inline UniqueDatagram() :
			m_size(0),
			m_data(),
			m_address()
		{}

		/*
			Creates a unique datagram from the given size and data.
			* @param[in] size The size of the data in chars
			* @param[in] data The data block
		*/
		inline explicit UniqueDatagram(size_t size, std::unique_ptr<char[]>&& data, const InetAddress& address) :
			m_size(size),
			m_data(std::move(data)),
			m_address(address)
		{}

		/*
			Creates a unique datagram of the given size and copies data from the given pointer.
			* @param[in] size The size of the data in chars
			* @param[in] ptr The pointer to copy data from
		*/
		inline explicit UniqueDatagram(size_t size, const char* ptr, const InetAddress& address) :
			m_size(size),
			m_data(new char[size]),
			m_address(address)
		{
			std::copy(ptr, ptr + size, m_data.get());
		}

		/*
			Copy-constructor.
		*/
		inline UniqueDatagram(const UniqueDatagram& obj) :
			m_size(obj.m_size),
			m_data(new char[obj.m_size]),
			m_address(obj.m_address)
		{
			std::copy(obj.m_data.get(), obj.m_data.get() + obj.m_size, m_data.get());
		}

		/*
			Move-constructor.
		*/
		inline UniqueDatagram(UniqueDatagram&& obj) noexcept :
			m_size(obj.m_size),
			m_data(std::move(obj.m_data)),
			m_address(std::move(obj.m_address))
		{
			obj.m_size = 0;
		}

	public:

		/*
			Returns the number of avaliable native chars.
		*/
		inline virtual size_t nativeSize() const override
		{
			return m_size;
		}

		/*
			Returns the pointer to the begining of native chars.
		*/
		inline virtual char* nativePointer() override
		{
			return m_data.get();
		}

		/*
			Returns the pointer to the begining of native chars.
		*/
		inline virtual const char* nativePointer() const override
		{
			return m_data.get();
		}

		/*
			Returns the address of the datagram.
			Could be either receiver (send to) or sender (received from).
		*/
		inline virtual const InetAddress& address() const override
		{
			return m_address;
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline UniqueDatagram& operator=(const UniqueDatagram& obj)
		{
			m_size = obj.m_size;
			m_data.reset(new char[m_size]);
			std::copy(obj.m_data.get(), obj.m_data.get() + obj.m_size, m_data.get());
			m_address = obj.m_address;
			return *this;
		}

		/*
			Move-assignment operator.
		*/
		inline UniqueDatagram& operator=(UniqueDatagram&& obj) noexcept
		{
			m_size = obj.m_size;
			m_data = std::move(obj.m_data);
			m_address = std::move(obj.m_address);
			obj.m_size = 0;
			return *this;
		}

	};

}