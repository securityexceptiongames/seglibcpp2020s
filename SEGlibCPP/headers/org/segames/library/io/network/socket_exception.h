#pragma once
#include <org/segames/library/io/io_exception.h>

namespace org::segames::library
{

	/*
		Exception for socket related errors.

		* @author	Philip Rosberg
		* @since	2021-03-01
		* @edited	2021-03-01
	*/
	class SEG_API SocketException :
		public IOException
	{
	protected:

		/*
			The error code.
		*/
		int m_code;

	public:

		/*
			Creates a socket exception with the given message.
			* @param[in] message The additional message to display
		*/
		inline explicit SocketException(const std::string_view& message) :
			IOException("org::segames::library::SocketException", message, StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1)),
			m_code(0)
		{}

		/*
			Creates a socket exception with the given message and code.
			* @param[in] message The additional message to display
			* @param[in] code The error code
		*/
		inline explicit SocketException(const std::string_view& message, int code) :
			IOException("org::segames::library::SocketException", message, StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1)),
			m_code(code)
		{}

	public:

		/*
			Returns the error code.
		*/
		inline int code() const noexcept
		{
			return m_code;
		}

	};

}