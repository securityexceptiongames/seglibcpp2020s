#pragma once
#include <org/segames/library/io/network/inet_address.h>
#include <org/segames/library/io/network/socket_input_stream.h>
#include <org/segames/library/io/network/socket_output_stream.h>
#include <org/segames/library/io/network/unique_datagram.h>

#include <istream>
#include <ostream>

#ifdef _WIN32
#include <WinSock2.h>
#endif

namespace org::segames::library
{

	/*
		Standard socket class for both TCP and UDP connections.

		* @author	Philip Rosberg
		* @since	2020-12-19
		* @edited	2021-11-29
	*/
	class SEG_API Socket final
	{
	public:

		/*
			Marker struct/flag for creation of datagrams that get a 
			port automatically assigned (gets bound) on first datagram 
			transmission.
		*/
		struct AutoAssignedDatagram {};

		/*
			The various socket types.

			* @author	Philip Rosberg
			* @since	2020-12-19
			* @since	2020-12-19
		*/
		enum class SocketType : char
		{
			STREAM,
			DATAGRAM
		};

		/*
			The various socket families.

			* @author	Philip Rosberg
			* @since	2020-12-19
			* @since	2020-12-19
		*/
		enum class SocketFamily : char
		{
			IPV4,
			IPV6
		};

	public:

		/*
			Maximum allowed data size for datagrams that are to be sent over IPV4.
		*/
		static constexpr size_t DATAGRAM_MAX_DATA_SIZE_IPV4 = 65535 - 8 - 20;

		/*
			Maximum allowed data size for datagrams that are to be sent over IPV6.
		*/
		static constexpr size_t DATAGRAM_MAX_DATA_SIZE_IPV6 = 65535 - 8 - 48;

	private:

		/*
			The size of the input stream buffer.
		*/
		static size_t s_inputBufferSize;

		/*
			The size of the output stream buffer.
		*/
		static size_t s_outputBufferSize;

	protected:

		/*
			True if the connection was "violently" broken instead of gracefully closed.
		*/
		bool m_broken;

		/*
			True if the socket is connected.
		*/
		bool m_connected;

		/*
			True if the socket is bound to a port.
		*/
		bool m_bound;

		/*
			The socket operation type.
		*/
		SocketType m_type;

		/*
			The socket operation family;
		*/
		SocketFamily m_family;

		/*
			The socket TCP connection address.
			Ie. this only exists if this socket is connected to some other end.
		*/
		InetAddress m_address;
#ifdef _WIN32
		/*
			The socket.
		*/
		SOCKET m_socket;
#endif
		/*
			The socket input stream.
		*/
		SocketInputStream m_inputStream;

		/*
			The socket output stream.
		*/
		SocketOutputStream m_outputStream;

	public:

		/*
			Creates an empty socket.
		*/
		inline Socket() :
			m_broken(false),
			m_connected(false),
			m_bound(false),
			m_type(),
			m_family(),
			m_address(),
#ifdef _WIN32
			m_socket(INVALID_SOCKET),
#endif
			m_inputStream(),
			m_outputStream()
		{}
				
		/*
			Creates and connects a TCP socket to the given address.
			* @param[in] address The address to connect via TCP to
			* @param[in] family The socket operation family
		*/
		explicit Socket(const InetAddress& address, SocketFamily family = SocketFamily::IPV4) noexcept(false);

		/*
			Creates and explicitly binds a server TCP or datagram UDP socket to the given port.
			* @param[in] port The port of the socket
			* @param[in] type The socket operation type
			* @param[in] family The socket operation family
		*/
		explicit Socket(unsigned short port, SocketType type, SocketFamily family = SocketFamily::IPV4) noexcept(false);

		/*
			Creates and a datagram UDP socket without explicit port binding.
			Port is assigned after first datagram transmission.
			* @param[in] family The socket operation family
		*/
		explicit Socket(AutoAssignedDatagram, SocketFamily family = SocketFamily::IPV4) noexcept(false);

		/*
			No copying.
		*/
		Socket(const Socket&) = delete;

		/*
			Move-constructor.
		*/
		inline Socket(Socket&& obj) noexcept :
			m_broken(obj.m_broken),
			m_connected(obj.m_connected),
			m_bound(obj.m_bound),
			m_type(obj.m_type),
			m_family(obj.m_family),
			m_address(std::move(obj.m_address)),
#ifdef _WIN32
			m_socket(obj.m_socket),
#endif
			m_inputStream(obj.m_inputStream.transferStreambuf(*this)),
			m_outputStream(obj.m_outputStream.transferStreambuf(*this))
		{
			obj.m_broken = false;
			obj.m_connected = false;
#ifdef _WIN32
			obj.m_socket = INVALID_SOCKET;
#endif
			if (m_inputStream.rdbuf())
				dynamic_cast<SocketStreambuf*>(m_inputStream.rdbuf())->setHostSocket(*this);
			if (m_outputStream.rdbuf())
				dynamic_cast<SocketStreambuf*>(m_outputStream.rdbuf())->setHostSocket(*this);
		}

		/*
			Destructor.
		*/
		~Socket();

	private:

#ifdef _WIN32
		/*
			Creates a socket instance with the given winsock2 socket.
			* @param[in] socket The socket
			* @param[in] type The socket operation type
			* @param[in] family The socket operation family
			* @param[in] address The socket address
		*/
		explicit Socket(SOCKET socket, SocketType type, SocketFamily family, InetAddress&& address) noexcept;
#endif

	public:

		/*
			Returns true if the socket has been connected (or is currently connected).
			NOTE! This method will throw if the socket is of datagram type.
		*/
		bool hasBeenConnected() const noexcept(false);

		/*
			Returns true if the connection was violently broken instead of 
			gracefully closed.
			NOTE! This method will throw if the socket is of datagram type.
			NOTE! This method returns false if the connection has not been 
			closed.
		*/
		bool wasBroken() const noexcept(false);

		/*
			Returns true if the socket is connected.
			WARNING! This may not be 100% accurate as the detection is 
			usually post read/write.
			NOTE! This method will throw if the socket is of datagram type.
		*/
		bool isConnected() const noexcept(false);

		/*
			Returns true if the socket is currently bound to a port.
		*/
		inline bool isBound() const noexcept
		{
			return m_bound;
		}

#ifdef _WIN32
		/*
			Returns the native socket.
		*/
		inline SOCKET getSocket() const noexcept
		{
			return m_socket;
		}
#endif
		/*
			The socket TCP connection address.
			Ie. this only exists if this socket is connected to some other end.
		*/
		inline const InetAddress& address() const noexcept
		{
			return m_address;
		}

		/*
			Returns a reference to the input stream for stream sockets.
			WARNING! This reference is not synchronized with the output stream!
			NOTE! This method will throw if the socket is of datagram type.
		*/
		SocketInputStream& getInputStream() noexcept(false);

		/*
			Returns a reference to the output stream for stream sockets.
			WARNING! This reference is not synchronized with the input stream!
			NOTE! This method will throw if the socket is of datagram type.
		*/
		SocketOutputStream& getOutputStream() noexcept(false);

		/*
			Receives a datagram through the socket.
			WARNING! This method is blocking!
			NOTE! This method will throw if the socket is of stream type.
		*/
		UniqueDatagram receiveDatagram() noexcept(false);

		/*
			Sends the given datagram.
			NOTE! This method will throw if the socket is of stream type.
			NOTE! This method will throw if the data is too large.
			* @param[in] datagram The datagram to send
		*/
		void sendDatagram(const Datagram& datagram) noexcept(false);

		/*
			Listens for and accepts an incoming connection attempt.
			WARNING! This method is blocking!
			NOTE! This method will throw if the socket is of datagram type.
		*/
		Socket acceptConnection() noexcept(false);

		/*
			Closes the socket.
		*/
		void close() noexcept;

		/*
			Sets the socket status to disconnected.
			NOTE! This should only be called when the backing socket is considered disconnected.
			* @param[in] broken True if the connection was "violently" broken instead of gracefully closed.
		*/
		inline void setDisconnected(bool broken) noexcept
		{
			m_broken = broken;
			m_connected = false;
			m_bound = false;
		}

	public:

		/*
			Move-assignment operator.
		*/
		inline Socket& operator=(Socket&& obj) noexcept
		{
#ifdef _WIN32
			destroyWin32__(m_type, m_socket);
#endif
			m_connected = obj.m_connected;
			obj.m_connected = false;
			m_type = obj.m_type;
			m_family = obj.m_family;
			m_address = std::move(m_address);
#ifdef _WIN32
			m_socket = obj.m_socket;
			obj.m_socket = INVALID_SOCKET;
#endif
			m_inputStream = obj.m_inputStream.transferStreambuf(*this);
			m_outputStream = obj.m_outputStream.transferStreambuf(*this);
			return *this;
		}

	public:

		/*
			Sets the size for input stream buffers.
			* @param[in] size The new size of the buffers
		*/
		inline static void setInputStreamBufferSize(size_t size)
		{
			s_inputBufferSize = size;
		}

		/*
			Sets the size for output stream buffers.
			* @param[in] size The new size of the buffers
		*/
		inline static void setOutputStreamBufferSize(size_t size)
		{
			s_outputBufferSize = size;
		}

	private:

#ifdef _WIN32
		/*
			Ensures that WSA is started.
		*/
		static void ensureWSAIsInitializedWin32__() noexcept(false);

		/*
			Returns the socket operation type ID for WIN32.
		*/
		static int getTypeIDWin32__(SocketType type) noexcept;

		/*
			Returns the socket operation protocol ID for WIN32.
		*/
		static int getProtocolIDWin32__(SocketType type) noexcept;

		/*
			Returns the socket operation family ID for WIN32.
		*/
		static int getFamilyIDWin32__(SocketFamily family) noexcept;

		/*
			Creates and connects a client TCP socket to the given address.
			* @param[in] address The address to connect to
			* @param[in] family The socket operation family
		*/
		static SOCKET initClientTCPSocketWin32__(const InetAddress& address, SocketFamily family) noexcept(false);

		/*
			Creates and binds a server TCP or datagram socket to the given port.
			* @param[in] port The port of the socket
			* @param[in] type The socket operation type
			* @param[in] family The socket operation family
		*/
		static SOCKET initReceiverSocketWin32__(unsigned short port, SocketType type, SocketFamily family) noexcept(false);
				
		/*
			Creates an unbound UDP socket.
			* @param[in] family The socket operation family
		*/
		static SOCKET initUnboundUDPSocketWin32__(SocketFamily family) noexcept(false);

		/*
			Destroys (disconnects and closes) the socket.
			* @param[in] type The socket operation type
			* @param[in/out] socket The socket to close
		*/
		static void destroyWin32__(SocketType type, SOCKET& socket) noexcept;

		/*
			Receives a datagram through the socket.
			WARNING! This method is locking!
			* @param[in] socket The socket
			* @param[in] bufferLength The data buffer length
		*/
		static UniqueDatagram receiveDatagramWin32__(SOCKET& socket, int bufferLength) noexcept(false);

		/*
			Sends the given datagram.
			* @param[in] datagram The datagram to send
			* @param[in] socket The socket
			* @param[in] family The socket operation family
		*/
		static void sendDatagramWin32__(const Datagram& datagram, SOCKET& socket, SocketFamily family) noexcept(false);

		/*
			Listens for and accepts an incoming connection.
			* @param[in] socket The socket
			* @param[in] family The socket operation family
		*/
		static Socket acceptConnectionWin32__(SOCKET& socket, SocketFamily family) noexcept(false);
#endif
	};

}
