#pragma once
#include <org/segames/library/io/network/socket_streambuf.h>
#include <org/segames/library/virtually_destructible.h>

#include <istream>

namespace org::segames::library
{

	/*
		Input stream for reading from stream sockets.

		* @author	Philip Rosberg
		* @since	2020-12-27
		* @edited	2020-12-27
	*/
	class SEG_API SocketInputStream :
		public std::istream,
		public VirtuallyDestructible
	{
	public:

		/*
			Creates an empty socket input stream.
		*/
		inline SocketInputStream() :
			std::istream(nullptr)
		{}

		/*
			Creates a socket input stream from the given stream buffer.
			* @param[in] streambuf The stream buffer to use
		*/
		inline SocketInputStream(SocketStreambuf* streambuf) :
			std::istream(streambuf)
		{}

		/*
			No copying.
		*/
		SocketInputStream(const SocketInputStream&) = delete;

		/*
			No explicit moving.
		*/
		SocketInputStream(SocketInputStream&&) noexcept = delete;

		/*
			Destructor.
		*/
		inline virtual ~SocketInputStream()
		{
			if(rdbuf())
				delete rdbuf();
		}

	public:

		/*
			Returns true if there is avaliable data to read.
		*/
		inline virtual bool hasAvaliableForRead() const noexcept(false)
		{
			if (rdbuf())
				return reinterpret_cast<const SocketStreambuf*>(rdbuf())->hasAvaliableForRead();
			else
				return false;
		}

		/*
			Transfers the socket stream buffer to the caller.
			* @param[in] socket The new host socket
		*/
		inline virtual SocketStreambuf* transferStreambuf(Socket& socket)
		{
			if (rdbuf())
			{
				SocketStreambuf* buf = dynamic_cast<SocketStreambuf*>(rdbuf());
				buf->setHostSocket(socket);
				set_rdbuf(nullptr);
				return buf;
			}
			else
				return nullptr;
		}

	public:

		/*
			Explicit move operation for the backing buffer.
		*/
		SocketInputStream& operator=(SocketStreambuf* buf) noexcept
		{
			if (rdbuf())
				delete rdbuf();
			set_rdbuf(buf);
			return *this;
		}

		/*
			Copy-assignment operator.
		*/
		SocketInputStream& operator=(const SocketInputStream&) = delete;

		/*
			Move-assignment operator.
		*/
		SocketInputStream& operator=(SocketInputStream&&) noexcept = delete;

	};

}