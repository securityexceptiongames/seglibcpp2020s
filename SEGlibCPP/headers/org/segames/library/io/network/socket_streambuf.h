#pragma once
#include <org/segames/library/util/array.h>

#include <streambuf>

namespace org::segames::library
{

	class Socket;

	/*
		Stream buffer for io-operations on stream sockets.

		* @author	Philip Rosberg
		* @since	2020-12-27
		* @edited	2020-12-27
	*/
	class SEG_API SocketStreambuf final : 
		public std::streambuf
	{
	private:

		/*
			The backing buffer.
		*/
		Array<char> m_buffer;

		/*
			Reference pointer to the socket.
		*/
		Socket* m_socket;

	public:
#ifdef _WIN32
		/*
			Creates a socket stream buffer of the given socket
			* @param[in] bufferSize The size of the backing buffer
			* @param[in] socket The socket
		*/
		inline SocketStreambuf(size_t bufferSize, Socket& socket) :
			m_buffer(bufferSize),
			m_socket(&socket)
		{
			setg(0, 0, 0);
			setp(m_buffer.nativePointer(), m_buffer.nativePointer() + m_buffer.nativeSize() - 1);
		}
#endif

		/*
			No copying.
		*/
		SocketStreambuf(const SocketStreambuf&) = delete;

		/*
			Move-constructor.
		*/
		SocketStreambuf(SocketStreambuf&&) noexcept = delete;

	public:

		/*
			Returns true if there is avaliable data to read.
		*/
		virtual bool hasAvaliableForRead() const noexcept(false);

		/*
			Underflow override.
		*/
		virtual std::streambuf::int_type underflow() noexcept(false) override;

		/*
			Overflow override.
		*/
		virtual std::streambuf::int_type overflow(std::streambuf::int_type value) noexcept(false) override;

		/*
			Sync override.
		*/
		virtual int sync() noexcept(false) override;

		/*
			Sets the host socket of the streambuf.
			* @param[in] socket The host socket
		*/
		inline virtual void setHostSocket(Socket& socket) noexcept
		{
			m_socket = &socket;
		}

	};

}