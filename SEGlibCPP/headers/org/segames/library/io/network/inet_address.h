#pragma once
#include <org/segames/library/string_visualizable.h>

#include <string>
#include <string_view>

namespace org::segames::library
{

	/*
		A class for representing an internet address.

		* @author	Philip Rosberg
		* @since	2020-12-19
		* @edited	2020-12-19
	*/
	class InetAddress final :
		public StringVisualizable
	{
	private:

		/*
			The port.
		*/
		unsigned short m_port;

		/*
			The address.
		*/
		std::string m_address;

	public:

		/*
			Creates an empty internet address.
		*/
		inline InetAddress() :
			m_port(0),
			m_address()
		{}


		/*
			Creates an internet address with the given port and address.
			* @param[in] port The port
			* @param[in] address The address string
		*/
		inline explicit InetAddress(unsigned short port, const std::string_view& address) :
			m_port(port),
			m_address(address)
		{}

		/*
			Copy-constructor.
		*/
		inline InetAddress(const InetAddress&) = default;

		/*
			Move-constructor.
		*/
		inline InetAddress(InetAddress&& obj) noexcept :
			m_port(obj.m_port),
			m_address(std::move(obj.m_address))
		{}

	public:

		/*
			Returns true if this is a null (zero/empty) address.
		*/
		inline bool isNullAddress() const
		{
			return m_address.size() == 0;
		}

		/*
			Returns the port of the address.
		*/
		inline unsigned short port() const
		{
			return m_port;
		}

		/*
			Returns the address string of the address.
		*/
		inline const std::string& address() const
		{
			return m_address;
		}

		/*
			Returns a string representation of the type.
		*/
		inline virtual std::string toString() const
		{
			return address() + ":" + std::to_string(port());
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline InetAddress& operator=(const InetAddress&) = default;

		/*
			Move-assignment operator.
		*/
		inline InetAddress& operator=(InetAddress&& obj) noexcept
		{
			m_port = obj.m_port;
			m_address = std::move(obj.m_address);
			return *this;
		}

	};

}