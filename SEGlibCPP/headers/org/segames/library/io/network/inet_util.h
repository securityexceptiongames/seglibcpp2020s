#pragma once
#include <org/segames/library/io/io_util.h>
#include <org/segames/library/library.h>

namespace org::segames::library
{

	/*
		Class/Namespace for network utilities.

		* @author	Philip Rosberg
		* @since	2021-01-01
		* @since	2021-01-01
	*/
	class SEG_API InetUtil final
	{
	public:

		/*
			Reads the given type from the input (network) stream, changing 
			its byte-order from the SEG network byte-order to SYSTEM_ENDIAN
			if necessary.
			* @param[in] input The input stream to read from
		*/
		template<typename T>
		inline static T next(std::istream& input)
		{
#ifdef SEG_API_NETWORK_BYTE_ORDER_BE
			return IOUtil::nextBE<T>(input);
#else
			return IOUtil::nextLE<T>(input);
#endif
		}

		/*
			Attemts to parse the given type from the given memory location, changing 
			its byte-order from the SEG network byte-order to SYSTEM_ENDIAN
			if necessary.
			WARNING! Make sure there is atleast sizeof(T) bytes avaliable at ptr.
			WARNING! This function changes the memory pointed to by ptr!
			* @param[in] ptr The memory location pointer
		*/
		template<typename T>
		inline static T parse(void* ptr) noexcept(false)
		{

#ifdef SEG_API_NETWORK_BYTE_ORDER_BE
			return IOUtil::parseBE<T>(ptr);
#else
			return IOUtil::parseLE<T>(ptr);
#endif
		}

		/*
			Attemts to parse the given type from the given memory location, changing
			its byte-order from the SEG network byte-order to SYSTEM_ENDIAN
			if necessary.
			NOTE! This method does not change the memory pointed to by ptr.
			WARNING! Make sure there is atleast sizeof(T) bytes avaliable at ptr.
			* @param[in] ptr The memory location pointer
		*/
		template<typename T>
		inline static T parseCopy(void* ptr) noexcept(false)
		{

#ifdef SEG_API_NETWORK_BYTE_ORDER_BE
			return IOUtil::parseBECopy<T>(ptr);
#else
			return IOUtil::parseLECopy<T>(ptr);
#endif
		}

		/*
			Writes the bytes of the given value to the output stream, changing 
			its byte-order from the SEG network byte-order to SYSTEM_ENDIAN
			if necessary.
			* @param[in] output The output (network) stream to write to
			* @param[in] value The value whose bytes to write to the output stream
		*/
		template<typename T>
		inline static std::ostream& put(std::ostream& output, const T& value)
		{
#ifdef SEG_API_NETWORK_BYTE_ORDER_BE
			return IOUtil::putBE<T>(output, value);
#else
			return IOUtil::putLE<T>(output, value);
#endif
		}

		/*
			Writes the bytes of the given value to the given memory location, changing
			its byte-order from the SEG network byte-order to SYSTEM_ENDIAN
			if necessary.
			Returns a pointer pointing to the end of the written memory (after the last 
			inserted byte).
			WARNING! Make sure there is atleast sizeof(T) bytes avaliable at ptr.
			* @param[in] ptr The memory location to write the value to
			* @param[in] value The value whose bytes to write to the output stream
		*/
		template<typename T>
		inline static void* write(void* ptr, const T& value) noexcept(false)
		{
#ifdef SEG_API_NETWORK_BYTE_ORDER_BE
			return IOUtil::writeBE<T>(ptr, value);
#else
			return IOUtil::writeLE<T>(ptr, value);
#endif
		}

	private:

		InetUtil() = delete;
		InetUtil(const InetUtil&) = delete;

	};

}