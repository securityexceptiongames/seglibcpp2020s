#pragma once
#include <org/segames/library/io/network/socket_streambuf.h>
#include <org/segames/library/virtually_destructible.h>

#include <ostream>

namespace org::segames::library
{

	/*
		Output stream for writing to stream sockets.
		NOTE! Writing to this stream may not send until buffer 
		overflow or flush.

		* @author	Philip Rosberg
		* @since	2020-12-27
		* @edited	2020-12-27
	*/
	class SEG_API SocketOutputStream :
		public std::ostream,
		public VirtuallyDestructible
	{
	public:

		/*
			Creates an empty socket input stream.
		*/
		inline SocketOutputStream() :
			std::ostream(nullptr)
		{}

		/*
			Creates a socket output stream from the given stream buffer.
			* @param[in] streambuf The stream buffer to use
		*/
		inline SocketOutputStream(SocketStreambuf* streambuf) :
			std::ostream(streambuf)
		{}

		/*
			No copying.
		*/
		SocketOutputStream(const SocketOutputStream&) = delete;

		/*
			No explicit moving.
		*/
		SocketOutputStream(SocketOutputStream&&) noexcept = delete;

		/*
			Destructor.
		*/
		inline virtual ~SocketOutputStream()
		{
			if (rdbuf())
				delete rdbuf();
		}

	public:

		/*
			Transfers the socket stream buffer to the caller.
			* @param[in] socket The new host socket
		*/
		inline virtual SocketStreambuf* transferStreambuf(Socket& socket)
		{
			if (rdbuf())
			{
				SocketStreambuf* buf = dynamic_cast<SocketStreambuf*>(rdbuf());
				buf->setHostSocket(socket);
				set_rdbuf(nullptr);
				return buf;
			}
			else
				return nullptr;
		}

	public:

		/*
			Explicit move operation for the backing buffer.
		*/
		SocketOutputStream& operator=(SocketStreambuf* buf) noexcept
		{
			if (rdbuf())
				delete rdbuf();
			set_rdbuf(buf);
			return *this;
		}

		/*
			Copy-assignment operator.
		*/
		SocketOutputStream& operator=(const SocketOutputStream&) = delete;

		/*
			Move-assignment operator.
		*/
		SocketOutputStream& operator=(SocketOutputStream&& obj) noexcept = delete;

	};

}