#pragma once
#include <org/segames/library/io/network/inet_address.h>
#include <org/segames/library/util/native_resource.h>
#include <org/segames/library/virtually_destructible.h>

namespace org::segames::library
{

	/*
		Interface for datagram packets.

		* @author	Philip Rosberg
		* @since	2020-12-19
		* @edited	2020-12-19
	*/
	class Datagram :
		public NativeResource,
		public VirtuallyDestructible
	{
	public:

		/*
			Returns the number of avaliable native chars.
		*/
		virtual size_t nativeSize() const override = 0;

		/*
			Returns the pointer to the begining of native chars.
		*/
		virtual char* nativePointer() override = 0;

		/*
			Returns the pointer to the begining of native chars.
		*/
		virtual const char* nativePointer() const override = 0;
				
		/*
			Returns the address of the datagram.
			Could be either receiver (send to) or sender (received from).
		*/
		virtual const InetAddress& address() const = 0;

	};

}