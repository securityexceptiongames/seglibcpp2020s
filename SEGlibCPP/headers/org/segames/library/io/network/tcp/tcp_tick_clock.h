#pragma once
#include <org/segames/library/virtually_destructible.h>

#include <memory>

namespace org::segames::library
{

	/*
		Interface for a module allowing for more control over 
		the communicator tick/loop speed.

		* @author	Philip Rosberg
		* @since	2021-03-02
		* @edited	2021-03-02
	*/
	class TCPTickClock :
		public VirtuallyDestructible
	{
	public:

		/*
			Sleeps the tick cycle to rest the thread if necessary.
			* @param[in] idleLoops The number of loop runs during which nothing was done
		*/
		virtual void sleepTick(size_t idleLoops) = 0;

		/*
			Returns a new dynamically allocated instance of this type.
		*/
		virtual std::unique_ptr<TCPTickClock> newInstance() = 0;

	};

}