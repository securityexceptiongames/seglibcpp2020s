#pragma once
#include <org/segames/library/virtually_destructible.h>

namespace org::segames::library
{

	/*
		Base interface for local (on this machine) TCP actions.

		* @author	Philip Rosberg
		* @since	2021-01-03
		* @edited	2021-01-03
	*/
	class TCPAction :
		public VirtuallyDestructible
	{
	public:

		/*
			Returns the ID of the TCP action.
		*/
		virtual int getActionID() const = 0;

	};

}