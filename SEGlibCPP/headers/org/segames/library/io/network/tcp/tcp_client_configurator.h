#pragma once
#include <org/segames/library/io/network/tcp/tcp_action_handler.h>
#include <org/segames/library/io/network/tcp/tcp_message_handler_client.h>

#include <unordered_map>

namespace org::segames::library
{

	class TCPClient;
			
	/*
		Interface for a type that configures the handlers of a client.

		* @author	Philip Rosberg
		* @since	2021-02-28
		* @edited	2021-02-28
	*/
	class TCPClientConfigurator
	{
	public:

		/*
			Returns the action handlers mapped after their action IDs.
			* @param[in] client The client requesting the action handlers
		*/
		virtual std::unordered_map<int, TCPActionHandler*> getActionHandlers(TCPClient& client) = 0;
				
		/*
			Returns the message handlers mapped after their message IDs.
			* @param[in] client The client requesting the message handlers
		*/
		virtual std::unordered_map<short, TCPMessageHandlerClient*> getMessageHandlers(TCPClient& client) = 0;

		/*
			Cleanup for the given client.
			* @param[in] client The client to cleanup after
		*/
		virtual void cleanup(TCPClient& client) = 0;

	};

}