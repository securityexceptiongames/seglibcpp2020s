#pragma once
#include <istream>

namespace org::segames::library
{

	class TCPCommunicator;

	/*
		Interface for types that process incomming TCP messages to the server.

		* @author	Philip Rosberg
		* @since	2021-02-19
		* @edited	2021-02-19
	*/
	class TCPMessageHandlerServer
	{
	public:

		/*
			Returns the message ID.
		*/
		virtual short getMessageID() const noexcept = 0;

		/*
			Called from a server side communicator when the communicator receives a message over TCP.
			* @param[in] communicator The server side communicator
			* @param[in] input The input stream from which to read
		*/
		virtual void processMessage(TCPCommunicator& communicator, std::istream& input) noexcept(false) = 0;

	};

}