#pragma once
#include <istream>

namespace org::segames::library
{

	class TCPClient;
			
	/*
		Interface for types that process incomming TCP messages to the client.

		* @author	Philip Rosberg
		* @since	2021-02-19
		* @edited	2021-02-19
	*/
	class TCPMessageHandlerClient
	{
	public:

		/*
			Returns the message ID.
		*/
		virtual short getMessageID() const noexcept = 0;

		/*
			Called from the client when the client receives a message over TCP.
			* @param[in] client The client that received the message
			* @param[in] input The input stream from which to read
		*/
		virtual void processMessage(TCPClient& client, std::istream& input) noexcept(false) = 0;

	};

}