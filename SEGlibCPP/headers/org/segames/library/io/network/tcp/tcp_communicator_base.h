#pragma once
#include <org/segames/library/io/network/tcp/tcp_action.h>
#include <org/segames/library/io/network/tcp/tcp_connection_listener.h>
#include <org/segames/library/io/network/tcp/tcp_tick_clock_default.h>
#include <org/segames/library/io/network/socket.h>
#include <org/segames/library/thread/condition.h>
#include <org/segames/library/thread/thread.h>
#include <org/segames/library/util/array_deque.h>

#include <unordered_set>
#include <atomic>

namespace org::segames::library
{

	/*
		Base communicator class for client- and server-side communicators.

		* @author	Philip Rosberg
		* @since	2021-01-01
		* @edited	2021-01-01
	*/
	class SEG_API TCPCommunicatorBase :
		private Runnable,
		public VirtuallyDestructible
	{
	private:

		/*
			The counter for generating IDs.
		*/
		static std::atomic<size_t> s_counterID;

		/*
			Storage of the IDs currently in use.
		*/
		static Volatile<std::unordered_set<size_t>> s_currentlyUsedIDs;

	protected:

		/*
			True if the communication thread should disconnect.
		*/
		bool m_disconnect;
				
		/*
			Flag for allowing the destruction of the communicator to proceed.
		*/
		bool m_destructionAllowed;

		/*
			The communicator unique ID.
		*/
		size_t m_id;

		/*
			The number of loop iterations during which the communicator has been idle.
		*/
		size_t m_idleLoops;

		/*
			The socket.
		*/
		Volatile<Socket> m_socket;

		/*
			The connection listener.
		*/
		Volatile<TCPConnectionListener*> m_connectionListener;

		/*
			The tick clock.
		*/
		std::unique_ptr<TCPTickClock> m_tickClock;

		/*
			The thread.
		*/
		Thread m_thread;

		/*
			A condition vairable for safe destruction.
		*/
		Condition m_destructionCondition;

		/*
			The queued actions to process.
		*/
		Volatile<ArrayDeque<std::unique_ptr<TCPAction>>> m_actions;

	public:

		/*
			Creates a communicator base and starts the thread.
			* @param[in] socket The socket
			* @param[in] connectionListener The connection listener
			* @param[in] tickClock The tick/loop rest clock
		*/
		inline explicit TCPCommunicatorBase(Socket&& socket, TCPConnectionListener* connectionListener, std::unique_ptr<TCPTickClock>&& tickClock) noexcept(false) :
			m_disconnect(false),
			m_destructionAllowed(false),
			m_id(generateID__()),
			m_idleLoops(0),
			m_socket(std::move(socket)),
			m_connectionListener(connectionListener),
			m_tickClock(tickClock ? std::move(tickClock) : std::unique_ptr<TCPTickClock>(new TCPTickClockDefault())),
			m_thread(this),
			m_destructionCondition(),
			m_actions()
		{}

		/*
			No copying.
		*/
		TCPCommunicatorBase(const TCPCommunicatorBase&) = delete;

		/*
			Move-constructor.
		*/
		inline TCPCommunicatorBase(TCPCommunicatorBase&& obj) noexcept :
			m_disconnect(obj.m_disconnect),
			m_destructionAllowed(obj.m_destructionAllowed),
			m_id(obj.m_id),
			m_idleLoops(obj.m_idleLoops),
			m_socket(std::move(obj.m_socket)),
			m_connectionListener(std::move(obj.m_connectionListener)),
			m_thread(std::move(obj.m_thread)),
			m_destructionCondition(),
			m_actions(std::move(obj.m_actions))
		{
			obj.m_disconnect = false;
			obj.m_destructionAllowed = true;
			obj.m_idleLoops = 0;
			std::swap(m_tickClock, obj.m_tickClock);
		}

		/*
			Destructor.
		*/
		virtual ~TCPCommunicatorBase() override;

	public:

		/*
			Returns the unique ID.
		*/
		inline virtual size_t getID() const
		{
			return m_id;
		}

		/*
			Returns the socket.
		*/
		inline virtual Volatile<Socket>& getSocket()
		{
			return m_socket;
		}

		/*
			Returns the socket.
		*/
		inline virtual const Volatile<Socket>& getSocket() const
		{
			return m_socket;
		}

		/*
			Returns the connection listener.
		*/
		inline virtual Volatile<TCPConnectionListener*>& getConnectionListener()
		{
			return m_connectionListener;
		}

		/*
			Returns the connection listener.
		*/
		inline virtual const Volatile<TCPConnectionListener*>& getConnectionListener() const
		{
			return m_connectionListener;
		}

		/*
			Returns the action queue.
		*/
		inline virtual Volatile<ArrayDeque<std::unique_ptr<TCPAction>>>& getActions()
		{
			return m_actions;
		}

		/*
			Returns the action queue.
		*/
		inline virtual const Volatile<ArrayDeque<std::unique_ptr<TCPAction>>>& getActions() const
		{
			return m_actions;
		}

		/*
			Adds the given action to the action queue.
		*/
		virtual void addAction(std::unique_ptr<TCPAction>&& action);

		/*
			Sets the connection to close.
		*/
		inline virtual void close() noexcept
		{
			m_disconnect = true;
		}

		/*
			Processes the given action and writes to the given output stream.
			* @param[in] output The output stream to write to
			* @param[in] action The action to process
		*/
		virtual void processAction(std::ostream& output, std::unique_ptr<TCPAction>&& action) noexcept(false) = 0;

		/*
			Processes the given message from the given input stream.
			* @param[in] input The input stream to read from
			* @param[in] messageID The ID of the message
		*/
		virtual void processMessage(std::istream& input, short messageID) noexcept(false) = 0;

	private:

		/*
			Thread life function.
		*/
		virtual void run() override;

		/*
			Ticks the communicator, reads input and processes actions.
		*/
		virtual void doCommunication__() noexcept(false);

	private:

		/*
			Generates an ID for the communicator.
		*/
		static size_t generateID__() noexcept(false);

	public:

		/*
			No copying.
		*/
		TCPCommunicatorBase& operator=(const TCPCommunicatorBase&) = delete;

		/*
			Move-assignment operator.
		*/
		TCPCommunicatorBase& operator=(TCPCommunicatorBase&& obj) noexcept
		{
			m_disconnect = obj.m_disconnect;
			m_destructionAllowed = obj.m_destructionAllowed;
			m_id = obj.m_id;
			m_idleLoops = obj.m_idleLoops;
			m_connectionListener = std::move(obj.m_connectionListener);
			m_socket = std::move(obj.m_socket);
			m_thread = std::move(obj.m_thread);
			m_actions = std::move(obj.m_actions);
			obj.m_disconnect = false;
			obj.m_destructionAllowed = true;
			obj.m_id = 0;
			obj.m_idleLoops = 0;
			std::swap(m_tickClock, obj.m_tickClock);
			return *this;
		}

	};

}