#pragma once
#include <org/segames/library/io/network/tcp/tcp_action.h>

#include <ostream>
#include <memory>

namespace org::segames::library
{

	/*
		Interface for types that process actions that the client or communicator should send over TCP.

		* @author	Philip Rosberg
		* @since	2021-02-19
		* @edited	2021-02-19
	*/
	class TCPActionHandler
	{
	public:

		/*
			Returns the action ID.
		*/
		virtual int getActionID() const noexcept = 0;

		/*
			Called from the client or communicator when the client or communicator receives an action to send over TCP.
			* @param[in] output The output stream to write to
			* @param[in] action The action to process
		*/
		virtual void processAction(std::ostream& output, std::unique_ptr<TCPAction>&& action) noexcept(false) = 0;

	};

}