#pragma once
#include <org/segames/library/io/network/tcp/tcp_communicator.h>

namespace org::segames::library
{
			
	/*
		An easy-to-use TCP server.

		* @author	Philip Rosberg
		* @since	2021-02-28
		* @edited	2021-02-28
	*/
	class SEG_API TCPServer :
		private Runnable,
		public VirtuallyDestructible,
		public TCPConnectionListener
	{
	protected:

		/*
			True if the server should shutdown.
		*/
		bool m_shutdown;

		/*
			Flag for allowing the destruction of the communicator to proceed.
		*/
		std::atomic<int> m_destructionAllowed;

		/*
			The handler configurator.
		*/
		TCPServerConfigurator* m_configurator;

		/*
			The connection listener.
		*/
		TCPConnectionListener* m_connectionListener;

		/*
			The communicator loop tick clock.
		*/
		std::unique_ptr<TCPTickClock> m_tickClock;

		/*
			The server socket.
		*/
		Volatile<Socket> m_socket;

		/*
			The thread.
		*/
		Thread m_thread;

		/*
			A condition vairable for safe destruction.
		*/
		Condition m_destructionCondition;

		/*
			The connected communicators.
		*/
		Volatile<std::unordered_map<int, std::unique_ptr<TCPCommunicator>>> m_communicators;

		/*
			A set with the communicators to dispose of.
		*/
		Volatile<std::unordered_set<std::unique_ptr<TCPCommunicator>>> m_disposeCommunicators;

	public:
				
		/*
			Attempts to start a server using the given port.
			* @param[in] port The port
			* @param[in] configurator The communicator handler configurator
			* @param[in] connectionListener The connection listener
			* @param[in] tickClock The tick/loop rest clock for the communicator loop
		*/
		inline explicit TCPServer(unsigned short port, TCPServerConfigurator* configurator, TCPConnectionListener* connectionListener) noexcept(false) :
			TCPServer(port, configurator, connectionListener, nullptr)
		{}

		/*
			Attempts to start a server using the given port.
			* @param[in] port The port
			* @param[in] configurator The communicator handler configurator
			* @param[in] connectionListener The connection listener
			* @param[in] tickClock The tick/loop rest clock for the communicator loop
		*/
		explicit TCPServer(unsigned short port, TCPServerConfigurator* configurator, TCPConnectionListener* connectionListener, std::unique_ptr<TCPTickClock>&& tickClock) noexcept(false);

		/*
			No copying.
		*/
		TCPServer(const TCPServer&) = delete;

		/*
			Move-constructor.
		*/
		inline TCPServer(TCPServer&& obj) noexcept :
			m_shutdown(obj.m_shutdown),
			m_destructionAllowed(obj.m_destructionAllowed.load()),
			m_configurator(obj.m_configurator),
			m_connectionListener(obj.m_connectionListener),
			m_socket(std::move(obj.m_socket)),
			m_thread(std::move(obj.m_thread)),
			m_destructionCondition(),
			m_communicators(std::move(obj.m_communicators)),
			m_disposeCommunicators()
		{
			obj.m_shutdown = false;
			obj.m_destructionAllowed = 2;
			obj.m_configurator = nullptr;
			obj.m_connectionListener = nullptr;
			std::swap(m_tickClock, obj.m_tickClock);
		}

		/*
			Destructor.
		*/
		virtual ~TCPServer() noexcept override;

	public:

		/*
			Returns the socket.
		*/
		inline virtual Volatile<Socket>& getSocket()
		{
			return m_socket;
		}

		/*
			Returns the socket.
		*/
		inline virtual const Volatile<Socket>& getSocket() const
		{
			return m_socket;
		}

		/*
			Returns the communicators.
		*/
		inline virtual Volatile<std::unordered_map<int, std::unique_ptr<TCPCommunicator>>>& getCommunicators()
		{
			return m_communicators;
		}

		/*
			Returns the communicators.
		*/
		inline virtual const Volatile<std::unordered_map<int, std::unique_ptr<TCPCommunicator>>>& getCommunicators() const
		{
			return m_communicators;
		}

		/*
			Sets the server to shutdown.
		*/
		virtual void close() noexcept;

		/*
			Called when the communicator has connected and started
			the communication thread.
			* @param[in] communicator The communicator that connected
		*/
		virtual void onConnect(TCPCommunicatorBase& communicator) override;

		/*
			Called when the communicator has connected and started
			the communication thread.
			* @param[in] communicator The communicator that disconnected
		*/
		virtual void onDisconnect(TCPCommunicatorBase& communicator) override;

	private:

		/*
			Thread life function.
		*/
		virtual void run() override;

	public:

		/*
			No copying.
		*/
		TCPServer& operator=(const TCPServer&) = delete;

		/*
			Move-assignment operator.
		*/
		inline TCPServer& operator=(TCPServer&& obj) noexcept
		{
			m_shutdown = obj.m_shutdown;
			m_destructionAllowed = obj.m_destructionAllowed.load();
			m_configurator = obj.m_configurator;
			m_connectionListener = obj.m_connectionListener;
			m_socket = std::move(obj.m_socket);
			m_thread = std::move(obj.m_thread);
			m_communicators = std::move(obj.m_communicators);
			obj.m_shutdown = false;
			obj.m_destructionAllowed = true;
			obj.m_configurator = nullptr;
			obj.m_connectionListener = nullptr;
			std::swap(m_tickClock, obj.m_tickClock);
			return *this;
		}

	};

}