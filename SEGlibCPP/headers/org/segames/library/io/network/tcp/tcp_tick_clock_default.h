#pragma once
#include <org/segames/library/io/network/tcp/tcp_tick_clock.h>
#include <org/segames/library/thread/thread.h>

namespace org::segames::library
{

	/*
		Interface for a module allowing for more control over
		the communicator tick/loop speed.

		* @author	Philip Rosberg
		* @since	2021-03-02
		* @edited	2021-03-02
	*/
	class SEG_API TCPTickClockDefault :
		public TCPTickClock
	{
	public:

		/*
			Sleeps the tick cycle to rest the thread if necessary.
			* @param[in] idleLoops The number of loop runs during which nothing was done
		*/
		inline virtual void sleepTick(size_t idleLoops)
		{
			if (idleLoops > 0)
			{
				if (idleLoops < 100)
					Thread::sleep(idleLoops * 10);
				else
					Thread::sleep(1000);
			}

		}

		/*
			Returns a new dynamically allocated instance of this type.
		*/
		inline virtual std::unique_ptr<TCPTickClock> newInstance()
		{
			return std::unique_ptr<TCPTickClock>(new TCPTickClockDefault());
		}

	};

}