#pragma once
#include <org/segames/library/io/network/tcp/tcp_client_configurator.h>
#include <org/segames/library/io/network/tcp/tcp_communicator_base.h>

namespace org::segames::library
{

	/*
		An easy-to-use TCP client.

		* @author	Philip Rosberg
		* @since	2021-02-28
		* @edited	2021-02-28
	*/
	class SEG_API TCPClient :
		public TCPCommunicatorBase
	{
	protected:

		/*
			The handler configurator.
		*/
		TCPClientConfigurator* m_configurator;

		/*
			The action handlers.
		*/
		Volatile<std::unordered_map<int, TCPActionHandler*>> m_actionHandlers;
				
		/*
			The message handlers.
		*/
		Volatile<std::unordered_map<short, TCPMessageHandlerClient*>> m_messageHandlers;

	public:
				
		/*
			Creates a client and connects it to the given address.
			* @param[in] address The address to connect to
			* @param[in] configurator The client handler configurator
			* @param[in] connectionListener The connection listener
		*/
		inline explicit TCPClient(const InetAddress& address, TCPClientConfigurator* configurator, TCPConnectionListener* connectionListener) noexcept(false) :
			TCPClient(address, configurator, connectionListener, nullptr)
		{}

		/*
			Creates a client and connects it to the given address.
			* @param[in] address The address to connect to
			* @param[in] configurator The client handler configurator
			* @param[in] connectionListener The connection listener
			* @param[in] tickClock The tick/loop rest clock
		*/
		explicit TCPClient(const InetAddress& address, TCPClientConfigurator* configurator, TCPConnectionListener* connectionListener, std::unique_ptr<TCPTickClock>&& tickClock) noexcept(false);

		/*
			No copying.
		*/
		TCPClient(const TCPClient&) = delete;

		/*
			Move-constructor.
		*/
		inline TCPClient(TCPClient&& obj) noexcept :
			TCPCommunicatorBase(std::move(obj)),
			m_configurator(obj.m_configurator),
			m_actionHandlers(std::move(obj.m_actionHandlers)),
			m_messageHandlers(std::move(obj.m_messageHandlers))
		{
			obj.m_configurator = nullptr;
		}

		/*
			Destructor.
		*/
		inline virtual ~TCPClient() override
		{
			if (m_configurator)
				m_configurator->cleanup(*this);
		}

	public:

		/*
			Returns the action handlers.
		*/
		inline virtual Volatile<std::unordered_map<int, TCPActionHandler*>>& getActionHandlers()
		{
			return m_actionHandlers;
		}

		/*
			Returns the action handlers.
		*/
		inline virtual const Volatile<std::unordered_map<int, TCPActionHandler*>>& getActionHandlers() const
		{
			return m_actionHandlers;
		}

		/*
			Returns the message handlers.
		*/
		inline virtual Volatile<std::unordered_map<short, TCPMessageHandlerClient*>>& getMessageHandlers()
		{
			return m_messageHandlers;
		}

		/*
			Returns the message handlers.
		*/
		inline virtual const Volatile<std::unordered_map<short, TCPMessageHandlerClient*>>& getMessageHandlers() const
		{
			return m_messageHandlers;
		}

		/*
			Processes the given action and writes to the given output stream.
			* @param[in] output The output stream to write to
			* @param[in] action The action to process
		*/
		virtual void processAction(std::ostream& output, std::unique_ptr<TCPAction>&& action) noexcept(false) override;

		/*
			Processes the given message from the given input stream.
			* @param[in] input The input stream to read from
			* @param[in] messageID The ID of the message
		*/
		virtual void processMessage(std::istream& input, short messageID) noexcept(false) override;

	public:

		/*
			No copying.
		*/
		TCPClient& operator=(const TCPClient&) = delete;

		/*
			Move-assignment operator.
		*/
		TCPClient& operator=(TCPClient&& obj) noexcept
		{
			TCPCommunicatorBase::operator=(std::move(obj));
			m_configurator = obj.m_configurator;
			m_actionHandlers = std::move(obj.m_actionHandlers);
			m_messageHandlers = std::move(obj.m_messageHandlers);
			obj.m_configurator = nullptr;
			return *this;
		}

	};

}