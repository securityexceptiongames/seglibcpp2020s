#pragma once
#include <org/segames/library/io/network/tcp/tcp_action.h>
#include <org/segames/library/dllmain.h>

namespace org::segames::library
{

	/*
		A simple TCP action consisting of only its ID.

		* @author	Philip Rosberg
		* @since	2021-01-03
		* @edited	2021-01-03
	*/
	class TCPMarkerAction :
		public TCPAction
	{
	protected:

		/*
			The action ID.
		*/
		int m_id;

	public:

		/*
			Creates a TCP marker action with the given ID.
			* @param[in] id The action ID
		*/
		inline explicit TCPMarkerAction(int id) :
			m_id(id)
		{}

		/*
			Copy-constructor.
		*/
		inline TCPMarkerAction(const TCPMarkerAction&) = default;

		/*
			Move-constructor.
		*/
		inline TCPMarkerAction(TCPMarkerAction&&) noexcept = default;

	public:

		/*
			Returns the ID of the TCP action.
		*/
		inline virtual int getActionID() const
		{
			return m_id;
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline TCPMarkerAction& operator=(const TCPMarkerAction&) = default;

		/*
			Move-assignment operator.
		*/
		inline TCPMarkerAction& operator=(TCPMarkerAction&&) noexcept = default;

	};

}