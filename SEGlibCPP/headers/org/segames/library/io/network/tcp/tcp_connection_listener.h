#pragma once

namespace org::segames::library
{

	class TCPCommunicatorBase;

	/*
		Listener interface for communicator related happenings.

		* @author	Philip Rosberg
		* @since	2021-01-01
		* @edited	2021-01-01
	*/
	class TCPConnectionListener
	{
	public:

		/*
			Called when the communicator has connected and started 
			the communication thread.
			* @param[in] communicator The communicator that connected
		*/
		virtual void onConnect(TCPCommunicatorBase& communicator) = 0;

		/*
			Called when the communicator has connected and started
			the communication thread.
			* @param[in] communicator The communicator that disconnected
		*/
		virtual void onDisconnect(TCPCommunicatorBase& communicator) = 0;

	};

}