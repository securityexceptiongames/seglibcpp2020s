#pragma once
#include <org/segames/library/io/network/tcp/tcp_action_handler.h>
#include <org/segames/library/io/network/tcp/tcp_message_handler_server.h>

#include <unordered_map>

namespace org::segames::library
{

	class TCPCommunicator;

	/*
		Interface for a type that configures the handlers of a communicator.

		* @author	Philip Rosberg
		* @since	2021-02-28
		* @edited	2021-02-28
	*/
	class TCPServerConfigurator
	{
	public:

		/*
			Returns the action handlers mapped after their action IDs.
			* @param[in] communicator The communicator requesting the action handlers
		*/
		virtual std::unordered_map<int, TCPActionHandler*> getActionHandlers(TCPCommunicator& communicator) = 0;

		/*
			Returns the message handlers mapped after their message IDs.
			* @param[in] communicator The communicator requesting the message handlers
		*/
		virtual std::unordered_map<short, TCPMessageHandlerServer*> getMessageHandlers(TCPCommunicator& communicator) = 0;

		/*
			Cleanup for the given communicator.
			* @param[in] communicator The communicator to cleanup after
		*/
		virtual void cleanup(TCPCommunicator& communicator) = 0;

	};

}