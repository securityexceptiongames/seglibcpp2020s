#pragma once
#include <org/segames/library/io/network/tcp/tcp_server_configurator.h>
#include <org/segames/library/io/network/tcp/tcp_communicator_base.h>

namespace org::segames::library
{

	/*
		A server-side client communicator.

		* @author	Philip Rosberg
		* @since	2021-02-28
		* @edited	2021-02-28
	*/
	class SEG_API TCPCommunicator :
		public TCPCommunicatorBase
	{
	protected:

		/*
			The handler configurator.
		*/
		TCPServerConfigurator* m_configurator;

		/*
			The action handlers.
		*/
		Volatile<std::unordered_map<int, TCPActionHandler*>> m_actionHandlers;

		/*
			The message handlers.
		*/
		Volatile<std::unordered_map<short, TCPMessageHandlerServer*>> m_messageHandlers;

	public:

		/*
			Creates a communicator inheriting the given socket.
			* @param[in] socket The socket to inherit
			* @param[in] configurator The communicator handler configurator
			* @param[in] connectionListener The connection listener
			* @param[in] tickClock The tick/loop rest clock
		*/
		explicit TCPCommunicator(Socket&& socket, TCPServerConfigurator* configurator, TCPConnectionListener* connectionListener, std::unique_ptr<TCPTickClock>&& tickClock) noexcept(false);

		/*
			No copying.
		*/
		TCPCommunicator(const TCPCommunicator&) = delete;

		/*
			Move-constructor.
		*/
		inline TCPCommunicator(TCPCommunicator&& obj) noexcept :
			TCPCommunicatorBase(std::move(obj)),
			m_configurator(obj.m_configurator),
			m_actionHandlers(std::move(obj.m_actionHandlers)),
			m_messageHandlers(std::move(obj.m_messageHandlers))
		{
			obj.m_configurator = nullptr;
		}

		/*
			Destructor.
		*/
		inline virtual ~TCPCommunicator() override
		{
			if (m_configurator)
				m_configurator->cleanup(*this);
		}

	public:

		/*
			Returns the action handlers.
		*/
		inline virtual Volatile<std::unordered_map<int, TCPActionHandler*>>& getActionHandlers()
		{
			return m_actionHandlers;
		}

		/*
			Returns the action handlers.
		*/
		inline virtual const Volatile<std::unordered_map<int, TCPActionHandler*>>& getActionHandlers() const
		{
			return m_actionHandlers;
		}

		/*
			Returns the message handlers.
		*/
		inline virtual Volatile<std::unordered_map<short, TCPMessageHandlerServer*>>& getMessageHandlers()
		{
			return m_messageHandlers;
		}

		/*
			Returns the message handlers.
		*/
		inline virtual const Volatile<std::unordered_map<short, TCPMessageHandlerServer*>>& getMessageHandlers() const
		{
			return m_messageHandlers;
		}

		/*
			Processes the given action and writes to the given output stream.
			* @param[in] output The output stream to write to
			* @param[in] action The action to process
		*/
		virtual void processAction(std::ostream& output, std::unique_ptr<TCPAction>&& action) noexcept(false) override;

		/*
			Processes the given message from the given input stream.
			* @param[in] input The input stream to read from
			* @param[in] messageID The ID of the message
		*/
		virtual void processMessage(std::istream& input, short messageID) noexcept(false) override;

	public:

		/*
			No copying.
		*/
		TCPCommunicator& operator=(const TCPCommunicator&) = delete;

		/*
			Move-assignment operator.
		*/
		TCPCommunicator& operator=(TCPCommunicator&& obj) noexcept
		{
			TCPCommunicatorBase::operator=(std::move(obj));
			m_configurator = obj.m_configurator;
			m_actionHandlers = std::move(obj.m_actionHandlers);
			m_messageHandlers = std::move(obj.m_messageHandlers);
			obj.m_configurator = nullptr;
			return *this;
		}

	};

}