#pragma once
#include <org/segames/library/io/file_attributes.h>
#include <org/segames/library/hashable.h>

#include <fstream>

namespace org::segames::library
{

	/*
		A class for representing a file in the standard file system.
		Note! This class is not thread safe, which means that a file instance
		should not be copied from one thread to another. Copying using the
		file attributes is thread safe (as thread safe as the os allows it to be).

		* @author	Philip Rosberg
		* @since	2018-11-05
		* @edited	2020-06-17
	*/
	class SEG_API File :
		public Hashable,
		public StringVisualizable
	{
	public:

		/*
			A file that is null.
			Essentially same as File().
		*/
		static const File NULL_FILE;

		/*
			A file that is the current project/program directory.
			Essentially same as File("").
		*/
		static const File PROJECT_DIR_FILE;

		/*
			A file that is the current project/program disc root directory.
			Essentially same as File("/").
		*/
		static const File DISC_ROOT_DIR_FILE;

	protected:

		/*
			The file attributes, eg. path, existance etc.
		*/
		std::shared_ptr<FileAttributes> m_attributes;

	public:

		/*
			Creates an empty file object.
		*/
		inline File() :
			m_attributes(nullptr)
		{}

		/*
			Creates a new file representation from the given path.
			* @param[in] path The path of the file
		*/
		inline explicit File(const char* path) :
			m_attributes(path ? new FileAttributes(path) : nullptr)
		{}

		/*
			Creates a new file representation from the given path.
			* @param[in] path The path of the file
		*/
		inline explicit File(const std::string_view& path) :
			File(path.data())
		{}

		/*
			Creates a copy of the file representation from the given file attributes (Thread safe copying).
			* @param[in] attrib The file attributes, gets copied
		*/
		inline explicit File(const FileAttributes& attrib) :
			m_attributes(new FileAttributes(attrib))
		{}

		/*
			Creates a file from the given file attributes (Thread safe moving).
			* @param[in] attrib The file attributes, gets moved
		*/
		inline explicit File(FileAttributes&& attrib) :
			m_attributes(new FileAttributes(attrib))
		{}

		/*
			Copy-constructor.
		*/
		inline File(const File& obj) :
			m_attributes(obj.m_attributes)
		{}

	public:

		/*
			Returns true if the file attributes is null.
		*/
		inline virtual bool isNull() const
		{
			return !m_attributes.get();
		}

		/*
			Returns true if the file exists.
		*/
		inline virtual bool exists() const
		{
			if (m_attributes.get())
				return m_attributes->exists();
			return false;
		}

		/*
			Returns true if the given file is a directory.
		*/
		inline virtual bool isDirectory() const
		{
			if (m_attributes.get())
				return m_attributes->isDirectory();
			return false;
		}

		/*
			Creates a new file and returns true if the creation succeeded.
		*/
		inline virtual bool createNewFile()
		{
			bool retVal = false;
#ifdef _WIN32
			retVal = createNewFileWindows__();
			if (retVal)
				m_attributes->update();
#endif
			return retVal;
		}

		/*
			Deletes the file or directory denoted by this path.
		*/
		inline virtual bool deleteFile()
		{
			if (m_attributes.get())
				if (std::remove(getPath().c_str()) == 0)
				{
					m_attributes->setExisting(false);
					return true;
				}
			return false;
		}

		/*
			Creates the directory to this directory.
		*/
		inline virtual bool mkdirs()
		{
			bool retVal = false;
#ifdef _WIN32
			retVal = mkdirsWindows__();
			if (retVal)
				m_attributes->update();
#endif
			return retVal;
		}

		/*
			Renames the file denoted by this pathname.
			* @param[in] path The path to rename to
		*/
		inline virtual bool renameTo(const char* path)
		{
			if (m_attributes.get())
				if (std::rename(getPath().c_str(), path) == 0)
				{
					this->m_attributes = std::shared_ptr<FileAttributes>(new FileAttributes(path));
					return true;
				}
			return false;
		}

		/*
			Renames the file denoted by this pathname.
			* @param[in] path The path to rename to
		*/
		inline virtual bool renameTo(const std::string_view& path)
		{
			return renameTo(path.data());
		}

		/*
			Renames the file denoted by this pathname.
			* @param[in] attrib The file attributes to rename to
		*/
		inline virtual bool renameTo(const FileAttributes& attrib)
		{
			return renameTo(attrib.getPath());
		}

		/*
			Renames the file denoted by this pathname.
			* @param[in] file The file to rename to
		*/
		inline virtual bool renameTo(const File& file)
		{
			return renameTo(file.getPath());
		}

		/*
			Returns the length of the file in num. bytes.
		*/
		inline virtual size_t length() const
		{
			if (m_attributes.get())
				return m_attributes->length();
			return false;
		}

		/*
			Returns the name (the last part of the path) of the file.
		*/
		inline virtual const std::string_view& getName() const
		{
			return m_attributes->getPathDissection().get(m_attributes->getPathDissection().size() - 1);
		}

		/*
			Returns the file path (absolute).
		*/
		inline virtual const std::string& getPath() const
		{
			return m_attributes->getPath();
		}

		/*
			Returns the path to the parent file.
		*/
		virtual std::string getParent() const;

		/*
			Returns the parent file or directory of this file.
		*/
		inline virtual File getParentFile() const
		{
			std::string path = getParent();
			if (path.size() == 0)
				return File();
			else
				return File(path);
		}

		/*
			Opens a binary input stream from the file.
		*/
		virtual std::ifstream openInputBin() const;

		/*
			Opens a character input stream from the file.
		*/
		virtual std::ifstream openInputChar() const;

		/*
			Opens a binary output stream to the file.
		*/
		virtual std::ofstream openOutputBin() const;

		/*
			Opens a character output stream to the file.
		*/
		virtual std::ofstream openOutputChar() const;

		/*
			Lists the names of the sub files and folders of this file.
			* @param[out] names The arraylist to place the file names in
		*/
		inline virtual void list(ArrayList<std::string>& names) const
		{
#ifdef _WIN32
			listWindows__(names);
#endif
		}

		/*
			Returns an arraylist with the names of with the sub files of this file.
		*/
		virtual ArrayList<std::string> list() const
		{
			ArrayList<std::string> out(1);
			list(out);
			return out;
		}

		/*
			Lists the sub files and folders of this file.
			* @param[out] files The arraylist to place the files in
		*/
		virtual void listFiles(ArrayList<File>& files) const
		{
#ifdef _WIN32
			listFilesWindows__(files);
#endif
		}

		/*
			Returns an arraylist with the with the sub files of this file.
		*/
		virtual ArrayList<File> listFiles() const
		{
			ArrayList<File> out(1);
			listFiles(out);
			return out;
		}

		/*
			Updates the file attributes.
		*/
		inline virtual void update()
		{
			if (m_attributes.get())
				m_attributes->update();
		}

		/*
			Returns a hash code from the path.
		*/
		inline virtual size_t hashCode() const override
		{
			if (m_attributes.get())
				return Hashable::hashCode(m_attributes->getPath());
			return 0;
		}

		/*
			Returns a string representation of the file attributes.
		*/
		inline virtual std::string toString() const override
		{
			std::string out = "File(";
			if (exists())
			{
				out += "E";
				out += (isDirectory() ? "D" : "F");
				out += "): \"";
			}
			else
				out += "--): \"";
			out += getPath();
			out += "\"";
			return out;
		}

	private:

		/*
			Creates a new file using the windows library and returns true if the creation succeeded.
		*/
		virtual bool createNewFileWindows__();

		/*
			Creates the directory to this directory using the windows library.
		*/
		virtual bool mkdirsWindows__();

		/*
			Lists the names of the sub files and folders of this file.
			* @param[out] names The arraylist to place the file names in
		*/
		virtual void listWindows__(ArrayList<std::string>& names) const;

		/*
			Lists the sub files and folders of this file using the windows library.
			* @param[out] files The arraylist to place the files in
		*/
		virtual void listFilesWindows__(ArrayList<File>& files) const;

	};

}