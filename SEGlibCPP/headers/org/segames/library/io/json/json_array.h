#pragma once
#include <org/segames/library/io/json/json_boolean.h>
#include <org/segames/library/io/json/json_number.h>
#include <org/segames/library/io/json/json_object.h>
#include <org/segames/library/io/json/json_string.h>
#include <org/segames/library/util/array_list.h>

#include <iostream>
#include <memory>

namespace org::segames::library
{

	/*
		Class for a JSON array/list element.

		* @author	Philip Rosberg
		* @since	2019-07-12
		* @edited	2020-06-18
	*/
	class SEG_API JSONArray :
		public virtual JSONElement,
		public virtual Clonable<JSONArray>
	{
	protected:

		/*
			The backing array.
		*/
		ArrayList<std::unique_ptr<JSONElement>> m_elements;

	public:

		/*
			Creates an empty json array.
		*/
		inline JSONArray() :
			m_elements()
		{}

		/*
			Creates a json array from the input stream.
			* @param[in] input The input stream to read from
			* @param[in/out] line The line number
			* @param[in/out] lineChar The character number on a line
		*/
		explicit JSONArray(std::istream& input, int& line, int& lineChar);

		/*
			Copies the given json array (deep copy).
			* @param[in] obj The json array to copy
		*/
		JSONArray(const JSONArray& obj);

		/*
			Default move constructor.
		*/
		JSONArray(JSONArray&&) = default;

	public:

		/*
			Returns the element type.
		*/
		inline virtual JSONElementType getType() const override
		{
			return JSONElementType::ARRAY;
		}

		/*
			Returns the size of the json array.
		*/
		inline virtual size_t size() const
		{
			return m_elements.size();
		}

		/*
			Returns the json element with the given index.
			* @param[in] index The index in the array of the element
		*/
		inline virtual JSONElement* get(const size_t index) const
		{
			return m_elements.get(index).get();
		}

		/*
			Returns the json element of the given type with the given index, or null if 
			the element with that name is not of the given type.
			* @param[in] index The index in the array of the element
		*/
		template<typename T>
		T* get(const size_t index) const;
				
		/*
			Returns the backing arraylist of elements.
		*/
		inline virtual ArrayList<std::unique_ptr<JSONElement>>& getElements()
		{
			return m_elements;
		}

		/*
			Returns the backing arraylist of elements.
		*/
		inline virtual const ArrayList<std::unique_ptr<JSONElement>>& getElements() const
		{
			return m_elements;
		}

		/*
			Wrties the json object using the proper decoration.
			* @param[in] tabs The number of tabs to insert at the front
			* @param[in] output The output stream to write to
		*/
		virtual void write(const int tabs, std::ostream& output) const override;

		/*
			Returns a new dynamically-allocated copy of this instance.
		*/
		inline virtual JSONArray* clone() const override
		{
			return new JSONArray(*this);
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline JSONArray& operator=(const JSONArray&) = default;

		/*
			Move-assignment operator.
		*/
		inline JSONArray& operator=(JSONArray&&) noexcept = default;

		/*
			Equals operator.
		*/
		bool operator==(const JSONArray& obj) const;

		/*
			Not-equals operator.
		*/
		inline bool operator!=(const JSONArray& obj) const
		{
			return !operator==(obj);
		}

	};

	template<>
	SEG_API JSONArray* JSONArray::get(const size_t index) const;

	template<>
	SEG_API JSONBoolean* JSONArray::get(const size_t index) const;

	template<>
	SEG_API JSONNumber* JSONArray::get(const size_t index) const;

	class JSONObject;
	template<>
	SEG_API JSONObject* JSONArray::get(const size_t index) const;

	template<>
	SEG_API JSONString* JSONArray::get(const size_t index) const;

}