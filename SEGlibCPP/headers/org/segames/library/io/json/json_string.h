#pragma once
#include <org/segames/library/io/json/json_element.h>
#include <org/segames/library/dllmain.h>
#include <org/segames/library/hashable.h>

#include <string>
#include <string_view>

namespace org::segames::library
{

	/*
		A class for a JSON string element.
		All escape sequences in the specification except unicode are supported.

		* @author	Philip Rosberg
		* @since	2019-07-02
		* @edited	2020-06-18
	*/
	class SEG_API JSONString :
		public virtual JSONElement,
		public virtual Clonable<JSONString>,
		public Hashable
	{
	protected:

		/*
			The wrapped string.
		*/
		std::string m_string;

	public:

		/*
			Creates an empty json string.
		*/
		inline JSONString() :
			m_string()
		{}

		/*
			Creates a json string from the given string.
			* @param[in] str The string to store
		*/
		inline explicit JSONString(const std::string_view& str) :
			m_string(str)
		{}

		/*
			Creates a json string by reading from the given stream.
			* @param[in] input The stream to read from
			* @param[in/out] line The line number
			* @param[in/out] lineChar The character number on a line
		*/
		inline explicit JSONString(std::istream& input, int& line, int& lineChar) :
			m_string(readJSONString(input, line, lineChar))
		{}

		/*
			Default copy constructor.
		*/
		inline JSONString(const JSONString&) = default;

		/*
			Default move constructor.
		*/
		inline JSONString(JSONString&&) = default;

	public:

		/*
			Returns the element type.
		*/
		inline virtual JSONElementType getType() const override
		{
			return JSONElementType::STRING;
		}
				
		/*
			Returns a reference to the contained string.
		*/
		inline virtual const std::string& getString() const
		{
			return m_string;
		}

		/*
			Sets the stored string.
			* @param[in] str The string to set the stored one to
		*/
		inline virtual void setString(const std::string_view& str)
		{
			m_string = str;
		}

		/*
			Wrties the json string using the proper decoration.
			* @param[in] tabs The number of tabs to insert at the front
			* @param[in] output The output stream to write to
		*/
		virtual void write(const int tabs, std::ostream& output) const override;

		/*
			Returns a hash-code for the object.
		*/
		inline virtual size_t hashCode() const override
		{
			return Hashable::hashCode(m_string);
		}

		/*
			Returns a new dynamically-allocated copy of this instance.
		*/
		inline virtual JSONString* clone() const override
		{
			return new JSONString(*this);
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline JSONString& operator=(const JSONString&) = default;

		/*
			Move-assignment operator.
		*/
		inline JSONString& operator=(JSONString&&) noexcept = default;

		/*
			Equals operator.
		*/
		inline bool operator==(const JSONString& obj) const
		{
			return obj.m_string == m_string;
		}

		/*
			Not-equals operator.
		*/
		inline bool operator!=(const JSONString& obj) const
		{
			return !operator==(obj);
		}

	public:

		/*
			Reads a json string by reading from the given stream.
			* @param[in] input The stream to read from
			* @param[in/out] line The line number
			* @param[in/out] lineChar The character number on a line
		*/
		static std::string readJSONString(std::istream& input, int& line, int& lineChar);

	private:

		/*
			Reads the escape sequence from the stream and adds it to the string.
			* @param[in] input The stream to read from
			* @param[out] str The string to write to
		*/
		static void evalEscapeSequence__(std::istream& input, std::string& str);

	};

}