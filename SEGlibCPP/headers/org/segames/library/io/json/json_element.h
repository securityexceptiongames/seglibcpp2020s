#pragma once
#include <org/segames/library/io/json/json_element_type.h>
#include <org/segames/library/clonable.h>
#include <org/segames/library/string_visualizable.h>
#include <org/segames/library/virtually_destructible.h>

#include <sstream>

namespace org::segames::library
{

	/*
		Super class for JSON elements.

		* @author	Philip Rosberg
		* @since	2019-07-02
		* @edited	2020-06-17
	*/
	class JSONElement :
		public virtual Clonable<JSONElement>,
		public StringVisualizable,
		public VirtuallyDestructible
	{
	public:

		/*
			Returns true if this element is of the given type.
			* @param[in] type Element type to test for
		*/
		inline bool isType(JSONElementType type) const
		{
			return getType() == type;
		}

		/*
			Returns the element type.
		*/
		virtual JSONElementType getType() const = 0;

		/*
			Wrties the json element using the proper decoration.
			* @param[in] tabs The number of tabs to insert at the front
			* @param[in] output The output stream to write to
		*/
		virtual void write(const int tabs, std::ostream& output) const = 0;

		/*
			Returns a new dynamically-allocated copy of this instance.
		*/
		virtual JSONElement* clone() const override = 0;

		/*
			Returns a string representation of the object.
		*/
		inline virtual std::string toString() const override
		{
			std::stringstream output;
			write(0, output);
			return output.str();
		}

	};

}