#pragma once
#include <org/segames/library/io/json/json_element.h>
#include <org/segames/library/dllmain.h>
#include <org/segames/library/hashable.h>

namespace org::segames::library
{

	/*
		A class for a JSON number element.

		* @author	Philip Rosberg
		* @since	2019-07-03
		* @edited	2020-06-17
	*/
	class SEG_API JSONNumber :
		public virtual JSONElement,
		public virtual Clonable<JSONNumber>,
		public Hashable
	{
	protected:

		/*
			Floating point value of the number.
		*/
		double m_float;

		/*
			Integer value of the number.
		*/
		long long m_int;

	public:

		/*
			Creates an empty json number.
		*/
		inline JSONNumber() :
			m_float(0),
			m_int(0)
		{}

		/*
			Creates a json number with the given value.
			* @param[in] val The value of the json number
		*/
		inline explicit JSONNumber(const long long val) :
			m_float(static_cast<double>(val)),
			m_int(val)
		{}

		/*
			Creates a json number with the given value.
			* @param[in] val The value of the json number
		*/
		inline explicit JSONNumber(const double val) :
			m_float(val),
			m_int(static_cast<long long>(val))
		{}

		/*
			Creates a json number from the input stream.
			* @param[in] input The input stream to read from
			* @param[in/out] line The line number
			* @param[in/out] lineChar The character number on a line
		*/
		explicit JSONNumber(std::istream& input, int& line, int& lineChar);

		/*
			Default copy constructor.
		*/
		inline JSONNumber(const JSONNumber&) = default;

		/*
			Default move constructor.
		*/
		inline JSONNumber(JSONNumber&&) noexcept = default;

	public:

		/*
			Returns the element type.
		*/
		virtual JSONElementType getType() const override
		{
			return JSONElementType::NUMBER;
		}

		/*
			Returns a the contained floating point value.
		*/
		virtual double getNumberf() const
		{
			return m_float;
		}
				
		/*
			Returns a the contained integer value.
		*/
		inline virtual long long getNumberi() const
		{
			return m_int;
		}

		/*
			Sets the json number.
			* @param[in] val The value to set
		*/
		inline virtual void setNumber(const double val)
		{
			m_float = val;
			m_int = static_cast<long long>(m_float);
		}

		/*
			Sets the json number.
			* @param[in] val The value to set
		*/
		virtual void setNumber(const long long val)
		{
			m_int = val;
			m_float = static_cast<double>(m_int);
		}

		/*
			Wrties the json number using the proper decoration.
			* @param[in] tabs The number of tabs to insert at the front
			* @param[in] output The output stream to write to
		*/
		inline virtual void write(const int tabs, std::ostream& output) const override
		{
			if (m_float - static_cast<double>(m_int) != 0.0)
				output << std::to_string(m_float);
			else
				output << std::to_string(m_int);
		}

		/*
			Returns a new dynamically-allocated copy of this instance.
		*/
		inline virtual JSONNumber* clone() const override
		{
			return new JSONNumber(*this);
		}

		/*
			Returns a hash-code for the object.
		*/
		inline virtual size_t hashCode() const override
		{
			const size_t* ptr = reinterpret_cast<const size_t*>(&m_float);
			return *ptr;
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline JSONNumber& operator=(const JSONNumber&) = default;

		/*
			Move-assignment operator.
		*/
		inline JSONNumber& operator=(JSONNumber&&) noexcept = default;

		/*
			Equals operator.
		*/
		inline bool operator==(const JSONNumber& obj) const
		{
			return obj.m_float == m_float;
		}

		/*
			Not-equals operator.
		*/
		inline bool operator!=(const JSONNumber& obj) const
		{
			return !operator==(obj);
		}

	public:

		/*
			Checks if the given character is allowed in a number.
			* @param[in] hex True if a hex code is expected, ie. the char has been preceded by "0x"
			* @param[in] c The character to check
		*/
		static bool checkChar(const bool hex, const int c);

	};

}