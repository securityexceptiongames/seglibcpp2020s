#pragma once

namespace org::segames::library
{

	/*
		The type of the json element.

		* @author	Philip Rosberg
		* @since	2019-06-30
		* @edited	2019-06-30
	*/
	enum class JSONElementType
	{
		OBJECT,
		ARRAY,
		STRING,
		NUMBER,
		BOOLEAN
	};

}