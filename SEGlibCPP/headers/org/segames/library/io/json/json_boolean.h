#pragma once
#include <org/segames/library/io/json/json_element.h>
#include <org/segames/library/dllmain.h>
#include <org/segames/library/hashable.h>

namespace org::segames::library
{
			
	/*
		A class for a JSON boolean element.

		* @author	Philip Rosberg
		* @since	2019-07-03
		* @edited	2020-06-18
	*/
	class SEG_API JSONBoolean :
		public virtual JSONElement,
		public virtual Clonable<JSONBoolean>,
		public Hashable
	{
	protected:

		/*
			The boolean value.
		*/
		bool m_flag;

	public:

		/*
			Creates an empty json boolean.
		*/
		inline JSONBoolean() :
			m_flag(false)
		{}

		/*
			Sets the value of the json boolean.
			* @param[in] flag The boolean value to set
		*/
		inline explicit JSONBoolean(const bool flag) :
			m_flag(flag)
		{}

		/*
			Creates a json boolean from the input stream.
			* @param[in] input The input stream to read from
			* @param[in/out] line The line number
			* @param[in/out] lineChar The character number on a line
		*/
		explicit JSONBoolean(std::istream& input, int& line, int& lineChar);

		/*
			Copy-constructor.
		*/
		inline JSONBoolean(const JSONBoolean&) = default;

		/*
			Move-constructor.
		*/
		inline JSONBoolean(JSONBoolean&&) noexcept = default;

	public:

		/*
			Returns the element type.
		*/
		inline virtual JSONElementType getType() const override
		{
			return JSONElementType::BOOLEAN;
		}

		/*
			Returns the value of the json boolean.
		*/
		inline virtual bool getBoolean() const
		{
			return m_flag;
		}

		/*
			Sets the boolean value.
			* @param[in] flag The boolean value
		*/
		inline virtual void setBoolean(const bool flag)
		{
			m_flag = flag;
		}
				
		/*
			Writes the json boolean using the proper decoration.
			* @param[in] tabs The number of tabs to insert at the front
			* @param[in] output The output stream to write to
		*/
		inline virtual void write(const int tabs, std::ostream& output) const override
		{
			output << (m_flag ? "true" : "false");
		}

		/*
			Returns a new dynamically-allocated copy of this instance.
		*/
		inline virtual JSONBoolean* clone() const override
		{
			return new JSONBoolean(*this);
		}

		/*
			Returns a hash-code for the object.
		*/
		inline virtual size_t hashCode() const override
		{
			return (size_t)m_flag;
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline JSONBoolean& operator=(const JSONBoolean&) = default;

		/*
			Move-assignment operator.
		*/
		inline JSONBoolean& operator=(JSONBoolean&&) noexcept = default;

		/*
			Equals operator.
		*/
		inline bool operator==(const JSONBoolean& obj) const
		{
			return obj.m_flag == m_flag;
		}

		/*
			Not-equals operator.
		*/
		inline bool operator!=(const JSONBoolean& obj) const
		{
			return !operator==(obj);
		}

	public:

		/*
			Checks if the given character is allowed in a boolean.
			* @param[in] c The character to check
		*/
		static bool checkChar(const int c);

	};

}