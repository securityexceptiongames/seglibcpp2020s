#pragma once
#include <org/segames/library/io/json/json_array.h>
#include <org/segames/library/io/json/json_boolean.h>
#include <org/segames/library/io/json/json_number.h>
#include <org/segames/library/io/json/json_string.h>

#include <unordered_map>
#include <iostream>
#include <memory>

namespace org::segames::library
{
			
	/*
		Class for a JSON object element.

		* @author	Philip Rosberg
		* @since	2019-07-12
		* @edited	2020-06-18
	*/
	class SEG_API JSONObject :
		public virtual JSONElement,
		public virtual Clonable<JSONObject>
	{
	protected:

		/*
			The elements in the object.
		*/
		std::unordered_map<std::string, std::unique_ptr<JSONElement>> m_elements;

	public:

		/*
			Creates an empty json object.
		*/
		inline JSONObject() :
			m_elements()
		{}

		/*
			Creates a json object from the input stream.
			* @param[in] input The input stream to read from
			* @param[in/out] line The line number
			* @param[in/out] lineChar The character number on a line
		*/
		explicit JSONObject(std::istream& input, int& line, int& lineChar);

		/*
			Copy constructor.
		*/
		JSONObject(const JSONObject& obj);

		/*
			Move constructor.
		*/
		inline JSONObject(JSONObject&& obj) :
			m_elements(std::move(obj.m_elements))
		{}

	public:

		/*
			Returns the element type.
		*/
		inline virtual JSONElementType getType() const override
		{
			return JSONElementType::OBJECT;
		}

		/*
			Returns the json element with the given name, or null if no such element exists.
			* @param[in] name The identifier name of the element
		*/
		inline virtual JSONElement* get(const std::string& name) const
		{
			auto itr = m_elements.find(name);
			if (itr != m_elements.end())
				return itr->second.get();
			else
				return nullptr;
		}

		/*
			Returns the json element of the given type with the given name, or null if no 
			such element exists or if the element with that name is not of the given type.
			* @param[in] name The identifier name of the json element
		*/
		template<typename T>
		T* get(const std::string& name) const;
				
		/*
			Returns a reference to the mapped elements in the json object.
		*/
		inline virtual std::unordered_map<std::string, std::unique_ptr<JSONElement>>& getElements()
		{
			return m_elements;
		}

		/*
			Returns a reference to the mapped elements in the json object.
		*/
		inline virtual const std::unordered_map<std::string, std::unique_ptr<JSONElement>>& getElements() const
		{
			return m_elements;
		}

		/*
			Wrties the json object using the proper decoration.
			* @param[in] tabs The number of tabs to insert at the front
			* @param[in] output The output stream to write to
		*/
		virtual void write(const int tabs, std::ostream& output) const override;

		/*
			Returns a new dynamically-allocated copy of this instance.
		*/
		inline virtual JSONObject* clone() const override
		{
			return new JSONObject(*this);
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline JSONObject& operator=(const JSONObject&) = default;

		/*
			Move-assignment operator.
		*/
		inline JSONObject& operator=(JSONObject&&) noexcept = default;

		/*
			Equals operator.
		*/
		bool operator==(const JSONObject& obj) const;

		/*
			Not-equals operator.
		*/
		inline bool operator!=(const JSONObject& obj) const
		{
			return !operator==(obj);
		}

	};

	class JSONArray;
	template<>
	SEG_API JSONArray* JSONObject::get(const std::string& name) const;

	template<>
	SEG_API JSONBoolean* JSONObject::get(const std::string& name) const;

	template<>
	SEG_API JSONNumber* JSONObject::get(const std::string& name) const;

	template<>
	SEG_API JSONObject* JSONObject::get(const std::string& name) const;

	template<>
	SEG_API JSONString* JSONObject::get(const std::string& name) const;

}