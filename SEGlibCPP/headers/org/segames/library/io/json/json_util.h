#pragma once
#include <org/segames/library/io/json/json_element.h>
#include <org/segames/library/io/file.h>

#include <iostream>

namespace org::segames::library
{

	/*
		A class/namespace for reading and writing json.

		* @author	Philip Rosberg
		* @since	2019-07-12
		* @edited	2020-06-18
	*/
	class SEG_API JSONUtil final
	{
	public:

		/*
			Parses json from the given file, returns null if no json was found.
			* @param[in] file The file to read from
		*/
		inline static std::unique_ptr<JSONElement> read(const File& file)
		{
			std::ifstream input = file.openInputChar();
			return read(input);
		}

		/*
			Parses json from the given stream, returns null if no json was found.
			* @param[in] input The stream to read from
		*/
		static std::unique_ptr<JSONElement> read(std::istream& input);

		/*
			Writes the json element (and all sub elements) to the given file.
			NOTE! The file must exist or this method will throw a file not found exception.
			* @param[in] element The json element to write to the file
			* @param[in] file The file to write to
		*/
		inline static void write(const JSONElement& element, const File& file)
		{
			std::ofstream output = file.openOutputChar();
			write(element, output);
		}

		/*
			Writes the json element (and all sub elements) to the given stream.
			* @param[in] element The json element to write to the stream
			* @param[in] output The output stream to write to
		*/
		inline static void write(const JSONElement& element, std::ostream& output)
		{
			element.write(0, output);
		}

	private:

		/*
			Returns true if the character search should continue
			* @param[in] c The current character
		*/
		static bool characterSearchContinue__(const int c);

	private:

		/*
			Ensure no instance of the class.
		*/
		JSONUtil() = delete;
		JSONUtil(const JSONUtil&) = delete;
		JSONUtil(JSONUtil&&) = delete;

	};

}