#pragma once
#include <org/segames/library/date_time.h>

#include <iostream>

#ifdef ERROR
#undef ERROR
#endif

namespace org::segames::library
{

	/*
		Field storing library core variables and functions.

		* @author	Philip Rosberg
		* @since	2018-10-26
		* @edited	2020-06-22
	*/
	class SEG_API Core final
	{
	public:

		/*
			The enum of labels to write at the start of the print.

			* @author	Philip Rosberg
			* @since	2018-10-27
			* @edited	2018-10-27
		*/
		enum class PrintLabel
		{
			NONE = -1,
			INFO = 0,
			WARNING = 1,
			ERROR = 2,
			NOTIFICATION = 3
		};

	private:
		/*
			True if the time should be printed.
		*/
		static bool s_printTime;

		/*
			The standard input stream.
		*/
		static std::istream* s_in;

		/*
			The standard output stream for errors.
		*/
		static std::ostream* s_err;

		/*
			The standard output stream for warnings.
		*/
		static std::ostream* s_wrn;

		/*
			The standard output stream.
		*/
		static std::ostream* s_out;

		/*
			The message labels.
		*/
		static const char* s_labels[4];

	public:

		/*
			Returns a reference to the standard input stream.
		*/
		inline static std::istream& in()
		{
			return *s_in;
		}

		/*
			Returns a reference to the standard output stream for errors.
		*/
		inline static std::ostream& err()
		{
			return *s_err;
		}

		/*
			Returns a reference to the standard output stream for warnings.
		*/
		inline static std::ostream& wrn()
		{
			return *s_wrn;
		}

		/*
			Returns a reference to the standard output stream.
		*/
		inline static std::ostream& out()
		{
			return *s_out;
		}

		/*
			Returns the date and time at which the library was compiled.
		*/
		static const DateTime& libraryCompileDate();

		/*
			Sets the standard input stream.
			* @param[in] in The standard input stream
		*/
		inline static void setIn(std::istream& in)
		{
			s_in = &in;
		}

		/*
			Sets the standard error output stream.
			* @param[in] err The standard error output stream
		*/
		inline static void setErr(std::ostream& err)
		{
			s_err = &err;
		}

		/*
			Sets the standard warning output stream.
			* @param[in] wrn The standard warning output stream
		*/
		inline static void setWrn(std::ostream& wrn)
		{
			s_wrn = &wrn;
		}

		/*
			Sets the standard output stream.
			* @param[in] out The standard output stream
		*/
		inline static void setOut(std::ostream& out)
		{
			s_out = &out;
		}

		/*
			Sets the output stream for errors, warnings and standard output.
			* @param[in] out The output stream for errors, warnings and standard output
		*/
		inline static void setAllOut(std::ostream& out)
		{
			s_out = (s_wrn = (s_err = &out));
		}

		/*
			Sets if the time should be printed.
			* @param[in] flag True if the time should be printed
		*/
		inline static void setPrintTime(const bool flag)
		{
			s_printTime = flag;
		}

		/*
			Prints the given message with the NONE label.
			* @param[in] message The message to print
		*/
		inline static void println(const std::string_view& message)
		{
			println(PrintLabel::NONE, message);
		}

		/*
			Prints the given message with the INFO label.
			* @param[in] message The message to print
		*/
		inline static void printinf(const std::string_view& message)
		{
			println(PrintLabel::INFO, message);
		}

		/*
			Prints the given message with the given label.
			* @param[in] label The label to print
			* @param[in] message The message to print
		*/
		static void println(const PrintLabel label, const std::string_view& message);
				
	private:

		/*
			Prints the given message with the given label to the given stream.
			* @param[in] label The label to print
			* @param[in] message The message to print
			* @param[in] stream The stream to print to
		*/
		static void printlnInternal__(const PrintLabel label, const std::string_view& message, std::ostream& stream);

	};

}