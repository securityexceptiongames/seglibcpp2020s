#pragma once

namespace org::segames::library
{

	class ThreadCarrier;

	/*
		Interface for a class that is called by the thread carrier when
		the carrier finishes one run.

		* @author	Philip Rosberg
		* @since	2018-08-09
		* @edited	2019-08-09
	*/
	class ThreadCarrierCallback
	{
	public:

		/*
			Called by the thread carrier when it has finished a run.
			* @param[in] carrier The thread carrier
		*/
		virtual void callbackFun(ThreadCarrier* carrier) = 0;

	};

}