#pragma once
#include <org/segames/library/thread/condition.h>
#include <org/segames/library/thread/runnable.h>
#include <org/segames/library/thread/thread_carrier_callback.h>
#include <org/segames/library/dllmain.h>

#include <thread>

namespace org::segames::library
{

	class ThreadCarrier;

	/*
		Signature for a function that is called by the thread carrier
		when it finishes one loop if and only if the callback object is
		null.
	*/
	typedef void(*ThreadCarrierCallbackFun)(ThreadCarrier*);

	/*
		An auxilliary class for storing a thread and its properties.
		Mainly used by Thread.

		* @author	Philip Rosberg
		* @since	2018-08-06
		* @edited	2020-06-15
	*/
	class SEG_API ThreadCarrier
	{
	protected:

		/*
			True if the thread should stop.
		*/
		bool m_stop;

		/*
			The callback to call on one loop run finish.
		*/
		ThreadCarrierCallback* m_callback;
				
		/*
			The callback function to use if m_callback is null.
		*/
		ThreadCarrierCallbackFun m_callbackFun;
				
		/*
			The runnable to call on thread "start".
		*/
		Runnable* m_runnable;

		/*
			The condition for waiting the thread.
		*/
		Condition m_condition;

		/*
			The carried thread.
		*/
		std::thread m_thread;

	public:

		/*
			Creates a zeroed thread carrier (one that cannot be started).
		*/
		ThreadCarrier();

		/*
			Creates a new thread carrier.
			* @param[in] callbackFun The callback function to use if the callback object is null.
		*/
		explicit ThreadCarrier(ThreadCarrierCallbackFun callbackFun);

		/*
			Copy constructor.
		*/
		ThreadCarrier(const ThreadCarrier&);

		/*
			Move constructor.
		*/
		ThreadCarrier(ThreadCarrier&&) noexcept;

		/*
			Destructor.
		*/
		virtual ~ThreadCarrier();

	public:

		/*
			Returns true if the internal thread is currently in a runnable.
		*/
		inline virtual bool isRunning() const
		{
			return m_runnable != nullptr;
		}

		/*
			Returns true if stop has been requested for the thread.
		*/
		inline virtual bool isStopped() const
		{
			return m_stop;
		}

		/*
			Returns a pointer to the backing thread.
		*/
		inline virtual const std::thread* getThread() const
		{
			return &m_thread;
		}

		/*
			Sets the callback object.
			* @param[in] callback The callback object to call when a thread loop is done
		*/
		inline virtual void setCallback(ThreadCarrierCallback* callback)
		{
			m_callback = callback;
		}

		/*
			Starts the given runnable if it can be started.
			Returns true if the start has occured or false if the thread was already running or any other issue was encountered.
			* @param[in] runnable The runnable to call
		*/
		inline virtual bool start(Runnable* runnable)
		{
			if (!isRunning() && m_callbackFun != nullptr)
			{
				m_runnable = runnable;
				m_condition.notify();
				return true;
			}
			return false;
		}

		/*
			Requests the internal thread to run out and not continue with another loop.
		*/
		inline virtual void requestStop()
		{
			m_stop = true;
			if (!isRunning())
				m_condition.notify();
		}

	private:
				
		/*
			The thread life method.
		*/
		virtual void threadLife();

	private:

		/*
			The thread start method.
			* @param[in] carrier The thread carrier that started the thread
		*/
		inline static void threadStart(ThreadCarrier* carrier)
		{
			carrier->threadLife();
		}

	};

}