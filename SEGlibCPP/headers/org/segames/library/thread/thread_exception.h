#pragma once
#include <org/segames/library/exception.h>

namespace org::segames::library
{

	/*
		An exception for various thread related errors.

		* @author	Philip Rosberg
		* @since	2018-08-09
		* @edited	2020-06-15
	*/
	class SEG_API ThreadException :
		public Exception
	{
	public:

		/*
			Creates a thread exception without message.
		*/
		inline ThreadException() :
			Exception("org::segames::library::ThreadException", "", StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1))
		{}

		/*
			Creates a thread exception with the given message.
			* @param[in] message The additional message to display
		*/
		inline explicit ThreadException(const std::string_view& message) :
			Exception("org::segames::library::ThreadException", message, StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1))
		{}

	};

}