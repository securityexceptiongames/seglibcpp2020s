#pragma once
#include <mutex>
#include <algorithm>

namespace org::segames::library
{

	/*
		A thread-wise lockable variable to ensure thread safety.

		Usage:
		Volatile<int> var(123); //Variable somewhere

		//Piece of code changing the volatile variable
		var.lock();

		int& v = *var;
		v ++;

		var.unlock();

		NOTE! This variable is possible to lock with std::unique_lock and similiar.

		* @author	Philip Rosberg
		* @since	2018-06-11
		* @edited	2019-08-29
	*/
	template<typename T>
	class Volatile final
	{
	private:

		/*
			The stored value
		*/
		T m_var;

		/*
			Mutex.
		*/
		std::mutex m_mutex;

	public:

		/*
			Creates a volatile var without setting the internal variable.
		*/
		inline Volatile() :
			m_var(T()),
			m_mutex()
		{}
				
		/*
			Creates a volatile var from the given variable.
			* @param[in] var The variable to wrap
		*/
		inline explicit Volatile(const T& var) :
			m_var(var),
			m_mutex()
		{}

		/*
			Creates a volatile var from the given variable.
			* @param[in] var The variable to wrap
		*/
		inline explicit Volatile(T&& var) :
			m_var(std::move(var)),
			m_mutex()
		{}

		/*
			Copies the stored variable without changing the lock-state.
			NOTE! This operation is synchronized. The volatile being copied should not be locked!
			* @param[in] obj The volatile variable to copy from
		*/
		inline Volatile(const Volatile<T>& obj) :
			m_var(T()),
			m_mutex()
		{
			std::unique_lock<Volatile<T>> lock(obj);
			m_var = obj.m_var;
		}

		/*
			Copies the stored variable without changing the lock-state.
			NOTE! This operation is synchronized. The volatile being moved should not be locked!
			* @param[in] obj The volatile variable to copy from
		*/
		inline Volatile(Volatile<T>&& obj) noexcept :
			m_var(T()),
			m_mutex()
		{
			std::unique_lock<Volatile<T>> lock(obj);
			m_var = std::move(obj.m_var);
		}

	public:

		/*
			Returns the backing mutex.
		*/
		inline std::mutex& mutex_b() noexcept
		{
			return m_mutex;
		}

		/*
			Locks the volatile variable so only one thread can use it at the same time.
		*/
		inline void lock()
		{
			m_mutex.lock();
		}

		/*
			Unlocks the volatile variable.
		*/
		inline void unlock()
		{
			m_mutex.unlock();
		}
				
		/*
			Returns a reference to the stored variable.
		*/
		inline T& operator*() noexcept
		{
			return m_var;
		}

		/*
			Returns a reference to the stored variable.
		*/
		inline const T& operator*() const noexcept
		{
			return m_var;
		}

		/*
			Override of the member access operator.
		*/
		inline T* operator->() noexcept
		{
			return &m_var;
		}

		/*
			Override of the member access operator.
		*/
		inline const T* operator->() const noexcept
		{
			return &m_var;
		}

	public:

		/*
			Copies the stored variable without changing the lock-state.
			NOTE! This operation is synchronized.
			NOTE! This volatile should not be locked!
			NOTE! The volatile being copied should not be locked!
			* @param[in] obj The volatile variable to copy from
		*/
		inline Volatile<T>& operator=(const Volatile<T>& obj)
		{
			std::unique_lock<Volatile<T>> lock(*this);
			std::unique_lock<Volatile<T>> lock2(obj);
			m_var = obj.m_var;
			return *this;
		}

		/*
			Moves the stored variable without changing the lock-state.
			NOTE! This operation is synchronized.
			NOTE! This volatile should not be locked!
			NOTE! The volatile being moved should not be locked!
			* @param[in] obj The volatile variable to copy from
		*/
		inline Volatile<T>& operator=(Volatile<T>&& obj) noexcept
		{
			std::unique_lock<Volatile<T>> lock(*this);
			std::unique_lock<Volatile<T>> lock2(obj);
			m_var = std::move(obj.m_var);
			return *this;
		}

	};

}