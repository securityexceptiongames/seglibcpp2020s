#pragma once

namespace org::segames::library
{

	/*
		Interface for classes that should be able to be started by a thread 
		in a nice and controlled way.

		* @author	Philip Rosberg
		* @since	2018-08-06
		* @edited	2019-08-06
	*/
	class Runnable
	{
	public:

		/*
			Usually called by a thread when it starts.
			Ie. this is the entry point of a thread.
		*/
		virtual void run() = 0;

	};

}