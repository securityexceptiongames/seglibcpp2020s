#pragma once
#include <org/segames/library/thread/volatile.h>

namespace org::segames::library
{

	/*
		A more exception safe way of using Volatile<T>::lock() and Volatile<T>::unlock().

		Usage:
		Volatile<int> var(123); // Variable somewhere

		// Piece of code changing the volatile variable
		{
			Synchronized<int> lock(var);
			int& v = var.var();
			v ++;
		}

		// Alternatively using operators
		{
			Synchronized<int> lock(var);
			(*var)++;
		}

		* @author	Philip Rosberg
		* @since	2018-06-11
		* @edited	2020-06-15
	*/
	template<typename T>
	class Synchronized final
	{
	private:

		/*
			The volatile var to lock and unlock.
		*/
		Volatile<T>& m_var;

	public:

		/*
			Creates a thread-synchronization lock on the given volatile variable.
			* @param[in] var The volatile variable to lock
		*/
		inline explicit Synchronized(Volatile<T>& var) :
			m_var(var)
		{
			m_var.lock();
		}

		/*
			Creates a thread-synchronization lock on the given volatile variable.
			* @param[in] var The volatile variable to lock
		*/
		inline explicit Synchronized(const Volatile<T>& var) :
			Synchronized(const_cast<Volatile<T>&>(var))
		{}

		/*
			Ensure no copying of locks.
		*/
		Synchronized(const Synchronized<T>&) = delete;

		/*
			Destructor.
		*/
		inline ~Synchronized()
		{
			m_var.unlock();
		}

	public:

		/*
			Ensure no copying of locks.
		*/
		Synchronized<T>& operator=(const Synchronized<T>&) const = delete;

	};

	/*
		Class/namespace for quickly instantiating locks.
		NOTE! The returned lock must be stored!

		* @author	Philip Rosberg
		* @since	2020-06-15
		* @edited	2020-08-29
	*/
	class Synchronize final
	{
	public:

		/*
			Locks the given volatile variable.
			NOTE! The returned lock must be stored!
			* @param[in] var The volatile variable to lock
		*/
		template<typename T>
		inline static Synchronized<T> lock(Volatile<T>& var)
		{
			return Synchronized<T>(var);
		}

		/*
			Locks the given volatile variable.
			NOTE! The returned lock must be stored!
			* @param[in] var The volatile variable to lock
		*/
		template<typename T>
		inline static Synchronized<T> lock(const Volatile<T>& var)
		{
			return Synchronized<T>(var);
		}

	private:

		Synchronize() = delete;
		Synchronize(const Synchronize&) = delete;

	};

}