#pragma once
#include <org/segames/library/thread/synchronized.h>
#include <org/segames/library/thread/condition.h>
#include <org/segames/library/thread/thread_exception.h>

#include <atomic>

namespace org::segames::library
{

	/*
		A resource to be shared and read by multiple threads at the same time.
		If the resource has to be written so the posibility to lock it exists.
		NOTE! This resource is possible to lock with std::unique_lock and similiar.

		* @author	Philip Rosberg
		* @since	2019-11-08
		* @edited	2021-07-31
	*/
	template<typename T>
	class Shared final
	{
	private:

		/*
			The stored variable.
		*/
		T m_var;

		/*
			Mutex for coordinating concurrent locking of the shared resource.
		*/
		std::mutex m_lockQueueMutex;

		/*
			True if the shared variable has been locked or is in the process of being locked.
		*/
		Volatile<std::atomic<bool>> m_locked;

		/*
			The number of current users.
		*/
		std::atomic<int> m_users;

		/*
			The wait condition for locking.
		*/
		Condition m_lockCondition;

	public:

		/*
			Creates a shared resource without setting the internal variable.
		*/
		inline Shared() :
			m_var(),
			m_lockQueueMutex(),
			m_locked(),
			m_users(0),
			m_lockCondition()
		{
			*m_locked = false;
		}
				
		/*
			Creates a shared resource with the given internal variable.
			* @param[in] var The variable to copy into this shared resource
		*/
		inline explicit Shared(const T& var) :
			m_var(var),
			m_lockQueueMutex(),
			m_locked(),
			m_users(0),
			m_lockCondition()
		{
			*m_locked = false;
		}

		/*
			Creates a shared resource with the given internal variable.
			* @param[in] var The variable to move into this shared resource
		*/
		inline explicit Shared(T&& var) :
			m_var(std::move(var)),
			m_lockQueueMutex(),
			m_locked(),
			m_users(0),
			m_lockCondition()
		{
			*m_locked = false;
		}

		/*
			Creates a copy of the given shared resource with a copy of its variable.
			* @param[in] shared The shared resource to copy
		*/
		inline Shared(const Shared<T>& shared) :
			Shared(shared.m_var)
		{}

		/*
			Moves the given shared resource's variable into this new one.
			* @param[in] shared The shared resource to copy
		*/
		inline Shared(Shared<T>&& shared) :
			Shared(shared.isLocked() ? 
				throw ThreadException("Cannot move a locked shared resource!") : 
				std::move(shared.m_var))
		{}

	public:

		/*
			Returns true if the shared resource is locked.
		*/
		inline bool isLocked() const noexcept
		{
			return *m_locked;
		}

		/*
			Returns the number of threads currently using this resource.
		*/
		inline int getNumUsers() const noexcept
		{
			return m_users;
		}

		/*
			Locks the shared resource.
		*/
		inline void lock()
		{
			m_lockQueueMutex.lock();

			{
				Synchronized<std::atomic<bool>> lock(m_locked);
				*m_locked = true;
			}

			do
			{
				m_lockCondition.waitIf([](Shared<T>& ref) { return ref.m_users > 0; }, *this);
			} while (m_users > 0);

		}

		/*
			Unlocks the shared resource.
		*/
		inline void unlock()
		{
			Synchronized<std::atomic<bool>> lock(m_locked);
			*m_locked = false;

			m_lockQueueMutex.unlock();
		}
				
		/*
			Attempts to use the resource and returns true on success or false on 
			failure.
			* @param[in] func The code (function) to use the resource in, the first 
				parameter will be the internally stored variable.
			* @param[in] args The other arguments to pass to the function
		*/
		template<typename Func, typename... Args>
		inline bool use(Func&& func, Args&&... args)
		{
			{
				Synchronized<std::atomic<bool>> lock(m_locked);
				if (isLocked())
					return false;
				else
					m_users++;
			}

			func(m_var, args...);

			if (--m_users == 0)
				m_lockCondition.notifyAll();
			return true;
		}

		/*
			Returns a reference to the internally stored variable.
			NOTE! The shared resource must be locked before this operation!
		*/
		inline T& operator*()
		{
			if (!isLocked())
				throw ThreadException("Cannot directly access an unlocked shared resource! (UNSAFE)");
			else
				return m_var;
		}

		/*
			Returns a reference to the internally stored variable.
			NOTE! The shared resource must be locked before this operation!
		*/
		inline const T& operator*() const
		{
			return const_cast<Shared<T>*>(this)->operator*();
		}

		/*
			Override of the member access operator.
			NOTE! The shared resource must be locked before this operation!
		*/
		inline T* operator->()
		{
			if (!isLocked())
				throw ThreadException("Cannot directly access an unlocked shared resource! (UNSAFE)");
			else
				return &m_var;
		}

		/*
			Override of the member access operator.
			NOTE! The shared resource must be locked before this operation!
		*/
		inline const T* operator->() const
		{
			return const_cast<Shared<T>*>(this)->operator->();
		}

	public:

		/*
			Copy assignement operator override.
			* @param[in] shared The shared resource whose variable to copy
		*/
		inline Shared<T>& operator=(const Shared<T>& shared)
		{
			m_var = shared.m_var;
			return *this;
		}

		/*
			Move assignement operator override.
			* @param[in] shared The shared resource whose variable to move
		*/
		inline Shared<T>& operator=(Shared<T>&& shared)
		{
			if (isLocked() || shared.isLocked())
				throw ThreadException("Cannot move a locked shared resource!");
			else
				m_var = std::move(shared.m_var);
			return *this;
		}

	};

}