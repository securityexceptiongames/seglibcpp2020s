#pragma once
#include <mutex>
#include <condition_variable>

namespace org::segames::library
{

	/*
		A variable used to wait and notify threads.

		* @author	Philip Rosberg
		* @since	2019-08-08
		* @edited	2020-07-31
	*/
	class Condition final
	{
	protected:

		/*
			Mutex for non-windows systems.
		*/
		std::mutex m_mutex;

		/*
			Condition variable to allow for waiting.
		*/
		std::condition_variable m_condition;

	public:

		/*
			Creates a new condition variable.
		*/
		inline Condition() :
			m_mutex(),
			m_condition()
		{}

		/*
			Copy constructor.
		*/
		inline Condition(const Condition&) :
			Condition()
		{}

		/*
			Move constructor.
		*/
		inline Condition(Condition&&) noexcept :
			Condition()
		{}

	public:

		/*
			Waits the thread calling this method until the condition is notified.
		*/
		inline void wait()
		{
			std::unique_lock<std::mutex> lock(m_mutex);
			m_condition.wait(lock);
		}

		/*
			Waits the thread calling this method until the condition is notified.
		*/
		inline void wait() const
		{
			const_cast<Condition*>(this)->wait();
		}

		/*
			Executes the given function with the given parameters after the mutex lock has 
			been aquired and then waits the thread calling this function. The flow is:
			AQUIRE MUTEX LOCK -> EXECUTE FUNCTION -> WAIT
			* @param[in] func The function to call under mutex lock
			* @param[in] args The arguments to pass to the function
		*/
		template<typename Func, typename... Args>
		inline void wait(Func&& func, Args&&... args)
		{
			std::unique_lock<std::mutex> lock(m_mutex);
			func(args...);
			m_condition.wait(lock);
			m_mutex.unlock();
		}
				
		/*
			Executes the given function with the given parameters after the mutex lock has 
			been aquired and then waits the thread calling this function. The flow is:
			AQUIRE MUTEX LOCK -> EXECUTE FUNCTION -> WAIT
			* @param[in] func The function to call under mutex lock
			* @param[in] args The arguments to pass to the function
		*/
		template<typename Func, typename... Args>
		inline void wait(Func&& func, Args&&... args) const
		{
			const_cast<Condition*>(this)->wait(func, args...);
		}

		/*
			Test if the thread calling this function should wait and then waits it if true is returned
			by the predicate function. The flow is:
			AQUIRE MUTEX LOCK -> TEST PREDICATE -> WAIT (not loop)
			* @param[in] pred The test function to call under mutex lock to test for if the thread should wait
			* @param[in] args The arguments to pass to the function
		*/
		template<typename Pred, typename... Args>
		inline void waitIf(Pred&& pred, Args&&... args)
		{
			std::unique_lock<std::mutex> lock(m_mutex);
			if (pred(args...))
			{
				m_condition.wait(lock);
				m_mutex.unlock();
			}

		}
				
		/*
			Test if the thread calling this function should wait and then waits it if true is returned
			by the predicate function. The flow is:
			AQUIRE MUTEX LOCK -> TEST PREDICATE -> WAIT (not loop)
			* @param[in] pred The test function to call under mutex lock to test for if the thread should wait
			* @param[in] args The arguments to pass to the function
		*/
		template<typename Pred, typename... Args>
		inline void waitIf(Pred&& pred, Args&&... args) const
		{
			const_cast<Condition*>(this)->waitIf(pred, args...);
		}

		/*
			Executes the given function with the given parameters after the mutex lock has
			been aquired and then test if the thread calling this function should wait. The flow is:
			AQUIRE MUTEX LOCK -> EXECUTE FUNCTION -> TEST PREDICATE -> WAIT (not loop)
			* @param[in] pred The test function to call under mutex lock to test for if the thread should wait
			* @param[in] func The function to call under mutex lock
			* @param[in] args The arguments to pass to the functions
		*/
		template<typename Pred, typename Func, typename... Args>
		inline void waitIfExec(Pred&& pred, Func&& func, Args&&... args)
		{
			std::unique_lock<std::mutex> lock(m_mutex);
			func(args...);
			if (pred(args...))
			{
				m_condition.wait(lock);
				m_mutex.unlock();
			}

		}

		/*
			Executes the given function with the given parameters after the mutex lock has
			been aquired and then test if thread calling this function should wait. The flow is:
			AQUIRE MUTEX LOCK -> EXECUTE FUNCTION -> TEST PREDICATE -> WAIT (not loop)
			* @param[in] pred The test function to call under mutex lock to test for if the thread should wait
			* @param[in] func The function to call under mutex lock
			* @param[in] args The arguments to pass to the functions
		*/
		template<typename Pred, typename Func, typename... Args>
		inline void waitIfExec(Pred&& pred, Func&& func, Args&&... args) const
		{
			const_cast<Condition*>(this)->waitIfExec(pred, func, args...);
		}

		/*
			Notifies ONE thread waiting on this condition.
		*/
		inline void notify()
		{
			std::lock_guard<std::mutex> lock(m_mutex);
			m_condition.notify_one();
		}

		/*
			Notifies ALL threads waiting on this condition.
		*/
		inline void notifyAll()
		{
			std::lock_guard<std::mutex> lock(m_mutex);
			m_condition.notify_all();
		}

	};

}