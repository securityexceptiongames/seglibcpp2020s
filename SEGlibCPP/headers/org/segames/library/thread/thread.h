#pragma once
#include <org/segames/library/thread/thread_carrier.h>
#include <org/segames/library/thread/thread_exception.h>
#include <org/segames/library/thread/volatile.h>
#include <org/segames/library/util/array_stack.h>

#include <unordered_map>

namespace org::segames::library
{

	/*
		A class handling the start of a thread.
		This class uses thread pooling to improve start speed of threads.

		NOTE! When using this class in a DLL, cleanupThreads() should be called before program exit.

		* @author	Philip Rosberg
		* @since	2018-08-10
		* @edited	2019-08-10
	*/
	class SEG_API Thread :
		protected ThreadCarrierCallback
	{
	private:

		/*
			The maximum number of idle threads. -1 for infinite (default).
		*/
		static int s_maxIdleThreads;

		/*
			The number of allocated threads.
		*/
		static int s_allocatedThreads;

	protected:

		/*
			The runnable to call on thread start.
		*/
		Runnable* m_runnable;

		/*
			The backing thread.
		*/
		ThreadCarrier* m_thread;

	public:

		/*
			Creates an empty (and not startable) thread.
		*/
		inline Thread() :
			Thread(nullptr)
		{}

		/*
			Creates a thread that starts the given runnable on thread start.
			* @param[in] runnable The runnable to call/start on thread start
		*/
		inline explicit Thread(Runnable* runnable) :
			m_runnable(runnable),
			m_thread(nullptr)
		{}

		/*
			Creates a thread that starts the given runnable on thread start.
			* @param[in] runnable The runnable to call/start on thread start
			* @param[in] autoStart True if the thread should start directly
		*/
		inline explicit Thread(Runnable* runnable, bool autoStart) :
			Thread(runnable)
		{
			if (autoStart)
				this->start();
		}

		/*
			Copy-constructor.
		*/
		inline Thread(const Thread& obj) :
			Thread(obj.m_runnable)
		{}

		/*
			Move-constructor.
		*/
		inline Thread(Thread&& obj) noexcept :
			m_runnable(obj.m_runnable),
			m_thread(std::move(obj.m_thread))
		{
			obj.m_runnable = nullptr;
			obj.m_thread = nullptr;
			if (m_thread)
				m_thread->setCallback(this);
		}

		/*
			Destructor.
		*/
		virtual ~Thread();

	public:

		/*
			Returns true if the current thread is running.
		*/
		inline virtual bool isRunning() const
		{
			return m_thread != nullptr;
		}

		/*
			Returns a pointer to the backing thread.
			Returns nullptr if the thread is not running.
		*/
		inline virtual const std::thread* getThread() const
		{
			if (m_thread)
				return m_thread->getThread();
			else
				return nullptr;
		}

		/*
			Starts the thread with earlierly given runnable.
			* @throws NullPointerException If the runnable was null
			* @throws ThreadException If the thread has already been started
		*/
		virtual void start();

	public:

		/*
			Copy-assignment operator.
		*/
		inline Thread& operator=(const Thread& obj)
		{
			if (isRunning())
				throw ThreadException("Cannot copy to a thread that is running.");
			m_runnable = obj.m_runnable;
			return *this;
		}
				
		/*
			Move-assignment operator.
		*/
		inline Thread& operator=(Thread&& obj) noexcept(false)
		{
			if (isRunning())
				throw ThreadException("Cannot move to a thread that is running.");
			m_runnable = obj.m_runnable;
			m_thread = std::move(obj.m_thread);
			m_thread->setCallback(this);
			obj.m_runnable = nullptr;
			obj.m_thread = nullptr;
			return *this;
		}

	private:

		/*
			Called by the thread carrier when it has finished a run. (Local variant)
			* @param[in] carrier The thread carrier
		*/
		inline virtual void callbackFun(ThreadCarrier* carrier) override
		{
			returnThreadCarrier(carrier);
			m_thread = nullptr;
		}

	private:

		/*
			Returns a thread carrier ready for use.
		*/
		static ThreadCarrier* fetchThreadCarrier();

		/*
			Returns the given thread carrier to the pool.
			* @param[in] thread The thread carrier to return
		*/
		static void returnThreadCarrier(ThreadCarrier* thread);

		/*
			Called by the thread carrier when it has finished a run.
			* @param[in] carrier The thread carrier
		*/
		static void staticCallbackFun(ThreadCarrier* carrier);

		/*
			The active threads mapped after their native thread's id.
		*/
		static Volatile<std::unordered_map<std::thread::id, Thread*>>& s_threadMapping__();

		/*
			The avaliable threads.
		*/
		static Volatile<ArrayStack<std::unique_ptr<ThreadCarrier>>>& s_avaliableThreads__();

	public:

		/*
			Returns the number of currently allocated backing threads.
		*/
		inline static int allocatedCount()
		{
			return s_allocatedThreads;
		}

		/*
			Returns an approximate of the number of active threads.
		*/
		inline static int activeCount()
		{
			return s_allocatedThreads - s_avaliableThreads__()->size();
		}

		/*
			Deletes the avaliable threads.
			WARNING! This method only deletes the avaliable threads. Any active thread will remain active.
		*/
		static void cleanupThreads();
				
		/*
			Returns the current thread or null if the thread was not created through this class.
		*/
		static Thread* currentThread();

		/*
			Sleeps the current thread the given amount of milliseconds.
			* @param[in] millis The milliseconds to sleep
		*/
		static void sleep(size_t millis);

	};

}