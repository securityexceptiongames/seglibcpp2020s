#pragma once

namespace org::segames::library
{
			
	/*
		Interface for (dynamically) clonable types.

		* @author	Philip Rosberg
		* @since	2021-07-25
		* @edited	2021-07-25
	*/
	template<typename SubType>
	class Clonable
	{
	public:

		/*
			Returns a new dynamically-allocated copy of this instance.
		*/
		virtual SubType* clone() const = 0;

	};

}