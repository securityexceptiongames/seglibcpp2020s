#pragma once
#include <org/segames/library/exception.h>

namespace org::segames::library
{

	/*
		Exception for unexpected operations.

		* @author	Philip Rosberg
		* @since	2020-06-11
		* @edited	2020-06-30
	*/
	class InvalidOperationException :
		public Exception
	{
	public:

		/*
			Creates an invalid operation exception without message.
		*/
		inline InvalidOperationException() :
			Exception("org::segames::library::InvalidOperationException", "", StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1))
		{}

		/*
			Creates an invalid operation exception with the given message.
			* @param[in] message The additional message to display
		*/
		inline explicit InvalidOperationException(const std::string_view& message) :
			Exception("org::segames::library::InvalidOperationException", message, StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1))
		{}


	};

}