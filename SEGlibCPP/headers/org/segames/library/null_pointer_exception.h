#pragma once
#include <org/segames/library/exception.h>

namespace org::segames::library
{

	/*
		Exception for unexpected null pointers.

		* @author	Philip Rosberg
		* @since	2020-06-10
		* @edited	2020-06-30
	*/
	class NullPointerException :
		public Exception
	{
	public:
				
		/*
			Creates a null pointer exception without message.
		*/
		inline NullPointerException() :
			Exception("org::segames::library::NullPointerException", "", StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1))
		{}
				
		/*
			Creates a null pointer exception with the given message.
			* @param[in] message The additional message to display
		*/
		inline explicit NullPointerException(const std::string_view& message) :
			Exception("org::segames::library::NullPointerException", message, StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1))
		{}

	};

}