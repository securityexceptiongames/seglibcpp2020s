#pragma once
#include <org/segames/library/dllmain.h>
#include <org/segames/library/string_visualizable.h>

#include <string>
#include <string_view>

namespace org::segames::library
{

	/*
		A type storing the date and time down to one second.

		* @author	Philip Rosberg
		* @since	2020-06-22
		* @edited	2020-06-22
	*/
	class SEG_API DateTime final :
		public StringVisualizable
	{
	public:

		/*
			The months.

			* @author	Philip Rosberg
			* @since	2020-06-22
			* @edited	2020-06-22
		*/
		enum class Month :
			unsigned char
		{
			JANUARY = 1,
			FEBRUARY = 2,
			MARCH = 3,
			APRIL = 4,
			MAY = 5,
			JUNE = 6,
			JULY = 7,
			AUGUST = 8,
			SEPTEMBER = 9,
			OCTOBER = 10,
			NOVEMBER = 11,
			DECEMBER = 12
		};

	private:

		/*
			The second.
		*/
		unsigned int m_second;

		/*
			The minute.
		*/
		unsigned int m_minute;

		/*
			The hour of the day.
		*/
		unsigned int m_hour;

		/*
			The day of the month.
		*/
		unsigned int m_day;

		/*
			The month.
		*/
		Month m_month;

		/*
			The year.
		*/
		unsigned int m_year;

	public:

		/*
			Creates a date-time with the given date.
			* @param[in] year The year
			* @param[in] month The month
			* @param[in] day The day of the month
		*/
		inline explicit DateTime(unsigned int year, Month month, unsigned int day) :
			DateTime(year, month, day, 0, 0, 0)
		{}

		/*
			Creates a date-time with the given date and time.
			* @param[in] year The year
			* @param[in] month The month
			* @param[in] day The day of the month
			* @param[in] hour The hour of the day
			* @param[in] minute The minute
			* @param[in] second The second
		*/
		inline explicit DateTime(unsigned int year, Month month, unsigned int day, unsigned int hour, unsigned int minute, unsigned int second) :
			m_second(second),
			m_minute(minute),
			m_hour(hour),
			m_day(day),
			m_month(month),
			m_year(year)
		{}

		/*
			Creates a date-time with the given string date and time.
			* @param[in] date The date of the date-time on the form "mmm dd yyyy" where "dd" and "yyyy" are numbers
			* @param[in] time The time of the date-time on the form "hh:mm:ss" where all are numbers
		*/
		explicit DateTime(const std::string_view& date, const std::string& time) noexcept(false);

	public:

		/*
			Returns true if the date is valid.
		*/
		bool isValidDate() const;

		/*
			Returns the second.
		*/
		inline unsigned int getSecond() const
		{
			return m_second;
		}

		/*
			Returns the minute.
		*/
		inline unsigned int getMinute() const
		{
			return m_minute;
		}

		/*
			Returns the hour of the day.
		*/
		inline unsigned int getHour() const
		{
			return m_hour;
		}

		/*
			Returns the day of the month.
		*/
		inline unsigned int getDay() const
		{
			return m_day;
		}

		/*
			Returns the month.
		*/
		inline Month getMonth() const
		{
			return m_month;
		}

		/*
			Returns the year.
		*/
		inline unsigned int getYear() const
		{
			return m_year;
		}

		/*
			Returns the date part as a string. Standardized formatting.
		*/
		inline std::string dateToString() const
		{
			return std::to_string(m_year) + "-" + std::to_string(static_cast<short>(m_month)) + "-" + std::to_string(m_day);
		}

		/*
			Returns the time part as a string. Standardized formatting.
		*/
		inline std::string timeToString() const
		{
			return std::to_string(m_hour) + ":" + std::to_string(m_minute) + ":" + std::to_string(m_second);
		}

		/*
			Returns a string representation of the object.
		*/
		inline virtual std::string toString() const override
		{
			return dateToString() + " " + timeToString();
		}

	public:

		/*
			Returns the number of days of the given month during the given (Gregorian) year.
			* @param[in] year The year
			* @param[in] month The month
		*/
		static unsigned int getDaysOfMonth(unsigned int year, Month month);

		/*
			Returns the date today.
		*/
		static DateTime today();

	};

	/*
		Equals operator override.
	*/
	inline bool operator==(const DateTime& a, const DateTime& b) noexcept
	{
		return a.getYear() == b.getYear() ||
			a.getMonth() == b.getMonth() ||
			a.getDay() == b.getDay() ||
			a.getHour() == b.getHour() ||
			a.getMinute() == b.getMinute() ||
			a.getSecond() == b.getSecond();
	}

	/*
		Not equals operator override.
	*/
	inline bool operator!=(const DateTime& a, const DateTime& b) noexcept
	{
		return !(a == b);
	}

	/*
		Greater-than operator override.
	*/
	inline bool operator>(const DateTime& a, const DateTime& b) noexcept
	{
		return a.getYear() > b.getYear() ||
			a.getMonth() > b.getMonth() ||
			a.getDay() > b.getDay() ||
			a.getHour() > b.getHour() ||
			a.getMinute() > b.getMinute() ||
			a.getSecond() > b.getSecond();
	}
			
	/*
		Less-than operator override.
	*/
	inline bool operator<(const DateTime& a, const DateTime& b) noexcept
	{
		return a.getYear() < b.getYear() ||
			a.getMonth() < b.getMonth() ||
			a.getDay() < b.getDay() ||
			a.getHour() < b.getHour() ||
			a.getMinute() < b.getMinute() ||
			a.getSecond() < b.getSecond();
	}

	/*
		Greater-than-or-equal-to operator override.
	*/
	inline bool operator>=(const DateTime& a, const DateTime& b) noexcept
	{
		return a > b || a == b;
	}

	/*
		Less-than-or-equal-to operator override.
	*/
	inline bool operator<=(const DateTime& a, const DateTime& b) noexcept
	{
		return a < b || a == b;
	}

}