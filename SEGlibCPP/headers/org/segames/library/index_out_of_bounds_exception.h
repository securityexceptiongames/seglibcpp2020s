#pragma once
#include <org/segames/library/exception.h>
#include <org/segames/library/library.h>

namespace org::segames::library
{

	/*
		Exception for an index or iterator that is out of bounds.

		* @author	Philip Rosberg
		* @since	2020-06-10
		* @edited	2020-06-30
	*/
	class IndexOutOfBoundsException :
		public Exception
	{
	public:

		/*
			Creates an IOOB exception without message.
		*/
		inline IndexOutOfBoundsException() :
			Exception("org::segames::library::IndexOutOfBoundsException", "", StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1))
		{}

		/*
			Creates an IOOB exception with a message about the given index.
			* @param[in] index the index that was out of bounds
		*/
		template<typename T>
		inline explicit IndexOutOfBoundsException(const T& index) :
			Exception("org::segames::library::IndexOutOfBoundsException", "Index \"" + std::to_string(index) + "\" is out of bounds.", StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1))
		{}

	};

}