#pragma once
#include <org/segames/library/dllmain.h>

#ifdef SEG_API_MEMORY_TRACKING
#include <org/segames/library/core.h>
#include <org/segames/library/stack_trace.h>

#include <unordered_map>
#include <mutex>

namespace org::segames::library
{

	/*
		A class handling tracking of dynamically allocated memory blocks. 
		NOTE! The tracker has to be enabled manually at the start of the program.

		* @author	Philip Rosberg
		* @since	2021-03-05
		* @edited	2021-03-05
	*/
	class MemoryTracker final
	{
	public:

		/*
			Structure of information for one memory block using only C types.

			* @author	Philip Rosberg
			* @since	2021-03-06
			* @since	2021-03-06
		*/
		struct MemoryBlock
		{
			void* ptr;
			size_t frameCount;
			StackTrace::CFrame* frames;
		};

	private:

		/*
			True if the memory tracker is enabled.
		*/
		static bool s_enabled;

		/*
			The tracker parallell usage mutex.
		*/
		static std::mutex s_mutex;

	private:

		/*
			The total size of the memory log.
		*/
		size_t m_logSize;

		/*
			The usage size of the log.
		*/
		size_t m_logUsage;

		/*
			The log of the memory usage
		*/
		MemoryBlock* m_memoryLog;

	protected:

		/*
			Creates a memory tracker.
		*/
		inline MemoryTracker() :
			m_logSize(1024),
			m_logUsage(0),
			m_memoryLog(static_cast<MemoryBlock*>(calloc(m_logSize, sizeof(MemoryBlock))))
		{}

		/*
			Destructor, displaying all memory pointers that were not freed.
		*/
		inline ~MemoryTracker()
		{
			printStatus();
		}

	public:

		/*
			Returns and logs a new memory block of the given size.
			* @param[in] size The size of the memory
		*/
		inline void* newMemory(size_t size) noexcept(false)
		{
			void* ptr = malloc(size);
			if (!ptr)
				throw std::bad_alloc();

			if (s_enabled)
			{
				if (m_logUsage == m_logSize)
					upscaleLog();

				MemoryBlock& block = m_memoryLog[m_logUsage];
				block.ptr = ptr;
				StackTrace::getStackTraceCAlloc(StackTrace::ALL_STACK_FRAMES, 2, &block.frameCount, &block.frames);
				m_logUsage++;
			}

			return ptr;
		}

		/*
			Frees and unregisters the given memory block.
			* @param[in] ptr The pointer whose memory to free
		*/
		inline void deleteMemory(void* ptr) noexcept(false)
		{
			if (ptr != NULL)
			{
				if (s_enabled)
				{
					size_t pos = -1;
					for (size_t i = 0; i < m_logUsage; i++)
						if (m_memoryLog[i].ptr == ptr)
						{
							pos = i;
							break;
						}

					if (pos != size_t(-1))
					{
						MemoryBlock& block = m_memoryLog[pos];
						for (size_t i = 0; i < block.frameCount; i++)
						{
							if (block.frames[i].file) free((block.frames[i]).file);
							if (block.frames[i].function) free((block.frames[i]).function);
						}
						free(block.frames);

						for (size_t i = pos + 1; i < m_logUsage; i++)
							m_memoryLog[i - 1] = m_memoryLog[i];
						m_logUsage--;
					}

				}

				free(ptr);
			}

		}

		/*
			Prints the status of the memory tracker, displaying all non-dellocated pointers.
		*/
		inline void printStatus() noexcept
		{
			for (size_t i = 0; i < m_logUsage; i++)
			{
				MemoryBlock& block = m_memoryLog[i];
				if (block.ptr != NULL)
				{
					Core::err() << "Potentially non-deallocated memory [" << std::hex << block.ptr << std::dec << "]. Allocated";
					if (block.frames != NULL)
					{
						for (size_t i2 = 0; i2 < block.frameCount; i2++)
						{
							StackTrace::CFrame& frame = block.frames[i2];
							if (frame.file != NULL && frame.function != NULL)
								Core::err() << "\n    from (" << std::hex << frame.address << std::dec << ") " << frame.file << ":" << frame.line << " - " << frame.function << "()";
							else if (frame.function != NULL)
								Core::err() << "\n    from (" << std::hex << frame.address << std::dec << ") ???:0" << " - " << frame.function << "()";
							else
								Core::err() << "\n    from (" << std::hex << frame.address << std::dec << ") ???:0";
						}
					}
					Core::err() << "\n---END---\n\n";
				}
				else
				{
					Core::err() << "break" << std::endl;
					break;
				}

			}
			Core::err() << std::endl;
		}

	private:

		/*
			Increases the size of the memory log.
		*/
		inline void upscaleLog()
		{
			size_t newSize = m_logSize * 2;
			MemoryBlock* newLog = static_cast<MemoryBlock*>(calloc(newSize, sizeof(MemoryBlock)));

			for (size_t i = 0; i < m_logUsage; i++)
				newLog[i] = m_memoryLog[i];
			free(m_memoryLog);

			m_logSize = newSize;
			m_memoryLog = newLog;
		}

	public:

		/*
			Sets if the memory tracker should be enabled.
			* @param[in] flag True if the memory tracker should be enabled
		*/
		inline static void setEnabled(bool flag)
		{
			s_enabled = flag;
		}

		/*
			Enables the memory tracker.
		*/
		inline static void enable()
		{
			setEnabled(true);
		}
				
		/*
			Locks the tracker.
		*/
		inline static std::unique_lock<std::mutex> lockTracker()
		{
			return std::unique_lock<std::mutex>(s_mutex);
		}

		/*
			Returns the one and only memory tracker instance.
		*/
		inline static MemoryTracker& getMemoryTracker()
		{
			static MemoryTracker tracker;
			return tracker;
		}

	};

}

static org::segames::library::MemoryTracker& memTrackerDummyMarkerDoNotUse = org::segames::library::MemoryTracker::getMemoryTracker();

inline void* operator new(size_t size) noexcept(false)
{
	auto lock = org::segames::library::MemoryTracker::lockTracker();
	return memTrackerDummyMarkerDoNotUse.newMemory(size);
}

inline void* operator new[](size_t size) noexcept(false)
{
	auto lock = org::segames::library::MemoryTracker::lockTracker();
	return memTrackerDummyMarkerDoNotUse.newMemory(size);
}

inline void operator delete(void* ptr) noexcept(false)
{
	auto lock = org::segames::library::MemoryTracker::lockTracker();
	memTrackerDummyMarkerDoNotUse.deleteMemory(ptr);
}

inline void operator delete[](void* ptr) noexcept(false)
{
	auto lock = org::segames::library::MemoryTracker::lockTracker();
	memTrackerDummyMarkerDoNotUse.deleteMemory(ptr);
}

#else

namespace org::segames::library
{
			
	/*
		A class handling tracking of dynamically allocated memory blocks.

		* @author	Philip Rosberg
		* @since	2021-03-07
		* @edited	2021-03-07
	*/
	class MemoryTracker final
	{
	public:

		/*
			Dummy method. Does nothing.
		*/
		inline static void setEnabled(bool flag) {}

		/*
			Dummy method. Does nothing.
		*/
		inline static void enable() {}

	};

}

#endif