#pragma once
#include <org/segames/library/exception.h>

namespace org::segames::library
{

	/*
		Exception for invalid values.

		* @author	Philip Rosberg
		* @since	2020-06-10
		* @edited	2020-06-30
	*/
	class InvalidValueException :
		public Exception
	{
	public:

		/*
			Creates an invalid value exception without message.
		*/
		inline InvalidValueException() :
			Exception("org::segames::library::InvalidValueException", "", StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1))
		{}

		/*
			Creates a invalid value exception with the given message.
			* @param[in] message The additional message to display
		*/
		inline explicit InvalidValueException(const std::string_view& message) :
			Exception("org::segames::library::InvalidValueException", message, StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1))
		{}

		/*
			Creates an invalid value exception with a message about the given type and value.
			* @param[in] value The value that was wrong
		*/
		template<typename T>
		inline explicit InvalidValueException(const T& value, bool) :
			Exception("org::segames::library::InvalidValueException", "\"" + std::to_string(value) + "\" is an invalid value.", StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1))
		{}

	};

}