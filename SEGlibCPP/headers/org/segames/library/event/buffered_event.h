#pragma once
#include <org/segames/library/event/event.h>
#include <org/segames/library/event/buffered_event_buffer.h>

namespace org::segames::library
{
			
	/*
		Super-class of events that are first placed inside of an event buffer 
		instead of being sent directly to the event router.

		* @author	Philip Rosberg
		* @since	2020-08-29
		* @edited	2020-08-29
	*/
	template<typename E>
	class BufferedEvent :
		public Event<E>,
		public BufferedEventBase
	{
	public:

		using Event<E>::fire;

	private:

		/*
			Reference to the event router to send this event to.
		*/
		const EventRouter<E>* m_router;

	public:

		/*
			Creates a buffered event.
		*/
		inline BufferedEvent() :
			m_router(nullptr)
		{}

		/*
			Copy-constructor.
		*/
		inline BufferedEvent(const BufferedEvent&) = default;

		/*
			Move-constructor.
		*/
		inline BufferedEvent(BufferedEvent&&) noexcept = default;

	public:

		/*
			Fires the event to the default event buffer, directed to the given router.
			NOTE! This fire method moves the event.
			* @param[in] router The event router
		*/
		inline virtual void fire(const EventRouter<E>& router) override
		{
			fire(router, BufferedEventBuffer::getDefaultEventBuffer());
		}

		/*
			Fires the event to the given event buffer, directed to the given router.
			NOTE! This fire method moves the event.
			* @param[in] router The event router
			* @param[in] buffer The event buffer to place the event in
		*/
		inline virtual void fire(const EventRouter<E>& router, BufferedEventBuffer& buffer)
		{
			m_router = &router;
			buffer.enqueue(std::unique_ptr<BufferedEventBase>(new E(std::move(dynamic_cast<E&>(*this)))));
		}

		/*
			Fires the event to the default event buffer, directed to the given router.
			NOTE! This fire method copies the event.
			* @param[in] router The event router
		*/
		inline virtual void fireCopy(const EventRouter<E>& router)
		{
			fireCopy(router, BufferedEventBuffer::getDefaultEventBuffer());
		}

		/*
			Fires the event to the given event buffer, directed to the given router.
			NOTE! This fire method copies the event.
			* @param[in] router The event router
			* @param[in] buffer The event buffer to place the event in
		*/
		inline virtual void fireCopy(const EventRouter<E>& router, BufferedEventBuffer& buffer)
		{
			m_router = &router;
			buffer.enqueue(std::unique_ptr<BufferedEventBase>(new E(dynamic_cast<const E&>(*this))));
		}

		/*
			Forwards the event to the event router.
		*/
		inline virtual void forward() override
		{
			m_router->reroute(dynamic_cast<E&>(*this));
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline BufferedEvent<E>& operator=(const BufferedEvent<E>&) = default;

		/*
			Move-assignment operator.
		*/
		inline BufferedEvent<E>& operator=(BufferedEvent<E>&&) noexcept = default;

	};

}