#pragma once
#include <org/segames/library/event/event.h>

namespace org::segames::library
{

	/*
		Super-class of events that call the listeners directly when fired.

		* @author	Philip Rosberg
		* @since	2020-08-29
		* @edited	2020-08-29
	*/
	template<typename E>
	class DirectEvent :
		public Event<E>
	{
	public:

		using Event<E>::fire;

	public:

		/*
			Fires the event, sending it directly to the given event router.
			* @param[in] router The event router to send the event to
		*/
		inline virtual void fire(const EventRouter<E>& router) override
		{
			router.reroute(dynamic_cast<E&>(*this));
		}

	};

}