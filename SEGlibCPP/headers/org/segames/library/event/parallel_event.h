#pragma once
#include <org/segames/library/event/event.h>
#include <org/segames/library/thread/thread.h>

namespace org::segames::library
{

	/*
		Super-class of events that create a new thread when fired.

		* @author	Philip Rosberg
		* @since	2020-08-29
		* @edited	2020-08-29
	*/
	template<typename E>
	class ParallelEvent :
		public Event<E>,
		private Runnable
	{
	public:

		using Event<E>::fire;

	private:

		/*
			Reference to the event router to send this event to.
		*/
		const EventRouter<E>* m_router;

	public:

		/*
			Creates a parallel event.
		*/
		inline ParallelEvent() :
			m_router(nullptr)
		{}

		/*
			Copy-constructor.
		*/
		inline ParallelEvent(const ParallelEvent&) = default;
				
		/*
			Move-constructor.
		*/
		inline ParallelEvent(ParallelEvent&&) noexcept = default;

	public:

		/*
			Fires the event, creating a new thread that then sends the event to the router.
			NOTE! This fire method moves the event.
			* @param[in] router The event router to send the event to
		*/
		inline virtual void fire(const EventRouter<E>& router) override
		{
			m_router = &router;
			cleanupThreads__();
			startThread__(std::unique_ptr<E>(new E(std::move(dynamic_cast<E&>(*this)))));
		}

		/*
			Fires the event, sending it to the default event router.
		*/
		inline virtual void fireCopy()
		{
			this->fireCopy(EventRouter<E>::getDefaultRouter());
		}

		/*
			Fires the event, creating a new thread that then sends the event to the router.
			NOTE! This fire method copies the event.
			* @param[in] router The event router to send the event to
		*/
		inline virtual void fireCopy(const EventRouter<E>& router)
		{
			m_router = &router;
			cleanupThreads__();
			startThread__(std::unique_ptr<E>(new E(dynamic_cast<const E&>(*this))));
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline ParallelEvent<E>& operator=(const ParallelEvent<E>&) = default;

		/*
			Move-assignment operator.
		*/
		inline ParallelEvent<E>& operator=(ParallelEvent<E>&&) noexcept = default;

	private:

		/*
			Run-method override.
		*/
		inline virtual void run() override
		{
			m_router->reroute(dynamic_cast<E&>(*this));
		}

		/*
			Cleans the old, not running, threads.
		*/
		inline void cleanupThreads__()
		{
			auto lock = Synchronize::lock(threadStorage__());
			for (auto itr = threadStorage__()->begin(); itr != threadStorage__()->end(); itr++)
				if (!(*itr)->second.isRunning())
					threadStorage__()->erase(itr);
		}

		/*
			Starts the thread carying the event.
			* @param[in] event The "copy" of the event
		*/
		void startThread__(std::unique_ptr<E>&& event)
		{
			auto lock = Synchronize::lock(threadStorage__());
			threadStorage__()->insert(
				std::unique_ptr<std::pair<std::unique_ptr<E>, Thread>>(
					new std::pair<std::unique_ptr<E>, Thread>(
						std::move(event),
						Thread(event.get(), true)
						)

					)

			);

		}

	private:

		/*
			Returns a reference to the static thread and event storage.
		*/
		inline static Volatile<std::unordered_set<std::unique_ptr<std::pair<std::unique_ptr<E>, Thread>>>>& threadStorage__()
		{
			static Volatile<std::unordered_set<std::unique_ptr<std::pair<std::unique_ptr<E>, Thread>>>> threads;
			return threads;
		}

	};

}