#pragma once
#include <org/segames/library/event/event_router.h>

namespace org::segames::library
{

	/*
		Interface for event listeners.

		* @author	Philip Rosberg
		* @since	2020-08-29
		* @edited	2020-08-29
	*/
	template<typename E>
	class EventListener :
		public VirtuallyDestructible
	{
	public:
				
		/*
			Creates an event listener and links it to the default router.
		*/
		inline EventListener() :
			EventListener(EventRouter<E>::getDefaultRouter())
		{}

		/*
			Creates an event listener and links it to the given router.
			* @param[in] router The router to link this listener to
		*/
		template<typename Router>
		inline EventListener(Router& router)
		{
			router.link(this);
		}

		/*
			Creates an event listener and links it to the given routers.
			* @param[in] router The router to link this listener to
			* @param[in] routers Other routers to link this listener to
		*/
		template<typename Router, typename... Routers>
		inline EventListener(Router& router, Routers&... routers) :
			EventListener(routers...)
		{
			router.link(this);
		}

		/*
			Destructor.
		*/
		inline virtual ~EventListener()
		{
			EventRouter<E>::unlinkUniversally(this);
		}

	public:
				
		/*
			Processes the given event.
			* @param[in] event The event
		*/
		virtual void process(E& event) = 0;

	};

}