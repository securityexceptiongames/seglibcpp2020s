#pragma once
#include <org/segames/library/thread/synchronized.h>
#include <org/segames/library/util/array_deque.h>
#include <org/segames/library/event/buffered_event_base.h>

namespace org::segames::library
{

	/*
		Buffer for buffered events.
				
		* @author	Philip Rosberg
		* @since	2020-08-29
		* @edited	2020-08-29
	*/
	class SEG_API BufferedEventBuffer :
		public Queue<std::unique_ptr<BufferedEventBase>>
	{
	protected:

		/*
			The buffer with events.
		*/
		Volatile<ArrayDeque<std::unique_ptr<BufferedEventBase>>> m_buffer;

	public:

		/*
			Creates an empty event buffer
		*/
		inline BufferedEventBuffer() :
			m_buffer()
		{}

		/*
			Copy-constructor.
		*/
		inline BufferedEventBuffer(const BufferedEventBuffer&) = default;

		/*
			Move-constructor.
		*/
		inline BufferedEventBuffer(BufferedEventBuffer&&) noexcept = default;

	public:

		/*
			Returns true if the buffer is empty.
		*/
		inline virtual bool empty() const override
		{
			auto lock = Synchronize::lock(m_buffer);
			return m_buffer->empty();
		}

		/*
			Returns the first value in the buffer.
		*/
		inline virtual std::unique_ptr<BufferedEventBase>& peek() override
		{
			auto lock = Synchronize::lock(m_buffer);
			return m_buffer->peek();
		}

		/*
			Returns the first value in the buffer.
		*/
		inline virtual const std::unique_ptr<BufferedEventBase>& peek() const override
		{
			auto lock = Synchronize::lock(m_buffer);
			return m_buffer->peek();
		}

		/*
			Removes the first element in the buffer and returns the value.
		*/
		inline virtual std::unique_ptr<BufferedEventBase> dequeue() override
		{
			auto lock = Synchronize::lock(m_buffer);
			return m_buffer->dequeue();
		}

		/*
			Adds the given value to the back of the buffer.
			* @param[in] value The value to add
		*/
		inline virtual void enqueue(const std::unique_ptr<BufferedEventBase>& value) override
		{
			auto lock = Synchronize::lock(m_buffer);
			return m_buffer->enqueue(value);
		}

		/*
			Adds the given value to the back of the buffer.
			* @param[in] value The value to add
		*/
		inline virtual void enqueue(std::unique_ptr<BufferedEventBase>&& value) override
		{
			auto lock = Synchronize::lock(m_buffer);
			return m_buffer->enqueue(std::move(value));
		}

		/*
			Removes all content from the buffer.
		*/
		inline virtual void clear() override
		{
			auto lock = Synchronize::lock(m_buffer);
			return m_buffer->clear();
		}

		/*
			Dequeues and forwards the first event.
		*/
		inline virtual void processFirst()
		{
			if (!empty())
				dequeue()->forward();
		}

		/*
			Dequeues and forwards the all events in the order they came in.
		*/
		inline virtual void processAll()
		{
			while (!empty())
				dequeue()->forward();
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline BufferedEventBuffer& operator=(const BufferedEventBuffer&) = default;

		/*
			Move-assignment operator.
		*/
		inline BufferedEventBuffer& operator=(BufferedEventBuffer&&) noexcept = default;

	public:

		/*
			Returns a reference to a default event buffer.
		*/
		inline static BufferedEventBuffer& getDefaultEventBuffer()
		{
			static BufferedEventBuffer buffer;
			return buffer;
		}

	};

}