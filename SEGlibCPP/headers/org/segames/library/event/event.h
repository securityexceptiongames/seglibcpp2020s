#pragma once
#include <org/segames/library/event/event_router.h>

namespace org::segames::library
{

	/*
		Interface for event types.

		* @author	Philip Rosberg
		* @since	2020-08-29
		* @edited	2020-08-29
	*/
	template<typename E>
	class Event
	{
	public:

		/*
			Fires the event, sending it to the default event router.
		*/
		inline virtual void fire()
		{
			this->fire(EventRouter<E>::getDefaultRouter());
		}

		/*
			Fires the event, sending it to the given event router.
			* @param[in] router The event router to send the event to
		*/
		virtual void fire(const EventRouter<E>& router) = 0;

	};

}