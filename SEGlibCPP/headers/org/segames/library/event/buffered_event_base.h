#pragma once
#include <org/segames/library/virtually_destructible.h>

namespace org::segames::library
{

	/*
		Super-base inteface for buffered events.

		* @author	Philip Rosberg
		* @since	2020-08-29
		* @edited	2020-08-29
	*/
	class BufferedEventBase :
		public VirtuallyDestructible
	{
	public:

		/*
			Forwards the event to the event routers.
		*/
		virtual void forward() = 0;

	};

}