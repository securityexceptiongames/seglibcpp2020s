#pragma once
#include <org/segames/library/thread/synchronized.h>
#include <org/segames/library/virtually_destructible.h>

#include <unordered_set>

namespace org::segames::library
{
			
	template<typename E>
	class EventListener;

	/*
		A device for routing fired events to the correct event listeners.
		THREADING INFO: This implementation is internally synchronized.

		* @author	Philip Rosberg
		* @since	2020-08-29
		* @edited	2020-08-29
	*/
	template<typename E>
	class EventRouter :
		public VirtuallyDestructible
	{
	protected:

		/*
			The linked listeners.
		*/
		Volatile<std::unordered_set<EventListener<E>*>> m_listeners;

	public:

		/*
			Creates an empty event router.
		*/
		inline EventRouter() :
			m_listeners()
		{
			auto lock = Synchronize::lock(routerStorage__());
			routerStorage__()->insert(this);
		}

		/*
			Copy-constructor.
		*/
		inline EventRouter(const EventRouter<E>& obj) :
			m_listeners(obj.m_listeners)
		{
			auto lock = Synchronize::lock(routerStorage__());
			routerStorage__()->insert(this);
		}

		/*
			Move-constructor.
		*/
		inline EventRouter(EventRouter<E>&& obj) noexcept :
			m_listeners(std::move(obj.m_listeners))
		{
			auto lock = Synchronize::lock(routerStorage__());
			routerStorage__()->insert(this);
		}

		/*
			Destructor.
		*/
		inline virtual ~EventRouter() override
		{
			auto lock = Synchronize::lock(routerStorage__());
			routerStorage__()->erase(this);
		}

	public:

		/*
			Reroutes the given event to the linked event listeners.
			NOTE! This method may cause a deadlock if any routed-to event listener tries to modify this router.
			* @param[in] e The event to reroute
		*/
		inline virtual void reroute(E& e) const
		{
			auto lock = Synchronize::lock(m_listeners);
			for (auto& itr : *m_listeners)
				itr->process(e);
		}

		/*
			Links the given event listener.
			* @param[in] list The listener to link
		*/
		inline virtual void link(EventListener<E>* list)
		{
			auto lock = Synchronize::lock(m_listeners);
			if (m_listeners->count(list) == 0)
				m_listeners->insert(list);
		}

		/*
			Unlinks the given event listener.
			* @param[in] list The listener to unlink
		*/
		inline virtual void unlink(EventListener<E>* list)
		{
			auto lock = Synchronize::lock(m_listeners);
			m_listeners->erase(list);
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline EventRouter<E>& operator=(const EventRouter<E>&) = default;

		/*
			Move-assignment operator.
		*/
		inline EventRouter<E>& operator=(EventRouter<E>&&) noexcept = default;

	public:

		/*
			Returns a reference to a default event router.
		*/
		inline static EventRouter<E>& getDefaultRouter()
		{
			static EventRouter<E> router;
			return router;
		}

		/*
			Unlinks the given listener from all event routers.
			* @param[in] list The listener to universally unlink
		*/
		inline static void unlinkUniversally(EventListener<E>* list)
		{
			auto lock = Synchronize::lock(routerStorage__());
			for (auto itr : *routerStorage__())
				itr->unlink(list);
		}

	private:

		/*
			Returns a reference to the static storage of all event routers.
		*/
		inline static Volatile<std::unordered_set<EventRouter<E>*>>& routerStorage__()
		{
			static Volatile<std::unordered_set<EventRouter<E>*>> routers;
			return routers;
		}

	};

}