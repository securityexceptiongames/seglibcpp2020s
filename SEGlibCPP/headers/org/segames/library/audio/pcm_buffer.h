#pragma once
#include <org/segames/library/audio/pcm_format.h>
#include <org/segames/library/util/native_resource.h>
#include <org/segames/library/virtually_destructible.h>

#include <memory>

namespace org::segames::library
{

	/*
		Interface for PCM data buffers.

		* @author	Philip Rosberg
		* @since	2020-09-06
		* @edited	2020-09-06
	*/
	class PCMBuffer :
		public NativeResource,
		public VirtuallyDestructible
	{
	public:

		/*
			Returns the number of avaliable native chars.
		*/
		virtual size_t nativeSize() const = 0;

		/*
			Returns the pointer to the begining of native chars.
		*/
		virtual char* nativePointer() = 0;

		/*
			Returns the pointer to the begining of native chars.
		*/
		virtual const char* nativePointer() const = 0;

		/*
			Returns the internal format of the buffer.
		*/
		virtual const PCMFormat& getFormat() const = 0;

		/*
			Puts the give PCM value into the buffer, converting it if necessary.
			* @param[in] c The channel value
		*/
		virtual void put(unsigned char c) = 0;

		/*
			Puts the give PCM values into the buffer, converting them if necessary.
			* @param[in] c1 The first channel value
			* @param[in] c2 The second channel value
		*/
		virtual void put(unsigned char c1, unsigned char c2) = 0;

		/*
			Puts the give PCM value into the buffer, converting it if necessary.
			* @param[in] c The channel value
		*/
		virtual void put(signed short c) = 0;

		/*
			Puts the give PCM values into the buffer, converting them if necessary.
			* @param[in] c1 The first channel value
			* @param[in] c2 The second channel value
		*/
		virtual void put(signed short c1, signed short c2) = 0;

		/*
			Puts the give PCM value into the buffer, converting it if necessary.
			* @param[in] c The channel value
		*/
		virtual void put(float c) = 0;

		/*
			Puts the give PCM values into the buffer, converting them if necessary.
			* @param[in] c1 The first channel value
			* @param[in] c2 The second channel value
		*/
		virtual void put(float c1, float c2) = 0;

		/*
			Discards the given amount of native units (chars) from the buffer.
			* @param[in] nativeCount The amount of native units (chars) to remove
		*/
		virtual void discard(size_t nativeCount) = 0;

	public:

		/*
			Returns the proper buffer for the given format.
			Throws an invalid format exception on failure.
			* @param[in] format The format of the buffer
		*/
		static std::unique_ptr<PCMBuffer> getBufferFor(const PCMFormat& format) noexcept(false);

	};

}