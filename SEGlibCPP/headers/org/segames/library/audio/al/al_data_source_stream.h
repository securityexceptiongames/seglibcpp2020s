#pragma once
#include <org/segames/library/audio/al/al_audio_source.h>
#include <org/segames/library/audio/al/al_data_source.h>
#include <org/segames/library/audio/pcm_parser.h>

namespace org::segames::library
{

	/*
		OpenAL data source that streams data.

		* @author	Philip Rosberg
		* @since	2020-12-13
		* @edited	2020-12-13
	*/
	class SEG_API ALDataSourceStream :
		public ALDataSource
	{
	public:

		/*
			The amount of internal buffers to keep.
		*/
		static constexpr size_t BUFFER_COUNT = ALAudioSource::QUEUE_COUNT + 1;

	protected:

		/*
			True if the parser has ended parsing.
		*/
		bool m_parserEnded;

		/*
			The current/next filling buffer.
		*/
		size_t m_current;

		/*
			Conversion buffer.
		*/
		std::unique_ptr<PCMBuffer> m_conversion;

		/*
			The parser to aquire data from.
		*/
		std::unique_ptr<PCMParser> m_parser;

		/*
			The buffers.
		*/
		std::unique_ptr<ALBuffer[]> m_buffers;

	public:

		/*
			Creates an empty unusable data source stream.
		*/
		inline ALDataSourceStream() :
			m_parserEnded(true),
			m_current(0),
			m_conversion(),
			m_parser(),
			m_buffers()
		{}

		/*
			Creates an data source stream with the given output format and parser.
			* @param[in] format The output data format
			* @param[in] parser The parser to parser from
		*/
		inline explicit ALDataSourceStream(const PCMFormat& format, std::unique_ptr<PCMParser>&& parser) noexcept(false) :
			m_parserEnded(false),
			m_current(0),
			m_conversion(std::move(PCMBuffer::getBufferFor(format))),
			m_parser(std::move(parser)),
			m_buffers(new ALBuffer[BUFFER_COUNT])
		{}

		/*
			No copying.
		*/
		ALDataSourceStream(const ALDataSourceStream&) = delete;

		/*
			Move-constructor.
		*/
		inline ALDataSourceStream(ALDataSourceStream&& obj) noexcept :
			m_parserEnded(obj.m_parserEnded),
			m_current(obj.m_current),
			m_conversion(std::move(obj.m_conversion)),
			m_parser(std::move(obj.m_parser)),
			m_buffers(std::move(obj.m_buffers))
		{
			obj.m_parserEnded = true;
			obj.m_current = 0;
		}

	public:

		/*
			Returns the format of the data.
		*/
		inline virtual const PCMFormat& getFormat() const override
		{
			return m_conversion->getFormat();
		}

		/*
			Returns a reference to the next buffer to play.
			Returns a reference to an empty buffer when finished.
		*/
		virtual const ALBuffer& nextBuffer() override;

		/*
			Attempts to rewind the source to the begining.
			Returns true on sucess.
		*/
		inline virtual bool rewind() override
		{
			if (m_parser)
			{
				m_parserEnded = false;
				return m_parser->rewind();
			}
			else
				return false;
		}

	public:

		/*
			No copying.
		*/
		ALDataSourceStream& operator=(const ALDataSourceStream&) = delete;

		/*
			Move-assignment operator.
		*/
		inline ALDataSourceStream& operator=(ALDataSourceStream&& obj) noexcept
		{
			m_parserEnded = obj.m_parserEnded;
			m_current = obj.m_current;
			m_conversion = std::move(obj.m_conversion);
			m_parser = std::move(obj.m_parser);
			m_buffers = std::move(obj.m_buffers);
			obj.m_parserEnded = true;
			obj.m_current = 0;
			return *this;
		}

	private:

		/*
			Length of the buffer in seconds.
		*/
		static double s_bufferLength;

	public:

		/*
			Returns the length of the buffer in seconds.
		*/
		inline static double getBufferLength()
		{
			return s_bufferLength;
		}

		/*
			Sets the length of the buffer in seconds.
			* @param[in] length The buffer length in seconds
		*/
		inline static void setBufferLength(double length)
		{
			s_bufferLength = length;
		}

	};

}