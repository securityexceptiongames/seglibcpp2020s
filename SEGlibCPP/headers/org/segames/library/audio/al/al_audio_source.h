#pragma once
#include <org/segames/library/audio/al/al_audio_source_state_listener.h>
#include <org/segames/library/audio/al/al_data_source.h>
#include <org/segames/library/thread/volatile.h>

#include <memory>

namespace org::segames::library
{
			
	/*
		OpenAL based audio source/track.

		* @author	Philip Rosberg
		* @since	2020-12-13
		* @edited	2020-12-13
	*/
	class SEG_API ALAudioSource :
		public VirtuallyDestructible
	{
	public:

		/*
			The amount of OpenAL buffers to place in the queue.
		*/
		static constexpr size_t QUEUE_COUNT = 2;

	protected:

		/*
			True if the source is currently playing.
		*/
		bool m_playing;

		/*
			True if the source has finished.
		*/
		bool m_finished;

		/*
			True if the source is a stream.
		*/
		bool m_stream;

		/*
			The volume of the source.
		*/
		float m_volume;

		/*
			The id of the OpenAL source.
		*/
		Volatile<ALuint> m_id;

		/*
			The data for the source.
		*/
		std::unique_ptr<ALDataSource> m_data;

		/*
			The state change listener.
		*/
		Volatile<ALAudioSourceStateListener*> m_listener;

	public:

		/*
			Creates an audio source with the given data source.
			* @param[in] data The data source to use
		*/
		inline explicit ALAudioSource(std::unique_ptr<ALDataSource>&& data) :
			m_playing(false),
			m_finished(false),
			m_stream(false),
			m_volume(1.0f),
			m_id(AL_NONE),
			m_data(std::move(data)),
			m_listener(nullptr)
		{}

		/*
			No copying.
		*/
		ALAudioSource(const ALAudioSource&) = delete;

		/*
			Copy-constructor.
		*/
		inline ALAudioSource(ALAudioSource&& obj) noexcept :
			m_playing(obj.m_playing),
			m_finished(obj.m_finished),
			m_stream(obj.m_stream),
			m_volume(obj.m_volume),
			m_id(std::move(obj.m_id)),
			m_data(std::move(obj.m_data)),
			m_listener(std::move(obj.m_listener))
		{
			obj.m_playing = false;
			obj.m_finished = false;
			obj.m_stream = false;
			obj.m_volume = 1.0f;
		}

		/*
			Destructor.
		*/
		inline virtual ~ALAudioSource()
		{
			destroy();
		}

	public:

		/*
			Returns true if the source is currently playing.
		*/
		inline virtual bool isPlaying() const
		{
			return m_playing;
		}

		/*
			Returns true if the source has finished.
		*/
		inline virtual bool isFinished() const
		{
			return m_finished;
		}

		/*
			Returns the ID of the OpenAL source.
		*/
		inline virtual const Volatile<ALuint>& getID() const
		{
			return m_id;
		}

		/*
			Starts playing the source, or resumes it if it is already playing.
		*/
		virtual void play();

		/*
			Pauses the playing of the source.
		*/
		virtual void pause();

		/*
			Attempts to rewind the source.
			Returns true on success.
		*/
		virtual bool rewind();

		/*
			Ticks the source. Swapping buffers and aquiring new ones.
		*/
		virtual void tick();

		/*
			Sets the audio source state listener.
			* @param[in] listener The new source state listener
		*/
		virtual void setStateListener(ALAudioSourceStateListener* listener);

		/*
			Sets the volume of the source.
			* @param[in] volume The volume of the source
		*/
		virtual void setVolume(float volume);

	public:

		/*
			No copying.
		*/
		ALAudioSource& operator=(const ALAudioSource&) = delete;

		/*
			Move-assignment operator.
		*/
		inline ALAudioSource& operator=(ALAudioSource&& obj) noexcept
		{
			destroy();
			m_playing = obj.m_playing;
			m_finished = obj.m_finished;
			m_stream = obj.m_stream;
			m_volume = obj.m_volume;
			m_id = std::move(obj.m_id);
			m_data = std::move(obj.m_data);
			m_listener = std::move(obj.m_listener);
			obj.m_playing = false;
			obj.m_finished = false;
			obj.m_stream = false;
			obj.m_volume = 1.0f;
			return *this;
		}

	protected:

		/*
			Ensures that the OpenAL resource exists.
		*/
		virtual void create() noexcept;

		/*
			Deletes the OpenAL resource.
		*/
		virtual void destroy() noexcept;

		/*
			Updates the audio buffers and returns the number of updated buffers.
		*/
		virtual ALint updateBuffers();

	private:

		/*
			Sets up the source in its initial state.
		*/
		void setupSourceInitial__();

		/*
			Fires the state change event.
			* @param[in] change The state change
		*/
		void fireStateChange__(ALAudioSourceStateListener::Change change);

	};

}