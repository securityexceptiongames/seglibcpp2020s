#pragma once
#include <org/segames/library/exception.h>

#include <al/al.h>

namespace org::segames::library
{

	/*
		An exception for OpenAL errors.

		* @author	Philip Rosberg
		* @since	2020-12-07
		* @edited	2020-12-07
	*/
	class SEG_API ALException :
		public Exception
	{
	private:

		/*
			Creates an OpenAL exception from the given error code and the given stackTrace depth offset.
			* @param[in] error The error code
			* @param[in] stackTraceOffset The stack trace depth offset
		*/
		inline explicit ALException(const ALenum error, size_t stackTraceOffset) :
			Exception("org::segames::library::ALException", ("OpenAL error " + std::string(stringFromCode__(error))), StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, stackTraceOffset))
		{}

	public:

		/*
			Creates an OpenAL exception from the given error code.
			* @param[in] error The error code
		*/
		inline explicit ALException(const ALenum error) :
			ALException(error, 1)
		{}

		/*
			Creates an OpenAL exception with message.
			* @param[in] message The message to use for the exception
		*/
		inline explicit ALException(const std::string_view& message) :
			Exception("org::segames::library::ALException", message, StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1))
		{}

	private:

		/*
			Returns a string representation of the given error code.
			* @param[in] error The error code
		*/
		static std::string_view stringFromCode__(const ALenum error);

	public:

		/*
			Test for errors and throws if an error exists.
			Also corrects stack trace depth.
		*/
		inline static void errorTestAndThrow() noexcept(false)
		{
			ALenum error;
			if ((error = alGetError()) != AL_NO_ERROR)
				throw ALException(error, 2);
		}

	};

}