#pragma once
#include <org/segames/library/audio/pcm_format.h>
#include <org/segames/library/virtually_destructible.h>

#include <algorithm>

namespace org::segames::library
{

	/*
		A standard OpenAL buffer object.
		Any OpenAL resource is deallocated on deletion of this object.

		* @author	Philip Rosberg
		* @since	2020-12-05
		* @edited	2020-12-06
	*/
	class SEG_API ALBuffer :
		public VirtuallyDestructible
	{
	protected:

		/*
			The buffer id.
		*/
		ALuint m_id;

		/*
			The upload length in bytes.
		*/
		ALsizei m_uploadLength;

		/*
			The format of the data.
		*/
		PCMFormat m_format;

		/*
			The next image in the image chain. (Ie. copies that use the same OpenGL resource)
		*/
		ALBuffer* m_nextImage;

	public:

		/*
			Creates an empty, non-existent buffer.
		*/
		inline constexpr ALBuffer() :
			m_id(AL_NONE),
			m_uploadLength(0),
			m_format(),
			m_nextImage(this)
		{}

		/*
			No copying.
		*/
		ALBuffer(const ALBuffer&) = delete;

		/*
			Move constructor.
		*/
		inline ALBuffer(ALBuffer&& obj) noexcept :
			m_id(obj.m_id),
			m_uploadLength(obj.m_uploadLength),
			m_format(std::move(obj.m_format)),
			m_nextImage(obj.m_nextImage)
		{
			obj.m_id = AL_NONE;
			obj.m_uploadLength = 0;
			obj.m_nextImage = &obj;
		}

		/*
			Destructor.
		*/
		inline virtual ~ALBuffer()
		{
			this->destroy();
		}

	public:

		/*
			Returns the ID of the buffer.
		*/
		inline virtual ALuint getID() const
		{
			return m_id;
		}

		/*
			Returns the upload length of the buffer.
		*/
		inline virtual ALsizei getUploadLength() const
		{
			return m_uploadLength;
		}

		/*
			Returns the format of the uploaded buffer.
		*/
		inline virtual const PCMFormat& getFormat() const
		{
			return m_format;
		}

		/*
			Makes *this* ALBuffer into an image buffer mapping the same OpenAL resource as the given buffer.
			NOTE! The OpenAL resource is released when the last image is destroyed.
			NOTE! Pointer binding information is NOT transfered.
			* @param[in] obj The buffer to share OpenAL resource with
		*/
		virtual void makeIntoImageOf(const ALBuffer& obj);

		/*
			Uploads the given data of the given format.
			* @param[in] format The format of the data
			* @param[in] len The number of bytes of the data
			* @param[in] data The data pointer
		*/
		virtual void upload(const PCMFormat& format, ALsizei len, const ALvoid* data) noexcept(false);

	public:

		/*
			No copying.
		*/
		ALBuffer& operator=(const ALBuffer&) = delete;
				
		/*
			Move-assignment operator.
		*/
		inline ALBuffer& operator=(ALBuffer&& obj) noexcept
		{
			destroy();
			m_id = obj.m_id;
			m_uploadLength = obj.m_uploadLength;
			m_nextImage = obj.m_nextImage;
			obj.m_id = AL_NONE;
			obj.m_uploadLength = 0;
			obj.m_nextImage = &obj;
			return *this;
		}
				
		/*
			Equals operator.
		*/
		inline bool operator==(const ALBuffer& obj) const
		{
			return (m_id == obj.m_id) &&
				(m_nextImage == obj.m_nextImage) &&
				(m_format == obj.m_format);
		}

		/*
			Not-equals operator.
		*/
		inline bool operator!=(const ALBuffer& obj) const
		{
			return !operator==(obj);
		}

	protected:

		/*
			Ensures that the OpenAL resource exists.
		*/
		virtual void create() noexcept;

		/*
			Deletes the OpenAL resource. Or if this buffer is part of an image chain,
			then this method removes this entry from the chain.
		*/
		virtual void destroy() noexcept;

	};

}