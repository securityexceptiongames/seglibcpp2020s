#pragma once

namespace org::segames::library
{

	class ALAudioSource;

	/*
		Interface for listeners that listen for state changes (or general events)
		in an OpenAL Audio source.

		* @author	Philip Rosberg
		* @since	2020-12-13
		* @edited	2020-12-13
	*/
	class ALAudioSourceStateListener
	{
	public:

		/*
			The state changes that may occur.
			NONE		No change
			STARTED		Source started playing
			FINISHED	Source finished playing
			PAUSED		Source was paused
			RESUMED		Source was resumed after being paused
			REWOUND		Source was rewound
			RECOVERED	Source recovered (restarted) after data shortage 
						(DATA SHORTAGE IS BAD)
		*/
		enum class Change : char
		{
			NONE,
			STARTED,
			FINISHED,
			PAUSED,
			RESUMED,
			REWOUND,
			REWIND_FAILURE,
			RECOVERED
		};

	public:

		/*
			Called when a state change (or general event) of the source has occured.
			* @param[in] change The state change
			* @param[in] source A reference to the source
		*/
		virtual void stateChanged(Change change, ALAudioSource& source) = 0;

	};

}