#pragma once
#include <org/segames/library/audio/al/al_data_source.h>
#include <org/segames/library/audio/pcm_parser.h>

#include <memory>

namespace org::segames::library
{

	/*
		OpenAL data source that uses a static OpenAL buffer.

		* @author	Philip Rosberg
		* @since	2020-12-05
		* @edited	2020-12-07
	*/
	class SEG_API ALDataSourceStatic :
		public ALDataSource
	{
	public:

		/*
			The amount of parses to force-break after.
		*/
		static constexpr size_t PARSE_BREAK_COUNT = 1024 * 1024;

	protected:

		/*
			True if the source has been used.
		*/
		bool m_used;

		/*
			The OpenAL buffer data.
		*/
		ALBuffer m_buffer;

	public:

		/*
			Creates an empty, non-existent static source.
		*/
		inline constexpr ALDataSourceStatic() :
			m_used(false),
			m_buffer()
		{}

		/*
			Creates a static source out of the given buffer. Taking ownership of it.
			* @param[in] buffer The buffer to use as the static resource 
		*/
		inline explicit ALDataSourceStatic(ALBuffer&& buffer) :
			m_used(false),
			m_buffer(std::move(buffer))
		{}

		/*
			Creates a static source with the given format from the given parser data.
			(Extracts all data from the parser)
			* @param[in] format The format of the final data
			* @param[in] parser The parser to extract data from
		*/
		explicit ALDataSourceStatic(const PCMFormat& format, PCMParser& parser) noexcept(false);

		/*
			Copy-constructor.
		*/
		inline ALDataSourceStatic(const ALDataSourceStatic& obj) :
			m_used(false),
			m_buffer()
		{
			m_buffer.makeIntoImageOf(obj.m_buffer);
		}

		/*
			Move-constructor.
		*/
		inline ALDataSourceStatic(ALDataSourceStatic&& obj) noexcept :
			m_used(obj.m_used),
			m_buffer(std::move(obj.m_buffer))
		{}

	public:

		/*
			Returns the format of the data.
		*/
		inline virtual const PCMFormat& getFormat() const override
		{
			return m_buffer.getFormat();
		}

		/*
			Returns a reference to the next buffer to play.
			Returns a reference to an empty buffer when finished.
		*/
		inline virtual const ALBuffer& nextBuffer() override
		{
			if (m_used)
			{
				static const ALBuffer emptyBuffer;
				return emptyBuffer;
			}
			else
			{
				m_used = true;
				return m_buffer;
			}

		}

		/*
			Attempts to rewind the source to the begining.
			Returns true on sucess.
		*/
		inline virtual bool rewind() override
		{
			m_used = false;
			return true;
		}

		/*
			Returns the backing OpenAL buffer.
		*/
		inline virtual const ALBuffer& getBackingBuffer() const
		{
			return m_buffer;
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline ALDataSourceStatic& operator=(const ALDataSourceStatic& obj)
		{
			m_used = false;
			m_buffer.makeIntoImageOf(obj.m_buffer);
			return *this;
		}

		/*
			Move-assignment operator.
		*/
		inline ALDataSourceStatic& operator=(ALDataSourceStatic&& obj) noexcept
		{
			m_used = obj.m_used;
			m_buffer = std::move(obj.m_buffer);
			return *this;
		}

	};

}