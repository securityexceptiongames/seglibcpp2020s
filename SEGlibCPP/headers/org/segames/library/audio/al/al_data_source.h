#pragma once
#include <org/segames/library/audio/al/al_buffer.h>
#include <org/segames/library/virtually_destructible.h>

namespace org::segames::library
{

	/*
		Interface for OpenAL data sources.

		* @author	Philip Rosberg
		* @since	2020-12-05
		* @edited	2020-12-05
	*/
	class ALDataSource :
		public VirtuallyDestructible
	{
	public:

		/*
			Returns the format of the data.
		*/
		virtual const PCMFormat& getFormat() const = 0;

		/*
			Returns a reference to the next buffer to play.
			Returns a reference to an empty buffer when finished.
		*/
		virtual const ALBuffer& nextBuffer() = 0;

		/*
			Attempts to rewind the source to the begining.
			Returns true on sucess.
		*/
		virtual bool rewind() = 0;

	};

}