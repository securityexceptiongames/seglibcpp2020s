#pragma once
#include <org/segames/library/audio/al/al_exception.h>
#include <org/segames/library/util/string_util.h>

#include <al/alc.h>

namespace org::segames::library
{

	/*
		The main OpenAL instance singleton.

		* @author	Philip Rosberg
		* @since	2020-12-12
		* @since	2020-12-12
	*/
	class ALInstance final
	{
	private:

		/*
			The opened audio device.
		*/
		ALCdevice* m_device;

		/*
			The OpenAL context.
		*/
		ALCcontext* m_context;

	private:

		/*
			Creates the OpenAL instance.
			* @param[in] device The name of the device to open
		*/
		inline explicit ALInstance(const ALchar* device) noexcept(false) :
			m_device(alcOpenDevice(device)),
			m_context(nullptr)
		{
			ALException::errorTestAndThrow();
			if (!m_device)
				throw ALException("Could not open device.");
					
			m_context = alcCreateContext(m_device, NULL);
			ALException::errorTestAndThrow();
			if (!m_context)
				throw ALException("Could not create context.");
					
			if (!alcMakeContextCurrent(m_context))
				ALException::errorTestAndThrow();
		}

	public:

		/*
			No copying, moving etc.
		*/
		ALInstance(const ALInstance&) = delete;

		/*
			Destructor.
		*/
		inline ~ALInstance()
		{
			if (m_context)
				alcDestroyContext(m_context);
			if (m_device)
				alcCloseDevice(m_device);
		}

	public:

		/*
			Lists the names of the devices avaliable to OpenAL.
			The first device will be the default device.
		*/
		inline static ArrayList<std::string_view> listAvaliableOutputDevices()
		{
			ArrayList<std::string_view> output;
			listAvaliableOutputDevices(output);
			return output;
		}

		/*
			Lists the names of the devices avaliable to OpenAL.
			The first device will be the default device.
			* @param[out] output The output list
		*/
		template<typename pos_t, typename itr_t, typename citr_t>
		inline static void listAvaliableOutputDevices(List<std::string_view, pos_t, itr_t, citr_t>& output)
		{
			if (alcIsExtensionPresent(NULL, "ALC_ENUMERATION_EXT") == AL_TRUE)
			{
				std::string_view defaultDevice(alcGetString(NULL, ALC_DEFAULT_DEVICE_SPECIFIER));
				output.add(defaultDevice);

				size_t position = 0;
				const ALchar* devicesString = alcGetString(NULL, ALC_DEVICE_SPECIFIER);
				while (devicesString[position] != '\0')
				{
					std::string_view device(devicesString + position);
					if (device != defaultDevice)
						output.add(device);
					for (; devicesString[position] != '\0'; position++);
				}

			}

		}

		/*
			Retruns the global current OpenAL instance.
			Creates the instance if it has not already been created.
			* @param[in] device The device to open
		*/
		inline static ALInstance& createInstance(const ALchar* device) noexcept(false)
		{
			static ALInstance instance(device);
			return instance;
		}

		/*
			Retruns the global current OpenAL instance.
			Creates the instance if it has not already been created.
			* @param[in] device The device to open
		*/
		inline static ALInstance& createInstance(const std::string_view& device) noexcept(false)
		{
			return createInstance(device.data());
		}

		/*
			Retruns the global current OpenAL instance.
			Creates the instance if it has not already been created.
		*/
		inline static ALInstance& getInstance() noexcept(false)
		{
			return createInstance(NULL);
		}
				
	};

}