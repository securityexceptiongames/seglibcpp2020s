#pragma once
#include <org/segames/library/audio/pcm_buffer.h>
#include <org/segames/library/math/math.h>
#include <org/segames/library/util/array_deque.h>

namespace org::segames::library
{

	/*
		Buffer that automatically converts input data to 16bit (signed short) mono PCM.

		* @author	Philip Rosberg
		* @since	2020-09-12
		* @edited	2020-09-12
	*/
	class PCMBuffer16bitMono :
		public PCMBuffer
	{
	protected:

		/*
			The format of the buffer.
		*/
		PCMFormat m_format;

		/*
			The actual buffer.
		*/
		ArrayDeque<unsigned char> m_buffer;

	public:

		/*
			Creates an empty 16-bit PCM mono buffer.
		*/
		inline PCMBuffer16bitMono() :
			m_format(1, 16, 0),
			m_buffer()
		{}

		/*
			Creates an 16-bit PCM mono buffer with the given attribute.
			* @param[in] sampleRate The buffer sample rate in Hz (reads per second)
		*/
		inline explicit PCMBuffer16bitMono(ALsizei sampleRate) :
			m_format(1, 16, sampleRate),
			m_buffer()
		{}

		/*
			Creates an 16-bit PCM mono buffer with the given attribute.
			* @param[in] initialCapacity The initial capacity of the underlying buffer
		*/
		inline explicit PCMBuffer16bitMono(size_t initialCapacity) :
			PCMBuffer16bitMono(0, initialCapacity)
		{}

		/*
			Creates an 16-bit PCM mono buffer with the given attributes.
			* @param[in] sampleRate The buffer sample rate in Hz (reads per second)
			* @param[in] initialCapacity The initial capacity of the underlying buffer
		*/
		inline explicit PCMBuffer16bitMono(ALsizei sampleRate, size_t initialCapacity) :
			m_format(1, 16, sampleRate),
			m_buffer(initialCapacity)
		{}

		/*
			Copy-constructor.
		*/
		inline PCMBuffer16bitMono(const PCMBuffer16bitMono&) = default;

		/*
			Move-constructor.
		*/
		inline PCMBuffer16bitMono(PCMBuffer16bitMono&&) noexcept = default;

	public:

		/*
			Returns the number of avaliable native chars.
		*/
		inline virtual size_t nativeSize() const override
		{
			return m_buffer.nativeSize();
		}

		/*
			Returns the pointer to the begining of native chars.
		*/
		virtual char* nativePointer() override
		{
			return m_buffer.nativePointer();
		}

		/*
			Returns the pointer to the begining of native chars.
		*/
		virtual const char* nativePointer() const override
		{
			return m_buffer.nativePointer();
		}

		/*
			Returns the internal format of the buffer.
		*/
		inline virtual const PCMFormat& getFormat() const override
		{
			return m_format;
		}

		/*
			Puts the give PCM value into the buffer, converting it if necessary.
			* @param[in] c The channel value
		*/
		inline virtual void put(unsigned char c) override
		{
			put(static_cast<signed short>((static_cast<short>(c) - 128) * 256));
		}

		/*
			Puts the give PCM values into the buffer, converting them if necessary.
			* @param[in] c1 The first channel value
			* @param[in] c2 The second channel value
		*/
		inline virtual void put(unsigned char c1, unsigned char c2) override
		{
			put(static_cast<signed short>((static_cast<short>(c1) - 128) * 256), static_cast<signed short>((static_cast<short>(c2) - 128) * 256));
		}

		/*
			Puts the give PCM value into the buffer, converting it if necessary.
			* @param[in] c The channel value
		*/
		inline virtual void put(signed short c) override
		{
			m_buffer.enqueue(static_cast<unsigned char>(c));
			m_buffer.enqueue(static_cast<unsigned char>(c >> 8));
		}

		/*
			Puts the give PCM values into the buffer, converting them if necessary.
			* @param[in] c1 The first channel value
			* @param[in] c2 The second channel value
		*/
		inline virtual void put(signed short c1, signed short c2) override
		{
			put(static_cast<signed short>((static_cast<int>(c1) + c2) / 2));
		}

		/*
			Puts the give PCM value into the buffer, converting it if necessary.
			* @param[in] c The channel value
		*/
		inline virtual void put(float c) override
		{
			const float cc = Math::max(Math::min(c, 1.0f), -1.0f);
			put(static_cast<signed short>((32767.5f * cc) - 0.5f));
		}

		/*
			Puts the give PCM values into the buffer, converting them if necessary.
			* @param[in] c1 The first channel value
			* @param[in] c2 The second channel value
		*/
		inline virtual void put(float c1, float c2) override
		{
			const float c1c = Math::max(Math::min(c1, 1.0f), -1.0f);
			const float c2c = Math::max(Math::min(c2, 1.0f), -1.0f);
			put(static_cast<signed short>((32767.5f * c1c) - 0.5f), static_cast<signed short>((32767.5f * c2c) - 0.5f));
		}

		/*
			Discards the given amount of native units (chars) from the buffer.
			* @param[in] nativeCount The amount of native units (chars) to remove
		*/
		inline virtual void discard(size_t nativeCount) override
		{
			for (size_t i = 0; i < nativeCount; i++)
				if (!m_buffer.empty())
					m_buffer.dequeue();
				else
					break;
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline PCMBuffer16bitMono& operator=(const PCMBuffer16bitMono&) = default;

		/*
			Move-assignment operator.
		*/
		inline PCMBuffer16bitMono& operator=(PCMBuffer16bitMono&&) noexcept = default;

	};

}