#pragma once
#include <org/segames/library/audio/pcm_buffer.h>

#include <limits>

namespace org::segames::library
{
			
	/*
		Interface for PCM data parsers/streams.

		* @author	Philip Rosberg
		* @since	2020-09-13
		* @edited	2020-09-13
	*/
	class PCMParser :
		public VirtuallyDestructible
	{
	public:

		/*
			All samples flag (value).
		*/
		static constexpr size_t ALL_SAMPLES = std::numeric_limits<size_t>::max();

	public:

		/*
			Returns the format of the input data.
		*/
		virtual const PCMFormat& getFormat() const = 0;

		/*
			Attempts to parse the given amount of samples.
			Returns the number of successfully parsed samples.
			* @param[in] requestedSamples The samples to attempt to parse
			* @param[in] output The output PCM buffer
		*/
		virtual size_t parse(size_t requestedSamples, PCMBuffer& output) = 0;

		/*
			Attempts to rewind the PCM parser.
			Returns true on success.
		*/
		virtual bool rewind() = 0;

	};

}