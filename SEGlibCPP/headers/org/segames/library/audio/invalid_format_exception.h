#pragma once
#include <org/segames/library/audio/pcm_format.h>
#include <org/segames/library/exception.h>

namespace org::segames::library
{

	/*
		Exception for invalid audio formats.

		* @author	Philip Rosberg
		* @since	2020-12-07
		* @edited	2020-12-07
	*/
	class InvalidFormatException :
		public Exception
	{
	public:

		/*
			Creates an invalid format exception without message.
		*/
		inline InvalidFormatException() :
			Exception("org::segames::library::InvalidFormatException", "", StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1))
		{}

		/*
			Creates a invalid format exception with the given message.
			* @param[in] message The additional message to display
		*/
		inline explicit InvalidFormatException(const std::string_view& message) :
			Exception("org::segames::library::InvalidFormatException", message, StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1))
		{}

		/*
			Creates an invalid format exception with a message about the given type and value.
			* @param[in] value The value that was wrong
		*/
		inline explicit InvalidFormatException(const PCMFormat& format) :
			Exception("org::segames::library::InvalidValueException", "\"" + std::to_string(format) + "\" is an invalid audio format.", StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1))
		{}

	};

}