#pragma once
#include <org/segames/library/audio/pcm_parser.h>

#include <memory>
#include <istream>

namespace org::segames::library
{

	/*
		Implementation for parsing WAV data to PCM.

		* @author	Philip Rosberg
		* @since	2020-09-13
		* @edited	2020-12-07
	*/
	class SEG_API PCMParserWAV :
		public PCMParser
	{
	public:

		/*
			The WAV format block codes.
			32bit Little endian integer.
		*/
		enum class WAVBlockCode : int
		{
			NONE = 0x00000000,
			RIFF = 0x46464952,
			FORMAT = 0x020746d66,
			DATA = 0x61746164
		};

		/*
			The WAV data formats.
			16bit Little endian short.
		*/
		enum class WAVDataFormat : short
		{
			PCM = 0x01,
			IEEEFloatingPoint = 0x03
		};

	protected:
				
		/*
			The amount of data left in the current data block.
		*/
		int m_dataRemaining;

		/*
			The data start position of the stream.
		*/
		std::streampos m_dataStart;

		/*
			The format of the input data.
		*/
		PCMFormat m_format;

		/*
			The format of the stream data.
		*/
		WAVDataFormat m_dataFormat;

		/*
			The input stream.
		*/
		std::unique_ptr<std::istream> m_input;

	public:

		/*
			Creates an empty non-usable PCM WAV parser.
		*/
		inline PCMParserWAV() :
			m_dataRemaining(0),
			m_dataStart(0),
			m_format(),
			m_dataFormat(WAVDataFormat::PCM),
			m_input()
		{}

		/*
			Creates a PCM WAV parser from the given input stream.
			* @param[in] input The input stream to parse from
		*/
		explicit PCMParserWAV(std::unique_ptr<std::istream>&& input) noexcept(false);

		/*
			No copying.
		*/
		PCMParserWAV(const PCMParserWAV&) = delete;
				
		/*
			Move-constructor.
		*/
		inline PCMParserWAV(PCMParserWAV&&) = default;

	public:

		/*
			Returns the format of the input data.
		*/
		inline virtual const PCMFormat& getFormat() const override
		{
			return m_format;
		}

		/*
			The data format of the WAV stream.
		*/
		inline virtual WAVDataFormat getDataFormat() const
		{
			return m_dataFormat;
		}

		/*
			Attempts to parse the given amount of samples.
			Returns the number of successfully parsed samples.
			* @param[in] requestedSamples The samples to attempt to parse
			* @param[in] output The output PCM buffer
		*/
		virtual size_t parse(size_t requestedSamples, PCMBuffer& output) override;

		/*
			Attempts to rewind the PCM parser.
			Returns true on success.
		*/
		virtual bool rewind() override;

	private:

		/*
			Seeks the next data block.
			Returns the size of the data block (0 on failure).
			* @param[in] input The input stream to seek in
		*/
		static int seekNextData__(std::istream& input);

		/*
			Extracts a sample of the given amount of channels from the input stream and places it in the buffer.
		*/
		template<typename T>
		inline static int extractSample__(unsigned char channels, std::istream& input, PCMBuffer& output) noexcept(false)
		{
			constexpr size_t maxChannels = 2;
			union
			{
				char data[maxChannels * sizeof(T)];
				T c[maxChannels];
			} conversion;
			int readTotal = 0;
					
			input.read(conversion.data, readTotal = ((channels < maxChannels ? channels : maxChannels) * sizeof(T)));
			if (channels > maxChannels)
			{
				int readSize = 0;
				input.ignore(readSize = ((channels - maxChannels) * sizeof(T)));
				readTotal += readSize;
			}

			if (channels == 1)
				output.put(conversion.c[0]);
			else
				output.put(conversion.c[0], conversion.c[1]);

			return readTotal;
		}

	};

}