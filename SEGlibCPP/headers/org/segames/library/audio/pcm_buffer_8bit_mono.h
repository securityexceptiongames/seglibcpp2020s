#pragma once
#include <org/segames/library/audio/pcm_buffer.h>
#include <org/segames/library/math/math.h>
#include <org/segames/library/util/array_deque.h>

namespace org::segames::library
{

	/*
		Buffer that automatically converts input data to 8bit (unsigned char) mono PCM.

		* @author	Philip Rosberg
		* @since	2020-09-11
		* @edited	2020-09-11
	*/
	class PCMBuffer8bitMono :
		public PCMBuffer
	{
	protected:

		/*
			The format of the buffer.
		*/
		PCMFormat m_format;

		/*
			The actual buffer.
		*/
		ArrayDeque<unsigned char> m_buffer;

	public:

		/*
			Creates an empty 8-bit PCM mono buffer.
		*/
		inline PCMBuffer8bitMono() :
			m_format(1, 8, 0),
			m_buffer()
		{}

		/*
			Creates an 8-bit PCM mono buffer with the given attribute.
			* @param[in] sampleRate The buffer sample rate in Hz (reads per second)
		*/
		inline explicit PCMBuffer8bitMono(ALsizei sampleRate) :
			m_format(1, 8, sampleRate),
			m_buffer()
		{}

		/*
			Creates an 8-bit PCM mono buffer with the given attribute.
			* @param[in] initialCapacity The initial capacity of the underlying buffer
		*/
		inline explicit PCMBuffer8bitMono(size_t initialCapacity) :
			PCMBuffer8bitMono(0, initialCapacity)
		{}

		/*
			Creates an 8-bit PCM mono buffer with the given attributes.
			* @param[in] sampleRate The buffer sample rate in Hz (reads per second)
			* @param[in] initialCapacity The initial capacity of the underlying buffer
		*/
		inline explicit PCMBuffer8bitMono(ALsizei sampleRate, size_t initialCapacity) :
			m_format(1, 8, sampleRate),
			m_buffer(initialCapacity)
		{}

		/*
			Copy-constructor.
		*/
		inline PCMBuffer8bitMono(const PCMBuffer8bitMono&) = default;

		/*
			Move-constructor.
		*/
		inline PCMBuffer8bitMono(PCMBuffer8bitMono&&) noexcept = default;

	public:

		/*
			Returns the number of avaliable native chars.
		*/
		inline virtual size_t nativeSize() const override
		{
			return m_buffer.nativeSize();
		}

		/*
			Returns the pointer to the begining of native chars.
		*/
		virtual char* nativePointer() override
		{
			return m_buffer.nativePointer();
		}

		/*
			Returns the pointer to the begining of native chars.
		*/
		virtual const char* nativePointer() const override
		{
			return m_buffer.nativePointer();
		}

		/*
			Returns the internal format of the buffer.
		*/
		inline virtual const PCMFormat& getFormat() const override
		{
			return m_format;
		}

		/*
			Puts the give PCM value into the buffer, converting it if necessary.
			* @param[in] c The channel value
		*/
		inline virtual void put(unsigned char c) override
		{
			m_buffer.enqueue(c);
		}

		/*
			Puts the give PCM values into the buffer, converting them if necessary.
			* @param[in] c1 The first channel value
			* @param[in] c2 The second channel value
		*/
		inline virtual void put(unsigned char c1, unsigned char c2) override
		{
			put(static_cast<unsigned char>((static_cast<int>(c1) + c2) / 2));
		}

		/*
			Puts the give PCM value into the buffer, converting it if necessary.
			* @param[in] c The channel value
		*/
		inline virtual void put(signed short c) override
		{
			put(static_cast<unsigned char>(c / 256 + 128));
		}

		/*
			Puts the give PCM values into the buffer, converting them if necessary.
			* @param[in] c1 The first channel value
			* @param[in] c2 The second channel value
		*/
		inline virtual void put(signed short c1, signed short c2) override
		{
			put(static_cast<unsigned char>(((static_cast<int>(c1) + c2) / 2) / 256 + 128));
		}

		/*
			Puts the give PCM value into the buffer, converting it if necessary.
			* @param[in] c The channel value
		*/
		inline virtual void put(float c) override
		{
			const float cc = Math::max(Math::min(c, 1.0f), -1.0f);
			put(static_cast<unsigned char>((127.5f * cc) + 127.5f));
		}

		/*
			Puts the give PCM values into the buffer, converting them if necessary.
			* @param[in] c1 The first channel value
			* @param[in] c2 The second channel value
		*/
		inline virtual void put(float c1, float c2) override
		{
			const float cc = Math::max(Math::min(0.5f * (c1 + c2), 1.0f), -1.0f);
			put(static_cast<unsigned char>((127.5f * cc) + 127.5f));
		}

		/*
			Discards the given amount of native units (chars) from the buffer.
			* @param[in] nativeCount The amount of native units (chars) to remove
		*/
		inline virtual void discard(size_t nativeCount) override
		{
			for (size_t i = 0; i < nativeCount; i++)
				if (!m_buffer.empty())
					m_buffer.dequeue();
				else
					break;
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline PCMBuffer8bitMono& operator=(const PCMBuffer8bitMono&) = default;

		/*
			Move-assignment operator.
		*/
		inline PCMBuffer8bitMono& operator=(PCMBuffer8bitMono&&) noexcept = default;

	};

}