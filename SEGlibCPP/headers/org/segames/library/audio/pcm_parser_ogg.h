#pragma once
#include <org/segames/library/audio/pcm_parser.h>

#define STB_VORBIS_HEADER_ONLY
#include <stb/stb_vorbis.c>

#include <memory>
#include <istream>

namespace org::segames::library
{

	/*
		Implementation for parsing vorbis data to PCM.

		* @author	Philip Rosberg
		* @since	2020-12-16
		* @edited	2020-12-16
	*/
	class SEG_API PCMParserOGG :
		public PCMParser
	{
	private:

		/*
			The maximum amount of bytes to read when aquiring new data.
		*/
		static size_t s_readSize;

	protected:

		/*
			The format of the input data.
		*/
		PCMFormat m_format;

		/*
			The amount of bytes used in the buffer.
		*/
		size_t m_bufferUsed;

		/*
			The total length of the buffer.
		*/
		size_t m_bufferLength;

		/*
			The raw input data buffer.
		*/
		std::unique_ptr<char[]> m_buffer;

		/*
			The data start position of the stream.
		*/
		std::streampos m_dataStart;

		/*
			The input stream.
		*/
		std::unique_ptr<std::istream> m_input;

		/*
			The vorbis instance.
		*/
		stb_vorbis* m_vorbis;

		/*
			The amount of used floats in the output.
		*/
		int m_outputUsed;

		/*
			The total length of the output.
		*/
		int m_outputLength;

		/*
			The stb-vorbis output.
		*/
		float** m_output;

	public:

		/*
			Creates an emptry PCM parser for OGG.
		*/
		inline PCMParserOGG() :
			m_format(),
			m_bufferUsed(0),
			m_bufferLength(0),
			m_buffer(),
			m_dataStart(),
			m_input(),
			m_vorbis(nullptr),
			m_outputUsed(0),
			m_outputLength(0),
			m_output(nullptr)
		{}

		/*
			Creates a PCM OGG parser from the given input stream.
			* @param[in] input The input stream to parse from
		*/
		explicit PCMParserOGG(std::unique_ptr<std::istream>&& input) noexcept(false);

		/*
			No copying.
		*/
		PCMParserOGG(const PCMParserOGG&) = delete;

		/*
			Copy-constructor.
		*/
		inline PCMParserOGG(PCMParserOGG&& obj) noexcept :
			m_format(std::move(obj.m_format)),
			m_bufferUsed(obj.m_bufferUsed),
			m_bufferLength(obj.m_bufferLength),
			m_buffer(std::move(obj.m_buffer)),
			m_dataStart(obj.m_dataStart),
			m_input(std::move(obj.m_input)),
			m_vorbis(obj.m_vorbis),
			m_outputUsed(obj.m_outputUsed),
			m_outputLength(obj.m_outputLength),
			m_output(obj.m_output)
		{
			obj.m_bufferUsed = 0;
			obj.m_bufferLength = 0;
			obj.m_dataStart = 0;
			obj.m_vorbis = nullptr;
			obj.m_outputUsed = 0;
			obj.m_outputLength = 0;
			obj.m_output = nullptr;
		}

		/*
			Destructor.
		*/
		virtual ~PCMParserOGG() noexcept;

	public:

		/*
			Returns the format of the input data.
		*/
		inline virtual const PCMFormat& getFormat() const
		{
			return m_format;
		}

		/*
			Attempts to parse the given amount of samples.
			Returns the number of successfully parsed samples.
			* @param[in] requestedSamples The samples to attempt to parse
			* @param[in] output The output PCM buffer
		*/
		virtual size_t parse(size_t requestedSamples, PCMBuffer& output);

		/*
			Attempts to rewind the PCM parser.
			Returns true on success.
		*/
		virtual bool rewind();

	public:

		/*
			No copying.
		*/
		PCMParserOGG& operator=(const PCMParserOGG&) = delete;

		/*
			Move-assignment operator.
		*/
		inline PCMParserOGG& operator=(PCMParserOGG&& obj) noexcept
		{
			m_format = std::move(obj.m_format);
			m_bufferUsed = obj.m_bufferUsed;
			m_bufferLength = obj.m_bufferLength;
			m_buffer = std::move(obj.m_buffer);
			m_dataStart = obj.m_dataStart;
			m_input = std::move(obj.m_input);
			m_vorbis = obj.m_vorbis;
			m_outputUsed = obj.m_outputUsed;
			m_outputLength = obj.m_outputLength;
			m_output = obj.m_output;
			obj.m_bufferUsed = 0;
			obj.m_bufferLength = 0;
			obj.m_dataStart = 0;
			obj.m_vorbis = nullptr;
			obj.m_outputUsed = 0;
			obj.m_outputLength = 0;
			obj.m_output = nullptr;
			return *this;
		}

	private:

		/*
			Attempts to aquire more data from the local input stream.
		*/
		bool aquireMoreData__();

		/*
			Attempts to aquire more audio samples.
			Returns true if it aquired samples or the data to aquire samples next time.
		*/
		bool aquireMoreSamples__();

	};

}