#pragma once
#include <org/segames/library/dllmain.h>
#include <org/segames/library/string_visualizable.h>

#include <al/al.h>

namespace org::segames::library
{

	/*
		Format structure for PCM data.

		* @author	Philip Rosberg
		* @since	2020-08-30
		* @edited	2020-08-30
	*/
	class PCMFormat final :
		public StringVisualizable
	{
	private:

		/*
			The amount of channels per sample.
		*/
		unsigned char m_channels;

		/*
			The bit depth per channel and sample.
		*/
		unsigned char m_bitDepth;

		/*
			The OpenAL internal format.
		*/
		ALenum m_alFormat;

		/*
			Sample rate in Hz (reads per second).
		*/
		ALsizei m_sampleRate;

	public:

		/*
			Creates a zeroed PCM format
		*/
		inline constexpr PCMFormat() :
			PCMFormat(0, 0, 0)
		{}

		/*
			Creates a PCM format with the given configuration.
			* @param[in] channels The number of channels
			* @param[in] bitDepth The bit depth per channel
			* @param[in] sampleRate The sample rate in Hz
		*/
		inline explicit constexpr PCMFormat(unsigned char channels, unsigned char bitDepth, ALsizei sampleRate) :
			m_channels(channels),
			m_bitDepth(bitDepth),
			m_alFormat(evaluateOpenALFormat(channels, bitDepth)),
			m_sampleRate(sampleRate)
		{}

		/*
			Creates a PCM format with the given configuration.
			* @param[in] alFormat The OpenAL format
			* @param[in] sampleRate The sample rate in Hz
		*/
		inline explicit constexpr PCMFormat(ALenum alFormat, ALsizei sampleRate) :
			m_channels(channelsOfALFormat(alFormat)),
			m_bitDepth(bitDepthOfALFormat(alFormat)),
			m_alFormat(alFormat),
			m_sampleRate(sampleRate)
		{}

		/*
			Copy-constructor.
		*/
		inline constexpr PCMFormat(const PCMFormat&) = default;

		/*
			Move-constructor.
		*/
		inline constexpr PCMFormat(PCMFormat&&) noexcept = default;

	public:

		/*
			Returns the channel count.
		*/
		inline constexpr unsigned char channels() const
		{
			return m_channels;
		}

		/*
			Returns the bit depth per channel.
		*/
		inline constexpr unsigned char bitDepth() const
		{
			return m_bitDepth;
		}

		/*
			Returns the OpenAL format.
		*/
		inline constexpr ALenum alFormat() const
		{
			return m_alFormat;
		}

		/*
			Returns the sample rate in Hz (reads per second).
		*/
		inline constexpr ALsizei sampleRate() const
		{
			return m_sampleRate;
		}

		/*
			Calculates and returns the number of bits per sample.
		*/
		inline constexpr ALsizei sampleSize() const
		{
			return static_cast<ALsizei>(channels()) * static_cast<ALsizei>(bitDepth());
		}

		/*
			Calculates and returns the number of bytes per sample.
		*/
		inline constexpr ALsizei sampleBytes() const
		{
			return sampleSize() / sizeof(ALchar);
		}

		/*
			Calculates and returns the total bits being processed per second.
		*/
		inline constexpr ALsizei totalBitRate() const
		{
			return sampleRate() * sampleSize();
		}

		/*
			Calculates and returns the total bytes being processed per second.
		*/
		inline constexpr ALsizei totalByteRate() const
		{
			return sampleRate() * sampleBytes();
		}

		/*
			Returns a string representation of the type.
		*/
		inline virtual std::string toString() const override
		{
			std::string out = "PCMFormat(";
			out += "channels = " + std::to_string(static_cast<int>(channels()));
			out += ", depth = " + std::to_string(static_cast<int>(bitDepth()));
			out += ", sample rate = " + std::to_string(sampleRate());
			return out += ")";
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline constexpr PCMFormat& operator=(const PCMFormat&) = default;

		/*
			Move-assignment operator.
		*/
		inline constexpr PCMFormat& operator=(PCMFormat&&) noexcept = default;

		/*
			Equals operator.
		*/
		inline constexpr bool operator==(const PCMFormat& obj) const
		{
			return (m_channels == obj.m_channels) &&
				(m_bitDepth == obj.m_bitDepth) &&
				(m_alFormat == obj.m_alFormat) &&
				(m_sampleRate == obj.m_sampleRate);
		}

		/*
			Not-equals operator.
		*/
		inline constexpr bool operator!=(const PCMFormat& obj) const
		{
			return !operator==(obj);
		}

	public:

		/*
			Evaluates the corresponding OpenAL format for the given channels and bit depth.
			* @param[in] channels The number of channels, 1 or 2
			* @param[in] bitDepth The bit depth per channel
		*/
		inline static constexpr ALenum evaluateOpenALFormat(unsigned char channels, unsigned char bitDepth)
		{
			if (bitDepth == 8)
			{
				switch (channels)
				{
				case 2:
					return AL_FORMAT_STEREO8;
				case 1:
					return AL_FORMAT_MONO8;
				default:
					return AL_NONE;
				}

			}
			else if (bitDepth == 16)
			{
				switch (channels)
				{
				case 2:
					return AL_FORMAT_STEREO16;
				case 1:
					return AL_FORMAT_MONO16;
				default:
					return AL_NONE;
				}

			}
			else
				return AL_NONE;
		}

		/*
			Returns the channel count of the given OpenAL format.
			* @param[in] format The OpenAL format
		*/
		inline static constexpr unsigned char channelsOfALFormat(ALenum format)
		{
			switch (format)
			{
			case AL_FORMAT_STEREO8:
			case AL_FORMAT_STEREO16:
				return 2;
			case AL_FORMAT_MONO8:
			case AL_FORMAT_MONO16:
				return 1;
			default:
				return 0;
			}

		}

		/*
			Returns the bit depth per channel of the given OpenAL format.
			* @param[in] format The OpenAL format
		*/
		inline static constexpr unsigned char bitDepthOfALFormat(ALenum format)
		{
			switch (format)
			{
			case AL_FORMAT_MONO16:
			case AL_FORMAT_STEREO16:
				return 16;
			case AL_FORMAT_MONO8:
			case AL_FORMAT_STEREO8:
				return 8;
			default:
				return 0;
			}

		}

	};

}