#pragma once
#include <cstddef>
#include <iostream>

namespace org::segames::library
{

	constexpr char BIG_ENDIAN = 0b00000001;
	constexpr char LITTLE_ENDIAN = 0b00000010;
	constexpr char SYSTEM_ENDIAN = 0b00000100;

	/*
		Class/Namespace for system related functions.

		* @author	Philip Rosberg
		* @since	2019-01-25
		* @since	2020-09-05
	*/
	class System final
	{
	public:

		/*
			Returns true if the system is big endian and false if the system is little endian.
		*/
		inline static bool isBigEndian()
		{
			unsigned short v = 1;
			return *((char*)&v) != 1;
		}

		/*
			Changes the byte-order of the given memory.
			* @param[in] count The amount of elements to flip the byte order of
			* @param[in] ptr The pointer to the memory block
		*/
		template<typename T, char mask>
		inline static T* bo_cast(size_t count, void* ptr)
		{
			static_assert(
				!(LITTLE_ENDIAN == mask) &&
				!(BIG_ENDIAN == mask) &&
				!((SYSTEM_ENDIAN | LITTLE_ENDIAN | BIG_ENDIAN) == mask), 
				"Incorrect byte-order conversion mask!");
			return bo_cast__<T, mask>(count, ptr, std::bool_constant<mask & SYSTEM_ENDIAN>());
		}

		/*
			Changes the byte-order of the given memory. (DYNAMIC MASK VERSION)
			* @param[in] count The amount of elements to flip the byte order of
			* @param[in] ptr The pointer to the memory block
			* @param[in] mask The conversion mask
		*/
		template<typename T>
		inline static T* bo_cast(size_t count, void* ptr, char mask)
		{
			if ((LITTLE_ENDIAN == mask) ||
				(BIG_ENDIAN == mask) ||
				((SYSTEM_ENDIAN | LITTLE_ENDIAN | BIG_ENDIAN) == mask))
			{
				return nullptr;
			}

			if (mask & SYSTEM_ENDIAN)
			{
				if ((isBigEndian() ? (mask & LITTLE_ENDIAN) : (mask & BIG_ENDIAN)) != 0)
					return bo_swap__<T>(count, ptr, std::true_type());
				else
					return bo_swap__<T>(count, ptr, std::false_type());
			}
			else if((mask & LITTLE_ENDIAN) && (mask & BIG_ENDIAN))
				return bo_swap__<T>(count, ptr, std::true_type());
			else
				return bo_swap__<T>(count, ptr, std::false_type());
		}

	private:

		/*
			Does not change the byte-order of the given memory.
			* @param[in] ptr The pointer to the memory block
		*/
		template<typename T>
		inline static T* bo_swap__(size_t count, void* ptr, std::false_type)
		{
			return static_cast<T*>(ptr);
		}

		/*
			Changes the byte-order of the given memory.
			* @param[in] count The amount of elements to flip the byte order of
			* @param[in] ptr The pointer to the memory block
		*/
		template<typename T>
		inline static T* bo_swap__(size_t count, void* ptr, std::true_type)
		{
			char* const pos = static_cast<char*>(ptr);
			for (size_t i = 0; i < (count * sizeof(T)); i += sizeof(T))
				for (size_t c = 0; c < sizeof(T) / 2; c++)
				{
					char* const a = pos + (i + c);
					char* const b = pos + (i + sizeof(T) - c - 1);
					const char temp = *a;
					*a = *b;
					*b = temp;
				}

			return static_cast<T*>(ptr);
		}

		/*
			Changes the byte-order of the given memory testing for the system endian.
			* @param[in] count The amount of elements to flip the byte order of
			* @param[in] ptr The pointer to the memory block
		*/
		template<typename T, char mask>
		inline static T* bo_cast__(size_t count, void* ptr, std::true_type)
		{
			if ((isBigEndian() ? (mask & LITTLE_ENDIAN) : (mask & BIG_ENDIAN)) != 0)
				return bo_swap__<T>(count, ptr, std::true_type());
			else
				return bo_swap__<T>(count, ptr, std::false_type());
		}

		/*
			Does not change the byte-order of the given memory.
			* @param[in] count The amount of elements to flip the byte order of
			* @param[in] ptr The pointer to the memory block
		*/
		template<typename T, char mask>
		inline static T* bo_cast__(size_t count, void* ptr, std::false_type)
		{
			return bo_swap__<T>(count, ptr, std::bool_constant<(mask & LITTLE_ENDIAN) && (mask & BIG_ENDIAN)>());
		}

	};

	/*
		Changes the byte-order of the given memory.
		* @param[in] count The amount of elements to flip the byte order of
		* @param[in] ptr The pointer to the memory block
	*/
	template<typename T, char mask>
	inline T* bo_cast(size_t count, void* ptr)
	{
		return System::bo_cast<T, mask>(count, ptr);
	}

}