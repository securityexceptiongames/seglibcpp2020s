#pragma once
#include <org/segames/library/dllmain.h>

#include <string>
#include <vector>
#include <limits>

namespace org::segames::library
{

	/*
		Class/Namespace for stack trace lookup.

		* @author	Philip Rosberg
		* @since	2020-06-09
		* @since	2020-06-09
	*/
	class SEG_API StackTrace final
	{
	public:

		static constexpr size_t ALL_STACK_FRAMES = std::numeric_limits<size_t>::max();

		static constexpr size_t MAX_STACK_FRAME_COUNT = 1024;

		static constexpr size_t MAX_FUNCTION_NAME_LENGTH = 1024;

	public:

		/*
			Structure of information for one stack frame.

			* @author	Philip Rosberg
			* @since	2020-06-09
			* @since	2020-06-09
		*/
		struct Frame
		{
			size_t line;
			size_t address;
			std::string function;
			std::string file;
		};

		/*
			Structure of information for one stack frame using C types.
			Generally all content of this type is allocated through malloc.

			* @author	Philip Rosberg
			* @since	2021-03-05
			* @since	2021-03-05
		*/
		struct CFrame
		{
			size_t line;
			size_t address;
			char* function;
			char* file;
		};

	private:

		/*
			True if SymInitialize has been sucessfully called.
		*/
		static bool s_symInitialized;

	public:
				
		/*
			Retrieves the requested number of stack frames and returns then in the given vector.
			NOTE! If the count exceeds the number of avaliable frames, the avaliable number is returned.
			* @param[in] count The requested frame count to return, max value to returns all avaliable frames
			* @param[in] offset The starting frame offset, excluding this fuction
		*/
		inline static std::vector<Frame> getStackTrace(const size_t count, const size_t offset)
		{
			std::vector<Frame> frames;
			getStackTrace(count, offset + 1, frames);
			return frames;
		}

		/*
			Retrieves the requested number of stack frames and returns then in the given vector.
			NOTE! If the count exceeds the number of avaliable frames, the avaliable number is returned.
			* @param[in] count The requested frame count to return, max value to returns all avaliable frames
			* @param[in] offset The starting frame offset, excluding this fuction
			* @param[in] output The output vector
		*/
		static void getStackTrace(const size_t count, const size_t offset, std::vector<Frame>& output);

		/*
			Retrieves the requested number of stack frames and returns then in the given vector.
			NOTE! This method only uses standard C functionallity to allocate the output frame array.
			NOTE! If the count exceeds the number of avaliable frames, the avaliable number is returned.
			* @param[in] count The requested frame count to return, max value to returns all avaliable frames
			* @param[in] offset The starting frame offset, excluding this fuction
			* @param[in] outCount The output frame array length
			* @param[in] output The output frame array
		*/
		static void getStackTraceCAlloc(const size_t count, const size_t offset, size_t* outCount, CFrame** output);

	public:

		StackTrace() = delete;

	};

}