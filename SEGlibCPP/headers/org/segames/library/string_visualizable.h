#pragma once
#include <string>
#include <ostream>

namespace org::segames::library
{

	/*
		Interface for types that are visualizable as a string.

		* @author	Philip Rosberg
		* @since	2020-06-15
		* @since	2020-06-15
	*/
	class StringVisualizable
	{
	public:

		/*
			Returns a string representation of the type.
		*/
		virtual std::string toString() const = 0;

	};

	/*
		Writes the string representation of the given object to the given output stream.
		* @param[in] out The output stream to write to
		* @param[in] obj The string-visualizable instance to print
	*/
	inline std::ostream& operator<<(std::ostream& out, const StringVisualizable& obj)
	{
		out << obj.toString();
		return out;
	}

}

namespace std
{

	/*
		Extension overload to the std::to_string to include string-visualizable types.
		Functions the same as calling toString() on the object.
		* @param[in] obj The string-visualizable instance to convert to a string
	*/
	inline std::string to_string(const org::segames::library::StringVisualizable& obj)
	{
		return obj.toString();
	}

	/*
		Extension overload to the std::to_string for characters.
		* @param[in] value The value
	*/
	inline std::string to_string(const char value)
	{
		return "" + value;
	}

	/*
		Extension overload to the std::to_string for short integers.
		* @param[in] value The value
	*/
	inline std::string to_string(const short value)
	{
		return std::to_string(static_cast<int>(value));
	}

	/*
		Extension overload to the std::to_string for unsigned characters.
		* @param[in] value The value
	*/
	inline std::string to_string(const unsigned char value)
	{
		return "" + value;
	}

	/*
		Extension overload to the std::to_string for unsigned short integers.
		* @param[in] value The value
	*/
	inline std::string to_string(const unsigned short value)
	{
		return std::to_string(static_cast<unsigned int>(value));
	}

	/*
		Extension overload to the std::to_string to include all things ever.
		Returns "NOSTRING" as there is no other method to call for the type.
		* @param[in] obj The type
	*/
	template<typename T>
	inline std::string to_string(const T& obj)
	{
		return "NOSTRING";
	}

}