#pragma once
#include <org/segames/library/util/random.h>
#include <org/segames/library/dllmain.h>

#include <type_traits>
#include <cmath>

namespace org::segames::library
{

	/*
		Namespace/class for math related functions.

		* @author	Philip Rosberg
		* @since	2019-06-22
		* @edited	2020-06-29
	*/
	class SEG_API Math final
	{
	public:

		static constexpr long double PI = 3.141592653589793238L;

	public:

		/*
			Returns the absolute of the given value.
			* @param[in] value The value
		*/
		template<typename T>
		inline static constexpr T abs(const T& value)
		{
			if (value < 0)
				return -value;
			return value;
		}

		/*
			A function that clamps the given value between the given minimum and maximum limit.
			* @param[in] value The value to clamp
			* @param[in] min The minimum limit
			* @param[in] max The maximum limit
		*/
		template<typename T, typename T2, typename T3>
		inline static constexpr T clamp(const T& value, const T2& min, const T3& max)
		{
			if (value > max)
				return static_cast<T>(max);
			else if (value < min)
				return static_cast<T>(min);
			return value;
		}
				
		/*
			Computes the inverse square root of the given number using the fast approximate method.
			* @param[in] number The number to calculate the inverse square root of
		*/
		template<size_t NewtonSteps = 1>
		inline static constexpr float fastInverseSquareRoot(const float number) noexcept
		{
			static_assert(std::numeric_limits<float>::is_iec559, "Wrong float standard!");
			static_assert(sizeof(float) == sizeof(int), "Wrong float/int sizes");

			const int bits = 0x5F3759DF - (reinterpret_cast<const int&>(number) >> 1);
			float y = reinterpret_cast<const float&>(bits);
			for (size_t n = 0; n < NewtonSteps; n++)
				y = y * (1.5f - (number * 0.5f * y * y));
			return y;
		}

		/*
			Computes the inverse square root of the given number using the fast approximate method.
			* @param[in] number The number to calculate the inverse square root of
		*/
		template<size_t NewtonSteps = 1>
		inline static constexpr double fastInverseSquareRoot(const double number) noexcept
		{
			static_assert(std::numeric_limits<double>::is_iec559, "Wrong float standard!");
			static_assert(sizeof(double) == sizeof(long long), "Wrong double/long long sizes");

			const long long bits = 0x5FE6EC85E7DE30DA - (reinterpret_cast<const long long&>(number) >> 1);
			double y = reinterpret_cast<const double&>(bits);
			for (size_t n = 0; n < NewtonSteps; n++)
				y = y * (1.5 - (number * 0.5 * y * y));
			return y;
		}

		/*
			Returns a reference to the given referenced value.
		*/
		template<typename T>
		inline static constexpr const T& max(const T& val)
		{
			return val;
		}

		/*
			A function that returns a reference to the latter value[s] if it is larger than the former, otherwise it returns a reference to the former.
			* @param[in] val The former value
			* @param[in] vals The latter value[s]
		*/
		template<typename T, typename... T2>
		inline static constexpr const T& max(const T& val, const T2&... vals)
		{
			static_assert(std::conjunction<std::is_same<T, T2>...>::value, "Cannot take max of different types!");
			const auto& maxVal = max(vals...);
			if (maxVal > val)
				return maxVal;
			else
				return val;
		}

		/*
			Returns a reference to the given referenced value.
		*/
		template<typename T>
		inline static constexpr const T& min(const T& val)
		{
			return val;
		}

		/*
			A function that returns a reference to the latter value[s] if it is smaller than the former, otherwise it returns a reference to the former.
			* @param[in] val The former value
			* @param[in] vals The latter value[s]
		*/
		template<typename T, typename... T2>
		inline static constexpr const T& min(const T& val, const T2&... vals)
		{
			static_assert(std::conjunction<std::is_same<T, T2>...>::value, "Cannot take min of different types!");
			const auto& minVal = min(vals...);
			if (minVal < val)
				return minVal;
			else
				return val;
		}

		/*
			Performs a linear interpolation between x and y using a to weight between them.
			* @param[in] x The primary value
			* @param[in] y The secondary value
			* @param[in] a The weighting value
		*/
		inline static constexpr float mix(const float x, const float y, const float a)
		{
			return x * (1 - a) + y * a;
		}

		/*
			Performs a linear interpolation between x and y using a to weight between them.
			* @param[in] x The primary value
			* @param[in] y The secondary value
			* @param[in] a The weighting value
		*/
		inline static constexpr double mix(const double x, const double y, const double a)
		{
			return x * (1 - a) + y * a;
		}

		/*
			Performs a linear interpolation between x and y using a to weight between them.
			* @param[in] x The primary value
			* @param[in] y The secondary value
			* @param[in] a The weighting value
		*/
		inline static constexpr long double mix(const long double x, const long double y, const long double a)
		{
			return x * (1 - a) + y * a;
		}

		/*
			Performes a smooth Hermite interpolation between 0 and 1 when edge0 < x < edge1. Undefined for edge0 >= edge1.
			* @param[in] edge0 The lower edge of the Hermite function
			* @param[in] edge1 The upper edge of the Hermite function
			* @param[in] x The source value for interpolation
		*/
		inline static constexpr float smoothstep(const float edge0, const float edge1, const float x)
		{
			long double t = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0);
			return static_cast<float>(t * t * (3.0 - 2.0 * t));
		}

		/*
			Performes a smooth Hermite interpolation between 0 and 1 when edge0 < x < edge1. Undefined for edge0 >= edge1.
			* @param[in] edge0 The lower edge of the Hermite function
			* @param[in] edge1 The upper edge of the Hermite function
			* @param[in] x The source value for interpolation
		*/
		inline static constexpr double smoothstep(const double edge0, const double edge1, const double x)
		{
			long double t = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0);
			return static_cast<double>(t * t * (3.0 - 2.0 * t));
		}

		/*
			Performes a smooth Hermite interpolation between 0 and 1 when edge0 < x < edge1. Undefined for edge0 >= edge1.
			* @param[in] edge0 The lower edge of the Hermite function
			* @param[in] edge1 The upper edge of the Hermite function
			* @param[in] x The source value for interpolation
		*/
		inline static constexpr long double smoothstep(const long double edge0, const long double edge1, const long double x)
		{
			long double t = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0);
			return static_cast<long double>(t * t * (3.0 - 2.0 * t));
		}

		/*
			Converts the given degrees to radians.
			* @param[in] degrees The degrees to convert to radians
		*/
		inline static constexpr float radians(const float degrees)
		{
			return static_cast<float>(degrees * PI / 180.0l);
		}

		/*
			Converts the given degrees to radians.
			* @param[in] degrees The degrees to convert to radians
		*/
		inline static constexpr double radians(const double degrees)
		{
			return static_cast<double>(degrees * PI / 180.0l);
		}

		/*
			Converts the given degrees to radians.
			* @param[in] degrees The degrees to convert to radians
		*/
		inline static constexpr long double radians(const long double degrees)
		{
			return static_cast<long double>(degrees * PI / 180.0l);
		}

		/*
			Converts the given radians to degrees.
			* @param[in] radians The radians to convert to degrees
		*/
		inline static constexpr float degrees(const float radians)
		{
			return static_cast<float>(radians * 180.0l / PI);
		}

		/*
			Converts the given radians to degrees.
			* @param[in] radians The radians to convert to degrees
		*/
		inline static constexpr double degrees(const double radians)
		{
			return static_cast<double>(radians * 180.0l / PI);
		}

		/*
			Converts the given radians to degrees.
			* @param[in] radians The radians to convert to degrees
		*/
		inline static constexpr long double degrees(const long double radians)
		{
			return static_cast<long double>(radians * 180.0l / PI);
		}

		/*
			Computes a Mersenne prime-number from the given exponent.
			* @param[in] exp The exponent of the Mersenne prime number, preferably a positive integer
		*/
		inline static constexpr size_t mersenne(const size_t exp)
		{
			size_t val = 1;
			for (size_t i = 0; i < exp; i++)
				val *= 2;
			return val - 1;
		}

	public:

		/*
			Returns a random value between 0 and 1 with standard floating point precision.
		*/
		inline static float randomf()
		{
			static thread_local Random<float> rng;
			return rng.random();
		}

		/*
			Returns a random value between 0 and 1 with double precision.
		*/
		inline static double random()
		{
			static thread_local Random<double> rng;
			return rng.random();
		}

	public:

		Math() = delete;
		Math(const Math&) = delete;
		Math(Math&&) = delete;

	};

}