#pragma once
#include <org/segames/library/math/math.h>
#include <org/segames/library/util/array_backed.h>
#include <org/segames/library/hashable.h>
#include <org/segames/library/index_out_of_bounds_exception.h>
#include <org/segames/library/invalid_value_exception.h>

#include <iostream>
#include <cstring>

#ifndef SEG_API_VECMATH_ZERO_OFFSET
#define SEG_API_VECMATH_ZERO_OFFSET 0.000000000001l
#endif

namespace org::segames::library
{

	/*
		Base class for a n-dimensional euclidean vector with templates for different data type and dimensions.

		* @author	Philip Rosberg
		* @since	2018-06-21
		* @edited	2022-01-28
	*/
	template<typename T, size_t dim>
	class BasicVector final
	{
	private:

		/*
			Vector data.
		*/
		T m_data[dim];

	public:

		/*
			Standard vector constructor.
		*/
		inline constexpr BasicVector() noexcept :
			m_data()
		{}

		/*
			Vector mem. pointer constructor. Constructs a vector from the values located at the given pointer.
			NOTE! Unsafe (not checked)!
			* @param[in] ptr The pointer to the values
		*/
		template<typename T2>
		inline constexpr explicit BasicVector(const T2* ptr) :
			m_data()
		{
			for (size_t i = 0; i < dimensions(); i++)
				m_data[i] = static_cast<T>(ptr[i]);
		}

		/*
			Vector value constructor. Constructs a vector from the given values.
			* @param[in] items The vector data to place in the vector
		*/
		template<typename... T2>
		inline constexpr BasicVector(const T2... items) noexcept :
			m_data()
		{
			static_assert(sizeof...(items) == dim, "Element count in parameter pack not equal to dimension!");
			T list[sizeof...(items)] = { (T)items... };
			for (size_t i = 0; i < dimensions(); i++)
				m_data[i] = list[i];
		}

		/*
			Cast vector/Copy constructor. A constructor that casts one type of vector size_to another if the element types are castable.
			* @param[in] obj The vector to cast from
		*/
		template<typename T2>
		inline constexpr BasicVector(const BasicVector<T2, dim>& obj) noexcept :
			m_data()
		{
			for (size_t i = 0; i < dimensions(); i++)
				m_data[i] = static_cast<T>(obj[i]);
		}

	public:

		/*
			Returns the number of dimensions of the vector.
		*/
		inline constexpr size_t dimensions() const noexcept
		{
			return dim;
		}

		/*
			Returns the size of the array.
		*/
		inline constexpr size_t size() const noexcept
		{
			return dimensions();
		}

		/*
			Returns the array pointer.
		*/
		inline T* pointer() noexcept
		{
			return reinterpret_cast<T*>(m_data);
		}

		/*
			Returns the (constant) array pointer.
		*/
		inline const T* pointer() const noexcept
		{
			return reinterpret_cast<const T*>(m_data);
		}

		/*
			Returns the point x (first dimension) value.
		*/
		template<int D = dim, std::enable_if_t<(D > 0), int> = 0>
		inline constexpr const T& x() const noexcept
		{
			return m_data[0];
		}

		/*
			Returns the point y (second dimension) value.
		*/
		template<int D = dim, std::enable_if_t<(D > 1), int> = 0>
		inline constexpr const T& y() const noexcept
		{
			return m_data[1];
		}

		/*
			Returns the point z (third dimension) value.
		*/
		template<int D = dim, std::enable_if_t<(D > 2), int> = 0>
		inline constexpr const T& z() const noexcept
		{
			return m_data[2];
		}

		/*
			Returns the vector w (fourth dimension) value.
		*/
		template<int D = dim, std::enable_if_t<(D > 3), int> = 0>
		inline constexpr const T& w() const noexcept
		{
			return m_data[3];
		}

	public:

		/*
			Returns a reference to the element at the given position.
			* @param[in] n The position of the element, composant, to return
		*/
		inline constexpr T& operator[](const size_t n)
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (n < 0 || (size_t)n >= dim)
				throw IndexOutOfBoundsException(n);
#endif
			return m_data[n];
		}

		/*
			Returns a reference to the element at the given position.
			* @param[in] n The position of the element, composant, to return
		*/
		inline constexpr const T& operator[](const size_t n) const
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (n < 0 || (size_t)n >= dim)
				throw IndexOutOfBoundsException(n);
#endif
			return m_data[n];
		}

		/*
			Returns true if the the given vector is equal to this one.
			* @param[in] obj The vector to check
		*/
		template<typename T2>
		inline constexpr bool operator==(const BasicVector<T2, dim>& obj) const noexcept
		{
			for (size_t i = 0; i < dimensions(); i++)
				if (((BasicVector<T, dim>&)obj)[i] != operator[](i))
					return false;
			return true;
		}

		/*
			Returns true if the the given vector is not equal to this one, inverse of operator==.
			* @param[in] obj The vector to check
		*/
		template<typename T2>
		inline constexpr bool operator!=(const BasicVector<T2, dim>& obj) const noexcept
		{
			return !(*this == obj);
		}
				
		/*
			Multiplies the vector by the given scalar.
			* @param[in] scalar The scalar to multiply this vector by
		*/
		template<typename T2>
		inline constexpr BasicVector<T, dim>& operator*=(const T2 scalar) noexcept
		{
			for (size_t i = 0; i < dimensions(); i++)
				m_data[i] = static_cast<T>(operator[](i) * scalar);
			return *this;
		}

		/*
			Adds the given vector to this one.
			* @param[in] obj The vector to add to this one
		*/
		template<typename T2>
		inline constexpr BasicVector<T, dim>& operator+=(const BasicVector<T2, dim>& obj) noexcept
		{
			for (size_t i = 0; i < dimensions(); i++)
				m_data[i] = static_cast<T>(operator[](i) + obj[i]);
			return *this;
		}

		/*
			Subtracts the given vector from this one.
			* @param[in] obj The vector to subtract from this one
		*/
		template<typename T2>
		inline constexpr BasicVector<T, dim>& operator-=(const BasicVector<T2, dim>& obj) noexcept
		{
			for (size_t i = 0; i < dimensions(); i++)
				m_data[i] = static_cast<T>(operator[](i) - obj[i]);
			return *this;
		}

		/*
			Sets the data of this vector to the data in the given one.
			* @param[in] obj The vector to read data from
		*/
		template<typename T2>
		inline constexpr BasicVector<T, dim>& operator=(const BasicVector<T2, dim>& obj) noexcept
		{
			for (size_t i = 0; i < dimensions(); i++)
				m_data[i] = static_cast<T>(obj[i]);
			return *this;
		}

	public:

		/*
			Creates a vector filled with random values in the range [0, 1).
		*/
		inline static BasicVector<T, dim> rand() noexcept
		{
			BasicVector<T, dim> out;
			for (size_t i = 0; i < dim; i++)
				out[i] = static_cast<T>(Math::random());
			return out;
		}

		/*
			A vector filled with zeros.
		*/
		inline static constexpr BasicVector<T, dim> zero() noexcept
		{
			BasicVector<T, dim> out;
			for (size_t i = 0; i < dim; i++)
				out[i] = static_cast<T>(0);
			return out;
		}

	};

	/*
		Writes the string representation of the vector to the given output stream.
		* @param[in] out The output stream to write to
		* @param[in] obj The vector to write the string representation of
	*/
	template<typename T, size_t dim>
	inline std::ostream& operator<<(std::ostream& out, const BasicVector<T, dim>& obj)
	{
		for (size_t i = 0; i < dim - 1; i++)
			out << obj[i] << ' ';
		out << obj[dim - 1];
		return out;
	}

	/*
		Base class for a m x n euclidean matrix with template parameters for data type and size.

		* @author	Philip Rosberg
		* @since	2018-06-21
		* @edited	2022-01-28
	*/
	template<typename T, size_t rows, size_t cols>
	class BasicMatrix final
	{
	private:

		/*
			Auxilliary class to ensure proper bounds checking for double [][] operations.

			* @author	Philip Rosberg
			* @since	2020-06-14
			* @edited	2022-01-28
		*/
		class CheckedRow final
		{
		private:

			/*
				Row pointer.
			*/
			T* m_row;

		public:

			/*
				Creates a checked row instance.
				* @param[in] row The row pointer
			*/
			inline constexpr CheckedRow(T* row) noexcept :
				m_row(row)
			{}

		public:

			/*
				Indexing operator for column lookup.
				* @param[in] j The column index
			*/
			inline constexpr T& operator[](const size_t j)
			{
#ifdef SEG_API_BOUNDS_CHECKING
				if (j < 0 || j >= cols)
					throw IndexOutOfBoundsException(j);
#endif
				return m_row[j];
			}

			/*
				Indexing operator for column lookup.
				* @param[in] j The column index
			*/
			inline constexpr const T& operator[](const size_t j) const
			{
#ifdef SEG_API_BOUNDS_CHECKING
				if (j < 0 || j >= cols)
					throw IndexOutOfBoundsException(j);
#endif
				return m_row[j];
			}

		};

	private:

		/*
			The matrix data.
		*/
		T m_data[rows][cols];

	public:

		/*
			Standard matrix constructor.
		*/
		inline constexpr BasicMatrix() noexcept :
			m_data()
		{}

		/*
			Matrix initializer list constructor. Constructs a matrix form the contents of the given initializer list.
			* @param[in] list The initializer list with data
		*/
		template<typename T2>
		inline BasicMatrix(const std::initializer_list<std::initializer_list<T2>>& list)
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (list.size() != (size_t)this->m())
				throw InvalidValueException("Initializer list size is not correct.");
			if (list.begin()->size() != (size_t)this->n())
				throw InvalidValueException("Initializer list size is not correct.");
#endif
			for (size_t i = 0; i < this->m(); i++)
				for (size_t j = 0; j < this->n(); j++)
					m_data[i][j] = static_cast<T>(list.begin()[i].begin()[j]);
		}

		/*
			Cast matrix constructor. A constructor that casts one type of matrix size_to another if the element types are castable.
			* @param[in] obj The matrix to cast from
		*/
		template<typename T2>
		inline constexpr BasicMatrix(const BasicMatrix<T2, rows, cols>& obj) noexcept
		{
			for (size_t i = 0; i < this->m(); i++)
				for (size_t j = 0; j < this->n(); j++)
					m_data[i][j] = static_cast<T>(obj[i][j]);
		}

	public:

		/*
			Returns the number of rows in the matrix.
		*/
		inline constexpr size_t m() const noexcept
		{
			return rows;
		}

		/*
			Returns the number of columns in the matrix.
		*/
		inline constexpr size_t n() const noexcept
		{
			return cols;
		}

		/*
			Returns a copy of the row of the given index of the given matrix.
			* @param[in] i The row index
			* @param[in] a The matrix
		*/
		inline constexpr BasicVector<T, cols> row(const size_t i) const
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (i < 0 || i >= m())
				throw IndexOutOfBoundsException(i);
#endif
			BasicVector<T, cols> out;
			for (size_t j = 0; j < cols; j++)
				out[j] = operator[](i)[j];
			return out;
		}

		/*
			Returns a copy of the column of the given index of the given matrix.
			* @param[in] j The column index
			* @param[in] a The matrix
		*/
		inline constexpr BasicVector<T, rows> column(const size_t j) const
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (j < 0 || j >= n())
				throw IndexOutOfBoundsException(j);
#endif
			BasicVector<T, rows> out;
			for (size_t i = 0; i < rows; i++)
				out[i] = operator[](i)[j];
			return out;
		}

		/*
			Returns the size of the array.
		*/
		inline constexpr size_t size() const noexcept
		{
			return m() * n();
		}

		/*
			Returns the array pointer.
		*/
		inline T* pointer() noexcept
		{
			return reinterpret_cast<T*>(m_data);
		}

		/*
			Returns the (constant) array pointer.
		*/
		inline const T* pointer() const noexcept
		{
			return reinterpret_cast<const T*>(m_data);
		}

	public:

		/*
			Returns a bounds-checked pointer to the given row of the matrix.
			* @param[in] i The index of the row to returns the pointer to
		*/
		inline constexpr CheckedRow operator[](const size_t i)
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (i < 0 || i >= m())
				throw IndexOutOfBoundsException(i);
#endif
			return CheckedRow(m_data[i]);
		}

		/*
			Returns a bounds-checked pointer to the given row of the matrix.
			* @param[in] i The index of the row to returns the pointer to
		*/
		inline constexpr const CheckedRow operator[](const size_t i) const
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (i < 0 || i >= m())
				throw IndexOutOfBoundsException(i);
#endif
			return CheckedRow(const_cast<T*>(m_data[i]));
		}

		/*
			Returns true if the the given matrix is equal to this one.
			* @param[in] obj The matrix to check
		*/
		template<typename T2>
		inline constexpr bool operator==(const BasicMatrix<T2, rows, cols>& obj) const noexcept
		{
			for (size_t i = 0; i < m(); i++)
				for (size_t j = 0; j < n(); j++)
					if (((BasicMatrix<T, rows, cols>&)obj)[i][j] != operator[](i)[j])
						return false;
			return true;
		}

		/*
			Returns true if the the given matrix is not equal to this one, inverse of operator==.
			* @param[in] obj The matrix to check
		*/
		template<typename T2>
		inline constexpr bool operator!=(const BasicMatrix<T2, rows, cols>& obj) const noexcept
		{
			return !(*this == obj);
		}

		/*
			Multiplies the matrix by the given scalar.
			* @param[in] scalar The scalar of to multiply the matrix with
		*/
		template<typename T2>
		inline constexpr BasicMatrix<T, rows, cols>& operator*=(const T2 scalar) noexcept
		{
			for (size_t i = 0; i < m(); i++)
				for (size_t j = 0; j < n(); j++)
					m_data[i][j] = static_cast<T>(operator[](i)[j] * scalar);
			return *this;
		}

		/*
			Adds the given matrix to this one.
			* @param[in] obj The matrix to add to this one
		*/
		template<typename T2>
		inline constexpr BasicMatrix<T, rows, cols>& operator+=(const BasicMatrix<T2, rows, cols>& obj) noexcept
		{
			for (size_t i = 0; i < m(); i++)
				for (size_t j = 0; j < n(); j++)
					m_data[i][j] = static_cast<T>(operator[](i)[j] + obj[i][j]);
			return *this;
		}

		/*
			Subtracts the given matrix from this one.
			* @param[in] obj The matrix to subtract from this one
		*/
		template<typename T2>
		inline constexpr BasicMatrix<T, rows, cols>& operator-=(const BasicMatrix<T2, rows, cols>& obj) noexcept
		{
			for (size_t i = 0; i < m(); i++)
				for (size_t j = 0; j < n(); j++)
					m_data[i][j] = static_cast<T>(operator[](i)[j] - obj[i][j]);
			return *this;
		}

		/*
			Sets the data of this matrix to the data of the given one.
			* @param[in] obj The matrix to read data from
		*/
		template<typename T2>
		inline constexpr BasicMatrix<T, rows, cols>& operator=(const BasicMatrix<T2, rows, cols>& obj) noexcept
		{
			for (size_t i = 0; i < m(); i++)
				for (size_t j = 0; j < n(); j++)
					m_data[i][j] = static_cast<T>(obj[i][j]);
			return *this;
		}

	public:

		/*
			Creates a matrix filled with random numbers in the range [0, 1).
		*/
		inline static BasicMatrix<T, rows, cols> rand() noexcept
		{
			BasicMatrix<T, rows, cols> out;
			for (size_t i = 0; i < rows; i++)
				for (size_t j = 0; j < cols; j++)
					out[i][j] = static_cast<T>(Math::random());
			return out;
		}

		/*
			A matrix filled with zeros.
		*/
		inline static constexpr BasicMatrix<T, rows, cols> zero() noexcept
		{
			BasicMatrix<T, rows, cols> out;
			for (size_t i = 0; i < rows; i++)
				for (size_t j = 0; j < cols; j++)
					out[i][j] = static_cast<T>(0);
			return out;
		}

		/*
			The identity matrix.
		*/
		inline static constexpr BasicMatrix<T, rows, cols> identity() noexcept
		{
			BasicMatrix<T, rows, cols> out;
			for (size_t i = 0; i < rows; i++)
				for (size_t j = 0; j < cols; j++)
					out[i][j] = static_cast<T>((i == j) ? 1 : 0);
			return out;
		}

		/*
			Creates a rotation martirx for a 2d rotation around the z-axis.
			* @param[in] angle The angle of the rotation in radians
		*/
		template<typename T2>
		inline static BasicMatrix<T, 2, 2> rotation(const T2 angle) noexcept
		{
			const T s = sin(static_cast<double>(angle)), c = cos(static_cast<double>(angle));
			return BasicMatrix<T, 2, 2>({ { c, s }, { -s, c } });
		}

		/*
			Creates a rotation matrix for a 3d rotation of the given angle around the given axis.
			* @param[in] angle The angle of the rotation in radians
			* @param[in] axis The axis to rotate around
		*/
		template<typename T2, typename T3>
		static BasicMatrix<T, 3, 3> rotation(const T2 angle, const BasicVector<T3, 3>& axis)
		{
			T dotVal;
			const double s = sin(static_cast<double>(angle)), c = cos(static_cast<double>(angle));
			const BasicVector<T, 3> zAxis = normalize(axis);

			if (abs(dotVal = dot(zAxis, BasicVector<T, 3>(1, 0, 0))) == 1)
				return BasicMatrix<T, 3, 3>({
					{ 1, 0, 0 },
					{ 0, c, s },
					{ 0, -s, c }
				}) *= dotVal;
			else if (abs(dotVal = dot(zAxis, BasicVector<T, 3>(0, 1, 0))) == 1)
				return BasicMatrix<T, 3, 3>({
					{ c, 0, -s },
					{ 0, 1, 0 },
					{ s, 0, c }
				}) *= dotVal;
			else if (abs(dotVal = dot(zAxis, BasicVector<T, 3>(0, 0, 1))) == 1)
				return BasicMatrix<T, 3, 3>({
					{ c, s, 0 },
					{ -s, c, 0 },
					{ 0, 0, 1 }
				}) *= dotVal;
			else
			{
				const T& ux = zAxis[0];
				const T& uy = zAxis[1];
				const T& uz = zAxis[2];
				const double ic = 1.0 - c;
				return BasicMatrix<T, 3, 3>({
					{c + ux * ux * ic, ux * uy * ic - uz * s, ux * uz * ic + uy * s},
					{uy * ux * ic + uz * s, c + uy * uy * ic, uy * uz * ic - ux * s},
					{uz * ux * ic - uy * s, uz * uy * ic + ux * s, c + uz * uz * ic}
				});

			}

		}

		/*
			Creates an orthogonal projection matrix.
			* @param[in] left The left clipping pane
			* @param[in] right The right clipping pane
			* @param[in] bottom The bottom clipping pane
			* @param[in] top The top clipping pane
			* @param[in] zNear The near clipping pane
			* @param[in] zFar The far clipping pane
		*/
		inline static constexpr BasicMatrix<T, 4, 4> ortho(const T left, const T right, const T bottom, const T top, const T zNear, const T zFar) noexcept
		{
			return BasicMatrix<T, 4, 4>({
				{ (2.0 / (right - left)), 0, 0, 0 },
				{ 0, (2.0 / (top - bottom)), 0, 0 },
				{ 0, 0, (-2.0 / (zFar - zNear)), 0 },
				{ (-(right + left) / (right - left)), (-(top + bottom) / (top - bottom)), (-(zFar + zNear) / (zFar - zNear)), 1 }
			});

		}

		/*
			Creates a standard camera projection matrix.
			* @param[in] fov The field of view in degrees
			* @param[in] aspect The aspect ratio
		*/
		inline static BasicMatrix<T, 4, 4> projection(const T fov, const T aspect, const T zNear, const T zFar) noexcept
		{
			const double top = zNear * tan(Math::radians(fov * 0.5));
			const double bottom = -top;
			const double right = top * aspect;
			const double left = -right;

			return BasicMatrix<T, 4, 4>({
				{ (2.0 * zNear / (right - left)), 0, 0, 0 },
				{ 0, (2.0 * zNear / (top - bottom)), 0, 0 },
				{ ((right + left) / (right - left)), ((top + bottom) / (top - bottom)), (-((zFar + zNear) / (zFar - zNear))), -1 },
				{ 0, 0, (-(2.0 * zFar * zNear / (zFar - zNear))), 0 }
			});

		}

		/**
			Creates a look-at matrix.
			* @param[in] eye the eye position in world space
			* @param[in] target the look at target in world space
			* @param[in] up the upwards vector, standard (0, 1, 0), changes rarely
		*/
		template<typename T2, typename T3, typename T4>
		static BasicMatrix<T, 4, 4> lookAt(const BasicVector<T2, 3>& eye, const BasicVector<T3, 3>& target, const BasicVector<T4, 3>& up) noexcept
		{
			const BasicVector<T2, 3> zAxis = normalize(eye - target);
			const BasicVector<T4, 3> nUp = normalize(up);
			const T4 upZDiff = dot(nUp, zAxis);

			BasicVector<T4, 3> xAxis;
			if (upZDiff == 1)
				xAxis = { 0, 0, -1 };
			else if (upZDiff == -1)
				xAxis = { 0, 0, 1 };
			else
				xAxis = cross(nUp, zAxis);

			const BasicVector<T2, 3> yAxis = cross(zAxis, xAxis);

			return BasicMatrix<T, 4, 4>({
				{ xAxis[0], yAxis[0], zAxis[0], 0 },
				{ xAxis[1], yAxis[1], zAxis[1], 0 },
				{ xAxis[2], yAxis[2], zAxis[2], 0 },
				{ (-dot(xAxis, eye)), (-dot(yAxis, eye)), (-dot(zAxis, eye)), 1 }
			});

		}

	};

	/*
		Writes the string representation of the matrix to the given output stream.
		* @param[in] out The output stream to write to
		* @param[in] obj The matrix to write the string representation of
	*/
	template<typename T, size_t rows, size_t cols>
	inline std::ostream& operator<<(std::ostream& out, const BasicMatrix<T, rows, cols>& obj)
	{
		for (size_t i = 0; i < rows - 1; i++)
		{
			for (size_t j = 0; j < cols - 1; j++)
				out << obj[i][j] << ' ';
			out << obj[i][cols - 1] << '\n';
		}

		for (size_t j = 0; j < cols - 1; j++)
			out << obj[rows - 1][j] << ' ';
		out << obj[rows - 1][cols - 1];
		return out;
	}


	/*
		Calculates the dot product of the two vectors.
		* @param[in] a The first vector
		* @param[in] b The second vector
	*/
	template<typename T, typename T2, size_t dim>
	inline constexpr T dot(const BasicVector<T, dim>& a, const BasicVector<T2, dim>& b) noexcept
	{
		T dotVal = 0;
		for (size_t i = 0; i < dim; i++)
			dotVal = static_cast<T>(dotVal + a[i] * b[i]);
		return dotVal;
	}

	/*
		Calculates the length of the vector.
		* @param[in] a The vector to calculate the length of
	*/
	template<typename T, size_t dim>
	inline T length(const BasicVector<T, dim>& a)
	{
		return static_cast<T>(sqrt(dot(a, a)));
	}

	/*
		Calculates the length of the vector.
		* @param[in] a The vector to calculate the length of
	*/
	template<size_t dim>
	inline float length(const BasicVector<float, dim>& a)
	{
		return sqrtf(dot(a, a));
	}

	/*
		Calculates the length of the vector.
		* @param[in] a The vector to calculate the length of
	*/
	template<size_t dim>
	inline long double length(const BasicVector<long double, dim>& a)
	{
		return sqrtl(dot(a, a));
	}

	/*
		Copies the given vector and normalizes it (the copy).
		* @param[in] a The vector to normalize
	*/
	template<typename T, size_t dim>
	inline BasicVector<T, dim> normalize(const BasicVector<T, dim>& a)
	{
		return a * (1.0 / length(a));
	}

	/*
		Copies and multiplies the vector (the copy) with the given scalar.
		* @param[in] scalar The scalar
		* @param[in] a The vector
	*/
	template<typename T, typename T2, size_t dim>
	inline constexpr BasicVector<T, dim> operator*(const T2 scalar, const BasicVector<T, dim>& a) noexcept
	{
		return BasicVector<T, dim>(a) *= scalar;
	}

	/*
		Copies and multiplies the vector (the copy) with the given scalar.
		* @param[in] a The vector
		* @param[in] scalar The scalar
	*/
	template<typename T, typename T2, size_t dim>
	inline constexpr BasicVector<T, dim> operator*(const BasicVector<T, dim>& a, const T2 scalar) noexcept
	{
		return scalar * a;
	}

	/*
		Adds the two given vectors.
		* @param[in] a The first vector
		* @param[in] b The second vector
	*/
	template<typename T, typename T2, size_t dim>
	inline constexpr BasicVector<T, dim> operator+(const BasicVector<T, dim>& a, const BasicVector<T2, dim>& b) noexcept
	{
		return BasicVector<T, dim>(a) += b;
	}

	/*
		Subtracts the second vector from the first and returns the resulting vector.
		* @param[in] a The first vector
		* @param[in] b The second vector
	*/
	template<typename T, typename T2, size_t dim>
	inline constexpr BasicVector<T, dim> operator-(const BasicVector<T, dim>& a, const BasicVector<T2, dim>& b) noexcept
	{
		return BasicVector<T, dim>(a) -= b;
	}

	/*
		Fills the given vector with the given value.
		* @param[out] a The vector to fill
		* @param[in] value The value to fill the vector with
	*/
	template<typename T, typename T2, size_t dim>
	inline constexpr void fill(BasicVector<T, dim>& a, const T2 value) noexcept
	{
		for (size_t i = 0; i < dim; i++)
			a[i] = static_cast<T>(value);
	}

	/*
		Calculates the cross product of the given vectors.
		* @param[in] a The first vector
		* @param[in] b The second vector
	*/
	template<typename T, typename T2>
	inline constexpr BasicVector<T, 3> cross(const BasicVector<T, 3>& a, const BasicVector<T2, 3>& b) noexcept
	{
		BasicVector<T, 3> out;
		out[0] = a[1] * b[2] - a[2] * b[1];
		out[1] = a[2] * b[0] - a[0] * b[2];
		out[2] = a[0] * b[1] - a[1] * b[0];
		return out;
	}

	/*
		Calculates the determinant of the given matrix.
		* @param[in] a The matrix
	*/
	template<typename T, size_t dim>
	inline constexpr T det(const BasicMatrix<T, dim, dim>& a) noexcept
	{
		if (dim == 1)
			return a[0][0];
		else if (dim == 2)
			return a[0][0] * a[1][1] - a[1][0] * a[0][1];
		else {
			T val = 1;
			BasicMatrix<T, dim, dim> temp = a;

			for (size_t e = 0; e < dim - 1; e++)
				for (size_t r = e + 1; r < dim; r++)
				{
					T multiple = temp[r][e] / temp[e][e];
					for (size_t c = e; c < dim; c++)
						temp[r][c] -= multiple * temp[e][c];
				}
			for (size_t i = 0; i < dim; i++)
				val *= temp[i][i];
			return val;
		}

	}

	/*
		Horizontally concatenates the given matrix and the given vector, in the given order.
		* @param[in] a The matrix
		* @param[in] b The vector
	*/
	template<typename T, typename T2, size_t rows, size_t cols>
	inline constexpr BasicMatrix<T, rows, cols + 1> horzCat(const BasicMatrix<T, rows, cols>& a, const BasicVector<T2, rows>& b) noexcept
	{
		BasicMatrix<T, rows, cols + 1> out;
		for (size_t i = 0; i < rows; i++)
			for (size_t j = 0; j < cols; j++)
				out[i][j] = a[i][j];

		for (size_t i = 0; i < rows; i++)
			out[i][cols] = b[i];
		return out;
	}

	/*
		Horizontally concatenates the given vector and the given matrix, in the given order.
		* @param[in] a The vector
		* @param[in] b The matrix
	*/
	template<typename T, typename T2, size_t rows, size_t cols>
	inline constexpr BasicMatrix<T, rows, cols + 1> horzCat(const BasicVector<T2, rows>& a, const BasicMatrix<T, rows, cols>& b) noexcept
	{
		BasicMatrix<T, rows, cols + 1> out;
		for (size_t i = 0; i < rows; i++)
			out[i][0] = a[i];

		for (size_t i = 0; i < rows; i++)
			for (size_t j = 0; j < cols; j++)
				out[i][j + 1] = b[i][j];
		return out;
	}

	/*
		Vertically concatenates the given matrix and the given vector, in the given order.
		* @param[in] a The matrix
		* @param[in] b The vector
	*/
	template<typename T, typename T2, size_t rows, size_t cols>
	inline constexpr BasicMatrix<T, rows + 1, cols> vertCat(const BasicMatrix<T, rows, cols>& a, const BasicVector<T2, cols>& b) noexcept
	{
		BasicMatrix<T, rows + 1, cols> out;
		for (size_t i = 0; i < rows; i++)
			for (size_t j = 0; j < cols; j++)
				out[i][j] = a[i][j];

		for (size_t j = 0; j < cols; j++)
			out[rows][j] = b[j];
		return out;
	}

	/*
		Vertically concatenates the given vector and the given matrix, in the given order.
		* @param[in] a The vector
		* @param[in] b The matrix
	*/
	template<typename T, typename T2, size_t rows, size_t cols>
	inline constexpr BasicMatrix<T, rows + 1, cols> vertCat(const BasicVector<T2, cols>& a, const BasicMatrix<T, rows, cols>& b) noexcept
	{
		BasicMatrix<T, rows + 1, cols> out;
		for (size_t j = 0; j < cols; j++)
			out[0][j] = a[j];

		for (size_t i = 0; i < rows; i++)
			for (size_t j = 0; j < cols; j++)
				out[i + 1][j] = b[i][j];
		return out;
	}

	/*
		Creates a permutation matrix from the given row permutation index vector.
		* @param[in] p The permutation index vector
	*/
	template<typename T, size_t dim>
	inline constexpr BasicMatrix<T, dim, dim> permutation(const BasicVector<T, dim>& p) noexcept
	{
		BasicMatrix<T, dim, dim> out;
		for (size_t i = 0; i < dim; i++)
			for (size_t j = 0; j < dim; j++)
				out[i][j] = (p[i] == j);
		return out;
	}

	/*
		Creates a row permutation index vector from the given permutation matrix.
		* @param[in] p The permutation matrix
	*/
	template<typename T, size_t dim>
	inline constexpr BasicVector<T, dim> permutation(const BasicMatrix<T, dim, dim>& p) noexcept
	{
		BasicVector<T, dim> out;
		for (size_t i = 0; i < dim; i++)
			out[i] = i;
		return p * out;
	}

	/*
		Creates a row permutated copy of the given vector.
		* @param[in] p The permutation index vector
		* @param[in] a The vector whose elements should be permuted
	*/
	template<typename T, size_t dim>
	inline constexpr BasicVector<T, dim> permutationOf(const BasicVector<T, dim>& p, const BasicVector<T, dim>& a)
	{
		BasicVector<T, dim> out;
		for (size_t i = 0; i < dim; i++)
			out[i] = a[static_cast<size_t>(p[i])];
		return out;
	}

	/*
		Creates a row permutated copy of the given matrix.
		* @param[in] p The permutation index vector
		* @param[in] a The matrix whose elements should be permuted
	*/
	template<typename T, size_t rows, size_t cols>
	inline constexpr BasicMatrix<T, rows, cols> permutationOf(const BasicVector<T, rows>& p, const BasicMatrix<T, rows, cols>& a)
	{
		BasicMatrix<T, rows, cols> out;
		for (size_t i = 0; i < rows; i++)
		{
			const size_t iIndex = static_cast<size_t>(p[i]);
			for (size_t j = 0; j < cols; j++)
				out[i][j] = a[iIndex][j];
		}
		return out;
	}

	/*
		Creates a column permutated copy of the given matrix.
		* @param[in] a The matrix whose elements should be permuted
		* @param[in] p The permutation index vector
	*/
	template<typename T, size_t rows, size_t cols>
	inline constexpr BasicMatrix<T, rows, cols> permutationOf(const BasicMatrix<T, rows, cols>& a, const BasicVector<T, cols>& p)
	{
		BasicMatrix<T, rows, cols> out;
		for (size_t j = 0; j < cols; j++)
		{
			const size_t jIndex = static_cast<size_t>(p[j]);
			for (size_t i = 0; i < rows; i++)
				out[i][j] = a[i][jIndex];
		}
		return out;
	}

	/*
		LU-factorizes the given matrix such that P*A = LU. Returns true on success.
		Supports both double and single memory LU.
		* @param[in] a The input matrix to LU-factorize
		* @param[out] l The lower-triangular matrix output
		* @param[out] u The upper-triangular matrix output
		* @param[out] p The row permutation vector
	*/
	template<typename T, size_t dim>
	bool lu(const BasicMatrix<T, dim, dim>& a, BasicMatrix<T, dim, dim>& l, BasicMatrix<T, dim, dim>& u, BasicVector<T, dim>& p) noexcept
	{
		const bool doubleMem = (&u != &l);
		u = a;
		if (doubleMem)
			l = BasicMatrix<T, dim, dim>::identity();

		for (size_t i = 0; i < dim; i++)
			p[i] = i;

		for (size_t j = 0; j < dim - 1; j++)
		{
			// Find maximum non-zero element
			T maxElem = u[j][j];
			size_t nonZero = -1;
			for (size_t i = j; i < dim; i++)
			{
				if (!(u[i][j] > -maxElem && u[i][j] < maxElem))
				{
					maxElem = u[i][j] < 0 ? -u[i][j] : u[i][j];
					nonZero = i;
				}

			}

			// Terminate on zero column
			if (nonZero == static_cast<size_t>(-1))
				return false;

			// Swap rows
			if (nonZero != j)
			{
				for (size_t jSub = j; jSub < dim; jSub++)
					std::swap(u[nonZero][jSub], u[j][jSub]);
				for (size_t jSub = 0; jSub < j; jSub++)
					std::swap(l[nonZero][jSub], l[j][jSub]);
				std::swap(p[nonZero], p[j]);
			}

			// Apply gauss matrix
			for (size_t i = j + 1; i < dim; i++)
				l[i][j] = u[i][j] / u[j][j];

			for (size_t jSub = j + 1; jSub < dim; jSub++)
				for (size_t i = j + 1; i < dim; i++)
					u[i][jSub] = u[i][jSub] - l[i][j] * u[j][jSub];

			if (doubleMem)
				for (size_t i = j + 1; i < dim; i++)
					u[i][j] = 0;
		}

		return true;
	}

	/*
		Solves the given pre-permuted right hand side using the given LU-factorization.
		Supports both double and single memory LU.
		* @param[in] rhsPerm Pre-permuted right hand side
		* @param[in] l The lower triangular matrix
		* @param[in] u The upper triangular matrix
	*/
	template<typename T, size_t dim>
	inline BasicVector<T, dim> luSolve(const BasicVector<T, dim>& rhsPerm, const BasicMatrix<T, dim, dim>& l, const BasicMatrix<T, dim, dim>& u) noexcept
	{
		BasicVector<T, dim> out = rhsPerm;

		for (size_t j = 0; j < dim - 1; j++)
			for (size_t i = j + 1; i < dim; i++)
				out[i] -= l[i][j] * out[j];

		for (size_t jInv = 1; jInv <= dim; jInv++)
		{
			const size_t j = dim - jInv;
			out[j] /= u[j][j];
			for (size_t i = 0; i < j; i++)
				out[i] -= u[i][j] * out[j];
		}

		return out;
	}

	/*
		Solves the given right hand side using the given LU-factorization.
		Supports both double and single memory LU.
		* @param[in] rhsPerm Pre-permuted right hand side
		* @param[in] l The lower triangular matrix
		* @param[in] u The upper triangular matrix
		* @param[in] p The row permutation index vector
	*/
	template<typename T, size_t dim>
	inline BasicVector<T, dim> luSolve(const BasicVector<T, dim>& rhs, const BasicMatrix<T, dim, dim>& l, const BasicMatrix<T, dim, dim>& u, const BasicVector<T, dim>& p)
	{
		return luSolve(permutationOf(p, rhs), l, u);
	}

	/*
		Solves the given linear system using LU-factorization.
		Returns the zero vector on failure.
		* @param[in] a System matrix
		* @param[in] rhs System right hand side
	*/
	template<typename T, size_t dim>
	inline BasicVector<T, dim> luSolve(const BasicMatrix<T, dim, dim>& a, const BasicVector<T, dim>& rhs)
	{
		BasicVector<T, dim> p;
		BasicMatrix<T, dim, dim> luMat;
		if (lu(a, luMat, luMat, p))
			return luSolve(rhs, luMat, luMat, p);
		else
			return BasicVector<T, dim>::zero();
	}

	/*
		Solves the given pre-permuted right hand side using the given LU-factorization.
		Supports both double and single memory LU.
		* @param[in] rhsPerm Pre-permuted right hand side
		* @param[in] l The lower triangular matrix
		* @param[in] u The upper triangular matrix
	*/
	template<typename T, size_t rows, size_t cols>
	inline BasicMatrix<T, rows, cols> luSolve(const BasicMatrix<T, rows, cols>& rhsPerm, const BasicMatrix<T, rows, rows>& l, const BasicMatrix<T, rows, rows>& u) noexcept
	{
		BasicMatrix<T, rows, cols> out = rhsPerm;

		for (size_t j = 0; j < rows - 1; j++)
			for (size_t i = j + 1; i < rows; i++)
				for (size_t j2 = 0; j2 < cols; j2++)
					out[i][j2] -= l[i][j] * out[j][j2];

		for (size_t jInv = 1; jInv <= rows; jInv++)
		{
			const size_t j = rows - jInv;
			for (size_t j2 = 0; j2 < cols; j2++)
				out[j][j2] /= u[j][j];

			for (size_t i = 0; i < j; i++)
				for (size_t j2 = 0; j2 < cols; j2++)
					out[i][j2] -= u[i][j] * out[j][j2];
		}

		return out;
	}

	/*
		Solves the given right hand side using the given LU-factorization.
		Supports both double and single memory LU.
		* @param[in] rhsPerm Pre-permuted right hand side
		* @param[in] l The lower triangular matrix
		* @param[in] u The upper triangular matrix
		* @param[in] p The row permutation index vector
	*/
	template<typename T, size_t rows, size_t cols>
	inline BasicMatrix<T, rows, cols> luSolve(const BasicMatrix<T, rows, cols>& rhs, const BasicMatrix<T, rows, rows>& l, const BasicMatrix<T, rows, rows>& u, const BasicVector<T, rows>& p)
	{
		return luSolve(permutationOf(p, rhs), l, u);
	}

	/*
		Solves the given linear system using LU-factorization.
		Returns a zero matrix on failure.
		* @param[in] a System matrix
		* @param[in] rhs System right hand side
	*/
	template<typename T, size_t rows, size_t cols>
	inline BasicMatrix<T, rows, cols> luSolve(const BasicMatrix<T, rows, rows>& a, const BasicMatrix<T, rows, cols>& rhs)
	{
		BasicVector<T, rows> p;
		BasicMatrix<T, rows, rows> luMat;
		if (lu(a, luMat, luMat, p))
			return luSolve(rhs, luMat, luMat, p);
		else
			return BasicMatrix<T, rows, cols>::zero();
	}

	/*
		Calculates and retruns the inverse of the given matrix.
		* @param[in] a The matrix to invert
	*/
	template<typename T, size_t dim>
	inline BasicMatrix<T, dim, dim> inv(const BasicMatrix<T, dim, dim>& a)
	{
		return luSolve(a, BasicMatrix<T, dim, dim>::identity());
	}

	/*
		Produces a reduced row-echelon version of the given matrix.
		* @param[in] a The matrix to produce a reduced form of
	*/
	template<typename T, size_t rows, size_t cols>
	inline BasicMatrix<T, rows, cols> rref(const BasicMatrix<T, rows, cols>& a)
	{
		const size_t pivots = Math::min(rows, cols);
		size_t rIndex[rows];
		for (size_t i = 0; i < rows; i++)
			rIndex[i] = i;

		BasicMatrix<T, rows, cols> cloned = a;

		size_t cMrk = 0;
		for (size_t p = 0; p < pivots && cMrk < cols; p++, cMrk++)
		{
			for (size_t j = cMrk; j < cols; j++)
				for (size_t i = p; i < rows; i++)
					if (abs(cloned[rIndex[i]][j]) > SEG_API_VECMATH_ZERO_OFFSET)
					{
						std::swap(rIndex[i], rIndex[p]);
						cMrk = j;
						j = cols;
						break;
					}

			const T pivotValInv = (T)(1.0l / cloned[rIndex[p]][cMrk]);

			for (size_t j = cMrk; j < cols; j++)
				cloned[rIndex[p]][j] *= pivotValInv;

			for (size_t i = 0; i < rows; i++)
				if (i != p)
				{
					const T rowVal = (T)cloned[rIndex[i]][cMrk];
					for (size_t j = cMrk; j < cols; j++)
						cloned[rIndex[i]][j] -= cloned[rIndex[p]][j] * rowVal;
				}

		}

		BasicMatrix<T, rows, cols> out;
		for (size_t i = 0; i < rows; i++)
			for (size_t j = 0; j < cols; j++)
					out[i][j] = cloned[rIndex[i]][j];
		return out;
	}

	/*
		Creates and returns the transpose of the given matrix.
		* @param[in] a The matrix to transpose
	*/
	template<typename T, size_t rows, size_t cols>
	inline constexpr BasicMatrix<T, cols, rows> transpose(const BasicMatrix<T, rows, cols>& a) noexcept
	{
		BasicMatrix<T, cols, rows> out;
		for (size_t i = 0; i < rows; i++)
			for (size_t j = 0; j < cols; j++)
				out[j][i] = a[i][j];
		return out;
	}

	/*
		Adds the given matrices.
		* @param[in] a The first matrix
		* @param[in] b The second matrix
	*/
	template<typename T, typename T2, size_t rows, size_t cols>
	inline constexpr BasicMatrix<T, rows, cols> operator+(const BasicMatrix<T, rows, cols>& a, const BasicMatrix<T2, rows, cols>& b) noexcept
	{
		return BasicMatrix<T, rows, cols>(a) += b;
	}

	/*
		Subtracts second matrix from the first.
		* @param[in] a The first matrix
		* @param[in] b The second matrix
	*/
	template<typename T, typename T2, size_t rows, size_t cols>
	inline constexpr BasicMatrix<T, rows, cols> operator-(const BasicMatrix<T, rows, cols>& a, const BasicMatrix<T2, rows, cols>& b) noexcept
	{
		return BasicMatrix<T, rows, cols>(a) -= b;
	}

	/*
		Multiplies the given matrix by the given scalar.
		* @param[in] scalar The scalar
		* @param[in] a The matrix
	*/
	template<typename T, typename T2, size_t rows, size_t cols>
	inline constexpr BasicMatrix<T, rows, cols> operator*(const T2 scalar, const BasicMatrix<T, rows, cols>& a) noexcept
	{
		return BasicMatrix<T, rows, cols>(a) *= scalar;
	}

	/*
		Multiplies the given matrix by the given scalar.
		* @param[in] a The matrix
		* @param[in] scalar The scalar
	*/
	template<typename T, typename T2, size_t rows, size_t cols>
	inline constexpr BasicMatrix<T, rows, cols> operator*(const BasicMatrix<T, rows, cols>& a, const T2 scalar) noexcept
	{
		return scalar * a;
	}

	/*
		Multiplies the given matrices.
		* @param[in] a The first matrix
		* @param[in] b The second matrix
	*/
	template<typename T, typename T2, size_t rows, size_t colsA, size_t colsB>
	inline constexpr BasicMatrix<T, rows, colsB> operator*(const BasicMatrix<T, rows, colsA>& a, const BasicMatrix<T2, colsA, colsB>& b) noexcept
	{
		BasicMatrix<T, rows, colsB> out;
		for (size_t i = 0; i < rows; i++)
			for (size_t j = 0; j < colsB; j++)
				out[i][j] = dot(a.row(i), b.column(j));
		return out;
	}

	/*
		Multiplies the given (row) vector by the given matrix.
		* @param[in] a The (row) vector
		* @param[in] b The matrix
	*/
	template<typename T, typename T2, size_t rows, size_t cols>
	inline constexpr BasicVector<T, cols> operator*(const BasicVector<T, rows>& a, const BasicMatrix<T2, rows, cols>& b) noexcept
	{
		BasicVector<T, cols> out;
		for (size_t i = 0; i < cols; i++)
			out[i] = dot(a, b.column(i));
		return out;
	}

	/*
		Multiplies the given matrix by the given (column) vector.
		* @param[in] a The matrix
		* @param[in] b The (column) vector
	*/
	template<typename T, typename T2, size_t rows, size_t cols>
	inline constexpr BasicVector<T2, rows> operator*(const BasicMatrix<T, rows, cols>& a, const BasicVector<T2, cols>& b) noexcept
	{
		BasicVector<T, rows> out;
		for (size_t i = 0; i < rows; i++)
			out[i] = dot(b, a.row(i));
		return out;
	}

	/*
		Fills the matrix with the given value.
		* @param[in] a The matrix
		* @param[in] value The value
	*/
	template<typename T, typename T2, size_t rows, size_t cols>
	inline constexpr void fill(BasicMatrix<T, rows, cols>& a, const T2 value) noexcept
	{
		for (size_t i = 0; i < rows; i++)
			for (size_t j = 0; j < cols; j++)
				a[i][j] = static_cast<T>(value);
	}

#ifndef SEG_API_VECMATH_NO_TYPEDEFS

	template<size_t dim> using Vector = BasicVector<double, dim>;
	template<size_t dim> using Vectorf = BasicVector<float, dim>;
	template<size_t dim> using Vectorl = BasicVector<long double, dim>;
	using vec2 = Vector<2>;
	using vec3 = Vector<3>;
	using vec4 = Vector<4>;
	using vec2f = Vectorf<2>;
	using vec3f = Vectorf<3>;
	using vec4f = Vectorf<4>;
	using vec2l = Vectorl<2>;
	using vec3l = Vectorl<3>;
	using vec4l = Vectorl<4>;

	template<size_t rows, size_t cols> using Matrix = BasicMatrix<double, rows, cols>;
	template<size_t rows, size_t cols> using Matrixf = BasicMatrix<float, rows, cols>;
	template<size_t rows, size_t cols> using Matrixl = BasicMatrix<long double, rows, cols>;
	using mat2 = Matrix<2, 2>;
	using mat3 = Matrix<3, 3>;
	using mat4 = Matrix<4, 4>;
	using mat2f = Matrixf<2, 2>;
	using mat3f = Matrixf<3, 3>;
	using mat4f = Matrixf<4, 4>;
	using mat2l = Matrixl<2, 2>;
	using mat3l = Matrixl<3, 3>;
	using mat4l = Matrixl<4, 4>;

#endif

}
