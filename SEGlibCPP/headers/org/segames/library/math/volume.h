#pragma once
#include <org/segames/library/math/dimension.h>
#include <org/segames/library/math/point.h>

namespace org::segames::library
{

	/*
		A generic class destribing a n-dimensional volume located somewhere in space.

		* @author	Philip Rosberg
		* @since	2021-03-19
		* @edited	2021-03-19
	*/
	template<typename T, int dim>
	class GeneralVolume :
		public Hashable,
		public StringVisualizable
	{
	protected:

		/*
			The position of the volume.
		*/
		Point<T, dim> m_position;

		/*
			The size of the volume.
		*/
		Dimension<T, dim> m_dimensions;

	public:

		/*
			Creates a volume of no relevant data.
		*/
		inline constexpr GeneralVolume() :
			m_position(),
			m_dimensions()
		{}

		/*
			Creates a volume and fills it with the given value
			* @param[in] value The value to fill the volume with
		*/
		inline constexpr GeneralVolume(const T value) :
			m_position(value),
			m_dimensions(value)
		{}

		/*
			Creates a volume with the given values.
			Position first, dimension after.
			* @param[in] item The first volume value
			* @param[in] items The volume values
		*/
		template<typename... T2>
		inline constexpr GeneralVolume(const T item, const T2... items) :
			m_position(),
			m_dimensions()
		{
			static_assert(1 + sizeof...(items) == 2*dim, "Element count in parameter pack not equal to dimension!");
			T list[1 + sizeof...(items)] = { item, (T)items... };
			for (int i = 0; i < dim; i++)
			{
				m_position[i] = list[i];
				m_dimensions[i] = list[dim + i];
			}

		}

		/*
			Copy-constructor.
		*/
		template<typename T2>
		inline constexpr GeneralVolume(const GeneralVolume& obj) :
			m_position(obj.m_position),
			m_dimensions(obj.m_dimensions)
		{}

	public:

		/*
			Returns the position of the volume.
		*/
		inline Point<T, dim>& position()
		{
			return m_position;
		}

		/*
			Returns the position of the volume.
		*/
		inline const Point<T, dim>& position() const
		{
			return m_position;
		}

		/*
			Returns the dimensions of the volume.
		*/
		inline Dimension<T, dim>& dimensions()
		{
			return m_dimensions;
		}

		/*
			Returns the dimensions of the volume.
		*/
		inline const Dimension<T, dim>& dimensions() const
		{
			return m_dimensions;
		}

		/*
			Returns a hash-code for the object.
		*/
		inline virtual size_t hashCode() const override
		{
			return Hashable::hashCode(&m_position, 2 * dim * sizeof(T));
		}

		/*
			Returns a string representation of the point.
		*/
		inline virtual std::string toString() const override
		{
			return "Volume(" + m_position.toString() + ", " + m_dimensions.toString() + ")";
		}

	public:

		/*
			Returns true if the given volume is equal to this volume.
			* @param[in] obj The volume to check
		*/
		inline bool operator==(const GeneralVolume<T, dim>& obj) const noexcept
		{
			return (m_position == obj.m_position) && (m_dimensions == obj.m_dimensions);
		}

		/*
			Returns true if the given volume is not equal to this volume.
			* @param[in] obj The volume to check
		*/
		inline bool operator!=(const GeneralVolume<T, dim>& obj) const noexcept
		{
			return !operator==(obj);
		}

		/*
			Adds the given position to this volume.
			* @param[in] pos The position to add to this volume
		*/
		inline GeneralVolume<T, dim>& operator+=(const Point<T, dim>& pos) noexcept
		{
			m_position -= pos;
			return *this;
		}

		/*
			Subtracts the given position from this one.
			* @param[in] pos The position to subtract from this volume
		*/
		inline GeneralVolume<T, dim>& operator-=(const Point<T, dim>& pos) noexcept
		{
			m_position -= pos;
			return *this;
		}

		/*
			Adds the given dimensions to this volume.
			* @param[in] size The dimensions to add to this volume
		*/
		inline GeneralVolume<T, dim>& operator+=(const Dimension<T, dim>& size) noexcept
		{
			m_dimensions += size;
			return *this;
		}

		/*
			Subtracts the given dimensions from this one.
			* @param[in] size The dimensions to subtract from this volume
		*/
		inline GeneralVolume<T, dim>& operator-=(const Dimension<T, dim>& size) noexcept
		{
			m_dimensions += size;
			return *this;
		}

	public:

		/*
			Returns the zero volume.
		*/
		inline static const GeneralVolume<T, dim>& zero()
		{
			static GeneralVolume<T, dim> out = GeneralVolume<T, dim>(0);
			return out;
		}

	};

	/*
		Adds the position to the volume.
		* @param[in] vol The volume
		* @param[in] pos The position
	*/
	template<typename T, int dim>
	inline GeneralVolume<T, dim> operator+(const GeneralVolume<T, dim>& vol, const Point<T, dim>& pos) noexcept
	{
		return GeneralVolume<T, dim>(vol) + pos;
	}

	/*
		Subtracts the position from the volume.
		* @param[in] vol The volume
		* @param[in] pos The position
	*/
	template<typename T, int dim>
	inline GeneralVolume<T, dim> operator-(const GeneralVolume<T, dim>& vol, const Point<T, dim>& pos) noexcept
	{
		return GeneralVolume<T, dim>(vol) - pos;
	}

	/*
		Adds the dimensions to the volume.
		* @param[in] vol The volume
		* @param[in] size The dimensions
	*/
	template<typename T, int dim>
	inline GeneralVolume<T, dim> operator+(const GeneralVolume<T, dim>& vol, const Dimension<T, dim>& size) noexcept
	{
		return GeneralVolume<T, dim>(vol) + size;
	}

	/*
		Subtracts the dimensions from the volume.
		* @param[in] vol The volume
		* @param[in] size The dimensions
	*/
	template<typename T, int dim>
	inline GeneralVolume<T, dim> operator-(const GeneralVolume<T, dim>& vol, const Dimension<T, dim>& size) noexcept
	{
		return GeneralVolume<T, dim>(vol) - size;
	}

	template<typename T> using Rectangle = GeneralVolume<T, 2>;
	using Rectanglei = Rectangle<int>;
	using Rectanglef = Rectangle<float>;
	using Rectangled = Rectangle<double>;

	template<typename T> using Volume = GeneralVolume<T, 3>;
	using Volumei = Volume<int>;
	using Volumef = Volume<float>;
	using Volumed = Volume<double>;

}