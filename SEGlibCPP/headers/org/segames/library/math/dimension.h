#pragma once
#include <org/segames/library/util/array_backed.h>
#include <org/segames/library/hashable.h>
#include <org/segames/library/index_out_of_bounds_exception.h>
#include <org/segames/library/invalid_value_exception.h>
#include <org/segames/library/string_visualizable.h>

namespace org::segames::library
{

	/*
		A generic class destribing a n-dimensional hypercube.

		* @author	Philip Rosberg
		* @since	2019-06-13
		* @edited	2020-06-29
		*			2021-03-19
	*/
	template<typename T, int dim>
	class Dimension :
		public ArrayBacked<T, int>,
		public Hashable,
		public StringVisualizable
	{
	protected:

		/*
			The dimension values.
		*/
		T m_data[dim];
				
	public:

		/*
			Creates a dimension with no relevant data.
		*/
		inline constexpr Dimension() :
			m_data()
		{}

		/*
			Creates a dimension and fills it with the given value
			* @param[in] value The value to fill the dimension with
		*/
		inline constexpr Dimension(const T value) :
			m_data()
		{
			for (int i = 0; i < dim; i++)
				m_data[i] = value;
		}

		/*
			Creates a dimension with the given values.
			* @param[in] item The first dimensional size
			* @param[in] items The dimensional sizes
		*/
		template<typename... T2>
		inline constexpr Dimension(const T item, const T2... items) :
			m_data()
		{
			static_assert(1 + sizeof...(items) == dim, "Element count in parameter pack not equal to dimension!");
			T list[1 + sizeof...(items)] = { item, (T)items... };
			for (int i = 0; i < dim; i++)
				m_data[i] = list[i];
		}
				
		/*
			Copy/conversion constructor.
			* @param[in] obj The dimension object to copy.
		*/
		template<typename T2>
		inline constexpr Dimension(const Dimension<T2, dim>& obj) :
			m_data()
		{
			for (int i = 0; i < dim; i++)
				m_data[i] = (T)obj[i];
		}

	public:

		/*
			Returns the size of the array ie. the number of dimensions of the dimension.
		*/
		inline virtual int size() const override
		{
			return dim;
		}

		/*
			Returns the array pointer.
		*/
		inline virtual T* pointer() override
		{
			return m_data;
		}

		/*
			Returns the (constant) array pointer.
		*/
		inline virtual const T* pointer() const override
		{
			return m_data;
		}

		/*
			Returns the width (first dimension) value.
		*/
		template<int D = dim, std::enable_if_t<(D > 0), int> = 0>
		inline const T& width() const
		{
			return m_data[0];
		}

		/*
			Returns the height (second dimension) value.
		*/
		template<int D = dim, std::enable_if_t<(D > 1), int> = 0>
		inline const T& height() const
		{
			return m_data[1];
		}

		/*
			Returns the depth (third dimension) value.
		*/
		template<int D = dim, std::enable_if_t<(D > 2), int> = 0>
		inline const T& depth() const
		{
			return m_data[2];
		}

		/*
			Sets the width (first dimension) value.
			* @param[in] value The value to set the width to
		*/
		template<int D = dim, std::enable_if_t<(D > 0), int> = 0>
		inline Dimension<T, dim>& setWidth(const T& value)
		{
			m_data[0] = value;
			return *this;
		}

		/*
			Sets the height (second dimension) value.
			* @param[in] value The value to set the height to
		*/
		template<int D = dim, std::enable_if_t<(D > 1), int> = 0>
		inline Dimension<T, dim>& setHeight(const T& value)
		{
			m_data[1] = value;
			return *this;
		}

		/*
			Sets the depth (third dimension) value.
			* @param[in] value The value to set the depth to
		*/
		template<int D = dim, std::enable_if_t<(D > 2), int> = 0>
		inline Dimension<T, dim>& setDepth(const T& value)
		{
			m_data[2] = value;
			return *this;
		}

		/*
			Returns a hash code of the object.
		*/
		inline virtual size_t hashCode() const override
		{
			return Hashable::hashCode(m_data, dim * sizeof(T));
		}

		/*
			Returns a string representation of the dimension.
		*/
		inline virtual std::string toString() const override
		{
			std::string out = "Dimension(";
			for (int i = 0; i < dim-1; i++)
				(out += std::to_string(m_data[i])) += ", ";
			(out += std::to_string(m_data[dim - 1])) += ")";
			return out;
		}

	public:

		/*
			Returns a reference to the element at the given position
			* @param[in] n The position of the element to return
		*/
		inline T& operator[](const int n)
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (n < 0 || n >= dim)
				throw IndexOutOfBoundsException(n);
#endif
			return m_data[n];
		}

		/*
			Returns a reference to the element at the given position
			* @param[in] n The position of the element to return
		*/
		inline const T& operator[](const int n) const
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (n < 0 || n >= dim)
				throw IndexOutOfBoundsException(n);
#endif
			return m_data[n];
		}

		/*
			Returns true if the given dimension is equal to this dimension.
			* @param[in] obj The dimension to check
		*/
		inline bool operator==(const Dimension<T, dim>& obj) const noexcept
		{
			for (int i = 0; i < dim; i++)
				if (obj.m_data[i] != m_data[i])
					return false;
			return true;
		}

		/*
			Returns true if the given dimension is not equal to this dimension.
			* @param[in] obj The dimension to check
		*/
		inline bool operator!=(const Dimension<T, dim>& obj) const noexcept
		{
			return !operator==(obj);
		}

		/*
			Adds the given dimension to this one.
			* @param[in] obj The dimension to add to this one
		*/
		inline Dimension<T, dim>& operator+=(const Dimension<T, dim>& obj) noexcept
		{
			for (int i = 0; i < dim; i++)
				m_data[i] += obj.m_data[i];
			return *this;
		}

		/*
			Subtracts the given dimension from this one.
			* @param[in] obj The dimension to subtract from this one
		*/
		inline Dimension<T, dim>& operator-=(const Dimension<T, dim>& obj) noexcept
		{
			for (int i = 0; i < dim; i++)
				m_data[i] -= obj.m_data[i];
			return *this;
		}

	public:

		/*
			Returns the zero/size-less dimension.
		*/
		inline static const Dimension<T, dim>& zero()
		{
			static Dimension<T, dim> out = Dimension<T, dim>(0);
			return out;
		}

	};

	/*
		Adds the two dimensions.
		* @param[in] a The first dimension
		* @param[in] b The second dimension
	*/
	template<typename T, int dim>
	inline Dimension<T, dim> operator+(const Dimension<T, dim>& a, const Dimension<T, dim>& b) noexcept
	{
		return Dimension<T, dim>(a) += b;
	}

	/*
		Subtracts the two dimensions.
		* @param[in] a The first dimension
		* @param[in] b The second dimension
	*/
	template<typename T, int dim>
	inline Dimension<T, dim> operator-(const Dimension<T, dim>& a, const Dimension<T, dim>& b) noexcept
	{
		return Dimension<T, dim>(a) -= b;
	}

	template<typename T> using Dimension1 = Dimension<T, 1>;
	using Dimension1i = Dimension1<int>;
	using Dimension1f = Dimension1<float>;
	using Dimension1d = Dimension1<double>;

	template<typename T> using Dimension2 = Dimension<T, 2>;
	using Dimension2i = Dimension2<int>;
	using Dimension2f = Dimension2<float>;
	using Dimension2d = Dimension2<double>;

	template<typename T> using Dimension3 = Dimension<T, 3>;
	using Dimension3i = Dimension3<int>;
	using Dimension3f = Dimension3<float>;
	using Dimension3d = Dimension3<double>;

}