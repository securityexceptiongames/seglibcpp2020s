#pragma once
#include <org/segames/library/util/array_backed.h>
#include <org/segames/library/hashable.h>
#include <org/segames/library/index_out_of_bounds_exception.h>
#include <org/segames/library/invalid_value_exception.h>
#include <org/segames/library/string_visualizable.h>

namespace org::segames::library
{

	/*
		A generic class destribing a n-dimensional point.

		* @author	Philip Rosberg
		* @since	2019-06-23
		* @edited	2020-06-29
		*			2021-03-19
	*/
	template<typename T, int dim>
	class Point :
		public ArrayBacked<T, int>,
		public Hashable,
		public StringVisualizable
	{
	protected:

		/*
			The point values.
		*/
		T m_data[dim];

	public:

		/*
			Creates a zeroed point.
		*/
		inline constexpr Point() :
			m_data()
		{}

		/*
			Creates a point and fills it with the given value
			* @param[in] value The value to fill the point with
		*/
		inline constexpr Point(const T value) :
			m_data()
		{
			for (int i = 0; i < dim; i++)
				m_data[i] = value;
		}

		/*
			Creates a point with the given values.
			* @param[in] item The first point value
			* @param[in] items The point values
		*/
		template<typename... T2>
		inline constexpr Point(const T item, const T2... items) :
			m_data()
		{
			static_assert(1 + sizeof...(items) == dim, "Element count in parameter pack not equal to dimension!");
			T list[1 + sizeof...(items)] = { item, (T)items... };
			for (int i = 0; i < dim; i++)
				m_data[i] = list[i];
		}

		/*
			Copy/conversion constructor.
			* @param[in] obj The point object to copy.
		*/
		template<typename T2>
		inline constexpr Point(const Point<T2, dim>& obj) :
			m_data()
		{
			for (int i = 0; i < dim; i++)
				m_data[i] = (T)obj[i];
		}

	public:

		/*
			Returns the size of the array ie. the number of dimensions of the point.
		*/
		inline virtual int size() const override
		{
			return dim;
		}

		/*
			Returns the array pointer.
		*/
		inline virtual T* pointer() override
		{
			return m_data;
		}

		/*
			Returns the (constant) array pointer.
		*/
		inline virtual const T* pointer() const override
		{
			return m_data;
		}

		/*
			Returns the point X (first dimension) value.
		*/
		template<int D = dim, std::enable_if_t<(D > 0), int> = 0>
		inline const T& x() const
		{
			return m_data[0];
		}

		/*
			Returns the point Y (second dimension) value.
		*/
		template<int D = dim, std::enable_if_t<(D > 1), int> = 0>
		inline const T& y() const
		{
			return m_data[1];
		}

		/*
			Returns the point Z (third dimension) value.
		*/
		template<int D = dim, std::enable_if_t<(D > 2), int> = 0>
		inline const T& z() const
		{
			return m_data[2];
		}

		/*
			Sets the X (first dimension) value.
			* @param[in] value The value to set the X to
		*/
		template<int D = dim, std::enable_if_t<(D > 0), int> = 0>
		inline Point<T, dim>& setX(const T& value)
		{
			m_data[0] = value;
			return *this;
		}

		/*
			Sets the Y (second dimension) value.
			* @param[in] value The value to set the Y to
		*/
		template<int D = dim, std::enable_if_t<(D > 1), int> = 0>
		inline Point<T, dim>& setY(const T& value)
		{
			m_data[1] = value;
			return *this;
		}

		/*
			Sets the Z (third dimension) value.
			* @param[in] value The value to set the Z to
		*/
		template<int D = dim, std::enable_if_t<(D > 2), int> = 0>
		inline Point<T, dim>& setZ(const T& value)
		{
			m_data[2] = value;
			return *this;
		}

		/*
			Returns a hash-code for the object.
		*/
		inline virtual size_t hashCode() const override
		{
			return Hashable::hashCode(m_data, dim * sizeof(T));
		}

		/*
			Returns a string representation of the point.
		*/
		inline virtual std::string toString() const override
		{
			std::string out = "Point(";
			for (int i = 0; i < dim - 1; i++)
				(out += std::to_string(m_data[i])) += ", ";
			(out += std::to_string(m_data[dim - 1])) += ")";
			return out;
		}

	public:
				
		/*
			Returns a reference to the element at the given position
			* @param[in] n The position of the element to return
		*/
		inline T& operator[](const int n)
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (n < 0 || n >= dim)
				throw IndexOutOfBoundsException(n);
#endif
			return m_data[n];
		}

		/*
			Returns a reference to the element at the given position
			* @param[in] n The position of the element to return
		*/
		inline const T& operator[](const int n) const
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (n < 0 || n >= dim)
				throw IndexOutOfBoundsException(n);
#endif
			return m_data[n];
		}

		/*
			Returns true if the given point is equal to this point.
			* @param[in] obj The point to check
		*/
		inline bool operator==(const Point<T, dim>& obj) const noexcept
		{
			for (int i = 0; i < dim; i++)
				if (obj.m_data[i] != m_data[i])
					return false;
			return true;
		}

		/*
			Returns true if the given point is not equal to this point.
			* @param[in] obj The point to check
		*/
		inline bool operator!=(const Point<T, dim>& obj) const noexcept
		{
			return !operator==(obj);
		}

		/*
			Adds the given point to this one.
			* @param[in] obj The point to add to this one
		*/
		inline Point<T, dim>& operator+=(const Point<T, dim>& obj) noexcept
		{
			for (int i = 0; i < dim; i++)
				m_data[i] += obj.m_data[i];
			return *this;
		}

		/*
			Subtracts the given point from this one.
			* @param[in] obj The point to subtract from this one
		*/
		inline Point<T, dim>& operator-=(const Point<T, dim>& obj) noexcept
		{
			for (int i = 0; i < dim; i++)
				m_data[i] -= obj.m_data[i];
			return *this;
		}

	public:

		/*
			Returns the zero/origin point.
		*/
		inline static const Point<T, dim>& zero()
		{
			static Point<T, dim> out = Point<T, dim>(0);
			return out;
		}

	};

	/*
		Adds the two points.
		* @param[in] a The first point
		* @param[in] b The second point
	*/
	template<typename T, int dim>
	inline Point<T, dim> operator+(const Point<T, dim>& a, const Point<T, dim>& b) noexcept
	{
		return Point<T, dim>(a) += b;
	}

	/*
		Subtracts the two points.
		* @param[in] a The first point
		* @param[in] b The second point
	*/
	template<typename T, int dim>
	inline Point<T, dim> operator-(const Point<T, dim>& a, const Point<T, dim>& b) noexcept
	{
		return Point<T, dim>(a) -= b;
	}

	template<typename T> using Point1 = Point<T, 1>;
	using Point1i = Point1<int>;
	using Point1f = Point1<float>;
	using Point1d = Point1<double>;
			
	template<typename T> using Point2 = Point<T, 2>;
	using Point2i = Point2<int>;
	using Point2f = Point2<float>;
	using Point2d = Point2<double>;

	template<typename T> using Point3 = Point<T, 3>;
	using Point3i = Point3<int>;
	using Point3f = Point3<float>;
	using Point3d = Point3<double>;

}