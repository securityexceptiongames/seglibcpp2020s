#pragma once
#include <org/segames/library/dllmain.h>
#include <org/segames/library/library.h>

#include <glad/glad.h>
#include <unordered_set>
#include <memory>

namespace org::segames::library
{

	/*
		A class storing core OpenGL constants and such.

		* @author	Philip Rosberg
		* @since	2019-06-12
		* @edited	2020-06-22
	*/
	class SEG_API GLCore final
	{
	private:

		/*
			The OpenGL version.
		*/
		static float s_version;

		/*
			The avaliable OpenGL extensions.
		*/
		static std::unique_ptr<std::unordered_set<std::string>> s_extensions;

	public:

		/*
			Returns the OpenGL version.
			Loads the version from OpenGL on first call.
		*/
		inline static float glVersion()
		{
			if (s_version == 0.0f)
			{
				GLint major, minor;
				glGetIntegerv(GL_MAJOR_VERSION, &major);
				glGetIntegerv(GL_MINOR_VERSION, &minor);
				s_version = major + minor * 0.1f;
			}
			return s_version;
		}

		/*
			Returns true if the given OpenGL extension is present.
		*/
		inline static bool hasExtension(const std::string& ext)
		{
			auto res = glExtensions().find(ext);
			return res != s_extensions->end();
		}

		/*
			Returns the present OpenGL extensions.
			Loads the extensions from OpenGL on first call.
		*/
		static const std::unordered_set<std::string>& glExtensions();

	};

}