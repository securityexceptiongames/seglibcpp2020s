#pragma once
#include <org/segames/library/gl/texture/gl_texture.h>
#include <org/segames/library/math/vecmath.h>

#include <array>

namespace org::segames::library
{
			
	/*
		Class for storing the information for a section of a texture.
		NOTE! This implementation will break if the texture is moved!

		* @author	Philip Rosberg
		* @since	2021-03-14
		* @edited	2021-03-14
	*/
	template<size_t _Size>
	class GLTextureSection final
	{
	private:

		/*
			The texture.
		*/
		const GLTexture* m_texture;

		/*
			The UV coordinates of the texture section.
		*/
		std::array<BasicVector<GLfloat, 2>, _Size> m_uv;

	public:

		/*
			Creates an empty texture section.
			NOTE! Should not be used!
		*/
		inline GLTextureSection() :
			m_texture(&GLTexture::nullTexture()),
			m_uv()
		{}

		/*
			Creates a texture section from the given texture and the given UV coordinates.
			* @param[in] tex Reference to the texture
			* @param[in] uvs The UV coordinates of the section
		*/
		inline explicit GLTextureSection(const GLTexture& tex, const std::array<BasicVector<GLfloat, 2>, _Size>& uvs) :
			m_texture(&tex),
			m_uv(uvs)
		{}

		/*
			Creates a texture section from the given texture and with the given UV xy-location and size.
			* @param[in] tex Reference to the texture
			* @param[in] x The x location of the section in UV
			* @param[in] y The y location of the section in UV
			* @param[in] w The width of the section in UV
			* @param[in] h The height of the section in UV
		*/
		inline explicit GLTextureSection(const GLTexture& tex, const float x, const float y, const float w, const float h) :
			m_texture(&tex),
			m_uv()
		{
			for (size_t i = 0; i < _Size; i++)
				m_uv[i] = {x + (((i + 1) / 2) % 2) * w, y + ((i / 2) % 2) * h};
		}

		/*
			Creates a texture section from the given texture.
			* @param[in] tex Reference to the texture
		*/
		inline explicit GLTextureSection(const GLTexture& tex) :
			GLTextureSection(tex, 0, 0, 1, 1)
		{}

		/*
			Copy-rescale constructor.
		*/
		template<size_t _OtherSize>
		inline explicit GLTextureSection(const GLTextureSection<_OtherSize>& obj) :
			m_texture(obj.m_texture),
			m_uv()
		{
			for (size_t i = 0; i < (_Size < _OtherSize ? _Size : _OtherSize); i++)
				m_uv[i] = obj.m_uv[i];
			if(_OtherSize > 0)
				for (size_t i = _OtherSize; i < _Size; i++)
					m_uv[i] = m_uv[_OtherSize - 1];
		}

		/*
			Copy-constructor.
		*/
		inline GLTextureSection(const GLTextureSection& obj) :
			m_texture(obj.m_texture),
			m_uv(obj.m_uv)
		{}

	public:

		/*
			Returns the texture of the section.
		*/
		inline const GLTexture& texture() const
		{
			return *m_texture;
		}

		/*
			Returns the UV-coordinates of the point of the given index.
			* @param[in] index The index in range [0, 3]
		*/
		inline const BasicVector<float, 2>& uv(const size_t index) const
		{
#ifdef SEG_API_BOUNDS_CHECKING
			if (index > _Size)
				throw IndexOutOfBoundsException(index);
#endif
			return m_uv[index];
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline GLTextureSection<_Size>& operator=(const GLTextureSection<_Size>& obj)
		{
			m_texture = obj.m_texture;
			m_uv = obj.m_uv;
			return *this;
		}

	};

	using GLTextureTriangle = GLTextureSection<3>;
	using GLTextureRectangle = GLTextureSection<4>;

}