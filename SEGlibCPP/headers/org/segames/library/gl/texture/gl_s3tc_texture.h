#pragma once
#include <org/segames/library/gl/texture/gl_physical_texture.h>
#include <org/segames/library/math/dimension.h>
#include <org/segames/library/gl/gl_core.h>

#include <memory>
#include <istream>

namespace org::segames::library
{

	/*
		A class handling S3 compressed textures stored on the hard drive.
		The first mipmap level is the same as the regular texture level 0.

		* @author	Philip Rosberg
		* @since	2019-06-15
		* @edited	2019-06-15
	*/
	class SEG_API GLS3TCTexture :
		public GLPhysicalTexture
	{
	protected:

		/*
			The number of mipmap levels.
		*/
		int m_mipmapLevels;

		/*
			The sizes of the mipmap levels.
		*/
		std::unique_ptr<Dimension2i[]> m_mipmapSizes;

		/*
			The data of the mipmap levels.
		*/
		std::unique_ptr<std::unique_ptr<char[]>[]> m_mipmapData;

	public:

		/*
			Creates an empty S3TC texture.
		*/
		inline GLS3TCTexture() :
			GLPhysicalTexture(),
			m_mipmapLevels(0),
			m_mipmapSizes(nullptr),
			m_mipmapData(nullptr)
		{}

		/*
			Creates a new S3TC texture.
			* @param[in] path The path to the texture file
		*/
		inline explicit GLS3TCTexture(const std::string_view& path) :
			GLPhysicalTexture(),
			m_mipmapLevels(0),
			m_mipmapSizes(nullptr),
			m_mipmapData(nullptr)
		{
			setPath(path);
		}

		/*
			No copying.
		*/
		GLS3TCTexture(const GLS3TCTexture&) = delete;

		/*
			Move constructor.
		*/
		inline GLS3TCTexture(GLS3TCTexture&& obj) noexcept :
			GLPhysicalTexture(std::move(obj)),
			m_mipmapLevels(obj.m_mipmapLevels),
			m_mipmapSizes(std::move(obj.m_mipmapSizes)),
			m_mipmapData(std::move(obj.m_mipmapData))
		{
			obj.m_mipmapLevels = 0;
		}

	public:

		/*
			Returns the number of mipmap/texture levels.
		*/
		inline virtual int getNumMipmaps() const
		{
			return m_mipmapLevels;
		}

		/*
			Returns an array of mipmap sizes.
		*/
		inline virtual const Dimension2i* getMipmapSizes() const
		{
			return m_mipmapSizes.get();
		}

		/*
			Uploads the internally stored data.
		*/
		virtual void upload() override;

		/*
			Imports the data for the texture.
		*/
		inline virtual void importTexture() override
		{
			importTexture(m_path);
		}

		/*
			Imports the data for the texture.
			* @param[in] path The path to the texture file
		*/
		virtual void importTexture(const std::string& path);

		/*
			Imports the data for the texture.
			* @param[in] input The input stream with texture data
		*/
		virtual void importTexture(std::istream& input);

	public:

		/*
			No copying.
		*/
		GLS3TCTexture& operator=(const GLS3TCTexture&) = delete;

		/*
			Move-assignment operator.
		*/
		GLS3TCTexture& operator=(GLS3TCTexture&& obj) noexcept
		{
			GLPhysicalTexture::operator=(std::move(obj));
			m_mipmapLevels = obj.m_mipmapLevels;
			m_mipmapSizes = std::move(obj.m_mipmapSizes);
			m_mipmapData = std::move(obj.m_mipmapData);
			obj.m_mipmapLevels = 0;
			return *this;
		}

	protected:

		/*
			Returns the four CC code as a OpenGL format.
			* @param[in] fourCC The four CC code from the dds file
		*/
		inline static GLenum asGLFormat(const int fourCC)
		{
			switch (fourCC)
			{
			case 827611204:
				return GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
			case 861165636:
				return GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
			case 894720068:
				return GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
			default:
				return GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
			}

		}

	public:

		/*
			Returns true if S3TC textures are supported.
		*/
		inline static bool isSupported()
		{
			return GLCore::hasExtension("GL_EXT_texture_compression_s3tc");
		}

	};

}