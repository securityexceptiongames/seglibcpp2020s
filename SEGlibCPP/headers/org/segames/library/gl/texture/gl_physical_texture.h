#pragma once
#include <org/segames/library/gl/texture/gl_texture.h>

#include <string>
#include <string_view>

namespace org::segames::library
{

	/*
		Abstract super class for textures that has a file on the hard drive.

		* @author	Philip Rosberg
		* @since	2019-06-15
		* @edited	2020-06-24
	*/
	class SEG_API GLPhysicalTexture :
		public GLTexture
	{
	protected:

		/*
			The path to the texture file.
		*/
		std::string m_path;

	public:

		/*
			Creates an empty texture.
		*/
		inline GLPhysicalTexture() :
			GLTexture(),
			m_path()
		{}

		/*
			Creates a 2D and RGBA texture object of the given size.
			* @param[in] size The size of the texture
		*/
		inline explicit GLPhysicalTexture(const Dimension3i& size) :
			GLTexture(size),
			m_path()
		{}

		/*
			Creates a 2D texture object with the given attributes.
			* @param[in] internalFormat The internal OpenGL format
			* @param[in] size The size of the texture
		*/
		inline explicit GLPhysicalTexture(const GLenum internalFormat, const Dimension3i& size) :
			GLTexture(internalFormat, size),
			m_path()
		{}

		/*
			Creates a texture object with the given attributes.
			* @param[in] type The type of the texture, GL_TEXTURE_2D, etc.
			* @param[in] internalFormat The internal OpenGL format
			* @param[in] format The texture format
			* @param[in] size The size of the texture
		*/
		inline explicit GLPhysicalTexture(const GLenum type, const GLenum internalFormat, const GLenum format, const Dimension3i& size) :
			GLTexture(type, internalFormat, format, size),
			m_path()
		{}

		/*
			No copying.
		*/
		GLPhysicalTexture(const GLPhysicalTexture&) = delete;

		/*
			Move constructor.
		*/
		inline GLPhysicalTexture(GLPhysicalTexture&& obj) noexcept :
			GLTexture(std::move(obj)),
			m_path(std::move(obj.m_path))
		{}

	public:

		/*
			Returns the path.
		*/
		virtual const std::string& getPath() const
		{
			return m_path;
		}

		/*
			Sets the path to the texture file.
			* @param[in] path The path to the texture file
		*/
		virtual void setPath(const std::string_view& path)
		{
			m_path = path;
		}

		/*
			Uploads the internally stored data.
		*/
		virtual void upload() = 0;

		/*
			Imports the texture from the internally stored path.
		*/
		virtual void importTexture() = 0;

	public:

		/*
			No copying.
		*/
		GLPhysicalTexture& operator=(const GLPhysicalTexture&) = delete;

		/*
			Move-assignment operator.
		*/
		inline GLPhysicalTexture& operator=(GLPhysicalTexture&& obj) noexcept
		{
			GLTexture::operator=(std::move(obj));
			m_path = std::move(obj.m_path);
			return *this;
		}

	};

}