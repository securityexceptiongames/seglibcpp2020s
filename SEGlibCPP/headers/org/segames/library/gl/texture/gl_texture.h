#pragma once
#include <org/segames/library/gl/gl_bindable.h>
#include <org/segames/library/gl/gl_exception.h>
#include <org/segames/library/gl/gl_core.h>
#include <org/segames/library/math/dimension.h>
#include <org/segames/library/library.h>

#include <glad/glad.h>

namespace org::segames::library
{

	/*
		A super class for OpenGL textures.

		* @author	Philip Rosberg
		* @since	2019-06-13
		* @edited	2020-06-23
	*/
	class SEG_API GLTexture :
		public GLBindable
	{
	private:

		/*
			The OpenGL max texture size.s
		*/
		static int s_glMaxTextureSize;

	protected:

		/*
			The texture id.
		*/
		GLuint m_id;

		/*
			The texture type, GL_TEXTURE_2D, etc.
		*/
		GLenum m_type;

		/*
			The internal OpenGL format.
		*/
		GLenum m_internalFormat;

		/*
			The texture format.
		*/
		GLenum m_format;

		/*
			The size of the texture.
		*/
		Dimension3i m_size;

	public:

		/*
			Creates an empty texture.
		*/
		inline GLTexture() :
			GLTexture(Dimension3i(0, 0, 0))
		{}

		/*
			Creates a 2D and RGBA texture object of the given size.
			* @param[in] size The size of the texture
		*/
		inline explicit GLTexture(const Dimension3i& size) :
			GLTexture(GL_RGBA8, size)
		{}

		/*
			Creates a 2D texture object with the given attributes.
			* @param[in] internalFormat The internal OpenGL format
			* @param[in] size The size of the texture
		*/
		inline explicit GLTexture(const GLenum internalFormat, const Dimension3i& size) :
			GLTexture(GL_TEXTURE_2D, internalFormat, formatFromInternalFormat(internalFormat), size)
		{}

		/*
			Creates a texture object with the given attributes.
			* @param[in] type The type of the texture, GL_TEXTURE_2D, etc.
			* @param[in] internalFormat The internal OpenGL format
			* @param[in] format The texture format
			* @param[in] size The size of the texture
		*/
		inline explicit GLTexture(const GLenum type, const GLenum internalFormat, const GLenum format, const Dimension3i& size) :
			m_id(0),
			m_type(type),
			m_internalFormat(internalFormat),
			m_format(format),
			m_size(size)
		{}

		/*
			No copying.
		*/
		GLTexture(const GLTexture&) = delete;

		/*
			Move constructor.
		*/
		inline GLTexture(GLTexture&& obj) noexcept :
			m_id(obj.m_id),
			m_type(obj.m_type),
			m_internalFormat(obj.m_internalFormat),
			m_format(obj.m_format),
			m_size(obj.m_size)
		{
			obj.m_id = GL_NONE;
		}

		/*
			Destructor.
			Handles release of OpenGL resources.
		*/
		inline virtual ~GLTexture()
		{
			if (m_id)
				glDeleteTextures(1, &m_id);
		}

	public:

		/*
			Returns the texture id.
		*/
		inline virtual GLuint getID() const
		{
			return m_id;
		}

		/*
			Returns the texture type.
		*/
		inline virtual GLenum getType() const
		{
			return m_type;
		}

		/*
			Returns the OpenGL internal texture format.
		*/
		inline virtual GLenum getInternalFormat() const
		{
			return m_internalFormat;
		}

		/*
			Returns the texture format.
		*/
		inline virtual GLenum getFormat() const
		{
			return m_format;
		}

		/*
			Returns the size of the texture.
		*/
		inline virtual const Dimension3i& getSize() const
		{
			return m_size;
		}

		/*
			Sets the internally stored size of the texture.
			NOTE! This does not change the texture stored on the graphics card and should therefore be used with caution.
			* @param[in] size The new size of the texture object
		*/
		inline virtual void setSize(const Dimension3i& size)
		{
			m_size = size;
		}

		/*
			Binds this texture.
		*/
		inline virtual const GLTexture& bind() const override
		{
			glBindTexture(m_type, m_id);
			return *this;
		}

		/*
			Releases this texture.
		*/
		inline virtual const GLTexture& release() const override
		{
			glBindTexture(m_type, GL_NONE);
			return *this;
		}

		/*
			Generates mipmap levels for the texture.
			NOTE! This feature uses OpenGL 3.0.
			NOTE! The texture should be bound before this operation.
		*/
		inline virtual const GLTexture& generateMipMap() const
		{
			if (GLCore::glVersion() >= 3.0f)
			{
				glGenerateMipmap(m_type);
#ifdef SEG_API_GL_ERROR_EXCEPTIONS
				GLenum error = glGetError();
				if (error != GL_NO_ERROR)
					throw GLException(error);
#endif
			}
			return *this;
		}

		/*
			Sets the OpenGL usage parameters of the texture.
			NOTE! The texture should be bound before this operation.
			* @param[in] wrapping The texture lookup wrapping, GL_REPEAT, GL_CLAMP etc.
			* @param[in] minFilter The minifying filter GL_LINEAR, GL_NEAREST etc.
			* @param[in] magFilter The magnifying filter GL_LINEAR, GL_NEAREST etc.
		*/
		virtual const GLTexture& setParameters(const GLenum wrapping, const GLenum minFilter, const GLenum magFilter) const;

		/*
			Uploads the given data.
			* @param[in] storage The storage format, GL_UNSIGNED_BYTE etc.
			* @param[in] data The texture data to upload
		*/
		virtual void upload(const GLenum storage, const void* data);

		/*
			Uploads the given data.
			* @param[in] level The mipmap level to upload to (default texture level is 0)
			* @param[in] size The size of the texture level
			* @param[in] storage The storage format, GL_UNSIGNED_BYTE etc.
			* @param[in] data The texture data to upload
		*/
		virtual void upload(const GLint level, const Dimension3i& size, const GLenum storage, const void* data);

	public:

		/*
			No copying.
		*/
		GLTexture& operator=(const GLTexture&) = delete;

		/*
			Move-assignment operator.
		*/
		inline GLTexture& operator=(GLTexture&& obj) noexcept
		{
			m_id = obj.m_id;
			m_type = obj.m_type;
			m_internalFormat = obj.m_internalFormat;
			m_format = obj.m_format;
			m_size = obj.m_size;
			obj.m_id = GL_NONE;
			return *this;
		}

	protected:

		/*
			Returns a format approximation of the given internal format.
			* @param[in] internalFormat The internal OpenGL format
		*/
		static GLenum formatFromInternalFormat(const GLenum internalFormat);

	public:

		/*
			Returns the OpenGL maximum texture size.
			Loads the size from OpenGL on first call.
		*/
		static int glMaxTextureSize();

		/*
			Returns a reference to an empty texture.
		*/
		inline static const GLTexture& nullTexture()
		{
			static GLTexture tex;
			return tex;
		}

	};

}