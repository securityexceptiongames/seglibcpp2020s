#pragma once
#include <org/segames/library/gl/texture/gl_physical_texture.h>

#include <memory>

namespace org::segames::library
{

	/*
		A texture class capable of detecting physical texture type and using the proper loading.

		* @author	Philip Rosberg
		* @since	2019-06-16
		* @edited	2020-06-24
	*/
	class SEG_API GLPhysicalTextureWrapper :
		public GLPhysicalTexture
	{
	public:

		/*
			The detected texture type.
		*/
		enum TextureType
		{
			DISC_TEXTURE,
			S3TC_TEXTURE
		};

	protected:

		/*
			The texture type being used.
		*/
		TextureType m_texType;

		/*
			The texture.
		*/
		std::unique_ptr<GLPhysicalTexture> m_texture;

	public:

		/*
			Creates an empty texture wrapper.
		*/
		inline GLPhysicalTextureWrapper() :
			GLPhysicalTextureWrapper("")
		{}

		/*
			Creates a texture wrapper with the given path.
			* @param[in] path The path to the texture file
		*/
		inline explicit GLPhysicalTextureWrapper(const std::string_view& path) :
			GLPhysicalTexture(),
			m_texType(determineTypeFrom(path)),
			m_texture(nullptr)
		{
			GLPhysicalTexture::setPath(path);
		}

		/*
			No copying.
		*/
		GLPhysicalTextureWrapper(const GLPhysicalTextureWrapper&) = delete;

		/*
			Move constructor.
		*/
		inline GLPhysicalTextureWrapper(GLPhysicalTextureWrapper&& obj) noexcept :
			GLPhysicalTexture(std::move(obj)),
			m_texType(obj.m_texType),
			m_texture(std::move(obj.m_texture))
		{}

	public:

		/*
			Returns the type of texture being used.
		*/
		inline virtual TextureType getTextureType() const
		{
			return m_texType;
		}

		/*
			Returns the backing texture.
		*/
		inline virtual GLPhysicalTexture* getTexture() const
		{
			return m_texture.get();
		}

		/*
			Sets the path to the texture file.
			* @param[in] path The path to the texture file
		*/
		inline virtual void setPath(const std::string_view& path) override
		{
			GLPhysicalTexture::setPath(path);
			m_texType = determineTypeFrom(path);
		}

		/*
			Sets the internally stored size of the texture.
			NOTE! This does not change the texture stored on the graphics card and should therefore be used with caution.
			* @param[in] size The new size of the texture object
		*/
		inline virtual void setSize(const Dimension3i& size) override
		{
			GLPhysicalTexture::setSize(size);
			if (m_texture)
				m_texture->setSize(size);
		}

		/*
			Binds this texture.
		*/
		inline virtual const GLPhysicalTextureWrapper& bind() const override
		{
			if (m_texture)
				m_texture->bind();
			return *this;
		}

		/*
			Releases this texture.
		*/
		inline virtual const GLPhysicalTextureWrapper& release() const override
		{
			if (m_texture)
				m_texture->release();
			return *this;
		}

		/*
			Uploads the texture data.
		*/
		inline virtual void upload() override
		{
			if (m_texture)
			{
				m_texture->upload();
				m_id = m_texture->getID();
			}

		}

		/*
			Imports the data for the texture.
		*/
		inline virtual void importTexture() override
		{
			importTexture(m_path);
		}

		/*
			Imports the data for the texture.
			* @param[in] path The path to the texture file
		*/
		virtual void importTexture(const std::string_view& path);

	public:

		/*
			No copying.
		*/
		GLPhysicalTextureWrapper& operator=(const GLPhysicalTextureWrapper&) = delete;

		/*
			Move-assignment operator.
		*/
		inline GLPhysicalTextureWrapper& operator=(GLPhysicalTextureWrapper&& obj) noexcept
		{
			GLPhysicalTexture::operator=(std::move(obj));
			m_texType = obj.m_texType;
			m_texture = std::move(obj.m_texture);
			return *this;
		}

	protected:

		/*
			Returns the type of texture from the given path.
			* @param[in] path The path to the texture file
		*/
		static TextureType determineTypeFrom(const std::string_view& path);

	};

}