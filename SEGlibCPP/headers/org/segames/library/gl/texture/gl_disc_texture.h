#pragma once
#include <org/segames/library/gl/texture/gl_physical_texture.h>

namespace org::segames::library
{

	/*
		A class handling regular uncompressed textures stored on the hard drive.
					
		* @author	Philip Rosberg
		* @since	2019-06-15
		* @edited	2020-06-24
	*/
	class SEG_API GLDiscTexture :
		public GLPhysicalTexture
	{
	protected:

		/*
			The stored texture data.
		*/
		void* m_data;

	public:

		/*
			Creates an empty disc texture.
		*/
		inline GLDiscTexture() :
			GLPhysicalTexture(),
			m_data(nullptr)
		{}

		/*
			Creates a new disc texture.
			* @param[in] path The path to the texture file
		*/
		inline explicit GLDiscTexture(const std::string_view& path) :
			GLPhysicalTexture(),
			m_data(nullptr)
		{
			GLPhysicalTexture::setPath(path);
		}
					
		/*
			No copying.
		*/
		GLDiscTexture(const GLDiscTexture&) = delete;

		/*
			Move constructor.
		*/
		inline GLDiscTexture(GLDiscTexture&& obj) noexcept :
			GLPhysicalTexture(std::move(obj)),
			m_data(obj.m_data)
		{
			obj.m_data = nullptr;
		}

		/*
			Destructor.
		*/
		virtual ~GLDiscTexture();

	public:

		/*
			Uploads the internally stored data.
		*/
		virtual void upload() override;

		/*
			Imports the data for the texture.
		*/
		inline virtual void importTexture() override
		{
			importTexture(0);
		}

		/*
			Imports the data for the texture.
			* @param[in] forceChannel The amount of channels to force, or 0 to not force
		*/
		inline virtual void importTexture(const int forceChannel)
		{
			importTexture(m_path, forceChannel);
		}

		/*
			Imports the data for the texture.
			* @param[in] path The path to the texture file
		*/
		virtual void importTexture(const std::string_view& path)
		{
			importTexture(path, 0);
		}

		/*
			Imports the data for the texture.
			* @param[in] path The path to the texture file
			* @param[in] forceChannel The amount of channels to force, or 0 to not force
		*/
		virtual void importTexture(const std::string_view& path, const int forceChannel);

	public:

		/*
			No copying.
		*/
		GLDiscTexture& operator=(const GLDiscTexture&) = delete;

		/*
			Move-assignment operator.
		*/
		inline GLDiscTexture& operator=(GLDiscTexture&& obj) noexcept
		{
			GLPhysicalTexture::operator=(std::move(obj));
			m_data = obj.m_data;
			obj.m_data = nullptr;
			return *this;
		}

	};

}
