#pragma once
#include <org/segames/library/gl/texture/gl_texture_section.h>
#include <org/segames/library/gl/gl_buffer.h>
#include <org/segames/library/gl/gl_shader.h>
#include <org/segames/library/math/volume.h>
#include <org/segames/library/util/array_list.h>

#include <unordered_map>

namespace org::segames::library
{

	/*
		Class/Namespace for texture atlasing utilities.

		* @author	Philip Rosberg
		* @since	2021-03-14
		* @edited	2021-04-11
	*/
	class SEG_API GLTextureAtlaser final
	{
	public:

		/*
			Available rescale filters for mipmap creation.

			* @author	Philip Rosberg
			* @since	2021-04-04
			* @edited	2021-04-11
		*/
		enum class RescaleFilter : char
		{
			DEFAULT = 2,
			NEAREST_NEIGHBOR = 0,
			OPENGL_LINEAR = 1,
			BILINEAR = 2,
			LANCZOS = 3
		};

		/*
			Structure containing the information of where to render a 
			certain texture reactangle in the atlas.

			* @author	Philip Rosberg
			* @since	2021-03-19
			* @edited	2021-03-19
		*/
		struct AtlasTile
		{
			Rectanglei tile;
			GLTextureRectangle section;
		};

	public:

		/*
			Packs (tiles) the atlas with the given texture rectangles.
			* @param[in] border The amount of empty pixels around each tile
			* @param[in] size The size of the atlas texture
			* @param[in] sections The texture rectangles to pack
		*/
		inline static ArrayList<AtlasTile> tileAtlas(const size_t border, const Dimension2i& size, const ArrayList<GLTextureRectangle>& sections)
		{
			return getAtlaser__().tileAtlas__(border, size, sections);
		}

		/*
			Renders the given atlas tiles onto a new texture.
			* @param[in] levels The number of mipmap levels (>= 1)
			* @param[in] size The size of the atlas texture
			* @param[in] inputTiles The tiles to render onto the atlas texture
			* @param[in] filter The rescaling filter to use
		*/
		inline static GLTexture renderAtlas(const size_t levels, const Dimension2i& size, const ArrayList<AtlasTile>& inputTiles, RescaleFilter filter)
		{
			return getAtlaser__().renderAtlas__(levels < 1 ? 1 : levels, size, inputTiles, filter);
		}

		/*
			Determines the final texture sections (rectangles) for the atlased tiles.
			* @param[in] halfPixelPadding True if the texture section should be surrounded by half a pixel of padding to avoid bleeding between tiles
			* @param[in] atlas The atlas texture
			* @param[in] inputTiles The tiles that were rendered onto the atlas texture
			* @param[out] outputSections The final texture sections
		*/
		inline static void computeAtlasSections(const bool halfPixelPadding, const GLTexture& atlas, const ArrayList<AtlasTile>& inputTiles, ArrayList<GLTextureRectangle>& outputSections)
		{
			getAtlaser__().computeAtlasSections__(halfPixelPadding, atlas, inputTiles, outputSections);
		}

	private:

		/*
			The render data buffer.
		*/
		GLBuffer m_buffer;

		/*
			The texture filter shaders.
		*/
		std::unordered_map<RescaleFilter, GLShader> m_shaders;

	private:

		/*
			Constructs a texture atlaser.
		*/
		inline GLTextureAtlaser() {
			m_buffer.setPointerBinding(GLDataType::VERTEX, GLPointerInf(GL_FLOAT, 2, sizeof(GLfloat) * 4, sizeof(GLfloat) * 2));
			m_buffer.setPointerBinding(GLDataType::TEX_COORD, GLPointerInf(GL_FLOAT, 2, sizeof(GLfloat) * 4, 0));
			createShaders__(m_shaders);
		}

		/*
			No copying.
		*/
		GLTextureAtlaser(const GLTextureAtlaser&) = delete;

	private:
				
		/*
			Returns the shader mapped to the given filter.
			* @param[in] filter The filter
		*/
		inline const GLShader& getShaderForFilter__(RescaleFilter filter)
		{
			auto res = m_shaders.find(filter);
			if (res != m_shaders.end())
				return res->second;
			else
				return m_shaders.find(RescaleFilter::DEFAULT)->second;
		}

		/*
			Creates the and maps the filter shaders to the given map.
			* @param[in] shaders The shader map to map the filter shaders in
		*/
		void createShaders__(std::unordered_map<RescaleFilter, GLShader>& shaders);

		/*
			Tiles the atlas.
			* @param[in] border The amount of empty pixels around each tile
			* @param[in] size The size of the atlas texture
			* @param[in] sections The texture rectangles to pack
		*/
		ArrayList<AtlasTile> tileAtlas__(const size_t border, const Dimension2i& size, const ArrayList<GLTextureRectangle>& sections);

		/*
			Renders the given atlas tile.
			* @param[in] texSizeUniform The uniform location of the texSize variable
			* @param[in] tile The tile to render
		*/
		void renderTile__(const GLint texSizeUniform, const AtlasTile& tile);

		/*
			Renders the atlas.
			* @param[in] levels The number of mipmap levels (>= 1)
			* @param[in] size The size of the atlas texture
			* @param[in] inputTiles The tiles to render onto the atlas texture
			* @param[in] filter The rescaling filter to use
		*/
		GLTexture renderAtlas__(const size_t levels, const Dimension2i& size, const ArrayList<AtlasTile>& inputTiles, RescaleFilter filter);

		/*
			Computes the atlas texture sections.
			* @param[in] halfPixelPadding True if the texture section should be surrounded by half a pixel of padding to avoid bleeding between tiles
			* @param[in] atlas The atlas texture
			* @param[in] inputTiles The tiles that were rendered onto the atlas texture
			* @param[out] outputSections The final texture sections
		*/
		void computeAtlasSections__(const bool halfPixelPadding, const GLTexture& atlas, const ArrayList<AtlasTile>& inputTiles, ArrayList<GLTextureRectangle>& outputSections);

	private:

		/*
			Returns the thread local atlaser singleton instance.
		*/
		inline static GLTextureAtlaser& getAtlaser__()
		{
			static thread_local GLTextureAtlaser atlaser;
			return atlaser;
		}

	};

}