#pragma once
#include <org/segames/library/gl/fbo/gl_abstract_framebuffer.h>

namespace org::segames::library
{

	/*
		Implementation of a OpenGL EXT framebuffer for older systems.

		* @author	Philip Rosberg
		* @since	2019-06-16
		* @edited	2020-06-29
	*/
	class SEG_API GLFramebufferEXT :
		public GLAbstractFramebuffer
	{
	public:

		/*
			Creates a zeroed/empty framebuffer.
		*/
		inline GLFramebufferEXT() :
			GLAbstractFramebuffer()
		{}

		/*
			Creates a EXT frame buffer.
			* @param[in] color The color component of the framebuffer, responsibility is handed over to this instance
			* @param[in] depth The depth component of the framebuffer, responsibility is handed over to this instance
			* @param[in] size The size of the frame buffer
		*/
		inline explicit GLFramebufferEXT(std::unique_ptr<GLTexture>&& color, std::unique_ptr<GLTexture>&& depth, const Dimension2i& size) :
			GLAbstractFramebuffer(std::move(color), std::move(depth), size)
		{}

		/*
			No copying.
		*/
		GLFramebufferEXT(const GLFramebufferEXT& obj) = delete;

		/*
			Move constructor.
		*/
		inline GLFramebufferEXT(GLFramebufferEXT&& obj) noexcept :
			GLAbstractFramebuffer(std::move(obj))
		{}

		/*
			Destructor.
		*/
		inline virtual ~GLFramebufferEXT() override
		{
			if (m_id)
				glDeleteFramebuffersEXT(1, &m_id);
		}

	public:

		/*
			Binds this framebuffer to the OpenGL context.
		*/
		inline virtual const GLFramebufferEXT& bind() const override
		{
			glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, m_id);
			return *this;
		}

		/*
			Releases this framebuffer from the OpenGL context.
		*/
		inline virtual const GLFramebufferEXT& release() const override
		{
			glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, GL_NONE);
			return *this;
		}

	protected:

		/*
			Links the frame buffer.
		*/
		virtual void buildFBO() override;

	public:

		/*
			Returns true if EXT framebuffers are supported.
		*/
		static bool isSupported();

	};

}