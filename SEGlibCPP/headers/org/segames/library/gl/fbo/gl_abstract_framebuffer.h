#pragma once
#include <org/segames/library/gl/fbo/gl_framebuffer.h>
#include <org/segames/library/virtually_destructible.h>

#include <memory>

namespace org::segames::library
{

	/*
		Abstract class for actual frame buffers.

		* @author	Philip Rosberg
		* @since	2019-06-16
		* @edited	2020-06-29
	*/
	class SEG_API GLAbstractFramebuffer :
		public GLFramebuffer,
		public VirtuallyDestructible
	{
	protected:

		/*
			True if the texture[s] should be reset/rescaled on build.
		*/
		bool m_resetTexture;

		/*
			The framebuffer id.
		*/
		GLuint m_id;

		/*
			Size of the framebuffer.
		*/
		Dimension2i m_size;

		/*
			The color component texture.
		*/
		std::unique_ptr<GLTexture> m_color;

		/*
			The depth component texture.
		*/
		std::unique_ptr<GLTexture> m_depth;

		/*
			The additional color components.
		*/
		ArrayList<std::unique_ptr<GLTexture>> m_additionalColors;

	public:

		/*
			Creates an empty frame buffer.
		*/
		inline GLAbstractFramebuffer() :
			m_resetTexture(false),
			m_id(GL_NONE),
			m_size(),
			m_color(),
			m_depth(),
			m_additionalColors()
		{}

		/*
			Creates an abstract frame buffer.
			* @param[in] color The color component of the framebuffer, responsibility is handed over to this instance
			* @param[in] depth The depth component of the framebuffer, responsibility is handed over to this instance
			* @param[in] size The size of the frame buffer
		*/
		inline explicit GLAbstractFramebuffer(std::unique_ptr<GLTexture>&& color, std::unique_ptr<GLTexture>&& depth, const Dimension2i& size) :
			m_resetTexture(true),
			m_id(GL_NONE),
			m_size(size),
			m_color(std::move(color)),
			m_depth(std::move(depth)),
			m_additionalColors()
		{
			if (m_color)
				m_color->setSize(Dimension3i(m_size.width(), m_size.height(), m_color->getSize().depth()));
			if (m_depth)
				m_depth->setSize(Dimension3i(m_size.width(), m_size.height(), m_depth->getSize().depth()));
		}

		/*
			No copying.
		*/
		GLAbstractFramebuffer(const GLAbstractFramebuffer&) = delete;

		/*
			Move constructor.
		*/
		inline GLAbstractFramebuffer(GLAbstractFramebuffer&& obj) noexcept :
			m_resetTexture(true),
			m_id(obj.m_id),
			m_size(obj.m_size),
			m_color(std::move(obj.m_color)),
			m_depth(std::move(obj.m_depth)),
			m_additionalColors(std::move(obj.m_additionalColors))
		{
			obj.m_id = GL_NONE;
			if (m_color)
				m_color->setSize(Dimension3i(m_size.width(), m_size.height(), m_color->getSize().depth()));
			if (m_depth)
				m_depth->setSize(Dimension3i(m_size.width(), m_size.height(), m_depth->getSize().depth()));
		}

	public:

		/*
			Returns the id of the frame buffer.
		*/
		inline virtual GLuint getID() const override
		{
			return m_id;
		}

		/*
			Returns the size of the frame buffer.
		*/
		inline virtual const Dimension2i& getSize() const override
		{
			return m_size;
		}

		/*
			Returns a pointer to the color component of the frame buffer.
		*/
		virtual GLTexture* getColorComp() override
		{
			return m_color.get();
		}

		/*
			Returns a pointer to the color component of the frame buffer.
		*/
		virtual const GLTexture* getColorComp() const override
		{
			return m_color.get();
		}

		/*
			Returns a pointer to the depth component of the frame buffer.
		*/
		virtual GLTexture* getDepthComp() override
		{
			return m_depth.get();
		}

		/*
			Returns a pointer to the depth component of the frame buffer.
		*/
		virtual const GLTexture* getDepthComp() const override
		{
			return m_depth.get();
		}

		/*
			Returns the list of additional color components.
		*/
		virtual const ArrayList<std::unique_ptr<GLTexture>>& getAdditionalColorComp() const override
		{
			return m_additionalColors;
		}

		/*
			Sets the size of the framebuffer.
			NOTE! FBO must be rebuilt for effects to take place.
			* @param[in] size The new size of the frame buffer
		*/
		virtual void setSize(const Dimension2i& size) override;

		/*
			Sets the color component of the framebuffer.
			NOTE! FBO must be rebuilt for effects to take place.
			* @param[in] tex The texture to use
		*/
		inline virtual void setColorComp(std::unique_ptr<GLTexture>&& tex) override
		{
			m_color = std::move(tex);
			m_resetTexture = true;
		}

		/*
			Sets the depth component of the framebuffer.
			NOTE! FBO must be rebuilt for effects to take place.
			NOTE! Should have internal format GL_DEPTH_COMPONENT
			* @param[in] tex The texture to use
		*/
		inline virtual void setDepthComp(std::unique_ptr<GLTexture>&& tex) override
		{
			m_depth = std::move(tex);
			m_resetTexture = true;
		}

		/*
			Adds an additional color component.
			NOTE! FBO must be rebuilt for effects to take place.
			NOTE! This operation does not replace the setColorComp()
			NOTE! The texture parameters should be set before the component is added
			* @param[in] tex The texture to use
		*/
		inline virtual void addAdditionalColorComp(std::unique_ptr<GLTexture>&& tex) override
		{
			m_additionalColors.add(std::move(tex));
		}

		/*
			Removes and returns the additional color component of the given index.
			* @param[in] index The index of the additional color component
		*/
		inline virtual std::unique_ptr<GLTexture> removeAdditionalColorComp(const int index) override
		{
			m_resetTexture = true;
			return m_additionalColors.remove(index);
		}

		/*
			Binds this framebuffer to the OpenGL context.
		*/
		virtual const GLAbstractFramebuffer& bind() const override = 0;

		/*
			Releases this framebuffer from the OpenGL context.
		*/
		virtual const GLAbstractFramebuffer& release() const override = 0;

		/*
			Builds the framebuffer.
		*/
		inline virtual void build() override
		{
			bool changed = false;
			buildTextures(changed);
			if (changed)
				buildFBO();
		}

	public:

		/*
			No copying.
		*/
		GLAbstractFramebuffer& operator=(const GLAbstractFramebuffer&) = delete;

		/*
			Move-assignment operator.
		*/
		inline GLAbstractFramebuffer& operator=(GLAbstractFramebuffer&& obj) noexcept
		{
			m_resetTexture = true;
			m_id = obj.m_id;
			m_size = obj.m_size;
			m_color = std::move(obj.m_color);
			m_depth = std::move(obj.m_depth);
			m_additionalColors = std::move(obj.m_additionalColors);
					
			obj.m_id = GL_NONE;
			if (m_color)
				m_color->setSize(Dimension3i(m_size.width(), m_size.height(), m_color->getSize().depth()));
			if (m_depth)
				m_depth->setSize(Dimension3i(m_size.width(), m_size.height(), m_depth->getSize().depth()));
			return *this;
		}

	protected:

		/*
			Links the frame buffer.
		*/
		virtual void buildFBO() = 0;

		/*
			Creates the textures for the buffer.
			* @param[out] changed Set to true if anything changed with the textures
		*/
		virtual void buildTextures(bool& changed);

	};

}