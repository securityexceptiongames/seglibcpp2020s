#pragma once
#include <org/segames/library/gl/fbo/gl_abstract_framebuffer.h>

namespace org::segames::library
{

	/*
		Implementation of a OpenGL 3.0 framebuffer.

		* @author	Philip Rosberg
		* @since	2019-06-16
		* @edited	2020-06-29
	*/
	class SEG_API GLFramebuffer30 :
		public GLAbstractFramebuffer
	{
	public:
					
		/*
			Creates a zeroed/empty framebuffer.
		*/
		inline GLFramebuffer30() :
			GLAbstractFramebuffer()
		{}

		/*
			Creates a GL30 frame buffer.
			* @param[in] color The color component of the framebuffer, responsibility is handed over to this instance
			* @param[in] depth The depth component of the framebuffer, responsibility is handed over to this instance
			* @param[in] size The size of the frame buffer
		*/
		inline explicit GLFramebuffer30(std::unique_ptr<GLTexture>&& color, std::unique_ptr<GLTexture>&& depth, const Dimension2i& size) :
			GLAbstractFramebuffer(std::move(color), std::move(depth), size)
		{}

		/*
			No copying.
		*/
		GLFramebuffer30(const GLFramebuffer30&) = delete;

		/*
			Move constructor.
		*/
		inline GLFramebuffer30(GLFramebuffer30&& obj) noexcept :
			GLAbstractFramebuffer(std::move(obj))
		{}

		/*
			Destructor.
		*/
		inline virtual ~GLFramebuffer30() override
		{
			if (m_id)
				glDeleteFramebuffers(1, &m_id);
		}

	public:

		/*
			Binds this framebuffer to the OpenGL context.
		*/
		inline virtual const GLFramebuffer30& bind() const override
		{
			glBindFramebuffer(GL_FRAMEBUFFER, m_id);
			return *this;
		}

		/*
			Releases this framebuffer from the OpenGL context.
		*/
		inline virtual const GLFramebuffer30& release() const override
		{
			glBindFramebuffer(GL_FRAMEBUFFER, GL_NONE);
			return *this;
		}

	public:

		/*
			No copying.
		*/
		GLFramebuffer30& operator=(const GLFramebuffer30&) = delete;

		/*
			Move-assignment operator.
		*/
		inline GLFramebuffer30& operator=(GLFramebuffer30&& obj) noexcept
		{
			GLAbstractFramebuffer::operator=(std::move(obj));
			return *this;
		}

	protected:

		/*
			Links the frame buffer.
		*/
		virtual void buildFBO() override;

	public:

		/*
			Returns true if GL30 framebuffers are supported.
		*/
		static bool isSupported();

	};

}