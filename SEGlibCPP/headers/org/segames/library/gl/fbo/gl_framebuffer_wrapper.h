#pragma once
#include <org/segames/library/gl/fbo/gl_abstract_framebuffer.h>
#include <org/segames/library/gl/gl_exception.h>
#include <org/segames/library/null_pointer_exception.h>

namespace org::segames::library
{

	/*
		A wrapper class for easy multi-version framebuffers.

		* @author	Philip Rosberg
		* @since	2019-06-16
		* @edited	2020-06-29
	*/
	class SEG_API GLFramebufferWrapper :
		public GLFramebuffer
	{
	protected:

		/*
			The actual buffer.
		*/
		std::unique_ptr<GLAbstractFramebuffer> m_buffer;

	public:
					
		/*
			Creates a zeroed/empty framebuffer.
		*/
		inline GLFramebufferWrapper() :
			m_buffer(nullptr)
		{}

		/*
			Creates a frame buffer with the given components.
			* @param[in] color True if a color component should be present
			* @param[in] depth True if a depth component should be present
			* @param[in] size The initial size of the framebuffer
		*/
		inline explicit GLFramebufferWrapper(const bool color, const bool depth, const Dimension2i& size) :
			GLFramebufferWrapper(
				std::unique_ptr<GLTexture>(color ? new GLTexture(GL_RGBA8, Dimension3i(size.width(), size.height(), 1)) : nullptr),
				std::unique_ptr<GLTexture>(depth ? new GLTexture(GL_DEPTH_COMPONENT, Dimension3i(size.width(), size.height(), 1)) : nullptr),
				size
			)
		{}

		/*
			Creates a frame buffer with the given components.
			* @param[in] color The color component of the framebuffer, responsibility is handed over to this instance
			* @param[in] depth The depth component of the framebuffer, responsibility is handed over to this instance
			* @param[in] size The initial size of the framebuffer
		*/
		inline explicit GLFramebufferWrapper(std::unique_ptr<GLTexture>&& color, std::unique_ptr<GLTexture>&& depth, const Dimension2i& size) :
			m_buffer(newSupportedFBO(std::move(color), std::move(depth), size))
		{
			if (!m_buffer)
				throw GLException("No framebuffer is supported on this machine.");
		}

		/*
			No copying.
		*/
		GLFramebufferWrapper(const GLFramebufferWrapper&) = delete;

		/*
			Move constructor.
		*/
		inline GLFramebufferWrapper(GLFramebufferWrapper&& obj) noexcept :
			m_buffer(std::move(obj.m_buffer))
		{}

	public:

		/*
			Returns true if the backing framebuffer is null.
		*/
		inline virtual bool isNull() const
		{
			return m_buffer.get() == nullptr;
		}

		/*
			Returns the id of the frame buffer.
		*/
		inline virtual GLuint getID() const override
		{
			if (m_buffer)
				return m_buffer->getID();
			else
				return GL_NONE;
		}

		/*
			Returns the size of the frame buffer.
		*/
		inline virtual const Dimension2i& getSize() const override
		{
			if (m_buffer)
				return m_buffer->getSize();
			else
				return Dimension2i::zero();
		}

		/*
			Returns a pointer to the color component of the frame buffer.
		*/
		inline virtual GLTexture* getColorComp() override
		{
			if (m_buffer)
				return m_buffer->getColorComp();
			else
				return nullptr;
		}

		/*
			Returns a pointer to the color component of the frame buffer.
		*/
		inline virtual const GLTexture* getColorComp() const override
		{
			if (m_buffer)
				return m_buffer->getColorComp();
			else
				return nullptr;
		}

		/*
			Returns a pointer to the depth component of the frame buffer.
		*/
		inline virtual GLTexture* getDepthComp() override
		{
			if (m_buffer)
				return m_buffer->getDepthComp();
			else
				return nullptr;
		}

		/*
			Returns a pointer to the depth component of the frame buffer.
		*/
		inline virtual const GLTexture* getDepthComp() const override
		{
			if (m_buffer)
				return m_buffer->getDepthComp();
			else
				return nullptr;
		}

		/*
			Returns the list of additional color components.
		*/
		inline virtual const ArrayList<std::unique_ptr<GLTexture>>& getAdditionalColorComp() const override
		{
			if (!m_buffer)
				throw NullPointerException("Cannot retrieve color additional comp. from null framebuffer.");
			return m_buffer->getAdditionalColorComp();
		}

		/*
			Sets the size of the framebuffer.
			NOTE! FBO must be rebuilt for effects to take place.
			* @param[in] size The new size of the frame buffer
		*/
		inline virtual void setSize(const Dimension2i& size) override
		{
			if (m_buffer)
				m_buffer->setSize(size);
		}

		/*
			Sets the color component of the framebuffer.
			NOTE! FBO must be rebuilt for effects to take place.
			* @param[in] tex The texture to use
		*/
		inline virtual void setColorComp(std::unique_ptr<GLTexture>&& tex) override
		{
			if (m_buffer)
				m_buffer->setColorComp(std::move(tex));
		}

		/*
			Sets the depth component of the framebuffer.
			NOTE! FBO must be rebuilt for effects to take place.
			NOTE! Should have internal format GL_DEPTH_COMPONENT
			* @param[in] tex The texture to use
		*/
		inline virtual void setDepthComp(std::unique_ptr<GLTexture>&& tex) override
		{
			if (m_buffer)
				m_buffer->setDepthComp(std::move(tex));
		}

		/*
			Adds an additional color component.
			NOTE! FBO must be rebuilt for effects to take place.
			NOTE! This operation does not replace the setColorComp()
			NOTE! The texture parameters should be set before the component is added
			* @param[in] tex The texture to use
		*/
		inline virtual void addAdditionalColorComp(std::unique_ptr<GLTexture>&& tex) override
		{
			if (m_buffer)
				m_buffer->addAdditionalColorComp(std::move(tex));
		}

		/*
			Removes and returns the additional color component of the given index.
			* @param[in] index The index of the additional color component
		*/
		inline virtual std::unique_ptr<GLTexture> removeAdditionalColorComp(const int index) override
		{
			if (m_buffer)
				return m_buffer->removeAdditionalColorComp(index);
			else
				return nullptr;
		}

		/*
			Binds this framebuffer to the OpenGL context.
		*/
		inline virtual const GLFramebuffer& bind() const override
		{
			if (m_buffer)
				m_buffer->bind();
			return *this;
		}

		/*
			Releases this framebuffer from the OpenGL context.
		*/
		inline virtual const GLFramebuffer& release() const override
		{
			if (m_buffer)
				m_buffer->release();
			return *this;
		}

		/*
			Builds the framebuffer.
		*/
		inline virtual void build() override
		{
			if (m_buffer)
				m_buffer->build();
		}

	public:

		/*
			No copying.
		*/
		GLFramebufferWrapper& operator=(const GLFramebufferWrapper&) = delete;

		/*
			Move-assignment operator.
		*/
		GLFramebufferWrapper& operator=(GLFramebufferWrapper&& obj) noexcept
		{
			m_buffer = std::move(obj.m_buffer);
			return *this;
		}

	protected:

		/*
			Returns a new instance of a type of framebuffer that is supported.
			* @param[in] color The color component of the framebuffer, responsibility is handed over to the returned instance
			* @param[in] depth The depth component of the framebuffer, responsibility is handed over to the returned instance
			* @param[in] size The initial size of the framebuffer
		*/
		static std::unique_ptr<GLAbstractFramebuffer> newSupportedFBO(std::unique_ptr<GLTexture>&& color, std::unique_ptr<GLTexture>&& depth, const Dimension2i& size);

	public:

		/*
			Returns true if any framebuffer is supported.
		*/
		static bool isSupported();

	};

}