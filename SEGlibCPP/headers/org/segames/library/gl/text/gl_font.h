#pragma once
#include <org/segames/library/gl/text/gl_font_alignment.h>
#include <org/segames/library/gl/text/gl_font_glyph.h>
#include <org/segames/library/gl/texture/gl_texture.h>
#include <org/segames/library/gl/gl_backed_buffer.h>
#include <org/segames/library/gl/gl_shader.h>
#include <org/segames/library/math/dimension.h>
#include <org/segames/library/util/array_list.h>
#include <org/segames/library/util/color.h>
#include <org/segames/library/util/utf8_iterator.h>

#include <fstream>
#include <memory>
#include <unordered_map>

namespace org::segames::library
{
				
	/*
		A class describing a font for OpenGL rendering.

		* @author	Philip Rosberg
		* @since	2019-06-18
		* @edited	2020-06-24
	*/
	class SEG_API GLFont
	{
	public:

		/*
			Standard ASCII characters, codes 32 - 127.
		*/
		static const ArrayList<unsigned int> ASCII_CHARACTERS;

	private:

		/*
			Vertex pointer information.
		*/
		static constexpr GLPointerInf s_vertexPointerInf = GLPointerInf(GL_FLOAT, 2, sizeof(GLfloat) * 4, 0);

		/*
			Texture coordinate pointer information.
		*/
		static constexpr GLPointerInf s_texCoordPointerInf = GLPointerInf(GL_FLOAT, 2, sizeof(GLfloat) * 4, sizeof(GLfloat) * 2);

		/*
			The default font shader.
		*/
		static GLShader s_defaultShader;
				
	protected:

		/*
			The atlas size.
		*/
		int m_atlasSize;

		/*
			The size of the font.
		*/
		float m_height;

		/*
			The font descent.
		*/
		float m_descent;

		/*
			Reference pointer to the font shader.
		*/
		const GLShader* m_shader;

		/*
			Variable keeping track of line lengths during rendering.
		*/
		ArrayList<float> m_lineLengths;

		/*
			The font geometry.
		*/
		std::unique_ptr<GLFloatBuffer> m_geometry;

		/*
			The texture with mapped glyphs.
		*/
		std::unique_ptr<GLTexture> m_atlas;

		/*
			The atlas data with the grayscale.
		*/
		std::unique_ptr<unsigned char[]> m_atlasData;

		/*
			The mapped glyphs.
		*/
		std::unordered_map<unsigned int, GLFontGlyph> m_glyphs;

	public:

		/*
			Creates an empty font and does not import anything.
		*/
		inline GLFont() :
			GLFont(0, 0, "", ArrayList<unsigned int>())
		{}

		/*
			Creates and imports a new font.
			* @param[in] size The size/height of a standard character
			* @param[in] oversampling How many times larger the characters should be rendered in the atlas for better quality, 1 (no extra) or 2 usually works fine
			* @param[in] path The path to the TTF file of the font
			* @param[in] chars The UTF8 character codes to render to the atlas
		*/
		inline explicit GLFont(const float size, const unsigned int oversampling, const std::string_view& path, const ArrayList<unsigned int>& chars) :
			GLFont(size, oversampling, path, nullptr, chars)
		{}

		/*
			Creates and imports a new font.
			* @param[in] size The size/height of a standard character
			* @param[in] oversampling How many times larger the characters should be rendered in the atlas for better quality, 1 (no extra) or 2 usually works fine
			* @param[in] path The path to the TTF file of the font
			* @param[in] shader A reference pointer to a shader that should be used for text rendering, this pointer is NOT deallocated by this instance
			* @param[in] chars The UTF8 character codes to render to the atlas
		*/
		inline explicit GLFont(const float size, const unsigned int oversampling, const std::string_view& path, const GLShader* shader, const ArrayList<unsigned int>& chars) :
			GLFont(size, oversampling, path, shader, chars.pointer(), chars.size())
		{}

		/*
			Creates and imports a new font.
			* @param[in] size The size/height of a standard character
			* @param[in] oversampling How many times larger the characters should be rendered in the atlas for better quality, 1 (no extra) or 2 usually works fine
			* @param[in] path The path to the TTF file of the font
			* @param[in] shader A reference pointer to a shader that should be used for text rendering, this pointer is NOT deallocated by this instance
			* @param[in] chars An array containing UTF8 character codes to render to the atlas
			* @param[in] numChars The amount of characters in the "chars" array
		*/
		explicit GLFont(const float size, const unsigned int oversampling, const std::string_view& path, const GLShader* shader, const unsigned int* chars, const int numChars);

		/*
			No copying.
		*/
		GLFont(const GLFont&) = delete;

		/*
			Move constructor.
		*/
		inline GLFont(GLFont&& obj) noexcept :
			m_atlasSize(obj.m_atlasSize),
			m_height(obj.m_height),
			m_descent(obj.m_descent),
			m_shader(std::move(obj.m_shader)),
			m_lineLengths(std::move(obj.m_lineLengths)),
			m_geometry(std::move(obj.m_geometry)),
			m_atlas(std::move(obj.m_atlas)),
			m_atlasData(std::move(obj.m_atlasData)),
			m_glyphs(std::move(obj.m_glyphs))
		{}

		/*
			Destructor.
		*/
		inline virtual ~GLFont()
		{}

	public:

		/*
			Returns the size/height of the font.
		*/
		inline float getHeight() const
		{
			return m_height;
		}

		/*
			Returns the descent of the font.
		*/
		inline float getDescent() const
		{
			return m_descent;
		}

		/*
			Returns a pointer to the internal atlas texture.
		*/
		inline GLTexture* getAtlas()
		{
			return m_atlas.get();
		}

		/*
			Returns a pointer to the internal atlas texture.
		*/
		inline const GLTexture* getAtlas() const
		{
			return m_atlas.get();
		}

		/*
			Returns the glyph with the given UTF8 code, returns nullptr if the glyph cannot be found.
			* @param[in] code The UTF8 code of the glyph
		*/
		inline const GLFontGlyph* getGlyph(const unsigned int code) const
		{
			auto itr = m_glyphs.find(code);
			if (itr == m_glyphs.end())
				return nullptr;
			else
				return &(itr->second);
		}

		/*
			Returns a reference to the map of glyphs of the font.
		*/
		inline const std::unordered_map<unsigned int, GLFontGlyph>& getGlyphs() const
		{
			return m_glyphs;
		}
				
		/*
			Computes the area of the text from the given string.
			* @param[in] str The string to compute the area of
		*/
		inline virtual Dimension<float, 2> computeArea(const std::string_view& str) const
		{
			return computeArea(UTF8Iterator(str.begin()), UTF8Iterator(str.end()));
		}

		/*
			Computes the area of the text from the given iterators.
			* @param[in] beg The begining iterator
			* @param[in] end The ending iterator
		*/
		virtual Dimension<float, 2> computeArea(const UTF8Iterator& beg, const UTF8Iterator& end) const;

		/*
			Uploads the data of the font.
		*/
		virtual void upload();

		/*
			Renders the text from the given string.
			* @param[in] x The x position of the text
			* @param[in] y The y position of the text
			* @param[in] str The string to render
		*/
		inline virtual void render(const float x, const float y, const std::string_view& str)
		{
			render(x, y, str, Color::WHITE);
		}

		/*
			Renders the text from the given string.
			* @param[in] x The x position of the text
			* @param[in] y The y position of the text
			* @param[in] str The string to render
			* @param[in] color The color of the text
		*/
		inline virtual void render(const float x, const float y, const std::string_view& str, const Color& color)
		{
			render(x, y, GLFontAlignment::LEFT, str, color);
		}

		/*
			Renders the text from the given string.
			* @param[in] x The x position of the text
			* @param[in] y The y position of the text
			* @param[in] alignment The text alignment relative the (x, y) position
			* @param[in] str The string to render
		*/
		inline virtual void render(const float x, const float y, const GLFontAlignment alignment, const std::string_view& str)
		{
			render(x, y, alignment, str, Color::WHITE);
		}

		/*
			Renders the text from the given string.
			* @param[in] x The x position of the text
			* @param[in] y The y position of the text
			* @param[in] alignment The text alignment relative the (x, y) position
			* @param[in] str The string to render
			* @param[in] color The color of the text
		*/
		inline virtual void render(const float x, const float y, const GLFontAlignment alignment, const std::string_view& str, const Color& color)
		{
			render(x, y, alignment, UTF8Iterator(str.begin()), UTF8Iterator(str.end()), color);
		}

		/*
			Renders the text from the given iterators.
			* @param[in] x The x position of the text
			* @param[in] y The y position of the text
			* @param[in] alignment The text alignment relative the (x, y) position
			* @param[in] beg The begining iterator
			* @param[in] end The ending iterator
			* @param[in] color The color of the text
		*/
		inline virtual void render(const float x, const float y, const GLFontAlignment alignment, const UTF8Iterator& beg, const UTF8Iterator& end, const Color& color)
		{
			switch (alignment)
			{
			case CENTER:
				renderAdjusted(x, y, beg, end, color, 0.5f);
				break;
			case RIGHT:
				renderAdjusted(x, y, beg, end, color, 1.f);
				break;
			default:
				renderSimple(x, y, beg, end, color);
				break;
			}

		}

	public:

		/*
			No copying.
		*/
		GLFont& operator=(const GLFont&) = delete;

		/*
			Move-assignment operator.
		*/
		inline GLFont& operator=(GLFont&& obj) noexcept
		{
			m_atlasSize = obj.m_atlasSize;
			m_descent = obj.m_descent;
			m_height = obj.m_height;
			m_shader = std::move(obj.m_shader);
			m_lineLengths = std::move(obj.m_lineLengths);
			m_geometry = std::move(obj.m_geometry);
			m_atlas = std::move(obj.m_atlas);
			m_atlasData = std::move(obj.m_atlasData);
			m_glyphs = std::move(obj.m_glyphs);
			return *this;
		}

	protected:

		/*
			Computes the size of the atlas texture.
			* @param[in] scale The font scale from stb
			* @param[in] oversampling The oversampling
			* @param[in] info The font info
			* @param[in] chars A pointer to the first element in an array of UTF8 character code-points
			* @param[in] numChars The amount of elements the the char array
		*/
		virtual int computeAtlasSize(const float scale, const unsigned int oversampling, stbtt_fontinfo* info, const unsigned int* chars, const int numChars);

		/*
			Builds the font atlas.
			* @param[in] oversampling The oversampling
			* @param[in] info The font info
			* @param[in] ttfData The raw truetype font data
			* @param[in] chars A pointer to the first element in an array of UTF8 character code-points
			* @param[in] numChars The amount of elements the the char array
			* @param[out] packedChars An array of character information from the creation of the atlas
		*/
		virtual void buildAtlas(const unsigned int oversampling, stbtt_fontinfo* info, unsigned char* ttfData, const unsigned int* chars, const int numChars, stbtt_packedchar* packedChars);

		/*
			Retrieves various character information for the font.
			* @param[in] scale The font scale from stb
			* @param[in] info The font info
			* @param[in] chars A pointer to the first element in an array of UTF8 character code-points
			* @param[in] numChars The amount of elements the the char array
			* @param[in] packedChars An array of character information from the creation of the atlas
		*/
		virtual void computeInfo(const float scale, stbtt_fontinfo* info, const unsigned int* chars, const int numChars, stbtt_packedchar* packedChars);

		/*
			Creates the font geometry.
			* @param[in] chars A pointer to the first element in an array of UTF8 character code-points
			* @param[in] numChars The amount of elements the the char array
		*/
		virtual void createGeometry(const unsigned int* chars, const int numChars);

		/*
			Renders the text from the given iterators with left alignment.
			* @param[in] x The x position of the text
			* @param[in] y The y position of the text
			* @param[in] beg The begining iterator
			* @param[in] end The ending iterator
			* @param[in] color The color of the text
		*/
		virtual void renderSimple(const float x, const float y, const UTF8Iterator& beg, const UTF8Iterator& end, const Color& color);

		/*
			Renders the text from the given iterators with adjusted (center or right) alignment.
			* @param[in] x The x position of the text
			* @param[in] y The y position of the text
			* @param[in] beg The begining iterator
			* @param[in] end The ending iterator
			* @param[in] color The color of the text
			* @param[in] adjustment The adjustment, line pushback factor
		*/
		virtual void renderAdjusted(const float x, const float y, const UTF8Iterator& beg, const UTF8Iterator& end, const Color& color, const float adjustment);

	public:

		/*
			Returns standard ASCII characters, codes 32 - 127.
		*/
		inline static ArrayList<unsigned int> getASCIICharacters()
		{
			ArrayList<unsigned int> chars;
			for (unsigned int i = 32; i < 128; i++)
				chars.add(i);
			return chars;
		}

	};

}