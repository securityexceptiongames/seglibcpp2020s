#pragma once
#include <org/segames/library/dllmain.h>

#include <stb/stb_truetype.h>

namespace org::segames::library
{

	/*
		A class storing information for one glyph.

		* @author	Philip Rosberg
		* @since	2019-06-18
		* @edited	2020-06-24
	*/
	class SEG_API GLFontGlyph
	{
	protected:

		/*
			The index of the character in eventual buffers.
		*/
		int m_index;

		/*
			The total width of the glyph including buffer zones.
		*/
		float m_advance;

		/*
			The width of the glyph.
		*/
		float m_width;

		/*
			The height of the glyph.
		*/
		float m_height;

		/*
			The offset distance from the left most edge to the start of the glyph.
		*/
		float m_xOff;

		/*
			The offset distance from the upper edge to the start of the glyph.
		*/
		float m_yOff;

		/*
			The backing stb quad.
		*/
		stbtt_aligned_quad m_quad;

	public:

		/*
			Creates an empty/zeroed glyph info.
		*/
		inline GLFontGlyph() :
			m_index(-1),
			m_advance(0),
			m_width(0),
			m_height(0),
			m_xOff(0),
			m_yOff(0),
			m_quad()
		{}

		/*
			Creates a glyph info from the given data.
			* @param[in] index The index of the glyph in eventual buffers
			* @param[in] advance The glyph advance
			* @param[in] width The glyph width
			* @param[in] height The glyph height
			* @param[in] xOff The glyph offset from the left
			* @param[in] yOff The glyph offset from the top
			* @param[in] quad The aligned quad from stb
		*/
		inline explicit GLFontGlyph(const int index, const float advance, const float width, const float height, const float xOff, const float yOff, const stbtt_aligned_quad& quad) :
			m_index(index),
			m_advance(advance),
			m_width(width),
			m_height(height),
			m_xOff(xOff),
			m_yOff(yOff),
			m_quad(quad)
		{}

	public:

		/*
			Returns the index of the glyph.
		*/
		inline virtual int getIndex() const
		{
			return m_index;
		}

		/*
			Returns the advance of the glyph. Ie. total width of the glyph including buffer zones.
		*/
		inline virtual float getAdvance() const
		{
			return m_advance;
		}

		/*
			Returns the width of the glyph.
		*/
		inline virtual float getWidth() const
		{
			return m_width;
		}

		/*
			Returns the height of the glyph.
		*/
		inline virtual float getHeight() const
		{
			return m_height;
		}

		/*
			Returns the offset distance from the left most edge to the start of the glyph.
		*/
		inline virtual float getXOffset() const
		{
			return m_xOff;
		}

		/*
			Returns the offset distance from the upper edge to the start of the glyph.
		*/
		inline virtual float getYOffset() const
		{
			return m_yOff;
		}

		/*
			Returns the stb aligned quad of the glyph.
		*/
		inline virtual const stbtt_aligned_quad& getQuad() const
		{
			return m_quad;
		}

	public:

		/*
			The zero font glyph, ie. no glyph.
		*/
		static const GLFontGlyph EMPTY;

	};

}