#pragma once
#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#include <nuklear/nuklear.h>

namespace org::segames::library
{

	/*
		Interface for a nuklear instance configuration.

		* @author	Philip Rosberg
		* @since	2019-07-15
		* @edited	2019-07-15
	*/
	class NkInstanceConfig
	{
	public:

		/*
			Returns the height of the UI canvas, eg. the window height.
		*/
		virtual int getCanvasHeight() const = 0;
				
		/*
			The copy callback.
			* @param[in] handle The nuklear handle
			* @param[in] text The clipboard text
			* @param[in] len The text len
		*/
		virtual void copy(nk_handle handle, const char* text, const int len) const = 0;

		/*
			The paste callback.
			* @param[in] handle The nuklear handle
			* @param[out] edit The clipboard edit
		*/
		virtual void paste(nk_handle handle, struct nk_text_edit* edit) const = 0;

	};

}