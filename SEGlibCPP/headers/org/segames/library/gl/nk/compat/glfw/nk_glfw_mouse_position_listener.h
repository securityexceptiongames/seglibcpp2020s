#pragma once
#include <org/segames/library/glfw/glfw_mouse_position_listener.h>
#include <org/segames/library/gl/nk/nk_instance.h>

namespace org::segames::library
{

	/*
		A mouse position listener for GLFW and nuklear.

		* @author	Philip Rosberg
		* @since	2019-08-01
		* @edited	2020-06-30
	*/
	class NkGLFWMousePositionListener :
		public GLFWMousePositionListener
	{
	protected:

		/*
			The nuklear instance.
		*/
		NkInstance* m_instance;

	public:

		/*
			Creates a nuklear GLFW mouse position listener from the given context.
			* @param[in] instance The nuklear instance
		*/
		inline explicit NkGLFWMousePositionListener(NkInstance& instance) :
			m_instance(&instance)
		{}

	public:

		/*
			The method that is called when the listener is activated.
			* @param[in] mx The mouse x position
			* @param[in] my The mouse y position
		*/
		inline virtual void invoke(double mx, double my) override
		{
			m_instance->setCursor(Point2i(static_cast<int>(round(mx)), static_cast<int>(round(my))));
			nk_input_motion(m_instance->getContext(), m_instance->getCursor().x(), m_instance->getCursor().y());
			GLFWMousePositionListener::invoke(mx, my);
		}

	};

}