#pragma once
#include <org/segames/library/glfw/glfw_text_listener.h>
#include <org/segames/library/gl/nk/nk_instance.h>

namespace org::segames::library
{

	/*
		A text listener for GLFW and nuklear.

		* @author	Philip Rosberg
		* @since	2020-05-30
		* @edited	2020-06-30
	*/
	class NkGLFWTextListener :
		public GLFWTextListener
	{
	protected:

		/*
			The nuklear instance.
		*/
		NkInstance* m_instance;

	public:

		/*
			Creates a nuklear GLFW text listener from the given context.
			* @param[in] instance The nuklear instance
		*/
		inline explicit NkGLFWTextListener(NkInstance& instance) :
			m_instance(&instance)
		{}

	public:

		/*
			The method that is called when the listener is activated.
			* @param[in] codepoint The UTF8 codepoint
		*/
		inline virtual void invoke(unsigned int codepoint) override
		{
			nk_input_unicode(m_instance->getContext(), codepoint);
			GLFWTextListener::invoke(codepoint);
		}

	};

}