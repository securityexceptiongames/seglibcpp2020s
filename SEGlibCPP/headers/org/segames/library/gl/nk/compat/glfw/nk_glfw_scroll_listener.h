#pragma once
#include <org/segames/library/glfw/glfw_scroll_listener.h>
#include <org/segames/library/gl/nk/nk_instance.h>

namespace org::segames::library
{

	/*
		A scroll listener for GLFW and nuklear.

		* @author	Philip Rosberg
		* @since	2019-08-04
		* @edited	2020-06-30
	*/
	class NkGLFWScrollListener :
		public GLFWScrollListener
	{
	protected:

		/*
			The nuklear instance.
		*/
		NkInstance* m_instance;

	public:

		/*
			Creates a nuklear GLFW scroll listener from the given context.
			* @param[in] instance The nuklear instance
		*/
		inline explicit NkGLFWScrollListener(NkInstance& instance) :
			m_instance(&instance)
		{}

	public:

		/*
			The method that is called when the listener is activated.
			* @param[in] sx The scroll x value
			* @param[in] sy The scroll y value
		*/
		inline virtual void invoke(double sx, double sy)
		{
			nk_input_scroll(m_instance->getContext(), nk_vec2(static_cast<float>(sx), static_cast<float>(sy)));
			GLFWScrollListener::invoke(sx, sy);
		}

	};

}