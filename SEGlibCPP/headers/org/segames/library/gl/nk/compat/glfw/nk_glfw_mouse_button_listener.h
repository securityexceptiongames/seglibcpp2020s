#pragma once
#include <org/segames/library/glfw/glfw_mouse_button_listener.h>
#include <org/segames/library/gl/nk/nk_instance.h>

namespace org::segames::library
{

	/*
		A mouse button listener for GLFW and nuklear.

		* @author	Philip Rosberg
		* @since	2019-08-04
		* @edited	2020-06-30
	*/
	class NkGLFWMouseButtonListener :
		public GLFWMouseButtonListener
	{
	protected:

		/*
			The nuklear instance.
		*/
		NkInstance* m_instance;

	public:

		/*
			Creates a nuklear GLFW mouse button listener from the given context.
			* @param[in] instance The nuklear instance
		*/
		inline explicit NkGLFWMouseButtonListener(NkInstance& instance) :
			m_instance(&instance)
		{}

	public:

		/*
			The method that is called when the listener is activated.
			* @param[in] button The mouse button that was pressed or released
			* @param[in] action One of GLFW_PRESS of GLFW_RELEASE
			* @param[in] mods Bitfield describing which modifiers keys were held down
		*/
		inline virtual void invoke(int button, int action, int mods)
		{
			nk_buttons nkButton;
			switch (button)
			{
			case GLFW_MOUSE_BUTTON_RIGHT:
				nkButton = NK_BUTTON_RIGHT;
				break;
			case GLFW_MOUSE_BUTTON_MIDDLE:
				nkButton = NK_BUTTON_MIDDLE;
				break;
			default:
				nkButton = NK_BUTTON_LEFT;
				break;
			}

			nk_input_button(m_instance->getContext(), nkButton, m_instance->getCursor().x(), m_instance->getCursor().y(), action == GLFW_PRESS);
			GLFWMouseButtonListener::invoke(button, action, mods);
		}

	};

}