#pragma once
#include <org/segames/library/glfw/glfw_key_listener.h>
#include <org/segames/library/gl/nk/nk_instance.h>

namespace org::segames::library
{

	/*
		A key listener for GLFW and nuklear.

		* @author	Philip Rosberg
		* @since	2020-05-30
		* @edited	2020-06-30
	*/
	class SEG_API NkGLFWKeyListener :
		public GLFWKeyListener
	{
	protected:

		/*
			The nuklear instance.
		*/
		NkInstance* m_instance;

	public:

		/*
			Creates a nuklear GLFW key listener from the given context.
			* @param[in] instance The nuklear instance
		*/
		inline explicit NkGLFWKeyListener(NkInstance& instance) :
			m_instance(&instance)
		{}

	public:

		/*
			The method that is called when the listener is activated.
			* @param[in] key The keyboard key that was pressed or released
			* @param[in] scancode The system-specific scancode of the key
			* @param[in] action The key action. One of: GLFW_PRESS, GLFW_RELEASE, GLFW_REPEAT
			* @param[in] mods Bitfield describing which modifiers keys were held down
		*/
		virtual void invoke(int key, int scancode, int action, int mods) override;

	};

}