#pragma once
#include <org/segames/library/gl/nk/nk_instance_config.h>
#include <org/segames/library/dllmain.h>

#ifndef GLFW_INCLUDED
#define GLFW_DLL
#include <glad/glad.h>
#include <glfw/glfw3.h>
#define GLFW_INCLUDED
#endif

namespace org::segames::library
{

	/*
		A nuklear instance configuration for GLFW.

		* @author	Philip Rosberg
		* @since	2019-07-31
		* @edited	2020-06-30
	*/
	class NkGLFWInstanceConfig :
		public NkInstanceConfig
	{
	protected:

		/*
			The GLFW window.
		*/
		GLFWwindow* m_window;

	public:

		/*
			Creates a nuklear instance config for GLFW.
			* @param[in] window The GLFW window id
		*/
		inline explicit NkGLFWInstanceConfig(GLFWwindow* window) :
			m_window(window)
		{}
				
	public:

		/*
			Returns the GLFW window id
		*/
		inline GLFWwindow* getWindowID() const
		{
			return m_window;
		}

		/*
			Returns the height of the UI canvas, eg. the window height.
		*/
		inline virtual int getCanvasHeight() const
		{
			int w, h;
			glfwGetWindowSize(m_window, &w, &h);
			return h;
		}

		/*
			The copy callback.
			* @param[in] handle The nuklear handle
			* @param[in] text The clipboard text
			* @param[in] len The text len
		*/
		inline virtual void copy(nk_handle handle, const char* text, const int len) const
		{
			glfwSetClipboardString(m_window, text);
		}

		/*
			The paste callback.
			* @param[in] handle The nuklear handle
			* @param[out] edit The clipboard edit
		*/
		inline virtual void paste(nk_handle handle, struct nk_text_edit* edit) const
		{
			const char* text = glfwGetClipboardString(m_window);
			if (text)
				nk_textedit_paste(edit, text, nk_strlen(text));
		}

	};

}