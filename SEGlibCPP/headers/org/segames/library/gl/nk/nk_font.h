#pragma once
#include <org/segames/library/gl/text/gl_font.h>

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#include <nuklear/nuklear.h>

#include <unordered_map>
#include <unordered_set>

namespace org::segames::library
{

	/*
		Font wrapper class for fonts to be used by nuklear.

		* @author	Philip Rosberg
		* @since	2019-07-24
		* @edited	2020-06-30
	*/
	class SEG_API NkFont
	{
	protected:

		/*
			The number of created fonts, used to determine user handle id.
		*/
		static int s_numFonts;

		/*
			The mapped nuklear font wrappers.
		*/
		static std::unordered_map<int, NkFont*> s_mappedFonts;

		/*
			The texture ids used for texture maps.
		*/
		static std::unordered_map<GLuint, int> s_fontTextureIds;

	protected:

		/*
			The nuklear font information.
		*/
		nk_user_font m_info;

		/*
			Reference pointer to the gl-font to use.
		*/
		const GLFont* m_font;

	public:

		/*
			Creates a null nuklear font wrapper.
		*/
		inline NkFont() :
			m_info(),
			m_font(nullptr)
		{}

		/*
			Creates a nuklear font wrapper from the given GLFont.
			* @param[in] font The GLFont to use
		*/
		explicit NkFont(const GLFont& font);

		/*
			Copy constructor.
		*/
		NkFont(const NkFont& obj);

		/*
			Move constructor
		*/
		inline NkFont(NkFont&& obj) :
			m_info(std::move(obj.m_info)),
			m_font(std::move(obj.m_font))
		{
			obj.m_info = nk_user_font();
			obj.m_font = GL_NONE;
		}

		/*
			Destructor.
		*/
		virtual ~NkFont();

	public:

		/*
			Returns the nuklear user font.
		*/
		inline virtual const nk_user_font* getInfo() const
		{
			return &m_info;
		}

		/*
			Returns the backing GLFont.
		*/
		inline virtual const GLFont* getFont() const
		{
			return m_font;
		}

		/*
			Sets this font as the current for the given nuklear context.
			* @param[in] ctx The nuklear context
		*/
		inline virtual void setAsCurrent(nk_context* ctx) const
		{
			nk_style_set_font(ctx, getInfo());
		}

	public:

		/*
			Copy-assignment operator.
		*/
		inline NkFont& operator=(const NkFont& obj)
		{
			this->~NkFont();
			m_info = obj.m_info;
			m_font = obj.m_font;

			if (m_info.userdata.id != 0)
			{
				m_info.userdata.id = ++s_numFonts;
				s_mappedFonts.insert_or_assign(m_info.userdata.id, this);
			}

			if (m_info.texture.id)
				s_fontTextureIds.find(m_info.texture.id)->second++;
			return *this;
		}

		/*
			Move-assignment operator.
		*/
		inline NkFont& operator=(NkFont&& obj) noexcept
		{
			m_info = std::move(obj.m_info);
			m_font = std::move(obj.m_font);
			obj.m_info = nk_user_font();
			obj.m_font = nullptr;
			return *this;
		}

	protected:

		/*
			Queries font widths.
			* @param[in] handle The font nk_handle
			* @param[in] height The font height
			* @param[in] str C-string pointer to the characters to sum the widths of
			* @param[in] len The length of the string
		*/
		float queryWidth(nk_handle handle, float height, const char* str, int len);

		/*
			Queries glyphs.
			* @param[in] handle The font nk_handle
			* @param[in] height The font height
			* @param[out] glyph The nuklear glyph
			* @param[in] codepoint The nuklear glyph codepoint
			* @param[in] nextCodepoint The codepoint for the next nuklear glyph
		*/
		void queryGlyph(nk_handle handle, float height, nk_user_font_glyph* glyph, nk_rune codepoint, nk_rune nextCodepoint);

	protected:

		/*
			Query callback for font widths.
			* @param[in] handle The font nk_handle
			* @param[in] height The font height
			* @param[in] str C-string pointer to the characters to sum the widths of
			* @param[in] len The length of the string
		*/
		inline static float queryWidthCallback(nk_handle handle, float height, const char* str, int len)
		{
			auto itr = s_mappedFonts.find(handle.id);
			if (itr != s_mappedFonts.end())
				return itr->second->queryWidth(handle, height, str, len);
			return 0.0f;
		}

		/*
			Query callback for glyphs.
			* @param[in] handle The font nk_handle
			* @param[in] height The font height
			* @param[out] glyph The nuklear glyph
			* @param[in] codepoint The nuklear glyph codepoint
			* @param[in] nextCodepoint The codepoint for the next nuklear glyph
		*/
		inline static void queryGlyphCallback(nk_handle handle, float height, nk_user_font_glyph* glyph, nk_rune codepoint, nk_rune nextCodepoint)
		{
			auto itr = s_mappedFonts.find(handle.id);
			if (itr != s_mappedFonts.end())
				itr->second->queryGlyph(handle, height, glyph, codepoint, nextCodepoint);
		}

	public:

		/*
			Returns true if the given ID is a font texture.
			* @param[in] id The ID to check
		*/
		inline static bool isFontTexture(const GLuint id)
		{
			return s_fontTextureIds.find(id) != s_fontTextureIds.end();
		}

	};

}