#pragma once
#include <org/segames/library/gl/nk/nk_font.h>
#include <org/segames/library/gl/nk/nk_layout.h>
#include <org/segames/library/gl/nk/nk_instance_config.h>
#include <org/segames/library/gl/gl_buffer.h>
#include <org/segames/library/gl/gl_shader.h>
#include <org/segames/library/util/array_list.h>
#include <org/segames/library/math/point.h>

#include <unordered_map>

namespace org::segames::library
{

	/*
		Instance of a nuklear UI system.

		* @author	Philip Rosberg
		* @since	2019-07-15
		* @edited	2020-06-30
	*/
	class SEG_API NkInstance
	{
	public:

		/*
			The null draw texture.
		*/
		static nk_draw_null_texture DRAW_NULL_TEXTURE;

		/*
			The draw vertex layout element configuration.
		*/
		static nk_draw_vertex_layout_element DRAW_VERTEX_LAYOUT_ELEMENT[4];

		/*
			The default nuklear geometry convert configuration.
		*/
		static nk_convert_config DEFAULT_CONVERT_CONFIG;

		/*
			The default nuklear shader.
		*/
		static GLShader DEFAULT_SHADER;

	protected:

		/*
			The size of the OpenGL buffer used for rendering.
		*/
		static nk_size s_bufferSize;

		/*
			The number of instances created (used to evaluate handle id).
		*/
		static int s_numInstaces;

		/*
			Instances mapped after their context's handle id.
		*/
		static std::unordered_map<int, NkInstance*> s_mappedInstances;

	protected:

		/*
			The cursor position.
		*/
		Point2i m_cursor;

		/*
			The geometry data buffer.
		*/
		std::unique_ptr<char[]> m_geometryData;

		/*
			The index data buffer.
		*/
		std::unique_ptr<char[]> m_indexData;

		/*
			The nuklear context.
		*/
		std::unique_ptr<nk_context> m_context;

		/*
			The nuklear draw command buffer.
		*/
		std::unique_ptr<nk_buffer> m_drawCommands;

		/*
			Reference pointer to the shader to use.
		*/
		const GLShader* m_shader;

		/*
			The geometry buffer.
		*/
		std::unique_ptr<GLBuffer> m_geometry;

		/*
			The element buffer.
		*/
		std::unique_ptr<GLBuffer> m_indices;

		/*
			Reference pointer to the instance configuration.
		*/
		const NkInstanceConfig* m_config;

		/*
			The (references to the) UI layouts to update.
		*/
		ArrayList<NkLayout*> m_layouts;

	public:
				
		/*
			Creates an empty nuklear instance.
		*/
		inline NkInstance() :
			m_geometryData(nullptr),
			m_indexData(nullptr),
			m_context(nullptr),
			m_drawCommands(nullptr),
			m_shader(nullptr),
			m_geometry(nullptr),
			m_indices(nullptr),
			m_config(nullptr),
			m_layouts()
		{}

		/*
			Creates a nuklear instance.
			* @param[in] shader The shader to use
			* @param[in] config The configuration to use for this instance
		*/
		explicit NkInstance(const GLShader& shader, const NkInstanceConfig& config);

		/*
			No copying.
		*/
		NkInstance(const NkInstance&) = delete;

		/*
			Move constructor.
		*/
		inline NkInstance(NkInstance&& obj) noexcept :
			m_geometryData(std::move(obj.m_geometryData)),
			m_indexData(std::move(obj.m_indexData)),
			m_context(std::move(obj.m_context)),
			m_drawCommands(std::move(obj.m_drawCommands)),
			m_shader(obj.m_shader),
			m_geometry(std::move(obj.m_geometry)),
			m_indices(std::move(obj.m_indices)),
			m_config(obj.m_config),
			m_layouts(std::move(obj.m_layouts))
		{
			obj.m_shader = nullptr;
			obj.m_config = nullptr;
		}

		/*
			Destructor.
		*/
		inline ~NkInstance()
		{
			if (m_context)
				s_mappedInstances.erase(m_context->clip.userdata.id);
		}

	public:

		/*
			Returns the stored cursor position.
		*/
		virtual const Point2i& getCursor() const
		{
			return m_cursor;
		}

		/*
			Returns a reference to the nuklear context.
		*/
		virtual nk_context* getContext() const
		{
			return m_context.get();
		}

		/*
			Returns the list of UI layouts currently being rendered.
		*/
		virtual ArrayList<NkLayout*>& getLayoutList()
		{
			return m_layouts;
		}

		/*
			Sets the cursor position.
			* @param[in] pos The cursor position
		*/
		virtual void setCursor(const Point2i& pos)
		{
			m_cursor = pos;
		}

		/*
			Updates and renders the UI.
		*/
		virtual void update() const;
				
	protected:

		/*
			Works around the window close bug of the current nuklear version.
		*/
		virtual void workAroundWindowCloseBug__() const;

	public:

		/*
			No copying.
		*/
		NkInstance& operator=(const NkInstance&) = delete;

		/*
			Move-assignment operator.
		*/
		inline NkInstance& operator=(NkInstance&& obj) noexcept
		{
			m_geometryData = std::move(obj.m_geometryData);
			m_indexData = std::move(obj.m_indexData);
			m_context = std::move(obj.m_context);
			m_drawCommands = std::move(obj.m_drawCommands);
			m_shader = obj.m_shader;
			m_geometry = std::move(obj.m_geometry);
			m_indices = std::move(obj.m_indices);
			m_config = obj.m_config;
			m_layouts = std::move(obj.m_layouts);
			obj.m_shader = nullptr;
			obj.m_config = nullptr;
			return *this;
		}

	protected:

		/*
			Creates and return the default convert config.
		*/
		static nk_convert_config makeDefaultConvertConfig();

		/*
			The nuklear paste callback.
		*/
		inline static void pasteCallback(nk_handle handle, struct nk_text_edit* edit)
		{
			auto itr = s_mappedInstances.find(handle.id);
			if (itr != s_mappedInstances.end())
				itr->second->m_config->paste(handle, edit);
		}

		/*
			The nuklear copy callback.
		*/
		inline static void copyCallback(nk_handle handle, const char* text, int len)
		{
			auto itr = s_mappedInstances.find(handle.id);
			if (itr != s_mappedInstances.end())
				itr->second->m_config->copy(handle, text, len);
		}

		/*
			Ensures that the draw null texture is created.
		*/
		static void ensureDrawNullTexture();

		/*
			Ensures that the default is created.
		*/
		static void ensureDefaultShader();

	};

}