#pragma once
#include <org/segames/library/dllmain.h>

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#include <nuklear/nuklear.h>

namespace org::segames::library
{

	/*
		An abstract class/interface for handling nuklear user 
		interface layouts.

		* @author	Philip Rosberg
		* @since	2019-06-30
		* @edited	2020-06-30
	*/
	class SEG_API NkLayout
	{
	private:

		/*
			True if the layout has been initialized.
		*/
		bool m_initialized;
			
	public:

		/*
			Constructs the base nk-layout.
		*/
		inline NkLayout() :
			m_initialized(false)
		{}

	protected:
				
		/*
			The initialization method of the interface structure (if any needs to be done)
			Return true when finished initializing.
			* @param[in] ctx The nuklear context pointer
		*/
		virtual bool initialize(nk_context* ctx) = 0;

		/*
			The update method of the interface structure, that builds the interface using 
			nuklear methods.
			* @param[in] ctx The nuklear context pointer
		*/
		virtual void layout(nk_context* ctx) = 0;

	public:
				
		/*
			The internal update method called from NkInstance.
			* @param[in] ctx The nuklear context pointer
		*/
		inline virtual void updateInternal(nk_context* ctx)
		{
			if (!m_initialized)
			{
				m_initialized = initialize(ctx);
				if (m_initialized)
					layout(ctx);
			}
			else
				layout(ctx);
		}

	};

}