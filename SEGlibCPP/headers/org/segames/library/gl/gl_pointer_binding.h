#pragma once
#include <org/segames/library/gl/gl_pointer_inf.h>
#include <org/segames/library/gl/gl_data_type.h>
#include <org/segames/library/gl/gl_exception.h>

namespace org::segames::library
{

	typedef void(GLPointerInf::*InfActivateFun)() const;

	/*
		A class for storing the binding of one pointer information object.

		* @author	Philip Rosberg
		* @since	2019-06-10
		* @edited	2020-06-22
	*/
	class SEG_API GLPointerBinding final
	{
	protected:

		/*
			The type of the binding.
		*/
		GLDataType m_type;

		/*
			The set[...]Pointer() mehtod from m_inf.
		*/
		InfActivateFun m_infSetMethod;

		/*
			The pointer information.
		*/
		GLPointerInf m_inf;

	public:

		/*
			Creates a new pointer binding.
		*/
		inline explicit GLPointerBinding() :
			GLPointerBinding(GLDataType::VERTEX, GLPointerInf::TWO_FLOAT_POINTER)
		{}

		/*
			Creates a new pointer information binding of the specified type and information.
			* @param[in] type The type of the binding
			* @param[in] inf The pointer information
		*/
		inline explicit GLPointerBinding(const GLDataType type, const GLPointerInf& inf) :
			m_type(type),
			m_inf(inf)
		{
			switch (m_type)
			{
			case GLDataType::VERTEX:
				m_infSetMethod = &GLPointerInf::setVertexPointer;
				break;
			case GLDataType::NORMAL:
				m_infSetMethod = &GLPointerInf::setNormalPointer;
				break;
			case GLDataType::COLOR:
				m_infSetMethod = &GLPointerInf::setColorPointer;
				break;
			case GLDataType::TEX_COORD:
				m_infSetMethod = &GLPointerInf::setTexCoordPointer;
				break;
			case GLDataType::INDEX:
				m_infSetMethod = &GLPointerInf::setIndexPointer;
				break;
			case GLDataType::EDGE_FLAG:
				m_infSetMethod = &GLPointerInf::setEdgeFlagPointer;
				break;
			default:
				m_infSetMethod = nullptr;
				break;
			}

		}

	public:

		/*
			Returns the type of the binding.
		*/
		inline GLDataType getType() const
		{
			return m_type;
		}

		/*
			Returns the pointer information.
		*/
		inline const GLPointerInf& getInf() const
		{
			return m_inf;
		}

		/*
			Sets the OpenGL context pointer to the data contained in this instance.
		*/
		inline void setPointer() const
		{
#ifdef SEG_API_GL_MISC_EXCEPTIONS
			if (m_type == GLDataType::ATTRIBUTE)
				throw GLException("Cannot bind attribute pointer information without attribute id.");
#endif
			(m_inf.*m_infSetMethod)();
		}

		/*
			Sets the OpenGL context pointer to the data contained in this instance for attributes.
			* @param[in] location The location id of the vertex attribute
		*/
		inline void setPointer(const GLuint location) const
		{
			m_inf.setAttribPointer(location);
		}

	public:

		/*
			Equals operator.
		*/
		inline bool operator==(const GLPointerBinding& obj) const
		{
			return (m_inf == obj.m_inf) && (m_type == obj.m_type);
		}

		/*
			Not-equals operator.
		*/
		inline bool operator!=(const GLPointerBinding& obj) const
		{
			return !operator==(obj);
		}

	};

}