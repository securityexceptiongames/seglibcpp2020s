#pragma once
#include <org/segames/library/gl/gl_bindable.h>
#include <org/segames/library/gl/gl_pointer_binding.h>
#include <org/segames/library/util/array_list.h>
#include <org/segames/library/virtually_destructible.h>

namespace org::segames::library
{

	/*
		A standard OpenGL buffer object.
		Any OpenGL resource is deallocated on deletion of this object.
				
		* @author	Philip Rosberg
		* @since	2019-06-10
		* @edited	2021-04-04
	*/
	class SEG_API GLBuffer :
		public GLBindable,
		public VirtuallyDestructible
	{
	protected:

		/*
			The id of the buffer.
		*/
		GLuint m_id;

		/*
			The buffer type.
		*/
		GLenum m_bufferType;

		/*
			The buffer upload length in bytes.
		*/
		GLsizeiptr m_uploadLength;

		/*
			The next image in the image chain. (Ie. copies that use the same OpenGL resource)
		*/
		GLBuffer* m_nextImage;

		/*
			The pointer information bindings.
		*/
		ArrayList<std::unique_ptr<GLPointerBinding>> m_infBindings;

	public:

		/*
			Creates a new buffer object.
		*/
		inline GLBuffer() :
			m_id(0),
			m_bufferType(GL_ARRAY_BUFFER),
			m_uploadLength(0),
			m_nextImage(this),
			m_infBindings(ArrayList<std::unique_ptr<GLPointerBinding>>(1))
		{
			for (size_t i = 0; i < to_underlying(GLDataType::ATTRIBUTE); i++)
				m_infBindings.add(nullptr);
		}

		/*
			Creates a new buffer object with the given information.
			* @param[in] type The type of the buffer data, ie. how to use the buffer
			* @param[in] inf The pointer information
		*/
		inline GLBuffer(const GLDataType type, const GLPointerInf& inf) :
			GLBuffer()
		{
			this->setPointerBinding(type, inf);
		}

		/*
			No copying.
		*/
		GLBuffer(const GLBuffer&) = delete;

		/*
			Move constructor.
		*/
		inline GLBuffer(GLBuffer&& obj) noexcept :
			m_id(obj.m_id),
			m_bufferType(obj.m_bufferType),
			m_uploadLength(obj.m_uploadLength),
			m_nextImage(obj.m_nextImage),
			m_infBindings(std::move(obj.m_infBindings))
		{
			obj.m_id = GL_NONE;
			obj.m_uploadLength = 0;
			obj.m_nextImage = &obj;
		}

		/*
			Destructor.
			Handles release of OpenGL resources.
		*/
		inline virtual ~GLBuffer()
		{
			this->destroy();
		}

	public:

		/*
			Returns the id of the buffer.
		*/
		inline virtual GLuint getID() const
		{
			return m_id;
		}

		/*
			Returns the type of the buffer.
		*/
		inline virtual GLenum getType() const
		{
			return m_bufferType;
		}

		/*
			Returns the length, in bytes, of the last uploaded version of the buffer, 0 if no upload has occured.
		*/
		inline virtual GLsizeiptr getUploadLength() const
		{
			return m_uploadLength;
		}

		/*
			Binds this buffer to the OpenGL context.
		*/
		inline virtual const GLBuffer& bind() const override
		{
			glBindBuffer(m_bufferType, m_id);
			return *this;
		}

		/*
			Releases this buffer (all buffers) from the OpenGL context.
		*/
		inline virtual const GLBuffer& release() const override
		{
			glBindBuffer(m_bufferType, GL_NONE);
			return *this;
		}

		/*
			Sets the pointer information for OpenGL.
			This method will throw an exception if no pointer binding is set.
			NOTE! The OpenGL resource should be bound before this operation.
		*/
		virtual const GLBuffer& setPointerInf() const;

		/*
			Sets the buffer type.
			* @param[in] type The type of the buffer, eg. GL_ARRAY_BUFFER or GL_ELEMENT_ARRAY_BUFFER
		*/
		inline virtual GLBuffer& setType(const GLenum type)
		{
			m_bufferType = type;
			return *this;
		}

		/*
			Sets the pointer information for the given buffer data type.
			For attributes, use setPointerInf(const GLDataType, const GLPointerInf&, const GLuint).
			* @param[in] type The type of the buffer data, ie. how to use the buffer
			* @param[in] inf The pointer information
		*/
		virtual GLBuffer& setPointerBinding(const GLDataType type, const GLPointerInf& inf);

		/*
			Sets the pointer information for the given buffer data type. Attributes.
			* @param[in] type The type of the buffer data, ie. how to use the buffer
			* @param[in] inf The pointer information
			* @param[in] location The attribute location
		*/
		virtual GLBuffer& setPointerBinding(const GLDataType type, const GLPointerInf& inf, const GLuint location);

		/*
			Removes the pointer information of the given type.
			For attributes, use removePointerInf(const GLDataType, const GLuint).
			* @param[in] type The type of the buffer data, ie. how to use the buffer
		*/
		virtual GLBuffer& removePointerBinding(const GLDataType type);

		/*
			Removes the pointer information of the given type. Attributes.
			* @param[in] type The type of the buffer data, ie. how to use the buffer
			* @param[in] location The attribute location
		*/
		virtual GLBuffer& removePointerBinding(const GLDataType type, const GLuint location);

		/*
			Makes *this* GLBuffer into an image buffer mapping the same OpenGL resource as the given buffer.
			NOTE! The OpenGL resource is released when the last image is destroyed.
			NOTE! Pointer binding information is NOT transfered.
			* @param[in] obj The buffer to share OpenGL resource with
		*/
		virtual void makeIntoImageOf(const GLBuffer& obj);

		/*
			Uploads the given data to the buffer.
			* @param[in] len the length of the data
			* @param[in] data Pointer to the block of data to upload
		*/
		inline virtual void upload(const GLsizeiptr len, const void* data)
		{
			upload(len, data, GL_STATIC_DRAW);
		}

		/*
			Uploads the given data to the buffer.
			* @param[in] len the length of the data
			* @param[in] data Pointer to the block of data to upload
			* @param[in] mode The buffer mode, GL_STATIC_DRAW, GL_DYNAMIC_DRAW, etc.
		*/
		virtual void upload(const GLsizeiptr len, const void* data, const GLenum mode);

		/*
			Uploads the given data into a sub-section of the buffer.
			NOTE! The sub-section must be contained in an existing buffer's data.
			NOTE! The OpenGL resource should be bound before this operation.
			* @param[in] offset The offset in the buffer
			* @param[in] len the length of the data
			* @param[in] data Pointer to the block of data to upload
		*/
		virtual void uploadSubData(const GLintptr offset, const GLsizeiptr len, const void* data) noexcept(false);

	public:

		/*
			No copying.
		*/
		GLBuffer& operator=(const GLBuffer&) = delete;

		/*
			Move-assignment operator.
		*/
		inline GLBuffer& operator=(GLBuffer&& obj) noexcept
		{
			destroy();
			m_id = obj.m_id;
			m_bufferType = obj.m_bufferType;
			m_uploadLength = obj.m_uploadLength;
			m_nextImage = obj.m_nextImage;
			m_infBindings = std::move(obj.m_infBindings);
			obj.m_id = GL_NONE;
			obj.m_uploadLength = 0;
			obj.m_nextImage = &obj;
			return *this;
		}

		/*
			Equals operator.
		*/
		inline bool operator==(const GLBuffer& obj) const
		{
			return (m_id == obj.m_id) &&
				(m_bufferType == obj.m_bufferType) &&
				(m_nextImage == obj.m_nextImage) &&
				(m_infBindings.size() == obj.m_infBindings.size());
		}

		/*
			Not-equals operator.
		*/
		inline bool operator!=(const GLBuffer& obj) const
		{
			return !operator==(obj);
		}

	protected:

		/*
			Ensures that the OpenGL resource exists.
		*/
		virtual void create() noexcept;

		/*
			Deletes the OpenGL resource. Or if this buffer is part of an image chain,
			then this method removes this entry from the chain.
		*/
		virtual void destroy() noexcept;

	};

}