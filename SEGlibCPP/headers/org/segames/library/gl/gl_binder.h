#pragma once
#include <org/segames/library/gl/gl_bindable.h>

namespace org::segames::library
{

	/*
		A semi-lock binding a OpenGL bindable for its life-time.

		* @author	Philip Rosberg
		* @since	2020-06-29
		* @edited	2020-06-29
	*/
	class GLBinder final
	{
	private:

		/*
			The bindable.
		*/
		const GLBindable& m_binable;

	public:

		/*
			Creates and binds the given bindable.
			* @param[in] bindable The bindable to bind
		*/
		inline GLBinder(const GLBindable& bindable) :
			m_binable(bindable)
		{
			m_binable.bind();
		}

		/*
			No copying.
		*/
		GLBinder(const GLBinder&) = delete;

		/*
			Destructor.
		*/
		inline ~GLBinder()
		{
			m_binable.release();
		}

	public:

		/*
			No copying.
		*/
		GLBinder& operator=(const GLBinder&) = delete;

	public:

		/*
			Creates a binder and binds the given bindable.
			* @param[in] bindable The bindable to bind
		*/
		inline static GLBinder bind(const GLBindable& bindable)
		{
			return GLBinder(bindable);
		}

	};

}