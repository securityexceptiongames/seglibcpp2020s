#pragma once
#include <org/segames/library/gl/gl_bindable.h>
#include <org/segames/library/io/file.h>
#include <org/segames/library/dllmain.h>

#include <glad/glad.h>

namespace org::segames::library
{

	/*
		A standard OpenGL shader.
		Any OpenGL resource is deallocated on deletion of this object.
				
		* @author	Philip Rosberg
		* @since	2019-06-08
		* @edited	2021-04-04
	*/
	class SEG_API GLShader :
		public GLBindable
	{
	protected:

		/*
			The shader program id.
		*/
		GLuint m_id;

		/*
			The vertex shader data.
		*/
		std::unique_ptr<std::string> m_vData;

		/*
			The geometry shader data.
		*/
		std::unique_ptr<std::string> m_gData;

		/*
			The fragment shader data.
		*/
		std::unique_ptr<std::string> m_fData;

	public:

		/*
			Creates a new shader object.
		*/
		inline GLShader() :
			m_id(0),
			m_vData(nullptr),
			m_gData(nullptr),
			m_fData(nullptr)
		{}

		/*
			Creates and uploads the shader with the given sources and name.
			* @param[in] name The name of the shader when uploaded
			* @param[in] vertSrc The vertex shader source
			* @param[in] geomSrc The geometry shader source
			* @param[in] fragSrc The fragment shader source
		*/
		inline explicit GLShader(const std::string& name, const std::string& vertSrc, const std::string& geomSrc, const std::string& fragSrc) :
			GLShader()
		{
			setVertexData(vertSrc);
			setGeometryData(geomSrc);
			setFragmentData(fragSrc);
			upload(name);
		}

		/*
			Creates and uploads the shader with the given source files and name.
			* @param[in] name The name of the shader when uploaded
			* @param[in] vertSrc The vertex shader source file
			* @param[in] geomSrc The geometry shader source file
			* @param[in] fragSrc The fragment shader source file
		*/
		inline explicit GLShader(const std::string& name, const File& vertSrc, const File& geomSrc, const File& fragSrc) :
			GLShader()
		{
			if (!vertSrc.isNull()) loadVertexData(vertSrc);
			if (!geomSrc.isNull()) loadGeometryData(geomSrc);
			if (!fragSrc.isNull()) loadFragmentData(fragSrc);
			upload(name);
		}

		/*
			No copying.
		*/
		GLShader(const GLShader&) = delete;

		/*
			Move constructor.
		*/
		inline GLShader(GLShader&& obj) noexcept :
			m_id(obj.m_id),
			m_vData(std::move(obj.m_vData)),
			m_gData(std::move(obj.m_gData)),
			m_fData(std::move(obj.m_fData))
		{
			obj.m_id = GL_NONE;
		}

		/*
			Destructor.
			Handles release of OpenGL resources.
		*/
		inline virtual ~GLShader()
		{
			if (m_id)
				glDeleteProgram(m_id);
		}
			
	public:

		/*
			Returns the id of the shader.
		*/
		inline virtual GLuint getID() const
		{
			return m_id;
		}

		/*
			Sets the stored vertex shader data.
			All deallocation responsibilities are given to this instance.
			* @param[in] src The source data
		*/
		inline virtual void setVertexData(const std::string& src)
		{
			if (src.size() > 0)
				m_vData.reset(new std::string(src));
			else
				m_vData.reset();
		}

		/*
			Sets the stored geometry shader data.
			All deallocation responsibilities are given to this instance.
			* @param[in] src The source data
		*/
		inline virtual void setGeometryData(const std::string& src)
		{
			if (src.size() > 0)
				m_gData.reset(new std::string(src));
			else
				m_gData.reset();
		}

		/*
			Sets the stored fragment shader data.
			All deallocation responsibilities are given to this instance.
			* @param[in] src The source data
		*/
		inline virtual void setFragmentData(const std::string& src)
		{
			if (src.size() > 0)
				m_fData.reset(new std::string(src));
			else
				m_fData.reset();
		}

		/*
			Loads vertex shader data from the file at the given path.
			* @param[in] path The path of the file to load from
		*/
		inline virtual void loadVertexData(const std::string& path)
		{
			loadVertexData(File(path));
		}

		/*
			Loads geometry shader data from the file at the given path.
			* @param[in] path The path of the file to load from
		*/
		inline virtual void loadGeometryData(const std::string& path)
		{
			loadGeometryData(File(path));
		}

		/*
			Loads fragment shader data from the file at the given path.
			* @param[in] path The path of the file to load from
		*/
		inline virtual void loadFragmentData(const std::string& path)
		{
			loadFragmentData(File(path));
		}

		/*
			Loads vertex shader data from the file at the given path.
			* @param[in] file The file to load from
		*/
		inline virtual void loadVertexData(const File& file)
		{
			setVertexData(readSource(file));
		}

		/*
			Loads geometry shader data from the file at the given path.
			* @param[in] file The file to load from
		*/
		inline virtual void loadGeometryData(const File& file)
		{
			setGeometryData(readSource(file));
		}

		/*
			Loads fragment shader data from the file at the given path.
			* @param[in] file The file to load from
		*/
		inline virtual void loadFragmentData(const File& file)
		{
			setFragmentData(readSource(file));
		}

		/*
			Binds this shader to the OpenGL context.
		*/
		inline virtual const GLShader& bind() const override
		{
			glUseProgram(m_id);
			return *this;
		}

		/*
			Releases this shader (all shaders) from the OpenGL context.
		*/
		inline virtual const GLShader& release() const override
		{
			glUseProgram(GL_NONE);
			return *this;
		}

		/*
			Uploads the currently set data. Returns true if new data was uploaded, otherwise false.
			Uses an identification name based on the time.
		*/
		virtual bool upload();

		/*
			Uploads the currently set data. Returns true if new data was uploaded, otherwise false.
			* @param[in] name Identification name of the shader for displaying in upload/compile errors
		*/
		virtual bool upload(const std::string& name);

	public:

		/*
			No copying.
		*/
		GLShader& operator=(const GLShader&) = delete;

		/*
			Move-assignment operator.
		*/
		inline GLShader& operator=(GLShader&& obj) noexcept
		{
			m_id = obj.m_id;
			m_vData = std::move(obj.m_vData);
			m_gData = std::move(obj.m_gData);
			m_fData = std::move(obj.m_fData);
			obj.m_id = GL_NONE;
			return *this;
		}

	protected:

		/*
			Compiles the given source into a shader.
			* @param[in] type The type of the shader, GL_VERTEX_SHADER etc.
			* @param[in] name The identification name for error outputs
			* @param[in] src The shader source
		*/
		static GLuint createAndCompileShader(const GLenum type, const std::string& name, const std::string& src);

		/*
			Reads the content of the given file and returns it as a dynamically allocated string.
			* @param[in] file The file to read
		*/
		static std::string readSource(const File& file);

	};

}