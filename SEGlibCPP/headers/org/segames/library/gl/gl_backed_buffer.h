#pragma once
#include <org/segames/library/gl/gl_buffer.h>
#include <org/segames/library/util/array_list.h>

namespace org::segames::library
{

	/*
		An OpenGL buffer object with a backing ArrayList.
		Any OpenGL resource is deallocated on deletion of this object.

		* @author	Philip Rosberg
		* @since	2019-06-10
		* @edited	2020-06-22
	*/
	template<typename T>
	class GLBackedBuffer :
		public GLBuffer,
		public ArrayList<T>
	{
	public:

		/*
			Creates a new backed buffer object.
		*/
		inline GLBackedBuffer() :
			GLBuffer(),
			ArrayList<T>()
		{}

		/*
			Creates a new backed buffer object with the given initial capacity.
			* @param[in] capacity The initial capacity of the backing ArrayList
		*/
		inline explicit GLBackedBuffer(const size_t capacity) :
			GLBuffer(),
			ArrayList<T>(capacity)
		{}

		/*
			Creates a new backed buffer object with the given information.
			* @param[in] type The type of the buffer data, ie. how to use the buffer
			* @param[in] inf The pointer information
		*/
		inline explicit GLBackedBuffer(const GLDataType type, const GLPointerInf& inf) :
			GLBuffer(type, inf),
			ArrayList<T>()
		{}

		/*
			Creates a new backed buffer object with the given information and initial capacity.
			* @param[in] capacity The initial capacity of the backing ArrayList
			* @param[in] type The type of the buffer data, ie. how to use the buffer
			* @param[in] inf The pointer information
		*/
		inline explicit GLBackedBuffer(const size_t capacity, const GLDataType type, const GLPointerInf& inf) :
			GLBuffer(type, inf),
			ArrayList<T>(capacity)
		{}

	public:

		/*
			Returns the number of elements uploaded in the last upload, 0 if no upload has occured.
		*/
		inline virtual GLsizeiptr getUploadCount() const
		{
			return getUploadLength() / sizeof(T);
		}

		/*
			Binds this buffer to the OpenGL context.
		*/
		inline virtual const GLBackedBuffer& bind() const override
		{
			GLBuffer::bind();
			return *this;
		}

		/*
			Releases this buffer (all buffers) from the OpenGL context.
		*/
		inline virtual const GLBackedBuffer& release() const override
		{
			GLBuffer::release();
			return *this;
		}

		/*
			Chainable add method. Adds the given value to the backing ArrayList.
			* @param[in] value The value to add
		*/
		inline virtual GLBackedBuffer& push(const T& value)
		{
			ArrayList<T>::add(value);
			return *this;
		}

		/*
			Uploads the internally stored data.
		*/
		inline virtual void upload()
		{
			upload(GL_STATIC_DRAW);
		}

		/*
			Uploads the internally stored data with the given buffer mode.
			* @param[in] mode The buffer mode, GL_STATIC_DRAW, GL_DYNAMIC_DRAW, etc.
		*/
		inline virtual void upload(const GLenum mode)
		{
			upload(ArrayList<T>::size() * sizeof(T), ArrayList<T>::pointer(), mode);
			ArrayList<T>::clear();
		}

		/*
			Uploads the given data to the buffer.
			* @param[in] len the length of the data
			* @param[in] data Pointer to the block of data to upload
		*/
		inline virtual void upload(const GLsizeiptr len, const void* data) override
		{
			upload(len, data, GL_STATIC_DRAW);
		}

		/*
			Uploads the given data to the buffer.
			* @param[in] len the length of the data
			* @param[in] data Pointer to the block of data to upload
			* @param[in] mode The buffer mode, GL_STATIC_DRAW, GL_DYNAMIC_DRAW, etc.
		*/
		inline virtual void upload(const GLsizeiptr len, const void* data, const GLenum mode) override
		{
			GLBuffer::upload(len, data, mode);
		}

		/*
			Uploads the given data into a sub-section of the buffer.
			NOTE! The sub-section must be contained in an existing buffer's data.
			NOTE! The OpenGL resource should be bound before this operation.
			* @param[in] offset The offset in the buffer
			* @param[in] len the length of the data
			* @param[in] data Pointer to the block of data to upload
		*/
		inline virtual void uploadSubData(const GLintptr offset, const GLsizeiptr len, const void* data) noexcept(false) override
		{
			GLBuffer::uploadSubData(offset, len, data);
		}

	};

#ifndef SEG_API_GL_NO_TYPEDEFS

	using GLDoubleBuffer = GLBackedBuffer<GLdouble>;
	using GLFloatBuffer = GLBackedBuffer<GLfloat>;
	using GLLongBuffer = GLBackedBuffer<GLint64>;
	using GLIntBuffer = GLBackedBuffer<GLint>;
	using GLShortBuffer = GLBackedBuffer<GLshort>;
				
#endif

}