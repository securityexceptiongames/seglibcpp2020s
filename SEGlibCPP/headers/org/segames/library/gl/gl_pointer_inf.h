#pragma once
#include <org/segames/library/dllmain.h>

#include <glad/glad.h>

namespace org::segames::library
{

	/*
		A storage type for specifying data pointer information.

		* @author	Philip Rosberg
		* @since	2019-06-03
		* @edited	2020-06-22
	*/
	class SEG_API GLPointerInf final
	{
	public:

		/*
			Standard configuration for a 1D float array.
		*/
		static GLPointerInf ONE_FLOAT_POINTER;

		/*
			Standard configuration for a 2D float array.
		*/
		static GLPointerInf TWO_FLOAT_POINTER;

		/*
			Standard configuration for a 3D float array.
		*/
		static GLPointerInf THREE_FLOAT_POINTER;

		/*
			Standard configuration for a 4D float array.
		*/
		static GLPointerInf FOUR_FLOAT_POINTER;

		/*
			Standard configuration for a 1D integer array.
		*/
		static GLPointerInf ONE_INT_POINTER;

		/*
			Standard configuration for a 2D integer array.
		*/
		static GLPointerInf TWO_INT_POINTER;

		/*
			Standard configuration for a 3D integer array.
		*/
		static GLPointerInf THREE_INT_POINTER;

		/*
			Standard configuration for a 4D integer array.
		*/
		static GLPointerInf FOUR_INT_POINTER;

	private:

		/*
			True if the values should be normalized before access (only attributes)
		*/
		GLboolean m_normalized;

		/*
			The type of the data, GL_FLOAT, GL_INTEGER etc.
		*/
		GLenum m_type;

		/*
			Specifies the number of components per generic vertex attribute. Must be 1, 2, 3, 4.
		*/
		GLint m_size;

		/*
			Specifies the byte offset between consecutive vertices. If stride is 0, the vertices are understood to be tightly packed in the array.
		*/
		GLint m_stride;

		/*
			The offset of the first piece of data in the buffer.
		*/
		size_t m_offset;

	public:

		/*
			Creates an empty/zeroed information.
		*/
		inline constexpr GLPointerInf() :
			GLPointerInf(GL_FLOAT, 2, 0, 0)
		{}

		/*
			Creates a new pointer information object.
			* @param[in] type The type of data, GL_FLOAT, GL_INTEGER etc.
			* @param[in] size Specifies the number of components per generic vertex attribute. Must be 1, 2, 3, 4.
			* @param[in] stride Specifies the byte offset between consecutive vertices. If stride is 0, the vertices are understood to be tightly packed in the array.
			* @param[in] offset The offset of the first piece of data in the buffer
		*/
		inline explicit constexpr GLPointerInf(const GLenum type, const GLint size, const GLint stride, const size_t offset) :
			GLPointerInf(false, type, size, stride, offset)
		{}

		/*
			Creates a new pointer information object for a vertex attribute.
			* @param[in] normalized True if the values should be normalized before access
			* @param[in] type The type of data, GL_FLOAT, GL_INTEGER etc.
			* @param[in] size Specifies the number of components per generic vertex attribute. Must be 1, 2, 3, 4.
			* @param[in] stride Specifies the byte offset between consecutive vertices. If stride is 0, the vertices are understood to be tightly packed in the array.
			* @param[in] offset The offset of the first piece of data in the buffer
		*/
		inline explicit constexpr GLPointerInf(const GLboolean normalized, const GLenum type, const GLint size, const GLint stride, const size_t offset) :
			m_normalized(normalized),
			m_type(type),
			m_size(size),
			m_stride(stride),
			m_offset(offset)
		{}

	public:

		/*
			Returns true if the values should be normalized before access (only attributes)
		*/
		inline GLboolean normalized() const
		{
			return m_normalized;
		}

		/*
			Returns the type of data, GL_FLOAT, GL_INTEGER etc.
		*/
		inline GLenum getType() const
		{
			return m_type;
		}

		/*
			Returns the the number of components per generic vertex attribute.
		*/
		inline GLint getSize() const
		{
			return m_size;
		}

		/*
			Returns the byte offset between consecutive vertices. If stride is 0, the vertices are understood to be tightly packed in the array.
		*/
		inline GLint getStride() const
		{
			return m_stride;
		}

		/*
			Returns the offset of the first piece of data in the buffer.
		*/
		inline GLint getOffset() const
		{
			return m_offset;
		}

		/*
			Sets the OpenGL vertex pointer information.
		*/
		inline void setVertexPointer() const
		{
			glVertexPointer(m_size, m_type, m_stride, reinterpret_cast<GLubyte*>(m_offset));
		}

		/*
			Sets the OpenGL normal pointer information.
		*/
		inline void setNormalPointer() const
		{
			glNormalPointer(m_type, m_stride, reinterpret_cast<GLubyte*>(m_offset));
		}

		/*
			Sets the OpenGL color pointer information.
		*/
		inline void setColorPointer() const
		{
			glColorPointer(m_size, m_type, m_stride, reinterpret_cast<GLubyte*>(m_offset));
		}

		/*
			Sets the OpenGL texture coordinate pointer information.
		*/
		inline void setTexCoordPointer() const
		{
			glTexCoordPointer(m_size, m_type, m_stride, reinterpret_cast<GLubyte*>(m_offset));
		}

		/*
			Sets the OpenGL vertex attribute pointer information.
			* @param[in] location The location id of the vertex attribute
		*/
		inline void setAttribPointer(const GLuint location) const
		{
			glVertexAttribPointer(location, m_size, m_type, m_normalized, m_stride, reinterpret_cast<GLubyte*>(m_offset));
		}

		/*
			Sets the OpenGL index pointer information.
		*/
		inline void setIndexPointer() const
		{
			glIndexPointer(m_type, m_stride, reinterpret_cast<GLubyte*>(m_offset));
		}

		/*
			Sets the OpenGL edge flag pointer information.
		*/
		inline void setEdgeFlagPointer() const
		{
			glEdgeFlagPointer(m_stride, reinterpret_cast<GLubyte*>(m_offset));
		}

	public:

		/*
			Equals operator.
		*/
		inline bool operator==(const GLPointerInf& obj) const
		{
			return (m_normalized == obj.m_normalized) &&
				(m_type == obj.m_type) &&
				(m_size == obj.m_size) &&
				(m_stride == obj.m_stride) &&
				(m_offset == obj.m_offset);
		}
				
		/*
			Not-equals operator.
		*/
		inline bool operator!=(const GLPointerInf& obj) const
		{
			return !operator==(obj);
		}

	};

}