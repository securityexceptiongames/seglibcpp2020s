#pragma once
#include <org/segames/library/exception.h>

#include <glad/glad.h>

namespace org::segames::library
{

	/*
		An exception for OpenGL errors.

		* @author	Philip Rosberg
		* @since	2019-06-07
		* @edited	2020-06-22
	*/
	class SEG_API GLException :
		public Exception
	{
	private:

		/*
			Creates an OpenGL exception from the given error code and the given stackTrace depth offset.
			* @param[in] error The error code
			* @param[in] stackTraceOffset The stack trace depth offset
		*/
		inline explicit GLException(const GLenum error, size_t stackTraceOffset) :
			Exception("org::segames::library::GLException", ("OpenGL error " + std::string(stringFromCode__(error))), StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, stackTraceOffset))
		{}

	public:

		/*
			Creates an OpenGL exception from the given error code.
			* @param[in] error The error code
		*/
		inline explicit GLException(const GLenum error) :
			GLException(error, 1)
		{}

		/*
			Creates an OpenGL exception with message.
			* @param[in] message The message to use for the exception
		*/
		inline explicit GLException(const std::string_view& message) :
			Exception("org::segames::library::GLException", message, StackTrace::getStackTrace(StackTrace::ALL_STACK_FRAMES, 1))
		{}

	private:

		/*
			Returns a string representation of the given error code.
			* @param[in] error The error code
		*/
		static std::string_view stringFromCode__(const GLenum error);

		/*
			Message callback for OpenGL.
		*/
		static void GLAPIENTRY messageCallback__(GLenum source,
			GLenum type,
			GLuint id,
			GLenum severity,
			GLsizei length,
			const GLchar* message,
			const void* userParam);

	public:

		/*
			Enables built in OpenGL debug callback.
		*/
		inline static void enableDebugCallback()
		{
			glEnable(GL_DEBUG_OUTPUT);
			glDebugMessageCallback(messageCallback__, NULL);
		}

		/*
			Test for errors and throws if an error exists.
			Also corrects stack trace depth.
		*/
		inline static void errorTestAndThrow() noexcept(false)
		{
			GLenum error;
			if ((error = glGetError()) != GL_NO_ERROR)
				throw GLException(error, 2);
		}

	};

}