#version 120
// Credits to Brad Larson for base code
uniform vec2 texSize;
varying vec2 centerTextureCoordinate;
varying vec2 oneStepLeftTextureCoordinate;
varying vec2 twoStepsLeftTextureCoordinate;
varying vec2 threeStepsLeftTextureCoordinate;
varying vec2 fourStepsLeftTextureCoordinate;
varying vec2 oneStepRightTextureCoordinate;
varying vec2 twoStepsRightTextureCoordinate;
varying vec2 threeStepsRightTextureCoordinate;
varying vec2 fourStepsRightTextureCoordinate;
void main()
{
	vec2 firstOffset = vec2(0.3333 / texSize.x, 0.3333 / texSize.y);
	vec2 secondOffset = vec2(2.0 * firstOffset.x, 2.0 * firstOffset.y);
	vec2 thirdOffset = vec2(3.0 * firstOffset.x, 3.0 * firstOffset.y);
	vec2 fourthOffset = vec2(4.0 * firstOffset.x, 4.0 * firstOffset.y);
	centerTextureCoordinate = gl_MultiTexCoord0.xy;
	oneStepLeftTextureCoordinate = gl_MultiTexCoord0.xy - firstOffset;
	twoStepsLeftTextureCoordinate = gl_MultiTexCoord0.xy - secondOffset;
	threeStepsLeftTextureCoordinate = gl_MultiTexCoord0.xy - thirdOffset;
	fourStepsLeftTextureCoordinate = gl_MultiTexCoord0.xy - fourthOffset;
	oneStepRightTextureCoordinate = gl_MultiTexCoord0.xy + firstOffset;
	twoStepsRightTextureCoordinate = gl_MultiTexCoord0.xy + secondOffset;
	threeStepsRightTextureCoordinate = gl_MultiTexCoord0.xy + thirdOffset;
	fourStepsRightTextureCoordinate = gl_MultiTexCoord0.xy + fourthOffset;
	gl_TexCoord[0] = gl_MultiTexCoord0;
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}