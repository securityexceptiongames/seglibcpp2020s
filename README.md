# SEGlibCPP2020s
SecurityExceptionGames; C++ game creation (support) library (2020 Summer)

This library contains general utilities for game development directly in C++, such as: basic graphical abstractions, mathematics and noise, audio and network utilities. The library code is written to work with the C++1z (C++17) standard, containing usage of `string_view` and nested namespace declarations.

As of February 2022, the library is public and free to be used, copied and modified by anyone. Contributions to the repository are highly welcome, but remember to contribute functionality of relevance and most importantly to follow the standard (style, not functionality) laid out in the current library code. Note that the repository does contain libraries and files-for-testing not created by SecurityExceptionGames. These libraries have their respective licenses somewhere in the libraries folder. Do not hesitate to contact SecurityExceptionGames regarding any missing licenses or reference. If this library is intended to be used, each dependency library and its license should be examined at its official source. Do not mistake these copies for the official ones.

## Builds
Builds for Windows:
* Debug 64bit MinGW: [SEGlibCPP-GCC-Debug-2022-02-26.zip](https://gitlab.com/securityexceptiongames/seglibcpp2020s/-/raw/master/builds/2022-02-26/SEGlibCPP-GCC-Debug-2022-02-13.zip)
* Optimized 64bit MinGW: [SEGlibCPP-GCC-Optimized-2022-02-26.zip](https://gitlab.com/securityexceptiongames/seglibcpp2020s/-/raw/master/builds/2022-02-26/SEGlibCPP-GCC-Optimized-2022-02-13.zip)

## Sub-Libraries
This library is split into a set of sub-libraries responsible for various tasks. Most sub-libraries are strictly dependent on the utilities, math, threading and base IO sub-libraries.

### Audio
Sub-library located in `org/segames/library/audio/`, responsible for parsing and general playback of audio and audio files. 

Currently supported audio format[s]: 
* Pure 8bit or 16bit PCM in mono or stereo.
* WAV files/data or similar.
* OGG files/data or similar.

Currently supported back-end[s]:
* OpenAL, abstractions located in `org/segames/library/audio/al/`

### Event
Sub-library in `org/segames/library/event/`, implementing a general event system with event re-routing and various types of events such as direct-call events, buffered events and multi-threaded events.

### OpenGL
Sub-library in `org/segames/library/gl/`, responsible for the main graphics of the library using standard OpenGL as the graphics back-end. This sub-library implements common OpenGL necessity abstractions, such as: textures, buffers, shaders, font-rendering and framebuffer objects.

Currently supported texture format[s]:
* RAW texture data.
* All files/data supported by Sean Barrett's `stb_image` (PNG, JPEG, GIF, BMP, TGA, etc.).
* S3TC (DDS) files/data.

Currently supported font format[s]:
* True type fonts (TTF files).

This sub-library also contains a crude abstraction of the light-weight UI library `nuklear` in `org/segames/library/gl/nk/`, which contains configurations for use with `GLFW` and as such may require the `GLFW` sub-library.

### GLFW
Sub-library in `org/segames/library/glfw/`, implementing abstractions for the windowing library `GLFW`.

### IO
Sub-library in `org/segames/library/io/`, responsible for various IO related tasks such as extended file representation. This sub-library contains a few sub-libraries of its own, but the base folder is the only part strictly required by other sub-libraries, `org/segames/library/*`.

The sub-libraries of the IO sub-library:
* `org/segames/library/io/json`: Parsing and writing of plain-text JSON.
* `org/segames/library/io/network`: TCP and UDP communication abstractions.

### Math
Sub-library in `org/segames/library/math/`, responsible for various basic mathematical tasks and vector mathematics.

### Threading
Sub-library in `org/segames/library/thread/`, containing various small threading utility abstractions and a (hopefully slightly) faster starting (under repeated use) thread type.

### Utilities
Sub-library in `org/segames/library/util/`, implementing various containers, noise generators, timers and other general utilities.

## Platforms and Compilers
This library only contains functionality for use on Windows, with possibility to extend to Linux, and has thus far only been compiled using 64bit MinGW.

## Future Improvements of Interest
These are some general library improvements that are currently considered to be (maybe) implemented eventually:

* Support for Linux.
* Basic video playback support.
* Model importing, rendering and animations.
* Cascaded shadow-mapping or similar.
* Audio synthetization, such as regularly shaped waves and audio sampling.
* Generalized graphics interface and Vulkan/DirectX abstractions.
* OpenCL abstractions.
* Improved (actually functioning) dynamic memory tracker.
* Support for other compilers, such as LLVM Clang or MSVC.
* Support for other platforms.

Note that this does not guarantee that they are going to be implemented. Feel free to contribute to the repository. :)

## License for (this) Library Source and Builds

MIT License

Copyright (c) 2022 SecurityExceptionGames

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
